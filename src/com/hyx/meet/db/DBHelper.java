package com.hyx.meet.db;


import com.hyx.meet.utils.Logger;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by lzw on 14-5-28.
 */
public class DBHelper extends SQLiteOpenHelper {

	
	
  public DBHelper(Context context, String name, int version) {
    super(context, name, null, version);
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
	DBMeet.createTable(db);
    DBMsg.createTable(db);
    DBNamecard.createTable(db);
//    DBMyReleaseMeet.createTable(db);
  }

  @Override
  public void onOpen(SQLiteDatabase db) {
    super.onOpen(db);
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    switch (oldVersion) {
      case 4:
    	  DBMeet.dropTable(db);
    	  DBMeet.createTable(db);
    	  Logger.d("[update db 4->5]");
    	  break;
      case 5:
    	  DBMsg.dropTable(db);
    	  DBMsg.createTable(db);
    	  Logger.d("[update db 5->6]");
    	  break;
      case 6:
    	  break;
    }
  }
  
  
}
