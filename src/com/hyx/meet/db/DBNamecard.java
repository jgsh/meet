package com.hyx.meet.db;

import java.util.ArrayList;
import java.util.List;

import com.hyx.meet.R.string;
import com.hyx.meet.Application.App;
import com.hyx.meet.entity.NameCard;
import com.hyx.meet.service.ChatService;
import com.hyx.meet.utils.Logger;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

public class DBNamecard {

	private static DBHelper dbHelper;

	public static final String TABLE_NAME = "namecard";
	public static final String USER_ID = "userId";
	public static final String OBJECT_ID = "namecardId";
	public static final String AVATOR = "avator";
	public static final String NAME = "name";
	public static final String SEX = "sex";
	public static final String IS_AUTH = "is_auth";
	public static final String MARK_TAG = "mark_tag";
	public static final String COMPANY_NAME = "companyName";
	public static final String JOB_TITLE = "jobTitle";
	public static final String INDUSTRY = "industry";
	public static final String ADDRESS = "address";
	public static final String MOBILE_PHONE = "mobilePhone";
	public static final String TEL_NUMBER = "telNumber";
	public static final String EMAIL = "email";
	public static final String WEIBO = "weibo";
	public static final String WECHAT = "wechat";
	public static final String QQ = "qq";
	public static final String HOBBY = "hobby";
	public static final String BUSINESS_MOTTO = "businessMotto";
	public static final String FAX = "fax";
	public static final String FIRST_MEETING = "firstMeeting";
	public static final String TIMESTAMP = "timestamp";
	public static final String HISTORY_ONE = "history1";
	public static final String HISTORY_TWO = "history2";
	public static final String HISTORY_THREE = "history3";
	public static final String OWER_ID = "ownerId";

	private static final String CURRENT_USERID = ChatService.getSelfId();
	private static final String QUERY_NAMECARDS_SQL = "select * from namecard where userId!=? and ownerId=?";
	private static final String QUERY_MYNAMECARDS_SQL = "select * from namecard where userId=?";
	private static final String QUERY_NAMECARDS_BY_NAMECARDID_SQL = "select * from namecard where namecardId=? and ownerId=?";
	private static final String QUERY_NAMECARDS_BY_USERID_SQL = "select * from namecard where userId=? and ownerId=?";

	public static void createTable(SQLiteDatabase db) {
		db.execSQL("create table if not exists namecard (id integer primary key, namecardId varchar(63) unique not null,"
				+ "userId varchar(63) unique not null,ownerId varchar(63)  not null,"
				+ "avator varchar(255) ,name varchar(255) not null, sex varchar(2) ,"
				+ "is_auth integer, mark_tag varchar(50),companyName varchar(50),jobTitle varchar(50),industry varchar(50),"
				+ "address varchar(50),mobilePhone varchar(50) not null,telNumber varchar(50),email varchar(50),weibo varchar(50),"
				+ "wechat varchar(50),qq varchar(50),hobby varchar(50),businessMotto varchar(50),fax varchar(50),"
				+ "firstMeeting varchar(50),"
				+ "history1 varchar(255),history2 varchar(255),history3 varchar(255),"
				+ " timestamp varchar(63))");
	}

	public static void dropTable(SQLiteDatabase db) {
		db.execSQL("drop table if exists namecard");
	}

	public static void deleteAll() {
		DBHelper dbHelper = new DBHelper(App.ctx, App.DB_NAME, App.DB_VER);
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		db.delete(TABLE_NAME, "userID!=?", new String[] { CURRENT_USERID });
		db.close();
	}

	public static int insertMyNameCard(NameCard nameCard) {
		List<NameCard> nameCards = new ArrayList<NameCard>();
		nameCards.add(nameCard);
		return insertNameCards(nameCards);
	}

	public static int insertNameCards(List<NameCard> nameCards) {

		DBHelper dbHelper = new DBHelper(App.ctx, App.DB_NAME, App.DB_VER);
		if (nameCards == null || nameCards.size() == 0) {
			return 0;
		}
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		db.beginTransaction();
		int n = 0;
		try {
			for (NameCard nameCard : nameCards) {
				ContentValues cValues = new ContentValues();
				cValues.put(USER_ID, nameCard.getUserId());
				cValues.put(OBJECT_ID, nameCard.getNamecardId());
				cValues.put(OWER_ID, CURRENT_USERID);
				cValues.put(AVATOR, nameCard.getAvator());
				cValues.put(NAME, nameCard.getName());
				cValues.put(SEX, nameCard.getSex());
				cValues.put(IS_AUTH, nameCard.getIs_auth());
				cValues.put(MARK_TAG, nameCard.getMark_tag());
				cValues.put(COMPANY_NAME, nameCard.getCompanyName());
				cValues.put(JOB_TITLE, nameCard.getJobTitle());
				cValues.put(INDUSTRY, nameCard.getIndustry());
				cValues.put(ADDRESS, nameCard.getAddress());
				cValues.put(MOBILE_PHONE, nameCard.getMobilePhone());
				cValues.put(TEL_NUMBER, nameCard.getTelNumber());
				cValues.put(EMAIL, nameCard.getEmail());
				cValues.put(WEIBO, nameCard.getWeibo());
				cValues.put(WECHAT, nameCard.getWechat());
				cValues.put(QQ, nameCard.getQq());
				cValues.put(HOBBY, nameCard.getHobby());
				cValues.put(BUSINESS_MOTTO, nameCard.getBusinessMotto());
				cValues.put(FAX, nameCard.getFax());
				cValues.put(FIRST_MEETING, nameCard.getFirstMeeting());
				cValues.put(TIMESTAMP, nameCard.getTimestamp());
				cValues.put(HISTORY_ONE, nameCard.getHistory1());
				cValues.put(HISTORY_TWO, nameCard.getHistory2());
				cValues.put(HISTORY_THREE, nameCard.getHistory3());

				db.insert(TABLE_NAME, null, cValues);

				n++;
			}
			db.setTransactionSuccessful();

		} catch (Exception e) {
			// TODO: handle exception
			Logger.e("insert->namecard :" + e.getMessage());
		} finally {
			db.endTransaction();
			db.close();

		}

		return n;
	}

	public static NameCard getMyNameCard(DBHelper dbHelper) {
		NameCard myNameCard = new NameCard();
		dbHelper = new DBHelper(App.ctx, App.DB_NAME, App.DB_VER);
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		assert db != null;
		Cursor c = db.rawQuery(QUERY_MYNAMECARDS_SQL,
				new String[] { CURRENT_USERID });
		while (c.moveToNext()) {
			myNameCard = createNamecardByCursor(c);
		}
		c.close();
		db.close();
		return myNameCard;
	}

	public static NameCard getNameCardByUserId(DBHelper dbHelper, String userId) {
		NameCard NameCard = new NameCard();
		NameCard.setName("null");
		dbHelper = new DBHelper(App.ctx, App.DB_NAME, App.DB_VER);
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		assert db != null;
		Cursor c = db.rawQuery(QUERY_NAMECARDS_BY_USERID_SQL, new String[] {
				userId, CURRENT_USERID });
		while (c.moveToNext()) {
			NameCard = createNamecardByCursor(c);
		}
		c.close();
		db.close();
		return NameCard;
	}

	public static boolean haveNameCardByUserId(String userId) {
		boolean haveNamecard = false;
		dbHelper = new DBHelper(App.ctx, App.DB_NAME, App.DB_VER);
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		Cursor cursor = db.rawQuery(QUERY_NAMECARDS_BY_USERID_SQL,
				new String[] { userId, CURRENT_USERID });
		while (cursor.moveToNext()) {
			haveNamecard = true;
		}
		cursor.close();
		db.close();
		return haveNamecard;

	}

	public static NameCard getNameCardByNameCardId(String namecardId) {
		NameCard NameCard = new NameCard();
		DBHelper dbHelper = new DBHelper(App.ctx, App.DB_NAME, App.DB_VER);
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		assert db != null;
		Cursor c = db.rawQuery(QUERY_NAMECARDS_BY_NAMECARDID_SQL, new String[] {
				namecardId, CURRENT_USERID });
		while (c.moveToNext()) {
			NameCard = createNamecardByCursor(c);
		}
		c.close();
		db.close();
		return NameCard;
	}

	public static List<NameCard> getNameCards(DBHelper dbHelper) {

		List<NameCard> nameCards = new ArrayList<NameCard>();
		dbHelper = new DBHelper(App.ctx, App.DB_NAME, App.DB_VER);
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		assert db != null;
		Cursor c = db.rawQuery(QUERY_NAMECARDS_SQL, new String[] {
				CURRENT_USERID, CURRENT_USERID });
		while (c.moveToNext()) {
			NameCard nameCard = createNamecardByCursor(c);
			nameCards.add(nameCard);
		}
		c.close();
		db.close();
		return nameCards;
	}

	private static NameCard createNamecardByCursor(Cursor c) {
		// TODO Auto-generated method stub
		NameCard nameCard = new NameCard();
		nameCard.setUserId(c.getString(c.getColumnIndex(USER_ID)));
		nameCard.setNamecardId(c.getString(c.getColumnIndex(OBJECT_ID)));
		nameCard.setAvator(c.getString(c.getColumnIndex(AVATOR)));
		nameCard.setName(c.getString(c.getColumnIndex(NAME)));
		nameCard.setSex(c.getString(c.getColumnIndex(SEX)));
		nameCard.setIs_auth(c.getInt(c.getColumnIndex(IS_AUTH)));
		nameCard.setMark_tag(c.getString(c.getColumnIndex(MARK_TAG)));
		nameCard.setCompanyName(c.getString(c.getColumnIndex(COMPANY_NAME)));
		nameCard.setJobTitle(c.getString(c.getColumnIndex(JOB_TITLE)));
		nameCard.setIndustry(c.getString(c.getColumnIndex(INDUSTRY)));
		nameCard.setAddress(c.getString(c.getColumnIndex(ADDRESS)));
		nameCard.setMobilePhone(c.getString(c.getColumnIndex(MOBILE_PHONE)));
		nameCard.setTelNumber(c.getString(c.getColumnIndex(TEL_NUMBER)));
		nameCard.setEmail(c.getString(c.getColumnIndex(EMAIL)));
		nameCard.setWeibo(c.getString(c.getColumnIndex(WEIBO)));
		nameCard.setWechat(c.getString(c.getColumnIndex(WECHAT)));
		nameCard.setQq(c.getString(c.getColumnIndex(QQ)));
		nameCard.setHobby(c.getString(c.getColumnIndex(HOBBY)));
		nameCard.setBusinessMotto(c.getString(c.getColumnIndex(BUSINESS_MOTTO)));
		nameCard.setFax(c.getString(c.getColumnIndex(FAX)));
		nameCard.setFirstMeeting(c.getString(c.getColumnIndex(FIRST_MEETING)));
		nameCard.setTimestamp(c.getString(c.getColumnIndex(TIMESTAMP)));
		nameCard.setHistory1(c.getString(c.getColumnIndex(HISTORY_ONE)));
		nameCard.setHistory2(c.getString(c.getColumnIndex(HISTORY_TWO)));
		nameCard.setHistory3(c.getString(c.getColumnIndex(HISTORY_THREE)));
		return nameCard;
	}

}
