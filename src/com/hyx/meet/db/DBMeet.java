/**
 * 
 */
package com.hyx.meet.db;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import com.hyx.meet.Application.App;
import com.hyx.meet.entity.MeetingDomain;
import com.hyx.meet.service.ChatService;
import com.hyx.meet.utils.Logger;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.wifi.WifiConfiguration.Status;


/**
 * 类说明
 * 
 * @author joy
 * 
 */
public class DBMeet {
	
	private static final String MEETING_ID = "objectId";
	private static final String HOSTOBJECT_ID = "hostObjectId";
	private static final String NAME = "name";
	private static final String CONTACT_PHONE = "contactPhone";
	private static final String COVERIMAGE = "coverImage";
	private static final String ADDRESS = "address";
	private static final String IS_PUBLIC = "isPublic";
	private static final String ATTENDER_NUMBER = "attenderNumber";
	private static final String USER_ID = "userId";
	private static final String START_TIME  = "startTime";
	private static final String END_TIME  = "endTime";
	private static final String STATUS  = "status";
	private static final String LATITUDE  = "lat";
	private static final String LONGITUDE  = "lng";


	
	

	public static final String TABLE_NAME = "meet";
	private static DBHelper dbHelper;
	private static final String QUERY_MEETINGLIST_SQL = "select * from meet where hostObjectId!=? and userId=? order by startTime DESC";
	private static final String CURRENT_USERID = ChatService.getSelfId();
	private static final String QUERY_MEETINGLIST_SQL_ALL = "select * from meet where  userId=? order by startTime DESC";

	public static void createTable(SQLiteDatabase db) {
		db.execSQL("create table if not exists meet (id INTEGER DEFAULT '1' NOT NULL PRIMARY KEY AUTOINCREMENT,"
				+ "objectId varchar(255) not null,hostObjectId varchar(255) not null,name varchar(63)  not null,contactPhone varchar(255) not null,"
				+ "coverImage varchar(255) ,address varchar(255) not null,isPublic int not null,"
				+ "attenderNumber int,userId varchar(255) not null,"
				+ "startTime long,endTime long,status varchar(255),"
				+"lat double,lng double)");
	}
	

	// 插入新数据
	public static void insertMeeting(Map<String, Object> map) {
		dbHelper = new DBHelper(App.ctx, App.DB_NAME, App.DB_VER);

		try {
			SQLiteDatabase db = dbHelper.getWritableDatabase();

			
				ContentValues values = new ContentValues();
				values.put("hostObjectId", (String)map.get("hostObjectId"));
				values.put("objectId", (String) map.get("objectId"));
				values.put("name", (String) map.get("name"));
				values.put("coverImage",
						(String) map.get("coverImage"));
				values.put("address", (String) map.get("address"));
				values.put("contactPhone",
						(String) map.get("contactPhone"));
				values.put("isPublic", (Integer) map
						.get("isPublic"));
				values.put("status", (String) map.get("status"));
				values.put("attenderNumber", (Integer) map.get("attenderNumber"));
				values.put("userId", CURRENT_USERID);
				JSONObject jsonObject = new JSONObject(map.get("position").toString());
				Logger.d("[position data]"+jsonObject.toString());
				values.put(LATITUDE, jsonObject.getDouble("lat"));
				values.put(LONGITUDE, jsonObject.getDouble("lng"));
				Date startTime = (Date) map.get("startTime");
				Date endTime = (Date) map.get("endTime");
				long dateLong=startTime.getTime();
				long endTimeLong = endTime.getTime();
				
				
//				String dateStr = new SimpleDateFormat("yyyy年MM月dd日")
//						.format(startTime);

				values.put("startTime", dateLong);
				values.put("endTime", endTimeLong);

				db.insert("meet", null, values);
			

			db.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	


	// 修改数据
	public static void deleteMeeting(List<Map<String, Object>> meetlist) {
		dbHelper = new DBHelper(App.ctx, App.DB_NAME, App.DB_VER);
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		db.delete(TABLE_NAME, null, null);
		db.close();

	}
	
	public static MeetingDomain getMeetingById(String meetingId){
		dbHelper = new DBHelper(App.ctx, App.DB_NAME, App.DB_VER);
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		Cursor cursor = db.rawQuery("select * from meet where objectId=? and userId=?", new String[]{meetingId,CURRENT_USERID});
		MeetingDomain meetingDomain = createMeetingDomain(cursor);
		cursor.close();
		db.close();
		return meetingDomain;

	}
	
	 public static  boolean haveMeeting(String meetingId){
		 boolean haveMeet = false;
		 dbHelper = new DBHelper(App.ctx, App.DB_NAME, App.DB_VER);
			SQLiteDatabase db = dbHelper.getReadableDatabase();
			Cursor cursor = db.rawQuery("select * from meet where objectId=? and userId=?", new String[]{meetingId,CURRENT_USERID});
		   while(cursor.moveToNext()){
			   haveMeet =  true;
		   }
		   cursor.close();
		   db.close();
		 return haveMeet;
	 }

	private static MeetingDomain createMeetingDomain(Cursor cursor) {
		// TODO Auto-generated method stub
		MeetingDomain meetingDomain = new MeetingDomain();
		while (cursor.moveToNext()) {
			String hostObjectId = cursor.getString(cursor
					.getColumnIndex("hostObjectId"));
			String objectid = cursor.getString(cursor
					.getColumnIndex("objectId"));
			String name = cursor.getString(cursor.getColumnIndex("name"));
			String address = cursor.getString(cursor.getColumnIndex("address"));
			String coverImage = cursor.getString(cursor
					.getColumnIndex("coverImage"));
			String contactPhone = cursor.getString(cursor
					.getColumnIndex("contactPhone"));
			String isPublic = cursor.getString(cursor
					.getColumnIndex("isPublic"));
			long startTime = cursor.getLong(cursor
					.getColumnIndex("startTime"));
			long endTime = cursor.getLong(cursor
					.getColumnIndex("endTime"));
			String status = cursor.getString(cursor.getColumnIndex("status"));

			meetingDomain.setHostObjectId(hostObjectId);
			meetingDomain.setAddress(address);
			meetingDomain.setIsPublic(isPublic);
			meetingDomain.setCoverImage(coverImage);
			meetingDomain.setStartTime(startTime);
			meetingDomain.setEndTime(endTime);
			meetingDomain.setName(name);
			meetingDomain.setContactPhone(contactPhone);
			meetingDomain.setObjectid(objectid);
			meetingDomain.setStatus(status);
		}
		return meetingDomain;

	}

	/**
	 * 获取会议列表（自己发布的除外）
	 * @return
	 */
	public static List<MeetingDomain> getInformation() {

		dbHelper = new DBHelper(App.ctx, App.DB_NAME, App.DB_VER);
		SQLiteDatabase db = dbHelper.getReadableDatabase();

		List<MeetingDomain> list = new ArrayList<MeetingDomain>();
		Cursor cursor = db.rawQuery(QUERY_MEETINGLIST_SQL, new String[]{CURRENT_USERID,CURRENT_USERID});
//		Cursor cursor =db.query(TABLE_NAME, null, null, null,null, null, "startTime DESC");
		list = createMeetingDomains(cursor);
		cursor.close();
		db.close();
		return list;
	}
	
	/**
	 * 获取全部会议列表（自己发布的和关注的）
	 * @return
	 */
	public static List<MeetingDomain> getInformationAll() {

		dbHelper = new DBHelper(App.ctx, App.DB_NAME, App.DB_VER);
		SQLiteDatabase db = dbHelper.getReadableDatabase();

		List<MeetingDomain> list = new ArrayList<MeetingDomain>();
		Cursor cursor = db.rawQuery(QUERY_MEETINGLIST_SQL_ALL, new String[]{CURRENT_USERID});
//		Cursor cursor =db.query(TABLE_NAME, null, null, null,null, null, "startTime DESC");
		list = createMeetingDomains(cursor);
		cursor.close();
		db.close();
		return list;
	}

	/**
	 * 创建会议列表
	 * @param cursor
	 * @return
	 */
	private static List<MeetingDomain> createMeetingDomains(Cursor cursor) {
		List<MeetingDomain> list = new ArrayList<MeetingDomain>();
		while (cursor.moveToNext()) {
			MeetingDomain meetingDomain = new MeetingDomain();
//			发布会议人ID
			String hostObjectId = cursor.getString(cursor
					.getColumnIndex("hostObjectId"));
			String objectid = cursor.getString(cursor
					.getColumnIndex("objectId"));
			String name = cursor.getString(cursor.getColumnIndex("name"));
			String address = cursor.getString(cursor.getColumnIndex("address"));
			String coverImage = cursor.getString(cursor
					.getColumnIndex("coverImage"));
			String contactPhone = cursor.getString(cursor
					.getColumnIndex("contactPhone"));
			String isPublic = cursor.getString(cursor
					.getColumnIndex("isPublic"));
			long startTime = cursor.getLong(cursor
					.getColumnIndex("startTime"));
			long endTime = cursor.getLong(cursor
					.getColumnIndex("endTime"));
			String status = cursor.getString(cursor.getColumnIndex("status"));
			String attenderNumber = cursor.getString(cursor.getColumnIndex("attenderNumber"));
			
			double lat = cursor.getDouble(cursor.getColumnIndex(LATITUDE));
			double lng = cursor.getDouble(cursor.getColumnIndex(LONGITUDE));
			
			meetingDomain.setHostObjectId(hostObjectId);
			meetingDomain.setAddress(address);
			meetingDomain.setIsPublic(isPublic);
			meetingDomain.setCoverImage(coverImage);
			meetingDomain.setStartTime(startTime);
			meetingDomain.setEndTime(endTime);
			meetingDomain.setName(name);
			meetingDomain.setContactPhone(contactPhone);
			meetingDomain.setObjectid(objectid);
			meetingDomain.setStatus(status);
			meetingDomain.setAttenderNumber(attenderNumber);
			meetingDomain.setLatitude(lat);
			meetingDomain.setLongitude(lng);
			list.add(meetingDomain);

		}

		return list;

	}

	// 删除表meet_cache
	public static void dropTable(SQLiteDatabase db) {
		db.execSQL("drop table if exists meet");
	}

}
