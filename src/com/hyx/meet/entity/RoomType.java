package com.hyx.meet.entity;

/**
* Created by lzw on 14/11/18.
*/
public enum RoomType {
//	单聊，群聊，公众号
  Single(0), Group(1),NOTIFY(2);

  int value;
  RoomType(int value){
    this.value=value;
  }

  public int getValue(){
    return value;
  }

  public static RoomType fromInt(int i){
    return values()[i];
  }
}
