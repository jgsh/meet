package com.hyx.meet.entity;

import java.io.Serializable;

/**
 * 通讯录联系人
 * @author ww
 *
 */
public class JuContact implements Serializable{

	/**
	 * Auto generate
	 */
	private static final long serialVersionUID = 7456873611936932572L;
	private String phoneNumber;
	private String name;
	private String ID;
	private boolean checked;
	private String sortLetters;

	public JuContact(){
		
	}
	
	public JuContact(String phoneNumber,String name,String ID) {
		// TODO Auto-generated constructor stub
		this.phoneNumber = phoneNumber;
		this.name = name;
		this.ID = ID;
	}
	
	public String getSortLetters() {
		return sortLetters;
	}

	public void setSortLetters(String sortLetters) {
		this.sortLetters = sortLetters;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	
	public boolean is_checked() {
		return checked;
	}
	public void set_checked(boolean is_checked) {
		this.checked = is_checked;
	}
	@Override
	public String toString() {
		return "JuContact [phoneNumber=" + phoneNumber + ", name=" + name
				+ ", ID=" + ID + "]";
	}
	
	
}
