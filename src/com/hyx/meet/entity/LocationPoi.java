package com.hyx.meet.entity;

import org.json.JSONException;
import org.json.JSONObject;

public class LocationPoi {
	
	private String district;
	
    private String name;
    
	private String address;
	
	private double latitude;
	
	private double longitude;
	
	private boolean is_select ;
	

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public boolean isIs_select() {
		return is_select;
	}

	public void setIs_select(boolean is_select) {
		this.is_select = is_select;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongtitude() {
		return longitude;
	}

	public void setLongtitude(double longtitude) {
		this.longitude = longtitude;
	}
	
   /**
    * 输出json字符串	
    * @return
    */
	public String toJsonString(){
		return "{"
	     +"\"district\":"+"\""+district+"\""
	     +",\"name\":"+"\""+name+"\""
	     +",\"address\":"+"\""+address+"\""
	     +",\"latitude\":"+latitude
	     +",\"longitude\":"+longitude
		+"}";
	}
	
	/**
	 * JSON转换LocationPoi
	 * @param jsonObject
	 * @return
	 */
	public LocationPoi JSontoLocationPoi(JSONObject jsonObject){
		LocationPoi locationPoi = new LocationPoi();
		try {
			locationPoi.setAddress(jsonObject.getString("address"));
			locationPoi.setDistrict(jsonObject.getString("district"));
			locationPoi.setLatitude(jsonObject.getDouble("latitude"));
			locationPoi.setLongtitude(jsonObject.getDouble("longtitude"));
			locationPoi.setName(jsonObject.getString("name"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return locationPoi;
	}
	
	

}
