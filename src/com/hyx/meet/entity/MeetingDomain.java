/**
 * 
 */
package com.hyx.meet.entity;

/**
 * 类说明
 * 
 * @author joy
 * 
 */
public class MeetingDomain {

	double latitude;
	double longitude;
	
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	String attenderNumber;
	/**
	 * @return the attenderNumber
	 */
	public String getAttenderNumber() {
		return attenderNumber;
	}
	/**
	 * @param attenderNumber the attenderNumber to set
	 */
	public void setAttenderNumber(String attenderNumber) {
		this.attenderNumber = attenderNumber;
	}
	/**
	 * @return the unconfirmNumber
	 */
	public String getUnconfirmNumber() {
		return unconfirmNumber;
	}
	/**
	 * @param unconfirmNumber the unconfirmNumber to set
	 */
	public void setUnconfirmNumber(String unconfirmNumber) {
		this.unconfirmNumber = unconfirmNumber;
	}
	String unconfirmNumber;
	String objectid;
	String status;
	public String getHostObjectId() {
		return hostObjectId;
	}
	public void setHostObjectId(String hostObjectId) {
		this.hostObjectId = hostObjectId;
	}
	String hostObjectId;
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the objectid
	 */
	public String getObjectid() {
		return objectid;
	}
	/**
	 * @param objectid the objectid to set
	 */
	public void setObjectid(String objectid) {
		this.objectid = objectid;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * @return the coverImage
	 */
	public String getCoverImage() {
		return coverImage;
	}
	/**
	 * @param coverImage the coverImage to set
	 */
	public void setCoverImage(String coverImage) {
		this.coverImage = coverImage;
	}
	/**
	 * @return the contactPhone
	 */
	public String getContactPhone() {
		return contactPhone;
	}
	/**
	 * @param contactPhone the contactPhone to set
	 */
	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}
	/**
	 * @return the isPublic
	 */
	public String getIsPublic() {
		return isPublic;
	}
	/**
	 * @param isPublic the isPublic to set
	 */
	public void setIsPublic(String isPublic) {
		this.isPublic = isPublic;
	}
	/**
	 * @return the startTime
	 */
	public long getStartTime() {
		return startTime;
	}
	/**
	 * @param startTime the startTime to set
	 */
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}
	String name;
	String address;
	String coverImage;
	String contactPhone;
	String isPublic;
	long startTime;
	public long getEndTime() {
		return endTime;
	}
	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}
	long endTime;

}
