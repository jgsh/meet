package com.hyx.meet.entity;

/**
 * 名片夹联系人对象
 * @author ww
 *
 */
public class NameCard {

	private String namecardId;
    private String timestamp;
	private String sex = "0";//0为男
	private int is_auth = 0;//0为未认证
	private String mark_tag;//备注
	private String userId;
	private String address;
	private String fax;
	private String email;
	private String weibo;
	private String telNumber;
	private String industry;// 行业
	private String name;
	private String mobilePhone;
	private String firstMeeting;
	private String qq;
	private String wechat;
	private String businessMotto;
	private String companyName;
	private String avator;
	private String jobTitle;
	private String hobby;
	private String history1;
	private String history2;
	private String history3;
	
	public String getHistory1() {
		return history1;
	}

	public void setHistory1(String history1) {
		this.history1 = history1;
	}

	public String getHistory2() {
		return history2;
	}

	public void setHistory2(String history2) {
		this.history2 = history2;
	}

	public String getHistory3() {
		return history3;
	}

	public void setHistory3(String history3) {
		this.history3 = history3;
	}

	public String getSortLetters() {
		return sortLetters;
	}

	public void setSortLetters(String sortLetters) {
		this.sortLetters = sortLetters;
	}

	private String sortLetters;

	public String getNamecardId() {
		return namecardId;
	}

	public void setNamecardId(String namecardId) {
		this.namecardId = namecardId;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public int getIs_auth() {
		return is_auth;
	}

	public void setIs_auth(int is_auth) {
		this.is_auth = is_auth;
	}

	public String getMark_tag() {
		return mark_tag;
	}

	public void setMark_tag(String mark_tag) {
		this.mark_tag = mark_tag;
	}


	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWeibo() {
		return weibo;
	}

	public void setWeibo(String weibo) {
		this.weibo = weibo;
	}

	public String getTelNumber() {
		return telNumber;
	}

	public void setTelNumber(String telNumber) {
		this.telNumber = telNumber;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getFirstMeeting() {
		return firstMeeting;
	}

	public void setFirstMeeting(String firstMeeting) {
		this.firstMeeting = firstMeeting;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getWechat() {
		return wechat;
	}

	public void setWechat(String wechat) {
		this.wechat = wechat;
	}

	public String getBusinessMotto() {
		return businessMotto;
	}

	public void setBusinessMotto(String businessMotto) {
		this.businessMotto = businessMotto;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getAvator() {
		return avator;
	}

	public void setAvator(String avator) {
		this.avator = avator;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getHobby() {
		return hobby;
	}

	public void setHobby(String hobby) {
		this.hobby = hobby;
	}

	@Override
	public String toString() {
		return "NameCard [namecardId=" + namecardId + ", timestamp="
				+ timestamp + ", sex=" + sex + ", is_auth=" + is_auth
				+ ", mark_tag=" + mark_tag + ", userId=" + userId
				+ ", address=" + address + ", fax=" + fax + ", email=" + email
				+ ", weibo=" + weibo + ", telNumber=" + telNumber
				+ ", industry=" + industry + ", name=" + name
				+ ", mobilePhone=" + mobilePhone + ", firstMeeting="
				+ firstMeeting + ", qq=" + qq + ", wechat=" + wechat
				+ ", businessMotto=" + businessMotto + ", companyName="
				+ companyName + ", avator=" + avator + ", jobTitle=" + jobTitle
				+ ", hobby=" + hobby + ", sortLetters=" + sortLetters + "]";
	}

}
