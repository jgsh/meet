package com.hyx.meet.entity;

import com.hyx.meet.avobject.ChatGroup;
import com.hyx.meet.avobject.User;



/**
 * Created by lzw on 14-9-26.
 */
public class Conversation {
  private Msg msg;
  private NameCard toUser;
  private ChatGroup toChatGroup;
  private int unreadCount;

  public Msg getMsg() {
    return msg;
  }

  public void setMsg(Msg msg) {
    this.msg = msg;
  }

  public NameCard getToUser() {
    return toUser;
  }

  public void setToUser(NameCard toUser) {
    this.toUser = toUser;
  }

  public ChatGroup getToChatGroup() {
    return toChatGroup;
  }

  public void setToChatGroup(ChatGroup toChatGroup) {
    this.toChatGroup = toChatGroup;
  }

  public int getUnreadCount() {
    return unreadCount;
  }

  public void setUnreadCount(int unreadCount) {
    this.unreadCount = unreadCount;
  }
}
