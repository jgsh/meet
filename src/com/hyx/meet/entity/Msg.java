package com.hyx.meet.entity;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;


import com.avos.avoscloud.im.v2.AVIMConversation;
import com.avos.avoscloud.im.v2.AVIMTypedMessage;
import com.avos.avoscloud.im.v2.messages.AVIMAudioMessage;
import com.avos.avoscloud.im.v2.messages.AVIMImageMessage;
import com.avos.avoscloud.im.v2.messages.AVIMTextMessage;
import com.avos.avoscloud.im.v2.messages.AVIMVideoMessage;
import com.hyx.meet.Application.App;
import com.hyx.meet.db.DBHelper;
import com.hyx.meet.db.DBNamecard;
import com.hyx.meet.R;
import com.hyx.meet.service.ChatService;
import com.hyx.meet.utils.EmotionUtils;
import com.hyx.meet.utils.Logger;
import com.hyx.meet.utils.PathUtils;

/**
 * Created by lzw on 14-8-7.
 */
public class Msg {
	public static enum Status {
		SendStart(0), SendSucceed(1), SendReceived(2), SendFailed(3), HaveRead(
				4);

		int value;

		Status(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

		public static Status fromInt(int i) {
			return values()[i];
		}
	}

	public static enum Type {
		Text(0), Image(1), Video(2), Audio(3), Location(4), Board(5), Invite(6), MAudit(
				7), FriednReq(8), FriednDel(9), Logout(10), Text2(11), Image2(
				12), Audio2(13), Video2(14), Location2(15);
		int value;

		Type(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

		public static Type fromInt(int i) {
			return values()[i];
		}
	}

	public static enum ReadStatus {
		Unread(0), HaveRead(1);
		int value;

		ReadStatus(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

		public static ReadStatus fromInt(int i) {
			return values()[i];
		}
	}

	// long timestamp;
	// String fromPeerId;
	// List<String> toPeerIds;
	// isRequestReceipt
	// AVMessage internalMessage;
	String content;
	String objectId;
	String convid;
	String fromUsername;
	AVIMTypedMessage avimMessage;
	AVIMConversation avimConversation;

	int confirm;// 当消息类型是好友请求时，0是添加好友的请求，1是同意好友的请求
	RoomType roomType = RoomType.Single;
	Status status = Status.SendStart;
	Type type = Type.Text;
	ReadStatus readStatus;

	long timestamp;

	String toPeerId;//接收人ID
	String fromPeerId;//发送人ID
	DBHelper dbHelper = new DBHelper(App.ctx, App.DB_NAME, App.DB_VER);
	String pushId;//推送消息ID
	double audioTime = 0;//语音消息时长
	public double getAudioTime() {
		return audioTime;
	}

	public void setAudioTime(double audioTime) {
		this.audioTime = audioTime;
	}

	public AVIMConversation getAvimConversation() {
		return avimConversation;
	}

	public void setAvimConversation(AVIMConversation avimConversation) {
		this.avimConversation = avimConversation;
	}

	public AVIMTypedMessage getAvimMessage() {
		return avimMessage;
	}

	public void setAvimMessage(AVIMTypedMessage avimMessage) {
		this.avimMessage = avimMessage;
	}

	public DBHelper getDbHelper() {
		return dbHelper;
	}

	public void setDbHelper(DBHelper dbHelper) {
		this.dbHelper = dbHelper;
	}

	public int getConfirm() {
		return confirm;
	}

	public void setConfirm(int confirm) {
		this.confirm = confirm;
	}

	public Msg() {

	}

	public String getFromUsername() {
		return fromUsername;
	}

	public void setFromUsername(String fromUsername) {
		this.fromUsername = fromUsername;
	}

	public long getTimestamp() {
		return this.timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	// public boolean isRequestReceipt() {
	// return avimMessage.;
	// }

	// public void setRequestReceipt(boolean isRequestReceipt) {
	// internalMessage.setRequestReceipt(isRequestReceipt);
	// }

	public String getConvid() {
		return convid;
	}

	public void setConvid(String convid) {
		this.convid = convid;
	}

	public RoomType getRoomType() {
		return roomType;
	}

	public void setRoomType(RoomType roomType) {
		this.roomType = roomType;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public ReadStatus getReadStatus() {
		return readStatus;
	}

	public void setReadStatus(ReadStatus readStatus) {
		this.readStatus = readStatus;
	}

	public String getContent() {
		return content;
	}

	public String getStatusDesc() {
		if (status == Status.SendStart) {
			return App.ctx.getString(R.string.sending);
		} else if (status == Status.SendReceived) {
			return App.ctx.getString(R.string.received);
		} else if (status == Status.SendSucceed) {
			return App.ctx.getString(R.string.sent);
		} else if (status == Status.SendFailed) {
			return App.ctx.getString(R.string.failed);
		} else if (status == Status.HaveRead) {
			return App.ctx.getString(R.string.haveRead);
		} else {
			throw new IllegalArgumentException("unknown status");
		}
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getObjectId() {
		return objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	public boolean isComeMessage() {
		String fromPeerId = getFromPeerId();
		return !fromPeerId.equals(ChatService.getSelfId());
	}

	/**
	 * 
	 * @return
	 */
	public String getFromPeerId() {
		// TODO Auto-generated method stub
		return this.fromPeerId;
	}

	public String getOtherId() {
		if (getRoomType() == RoomType.Group) {
			return getToPeerId();
		} else {
			String fromPeerId = getFromPeerId();
			String selfId = ChatService.getSelfId();
			if (fromPeerId.equals(selfId)) {
				return getToPeerId();
			} else {
				return fromPeerId;
			}
		}
	}

	public void setToPeerId(String toPeerId) {
		this.toPeerId = toPeerId;
	}

	public void setFromPeerId(String fromPeerId) {
		this.fromPeerId = fromPeerId;
	}

	/**
	 * 
	 * @return
	 */
	public String getToPeerId() {
		// TODO Auto-generated method stub
		return this.toPeerId;
	}

	public String getFromName() {
		String peerId = getFromPeerId();
		// User user = CacheService.lookupUser(peerId);
		// if (user != null) {
		// return user.getString("realName");
		// } else {
		// }
		NameCard nameCard = DBNamecard.getNameCardByUserId(dbHelper, peerId);
		return nameCard.getName();

	}

	public CharSequence getNotifyContent() {
		switch (type) {
		case Audio:
			return App.ctx.getString(R.string.audio);
		case Text:
			if (EmotionUtils.haveEmotion(getContent())) {
				return App.ctx.getString(R.string.emotion);
			} else {
				return getContent();
			}
		case Image:
			return App.ctx.getString(R.string.image);
		case Location:
			return App.ctx.getString(R.string.position);
		case Board:
			return App.ctx.getString(R.string.boardNotify);
		case Invite:
			return App.ctx.getString(R.string.inviteNotify);
		case Audio2:
			return App.ctx.getString(R.string.audio);
		case Text2:
			if (EmotionUtils.haveEmotion(getContent())) {
				return App.ctx.getString(R.string.emotion);
			} else {
				return getContent();
			}
		case Image2:
			return App.ctx.getString(R.string.image);
		case Location2:
			return App.ctx.getString(R.string.position);

		default:
			return App.ctx.getString(R.string.newMessage);
		}
	}

	/**
	 * 推送消息数据转化
	 * 
	 * @param jsonObject
	 * @return
	 */
	public static Msg fromPushData(org.json.JSONObject jsonObject) {
		Msg msg = new Msg();
		if (jsonObject != null) {
			try {
				String fromPeerId = jsonObject.getString("_channel");
				org.json.JSONObject payLoad = jsonObject
						.getJSONObject("payload");
				org.json.JSONObject contentJsObject = payLoad
						.getJSONObject("content");

				String content = contentJsObject.getString("msgBody");
				String objectId = payLoad.getString("objectId");
				Integer typeInt = (Integer) payLoad.get("type");

				if (objectId == null || content == null || typeInt == null) {
					throwNullException();
				}

				// SimpleDateFormat formatter = new SimpleDateFormat
				// ("yyyy-MM-dd HH:mm:ss");
				// Date curDate = new Date(System.currentTimeMillis());//获取当前时间
				// String timeStamp = formatter.format(curDate);

				msg.setTimestamp(System.currentTimeMillis());
				msg.setFromPeerId(fromPeerId);
				// msg.setFromUsername(fromUsername);
				msg.setObjectId(objectId);
				msg.setContent(content);
				Type type = Type.fromInt(typeInt);
				if (typeInt == 10) {
					// 登出令牌
					msg.setObjectId(contentJsObject.getString("token"));
				} else if (typeInt == 8) {
					// Logger.e("friendReqData->"+contentJsObject.toString());
					msg.setConfirm(contentJsObject.getInt("confirm"));
				} else if (typeInt == 5) {
					// Logger.e("pushId->"+contentJsObject.getString("pushId"));
					msg.setPushId(contentJsObject.getString("pushId"));
				}
				msg.setType(type);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return msg;

	}

	/**
	 * 主动拉取数据转化
	 * 
	 * @param jsonObject
	 * @return
	 */
	public static Msg fromGetMessages(Map<String, Object> data) {
		Msg msg = new Msg();
		String messageBody = "";
		if (data.get("messageBody") != null) {
			messageBody = data.get("messageBody").toString();
		} else {
			Logger.e("[Msg fromGetMessages]严重空值：" + data.toString());
		}
		// int sendRead = (Integer) data.get("sendRead");
		// int sendTotal = (Integer) data.get("sendTotal");
		String meetingName = "";
		if (data.get("meetingName") != null) {
			meetingName = data.get("meetingName").toString();
		} else {
			Logger.e("[Msg fromGetMessages]严重空值：" + data.toString());
		}
		// 18106570383 123456789
		// String channelId = data.get("channelId").toString();
		String objectId = "";
		if (data.get("objectId") != null) {
			objectId = data.get("objectId").toString();
		} else {
			Logger.e("[Msg fromGetMessages]严重空值：" + data.toString());
		}

		String meetingId = "";
		if (data.get("meetingId") != null) {
			meetingId = data.get("meetingId").toString();
		} else {
			Logger.e("[Msg fromGetMessages]严重空值：" + data.toString());
		}

		String type = "";
		if (data.get("type") != null) {
			type = data.get("type").toString();
		} else {
			Logger.e("[Msg fromGetMessages]严重空值：" + data.toString());
		}

		String updatedTime = "";
		if (data.get("updatedAt") != null) {
			updatedTime = data.get("updatedAt").toString();
		} else {
			Logger.e("[Msg fromGetMessages]严重空值：" + data.toString());
		}

		String dates = updatedTime.substring(0, 19);
		// Logger.e("dates"+dates);
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd'T'HH:mm:ss");
		// dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));//减少八个小时
		long timeStamp = System.currentTimeMillis();
		try {
			Date updatedDate = dateFormat.parse(dates);
			timeStamp = updatedDate.getTime();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			Logger.e("parseException" + e.getMessage());

		}
		// Logger.e("date"+ timeStamp);
		long change = 8 * 60 * 60 * 1000;// 当前时间与开始时间格式上差少了八个小时
		// int confirm = (Integer) data.get("confirm");
		if (Integer.parseInt(type) == 5) {
			msg.setContent(messageBody);
			msg.setFromPeerId(meetingId);
			msg.setObjectId(objectId);
			msg.setType(Type.fromInt(Integer.parseInt(type)));
			msg.setPushId(objectId);
			msg.setTimestamp(timeStamp + change);

		}

		return msg;

	}

	public String getPushId() {
		return pushId;
	}

	public void setPushId(String pushId) {
		this.pushId = pushId;
	}

	// @SuppressWarnings("unchecked")
	// public static Msg fromAVMessage(AVMessage avMsg) {
	// Msg msg = new Msg();
	// msg.setInternalMessage(avMsg);
	// if (!AVUtils.isBlankString(avMsg.getMessage())) {
	// HashMap<String, Object> params = JSON.parseObject(
	// avMsg.getMessage(), HashMap.class);
	// // HashMap<String, Object> contentMap = JSON.parseObject((String)
	// // params.get("content"),HashMap.class);
	// JSONObject contentJsonObject = (JSONObject) params.get("content");
	// String objectId = (String) params.get("objectId");
	// // String content = (String) contentMap.get("msgBody");
	// String content = contentJsonObject.getString("msgBody");
	// // System.out.println("msg->"+avMsg.getMessage());
	// // System.out.println("msgFromAvmsg->"+params.get("content").getClass());
	// //
	// System.out.println("msgFromContent->"+contentJsonObject.getString("msgBody"));
	// // String content = (String) contentMap.get("msgBody");
	// Integer typeInt = (Integer) params.get("type");
	// if (objectId == null || content == null || typeInt == null) {
	// throwNullException();
	// }
	// msg.setObjectId(objectId);
	// msg.setContent(content);
	// Type type = Type.fromInt(typeInt);
	// msg.setType(type);
	// }
	// return msg;
	// }

	// public AVMessage toAVMessage() {
	// if (convid == null || content == null || objectId == null
	// || roomType == null || status == null || type == null) {
	// throwNullException();
	// }
	// HashMap<String, Object> params = new HashMap<String, Object>();
	// HashMap<String, Object> contentMap = new HashMap<String, Object>();//
	// 文本对象
	// contentMap.put("msgBody", content);
	// // content = "{ \"msgBody\":"+content+"}";
	// params.put("objectId", objectId);
	// params.put("content", contentMap);
	// params.put("type", type.getValue());
	// internalMessage.setMessage(JSON.toJSONString(params));
	// // System.out.println("msgtoAvmsg->"+JSON.toJSONString(params));
	// return internalMessage;
	// }
	//
	/**
	 * V2消息转换成msg(接收消息时转换)
	 * 
	 * @param avimTypedMessage
	 * @return
	 */
	public static Msg fromAVIMMessage(AVIMTypedMessage avimTypedMessage,
			AVIMConversation avimConversation) {
		Msg msg = new Msg();
		msg.setAvimMessage(avimTypedMessage);
		msg.setAvimConversation(avimConversation);
		// if (!AVUtils.isBlankString(avimTypedMessage.getContent())) {
		String objectId = avimTypedMessage.getMessageId();
		// String convId = avimTypedMessage.getConversationId();
		int typeInt = -(avimTypedMessage.getMessageType()) + 10;

		// msg.setConvid(convId);
		Type type = Type.fromInt(typeInt);
		if (type == Type.Text2) {
			AVIMTextMessage avimTextMessage = (AVIMTextMessage) avimTypedMessage;
			msg.setContent(avimTextMessage.getText());
		} else if (type == Type.Image2) {
			AVIMImageMessage avimImageMessage = (AVIMImageMessage) avimTypedMessage;
			msg.setContent(avimImageMessage.getFileUrl());
		} else if (type == Type.Video2) {
			AVIMVideoMessage avimVideoMessage = (AVIMVideoMessage) avimTypedMessage;
			msg.setContent(avimVideoMessage.getFileUrl());
		} else if (type == Type.Audio2) {
			AVIMAudioMessage avimAudioMessage = (AVIMAudioMessage) avimTypedMessage;
			msg.setContent(avimAudioMessage.getFileUrl());
			if (avimAudioMessage.getAttrs() != null) {
				msg.setAudioTime(Double.parseDouble((String) avimAudioMessage.getAttrs().get("voiceDuration")));		
			}
			Logger.d("audioCome"+avimAudioMessage.getFileMetaData().toString());
		}
		msg.setTimestamp(avimTypedMessage.getTimestamp());
		msg.setObjectId(objectId);
		msg.setType(type);
		msg.setFromPeerId(avimTypedMessage.getFrom());
		msg.setToPeerId(ChatService.getSelfId());
		// }
		return msg;

	}

	/**
	 * 获取图片消息
	 * 
	 * @return
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public AVIMImageMessage getImageMessage() throws FileNotFoundException,
			IOException {
		if (convid == null || objectId == null || roomType == null
				|| status == null || type == null) {
			throwNullException();
		}
		AVIMImageMessage avimImageMessage = new AVIMImageMessage(getImagePath());
		// avimImageMessage.setContent(content);
		avimImageMessage.setText(content);
		avimImageMessage.setMessageId(objectId);
		avimMessage = avimImageMessage;
		return avimImageMessage;
	}

	/**
	 * 获取音频消息
	 * 
	 * @return
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public AVIMAudioMessage getAudioMessage() throws FileNotFoundException,
			IOException {
		if (convid == null || objectId == null || roomType == null
				|| status == null || type == null) {
			throwNullException();
		}
		AVIMAudioMessage avimAudioMessage = new AVIMAudioMessage(
				getAudioPath2());
		avimAudioMessage.setText(content);
		avimAudioMessage.setMessageId(objectId);
		Map<String, Object> attrs = new HashMap<String, Object>();
		attrs.put("voiceDuration", audioTime+"");
		avimAudioMessage.setAttrs(attrs);
		// Utils.toast("url"+avimAudioMessage.getFileUrl());
		avimMessage = avimAudioMessage;
		return avimAudioMessage;
	}

	/**
	 * 
	 * @return
	 */
	public AVIMTextMessage getTextMessage() {
		if (convid == null || content == null || objectId == null
				|| roomType == null || status == null || type == null) {
			throwNullException();
		}
		AVIMTextMessage avimTextMessage = new AVIMTextMessage();
		avimTextMessage.setText(content);
		avimTextMessage.setMessageId(objectId);
		avimMessage = avimTextMessage;
		return avimTextMessage;
	}

	public static void throwNullException() {
		throw new NullPointerException(
				"at least one of these is null: content,objectId,type");
	}

	@Override
	public String toString() {
		return "{content:" + getContent() + " objectId:" + getObjectId()
				+ " status:" + getStatus() + " fromPeerId:" + getFromPeerId()
				+ " toPeerIds:" + " timestamp:" + getTimestamp() + " type="
				+ getType() + "}";
	}

	/**
	 * 图像与音频本地资源地址
	 * 
	 * @return
	 */
	public String getAudioPath2() {
		return PathUtils.getAudioFilePath(getObjectId());
	}

	/**
	 * 图像与音频本地资源地址
	 * 
	 * @return
	 */
	public String getImagePath() {
		return PathUtils.getChatFileDir() + getObjectId();
	}

}
