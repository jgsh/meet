package com.hyx.meet.entity;

import java.io.IOException;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.hyx.meet.service.ChatService;
import com.hyx.meet.utils.AVOSUtils;
import com.hyx.meet.utils.PathUtils;
import com.hyx.meet.utils.Utils;

public class MsgBuilder {
  Msg msg;

  public MsgBuilder() {
    msg = new Msg();
  }

  public void target(RoomType roomType, String toId,String fromUsername) {
    String convid;
    msg.setRoomType(roomType);
    if (roomType == RoomType.Single) {
      msg.setToPeerId(toId);
//      msg.setRequestReceipt(true);
      msg.setFromUsername(fromUsername);
      convid = AVOSUtils.convid(ChatService.getSelfId(), toId);
    }else if (roomType == RoomType.NOTIFY) {
    		msg.setToPeerId(toId);
//        msg.setRequestReceipt(true);
        msg.setFromUsername(fromUsername);
        convid = AVOSUtils.convid(ChatService.getSelfId(), toId);		
	} else {
      convid = toId;
    }
    msg.setConvid(convid);
    msg.setReadStatus(Msg.ReadStatus.HaveRead);
  }

  public void target2(RoomType roomType, String toId,String fromUsername) {
	    msg.setRoomType(roomType);
	    String convid;
	    if (roomType == RoomType.Single) {
	      msg.setToPeerId(toId);
//	      msg.setRequestReceipt(true);
	      msg.setFromUsername(fromUsername);
	      convid = AVOSUtils.convid(ChatService.getSelfId(), toId);
	    }else if (roomType == RoomType.NOTIFY) {
	    		msg.setToPeerId(toId);
//	        msg.setRequestReceipt(true);
	        msg.setFromUsername(fromUsername);
	        convid = AVOSUtils.convid(ChatService.getSelfId(), toId);		
		} else {
			convid = toId;
	    }
	    msg.setConvid(convid);
	    msg.setReadStatus(Msg.ReadStatus.HaveRead);
	  }

  public void text(String content) {
    msg.setType(Msg.Type.Text);
    msg.setContent(content);
  }
  
  public void text2(String content) {
	    msg.setType(Msg.Type.Text2);
	    msg.setContent(content);
	  }
  public void image2(String objectId) {
	    file(Msg.Type.Image2, objectId);
	  }


  private void file(Msg.Type type, String objectId) {
    msg.setType(type);
    msg.setObjectId(objectId);
  }

  public void image(String objectId) {
    file(Msg.Type.Image, objectId);
  }

  public void location(String address, double latitude, double longitude) {
    String content = address + "&" + latitude + "&" + longitude;
    msg.setContent(content);
    msg.setType(Msg.Type.Location);
  }

  public void audio(String objectId) {
    file(Msg.Type.Audio, objectId);
  }
  public void audio2(String objectId,double	audioTime) {
	    file(Msg.Type.Audio2, objectId);
	    msg.setAudioTime(audioTime);
	  }

  public Msg preBuild() {
    msg.setStatus(Msg.Status.SendStart);
    msg.setTimestamp(System.currentTimeMillis());
    msg.setFromPeerId(ChatService.getSelfId());
    if (msg.getObjectId() == null) {
      msg.setObjectId(Utils.uuid());
    }
    return msg;
  }

  public static String uploadMsg(Msg msg) throws IOException, AVException {
    if (msg.getType() != Msg.Type.Audio && msg.getType() != Msg.Type.Image) {
      return null;
    }
    String objectId = msg.getObjectId();
    if (objectId == null) {
      throw new NullPointerException("objectId mustn't be null");
    }
    String filePath = PathUtils.getChatFilePath(objectId);
    AVFile file = AVFile.withAbsoluteLocalPath(objectId, filePath);
    file.save();
    String url = file.getUrl();
    //转为V2后直接保存filePath
    msg.setContent(url);
    return url;
  }
}
