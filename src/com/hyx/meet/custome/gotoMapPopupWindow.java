package com.hyx.meet.custome;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.hyx.meet.R;
import com.hyx.meet.meet.JuMainActivity;

public class gotoMapPopupWindow extends PopupWindow {

	private LinearLayout goto_baidu, goto_gaode, btn_cancel,
			goto_local, biglinear;

	private View mMenuView;
	private RelativeLayout re;

	public gotoMapPopupWindow(Activity context, OnClickListener itemsOnClick) {

		super(context);

		LayoutInflater inflater = (LayoutInflater) context

		.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		mMenuView = inflater.inflate(R.layout.popuvwindow_gotonavi, null);

		goto_baidu = (LinearLayout) mMenuView
				.findViewById(R.id.id_goto_baidu);

		goto_gaode = (LinearLayout) mMenuView
				.findViewById(R.id.id_goto_gaode);

		goto_local = (LinearLayout) mMenuView.findViewById(R.id.id_goto_local);

		btn_cancel = (LinearLayout) mMenuView
				.findViewById(R.id.id_goto_map_cancle);

		re = (RelativeLayout) mMenuView.findViewById(R.id.id_me_poplayout);
		
		re.setAlpha(160);
		
		// 取消按钮
		biglinear = (LinearLayout) mMenuView.findViewById(R.id.bigLinear);
		btn_cancel.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				// 销毁弹出框

				dismiss();

			}

		});
		biglinear.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				// 销毁弹出框

				dismiss();

			}

		});

		// 设置按钮监听

		goto_baidu.setOnClickListener(itemsOnClick);

		goto_gaode.setOnClickListener(itemsOnClick);

		goto_local.setOnClickListener(itemsOnClick);
		// 设置SelectPicPopupWindow的View

		this.setContentView(mMenuView);

		// 设置SelectPicPopupWindow弹出窗体的宽

		int width = (LayoutParams.FILL_PARENT);

		// int width=(JuMainActivity.WIDTH/6)*5;
		this.setWidth(width);

		// 设置SelectPicPopupWindow弹出窗体的高

		this.setHeight(LayoutParams.FILL_PARENT);

		// 设置SelectPicPopupWindow弹出窗体可点击

		this.setFocusable(true);

		// 设置SelectPicPopupWindow弹出窗体动画效果

		this.setAnimationStyle(R.style.Animations_Bottom);

		// 实例化一个ColorDrawable颜色为半透明

		ColorDrawable dw = new ColorDrawable(-00000);

		// 设置SelectPicPopupWindow弹出窗体的背景

		this.setBackgroundDrawable(dw);
		
		this.getBackground().setAlpha(140);

		// mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框

		mMenuView.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View v, MotionEvent event) {

				int height = mMenuView.findViewById(R.id.id_me_poplayout)
						.getTop();

				int y = (int) event.getY();

				if (event.getAction() == MotionEvent.ACTION_UP) {

					if (y < height) {

						dismiss();

					}

				}

				return true;

			}

		});

	}

}