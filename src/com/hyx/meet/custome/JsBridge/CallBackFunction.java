package com.hyx.meet.custome.JsBridge;

import org.json.JSONObject;

public interface CallBackFunction {
	
	public void onCallBack(String data);
	
	public void onCallBackByJson(JSONObject jsonObject);

}
