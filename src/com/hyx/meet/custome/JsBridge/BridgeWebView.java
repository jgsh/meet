package com.hyx.meet.custome.JsBridge;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Looper;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.xwalk.core.XWalkPreferences;
import org.xwalk.core.XWalkResourceClient;
import org.xwalk.core.XWalkUIClient;
import org.xwalk.core.XWalkView;

import com.hyx.meet.JuHYXWebviewActivity;
import com.hyx.meet.R;
import com.hyx.meet.utils.Logger;

@SuppressLint("SetJavaScriptEnabled")
public class BridgeWebView extends XWalkView implements WebViewJavascriptBridge {

	private final String TAG = "BridgeWebView";
//	private LinearLayout mDialog;
//	private Context context;
	
	private JuHYXWebviewActivity juHYXWebviewActivity;

	String toLoadJs = "WebViewJavascriptBridge.js";
	Map<String, CallBackFunction> responseCallbacks = new HashMap<String, CallBackFunction>();
	Map<String, BridgeHandler> messageHandlers = new HashMap<String, BridgeHandler>();
	BridgeHandler defaultHandler = new DefaultHandler();

	List<Message> startupMessage = new ArrayList<Message>();
	long uniqueId = 0;

	public BridgeWebView(Context context, Activity activity) {
		// TODO Auto-generated constructor stub
		super(context, activity);
//		this.context = context;
//		Logger.e("jssdk activity");
//		mDialog = (LinearLayout) activity.findViewById(R.id.common_dialog_layout);
		init();
	}

	public BridgeWebView(Context context, AttributeSet attrs) {
		super(context, attrs);
//		Logger.e("jssdk context");
//		this.context = context;
		init();
	}
	
	

	public void setActivity(JuHYXWebviewActivity activity){
		
		juHYXWebviewActivity = activity;

	}
	/**
	 * 
	 * @param handler
	 *            default handler,handle messages send by js without assigned
	 *            handler name, if js message has handler name, it will be
	 *            handled by named handlers registered by native
	 */
	public void setDefaultHandler(BridgeHandler handler) {
		this.defaultHandler = handler;
	}

	private void init() {
				this.setVerticalScrollBarEnabled(false);
		this.setHorizontalScrollBarEnabled(false);
		this.getSettings().setJavaScriptEnabled(true);
		XWalkPreferences.setValue("remote-debugging", true);
//		this.getSettings().setAllowContentAccess(true);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			WebView.setWebContentsDebuggingEnabled(true);
		}
		this.setResourceClient(new BridgeWebViewClient(this));
		this.setUIClient(new MyUIClient(this));
		

		
	}

	private void handlerReturnData(String url) {
		Logger.e("formJsUrl   handlerReturnData ->" + url);

		String functionName = BridgeUtil.getFunctionFromReturnUrl(url);
		CallBackFunction f = responseCallbacks.get(functionName);
		String data = BridgeUtil.getDataFromReturnUrl(url);
//		Logger.e("formJsUrl   handlerReturnData  data->" + data);

		if (f != null) {
			f.onCallBack(data);
			responseCallbacks.remove(functionName);
			return;
		}
	}

	class MyUIClient extends XWalkUIClient {

		public MyUIClient(XWalkView view) {
			super(view);
			// TODO Auto-generated constructor stub
//			mDialog = (LinearLayout) ((Activity) context)
//					.findViewById(R.id.common_dialog_layout);

		}

		@Override
		public void onFullscreenToggled(XWalkView view, boolean enterFullscreen) {
			// TODO Auto-generated method stub
			super.onFullscreenToggled(view, enterFullscreen);
		}
	}

	class BridgeWebViewClient extends XWalkResourceClient {

		public BridgeWebViewClient(XWalkView view) {
			super(view);
//			mDialog = (LinearLayout) ((Activity) context)
//					.findViewById(R.id.common_dialog_layout);

			// TODO Auto-generated constructor stub
			if (toLoadJs != null) {
				BridgeUtil.webViewLoadLocalJs(view, toLoadJs);
			}

		}

		@Override
		public void onProgressChanged(XWalkView view, int progressInPercent) {
			// TODO Auto-generated method stub
			super.onProgressChanged(view, progressInPercent);
			if (progressInPercent == 100) {
				juHYXWebviewActivity.closeDialog();
				juHYXWebviewActivity.isOpenGuideOpt();
			}

//			}else {
//				jsSdkDemo.showDialog();
//			}
			Logger.e(""+progressInPercent);
;		}

		@Override
		public boolean shouldOverrideUrlLoading(XWalkView view, String url) {
			try {
				url = URLDecoder.decode(url, "UTF-8");
				// Logger.e("bridge get url->"+url);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			if (url.startsWith(BridgeUtil.YY_RETURN_DATA)) { // 如果是返回数据
				handlerReturnData(url);
				return true;
			} else if (url.startsWith(BridgeUtil.YY_OVERRIDE_SCHEMA)) { //
				flushMessageQueue();
				return true;
			} else {
				return super.shouldOverrideUrlLoading(view, url);
			}
		}

		@Override
		public void onLoadStarted(XWalkView view, String url) {
			// TODO Auto-generated method stub
			super.onLoadStarted(view, url);
//			if (toLoadJs != null) {
//				BridgeUtil.webViewLoadLocalJs(view, toLoadJs);
//			}
//			if (mDialog.getVisibility() == View.GONE) {
//				mDialog.setVisibility(View.VISIBLE);
//			}
//			jsSdkDemo.showDialog();

		}

		@Override
		public void onLoadFinished(XWalkView view, String url) {
			// TODO Auto-generated method stub
			super.onLoadFinished(view, url);
			// Logger.e("onloadFinish url->"+url);
			// if (toLoadJs != null) {
			// BridgeUtil.webViewLoadLocalJs(view, toLoadJs);
			// }

			//
			if (startupMessage != null) {
				for (Message m : startupMessage) {
					dispatchMessage(m);
				}
				startupMessage = null;
			}
//			juHYXWebviewActivity.closeDialog();
//			if (mDialog.getVisibility() == View.VISIBLE) {
//				mDialog.setVisibility(View.GONE);
//			}
//
		}

		@Override
		public void onReceivedLoadError(XWalkView view, int errorCode,
				String description, String failingUrl) {
			// TODO Auto-generated method stub
			super.onReceivedLoadError(view, errorCode, description, failingUrl);
			juHYXWebviewActivity.closeDialog();
		}
	}

	@Override
	public void send(String data) {
		send(data, null);
	}

	@Override
	public void send(String data, CallBackFunction responseCallback) {
		doSend(null, data, responseCallback);
	}

	private void doSend(String handlerName, String data,
			CallBackFunction responseCallback) {
		Message m = new Message();
		if (!TextUtils.isEmpty(data)) {
			m.setData(data);
		}
		if (responseCallback != null) {
			String callbackStr = String.format(
					BridgeUtil.CALLBACK_ID_FORMAT,
					++uniqueId
							+ (BridgeUtil.UNDERLINE_STR + SystemClock
									.currentThreadTimeMillis()));
			responseCallbacks.put(callbackStr, responseCallback);
			m.setCallbackId(callbackStr);
		}
		if (!TextUtils.isEmpty(handlerName)) {
			m.setHandlerName(handlerName);
		}
		queueMessage(m);
	}

	private void queueMessage(Message m) {
		if (startupMessage != null) {
			startupMessage.add(m);
		} else {
			dispatchMessage(m);
		}
	}

	private void dispatchMessage(Message m) {
		String messageJson = m.toJson();
		// escape special characters for json string
		messageJson = messageJson.replaceAll("(\\\\)([^utrn])", "\\\\\\\\$1$2");
		messageJson = messageJson.replaceAll("(?<=[^\\\\])(\")", "\\\\\"");
		// String js =
		// "javascript:WebViewJavascriptBridge._handleMessageFromNative('"+messageJson+"')";
		String javascriptCommand = String.format(
				BridgeUtil.JS_HANDLE_MESSAGE_FROM_JAVA, messageJson);
		if (Thread.currentThread() == Looper.getMainLooper().getThread()) {
			this.load(javascriptCommand, null);
		}
	}

	public void flushMessageQueue() {
		if (Thread.currentThread() == Looper.getMainLooper().getThread()) {
			loadUrl(BridgeUtil.JS_FETCH_QUEUE_FROM_JAVA,
					new CallBackFunction() {

						@Override
						public void onCallBack(String data) {
							// deserializeMessage
							Logger.e("formJsData  oncallback->" + data);
							List<Message> list = null;
							try {
								list = Message.toArrayList(data);
							} catch (Exception e) {
								e.printStackTrace();
								return;
							}
							if (list == null || list.size() == 0) {
								return;
							}
							for (int i = 0; i < list.size(); i++) {
								Message m = list.get(i);
								String responseId = m.getResponseId();
								// 是否是response
								if (!TextUtils.isEmpty(responseId)) {
									CallBackFunction function = responseCallbacks
											.get(responseId);
									String responseData = m.getResponseData();
									function.onCallBack(responseData);
									responseCallbacks.remove(responseId);
								} else {
									CallBackFunction responseFunction = null;
									// if had callbackId
									final String callbackId = m.getCallbackId();
									if (!TextUtils.isEmpty(callbackId)) {
										responseFunction = new CallBackFunction() {
											@Override
											public void onCallBack(String data) {
												Message responseMsg = new Message();
												responseMsg
														.setResponseId(callbackId);
												responseMsg
														.setResponseData(data);
												try {
													responseMsg
															.setResponseJsonData(new JSONObject(
																	data));
												} catch (JSONException e) {
													// TODO Auto-generated catch
													// block
													e.printStackTrace();
												}
												queueMessage(responseMsg);
											}

											@Override
											public void onCallBackByJson(
													JSONObject jsonObject) {
												// TODO Auto-generated method
												// stub
												Message responseMsg = new Message();
												responseMsg
														.setResponseId(callbackId);
												responseMsg
														.setResponseJsonData(jsonObject);
												queueMessage(responseMsg);
											}
										};
									} else {
										responseFunction = new CallBackFunction() {
											@Override
											public void onCallBack(String data) {
												// do nothing
											}

											@Override
											public void onCallBackByJson(
													JSONObject jsonObject) {
												// TODO Auto-generated method
												// stub

											}
										};
									}
									BridgeHandler handler;
									if (!TextUtils.isEmpty(m.getHandlerName())) {
										handler = messageHandlers.get(m
												.getHandlerName());
									} else {
										handler = defaultHandler;
									}
									Logger.e("formJsData  hander->"
											+ m.getData());
									handler.handler(m.getData(),
											responseFunction);
								}
							}
						}

						@Override
						public void onCallBackByJson(JSONObject jsonObject) {
							// TODO Auto-generated method stub
							List<Message> list = null;
							try {
								list = Message
										.toArrayListByJsonobject(jsonObject);
							} catch (Exception e) {
								e.printStackTrace();
								return;
							}
							if (list == null || list.size() == 0) {
								return;
							}
							for (int i = 0; i < list.size(); i++) {
								Message m = list.get(i);
								String responseId = m.getResponseId();
								// 是否是response
								if (!TextUtils.isEmpty(responseId)) {
									CallBackFunction function = responseCallbacks
											.get(responseId);
									String responseData = m.getResponseData();
									function.onCallBack(responseData);
									responseCallbacks.remove(responseId);
								} else {
									CallBackFunction responseFunction = null;
									// if had callbackId
									final String callbackId = m.getCallbackId();
									if (!TextUtils.isEmpty(callbackId)) {
										responseFunction = new CallBackFunction() {
											@Override
											public void onCallBack(String data) {
												Message responseMsg = new Message();
												responseMsg
														.setResponseId(callbackId);
												responseMsg
														.setResponseData(data);
												try {
													responseMsg
															.setResponseJsonData(new JSONObject(
																	data));
												} catch (JSONException e) {
													// TODO Auto-generated catch
													// block
													e.printStackTrace();
												}
												queueMessage(responseMsg);
											}

											@Override
											public void onCallBackByJson(
													JSONObject jsonObject) {
												// TODO Auto-generated method
												// stub
												Message responseMsg = new Message();
												responseMsg
														.setResponseId(callbackId);
												responseMsg
														.setResponseJsonData(jsonObject);
												queueMessage(responseMsg);
											}
										};
									} else {
										responseFunction = new CallBackFunction() {
											@Override
											public void onCallBack(String data) {
												// do nothing
											}

											@Override
											public void onCallBackByJson(
													JSONObject jsonObject) {
												// TODO Auto-generated method
												// stub

											}
										};
									}
									BridgeHandler handler;
									if (!TextUtils.isEmpty(m.getHandlerName())) {
										handler = messageHandlers.get(m
												.getHandlerName());
									} else {
										handler = defaultHandler;
									}
									handler.handler(m.getData(),
											responseFunction);
								}
							}
						}
					});
		}
	}

	public void loadUrl(String jsUrl, CallBackFunction returnCallback) {
		this.load(jsUrl, null);
		responseCallbacks.put(BridgeUtil.parseFunctionName(jsUrl),
				returnCallback);
	}

	/**
	 * register handler,so that javascript can call it
	 * 
	 * @param handlerName
	 * @param handler
	 */
	public void registerHandler(String handlerName, BridgeHandler handler) {
		if (handler != null) {
			messageHandlers.put(handlerName, handler);
		}
	}

	/**
	 * call javascript registered handler
	 *
	 * @param handlerName
	 * @param data
	 * @param callBack
	 */
	public void callHandler(String handlerName, String data,
			CallBackFunction callBack) {
		doSend(handlerName, data, callBack);
	}
}
