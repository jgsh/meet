package com.hyx.meet.custome;

import java.util.ArrayList;
import java.util.List;

import com.hyx.meet.entity.JuContact;

import android.R.integer;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class JuContactTextView extends TextView {

	private List<JuContact> mList;
	public JuContactTextView(Context context, AttributeSet attrs, int defStyle){
		super(context, attrs, defStyle);
	}
	
	public JuContactTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		mList = new ArrayList<JuContact>();
	}

	public JuContactTextView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		mList = new ArrayList<JuContact>();
	}
	
		
	/**
	 * 设置已选联系人
	 * @param list
	 */
	public void setContacts(List<JuContact> list){
		String text = "";
		for(int i = 0;i<list.size();i++){
			text += "<"+list.get(i).getName()+">;";
		}
		setText(text);
		
	}
	
	

}
