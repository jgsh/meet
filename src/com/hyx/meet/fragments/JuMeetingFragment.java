/**
 * 
 */
package com.hyx.meet.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVQuery.CachePolicy;
import com.avos.avoscloud.AVCloud;
import com.avos.avoscloud.AVRelation;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.FunctionCallback;
import com.avos.avoscloud.GetCallback;
import com.avos.avoscloud.RelationPropertyFilter;
import com.avos.avoscloud.SaveCallback;
import com.hyx.meet.JuHYXWebviewActivity;
import com.hyx.meet.R;
import com.hyx.meet.Application.App;
import com.hyx.meet.adapter.JuMeetAdapter;
import com.hyx.meet.custome.xlist.XListView;
import com.hyx.meet.custome.xlist.XListView.IXListViewListener;
import com.hyx.meet.custome.xlist.XListViewHeader;
import com.hyx.meet.db.DBHelper;
import com.hyx.meet.db.DBMeet;
import com.hyx.meet.entity.MeetingDomain;
import com.hyx.meet.namecard.JuNameCardShowActivity;
import com.hyx.meet.qr.MipcaActivityCapture;
import com.hyx.meet.service.ChatService;
import com.hyx.meet.service.MeetingService;
import com.hyx.meet.service.listener.MeetingListener;
import com.hyx.meet.utils.AndroidConfig;
import com.hyx.meet.utils.JuUrl;
import com.hyx.meet.utils.Logger;
import com.hyx.meet.utils.Utils;

import android.R.integer;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.BaseBundle;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewGroup.LayoutParams;
import android.view.View.OnTouchListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 会议列表
 * 
 * @author joy
 * 
 */
public class JuMeetingFragment extends Fragment implements OnItemClickListener,
		IXListViewListener, MeetingListener, android.view.View.OnClickListener,OnDismissListener {

	private PopupWindow mPopupWindow;
	private View meetingView;
	private XListView meetingListView;
	private Context context;
	private JuMeetAdapter myAdapter;
	private List<MeetingDomain> newData;
	private final static int MEET_ALL = 0;// 0 全部
	private final static int MEET_MY = 1;// 0 我发布的会议
	private final static int MEET_GOING = 2;// 0 正在进行
	private final static int MEET_WILL = 3;// 0 即将开始
	private final static int MEET_END = 4;// 0 圆满结束
	private static int NOW_MEET_TAG = 0;// 当前选择状态

	LinearLayout qrLinear;
	LinearLayout meeting_null;
	View customView;
	TextView getAll;
	TextView getMyMeets;
	TextView getGoing;
	TextView getWill;
	TextView getEnd;
	ImageView mSelectArrow;
	private TextView mSelector;// 顶部下拉按钮
	private RelativeLayout mSelectorLayout;

	public JuMeetingFragment() {

	}

	public JuMeetingFragment(Context context) {
		this.context = context;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		meetingView = inflater.inflate(R.layout.fragment_meetingfragment,
				container, false);
		initView();
		initData();
		onRefresh();

		return meetingView;

	}

	public void initView() {
		customView = getActivity().getLayoutInflater().inflate(
				R.layout.meet_group_selector, null);
		// 全部会议
		 getAll = (TextView) customView
				.findViewById(R.id.id_get_meeting_all);
		getAll.setOnClickListener(this);
		// 我发布的会议
		 getMyMeets = (TextView) customView
				.findViewById(R.id.id_get_meeting_MyMeets);
		getMyMeets.setOnClickListener(this);
		// 正在进行
		 getGoing = (TextView) customView
				.findViewById(R.id.id_get_meeting);
		getGoing.setOnClickListener(this);
		// 即将开始
		 getWill = (TextView) customView
				.findViewById(R.id.id_get_meeting_unstart);
		getWill.setOnClickListener(this);
		// 圆满结束
		 getEnd = (TextView) customView
				.findViewById(R.id.id_get_meeting_end);
		getEnd.setOnClickListener(this);

		// mSwipeLayout.setOnRefreshListener(this);
		meetingListView = (XListView) meetingView.findViewById(R.id.xlistView);
		meetingListView.setOnItemClickListener(this);
		meetingListView.setPullLoadEnable(false);// 设置上拉刷新
		meetingListView.setPullRefreshEnable(true);// 设置下拉刷新
		meetingListView.setXListViewListener(this);
		meetingListView.setFooterDividersEnabled(false);

		qrLinear = (LinearLayout) meetingView.findViewById(R.id.id_qr_Lay);
		mSelector = (TextView) meetingView.findViewById(R.id.id_meet_select_Btn);
		mSelectArrow = (ImageView) meetingView.findViewById(R.id.id_meet_select_arrow);
		mSelectorLayout = (RelativeLayout) meetingView.findViewById(R.id.id_meet_select_Lay);

		meeting_null = (LinearLayout) meetingView.findViewById(R.id.meetnull);
		qrLinear.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent qrScanIntent = new Intent(context,
						MipcaActivityCapture.class);
				startActivity(qrScanIntent);
				((Activity) context).overridePendingTransition(R.anim.zoomin,
						R.anim.zoomout_static);
			}
		});
		mSelectorLayout.setOnClickListener(this);

		newData = new ArrayList<MeetingDomain>();
		myAdapter = new JuMeetAdapter(context, newData);
		meetingListView.setAdapter(myAdapter);

	}

	public void initData() {

		newData.addAll(DBMeet.getInformationAll());
		myAdapter.notifyDataSetChanged();

		if (newData.size() == 0) {
			meeting_null.setVisibility(View.VISIBLE);

		} else {
			meeting_null.setVisibility(View.GONE);
		}

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		JuHYXWebviewActivity.addMeetingListener(this);
		// 刷新会议列表
		// onRefresh();
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		JuHYXWebviewActivity.removeMeetingListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view,
			final int position, long id) {
		// TODO Auto-generated method stub

		// newData = DBMeet.getInformation();
		// Toast.makeText(App.ctx, "123" + newData.size(), 0).show();
		// for(int i=0;i<newData.size();i++)
		try {
			// String name = null;

			String meetingId = newData.get(position - 1).getObjectid();

			// name = newData.get(position - 1).getName();
			Intent intent = new Intent();
			String url = "";
			if (JuUrl.MODE_STYLE.equals("prod")) {
				url = String.format(JuUrl.MEETING_DETAIL_PROD, meetingId,
						ChatService.getSelfId());
			} else {
				url = String.format(JuUrl.MEETING_DETAIL_DEV, meetingId,
						ChatService.getSelfId());
			}
			intent.putExtra("url", url);
			intent.putExtra("showGuide", 1);
			intent.setClass(App.ctx, JuHYXWebviewActivity.class);
			startActivity(intent);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)刷新会议列表 *
	 * 
	 * @see com.example.custome.XListView.IXListViewListener#onRefresh()
	 */
	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		meetingListView.pullRefreshing();
		// 重新获取数据
		AVUser user = AVUser.getCurrentUser();
		String id = user.getObjectId();
		final Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("userId", id);
		AVCloud.callFunctionInBackground("getAttendMeetingList", parameters,
				new FunctionCallback<Map<String, Object>>() {

					@Override
					public void done(Map<String, Object> object, AVException e) {
						// TODO Auto-generated method stub
						// System.out.println("meetdata->"+object.toString());
						if (e == null) {
							// System.out.println(parameters.toString());

							int errorCode = (Integer) object.get("errorCode");
							if (errorCode == 0) {

								@SuppressWarnings("unchecked")
								List<Map<String, Object>> meetingList = (List<Map<String, Object>>) object
										.get("data");
								meetingListView.stopRefresh();
								DBMeet.deleteMeeting(meetingList);
								for (int i = 0; i < meetingList.size(); i++) {
									DBMeet.insertMeeting(meetingList.get(i));
									Logger.d("会议列表数据-》"
											+ meetingList.get(i).toString());
								}

								//标记时间戳以后会议消息
								MeetingService.getMessages();
								MeetingService.setGetMessageTimeStamp();

								//过滤会议数据
								List<MeetingDomain> meeting = new ArrayList<MeetingDomain>();
								meeting = MeetingService.filterMeetings(NOW_MEET_TAG);
								if (meeting.size() > 0) {
									meeting_null.setVisibility(View.GONE);
								} else {
									// Toast.makeText(App.ctx, "haha",
									// 0).show();
									meeting_null.setVisibility(View.VISIBLE);
								}
								newData.clear();
								newData.addAll(meeting);
								myAdapter.notifyDataSetChanged();
								meetingListView.startLayoutAnimation();
								// 订阅会议频道
								if (NOW_MEET_TAG == MEET_ALL) {
									MeetingService.subcribeByIDs(DBMeet
											.getInformationAll());									
								}								
								// Toast.makeText(App.ctx, "已经是最新的会议列表啦！", 0)
								// .show();
							}

						} else {
							// System.out.println(parameters.toString());
							meetingListView.stopRefresh();
							Toast.makeText(App.ctx, "请检查网络设置", 0).show();
						}

					}
					// else{
					//
					// }

				});

	}

	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.example.custome.XListView.IXListViewListener#onLoadMore()
	 */
	@Override
	public void onLoadMore() {
		/*
		 * if(isLoading==false){ isLoading=true; }else{ xlistViewLoadFinish(); }
		 */

	}

	private void xlistViewLoadFinish() {

	}

	@Override
	public boolean onMeetingListUpdate() {
		// TODO Auto-generated method stub
		onRefresh();
		return false;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.id_meet_select_Lay:
			if (mPopupWindow != null && mPopupWindow.isShowing()) {
				mPopupWindow.dismiss();
			} else {
				initmPopuwindow();
				// 获取宽度
				int w = View.MeasureSpec.makeMeasureSpec(0,
						View.MeasureSpec.UNSPECIFIED);
				int h = View.MeasureSpec.makeMeasureSpec(0,
						View.MeasureSpec.UNSPECIFIED);
				customView.measure(w, h);
				int height = customView.getMeasuredHeight();
				int width = customView.getMeasuredWidth();
//				Utils.toast(width + "/" + mSelector.getWidth());
				mPopupWindow.showAsDropDown(v,
						(mSelectorLayout.getWidth() - width) / 2, 0);

			}
			break;
		case R.id.id_get_meeting_MyMeets:
			mSelector.setText("我发布的");
			NOW_MEET_TAG = MEET_MY;
			onrefreshReady();
			onRefresh();
			mPopupWindow.dismiss();
			break;
		case R.id.id_get_meeting:
			mSelector.setText("正在进行");
			NOW_MEET_TAG = MEET_GOING;
			onrefreshReady();
			onRefresh();
			mPopupWindow.dismiss();
			break;
		case R.id.id_get_meeting_unstart:
			mSelector.setText("即将开始");
			NOW_MEET_TAG = MEET_WILL;
			onrefreshReady();
			onRefresh();
			mPopupWindow.dismiss();
			break;
		case R.id.id_get_meeting_end:
			mSelector.setText("已经结束");
			NOW_MEET_TAG = MEET_END;
			onrefreshReady();
			onRefresh();
			mPopupWindow.dismiss();
			break;
		case R.id.id_get_meeting_all:
			mSelector.setText("全部会议");
			NOW_MEET_TAG = MEET_ALL;
			onrefreshReady();
			onRefresh();
			mPopupWindow.dismiss();

		default:
			break;
		}
	}

	private void onrefreshReady() {
		// TODO Auto-generated method stub
		meetingListView.setSelection(0);
	}

	/**
	 * 初始化下拉列表
	 */
	private void initmPopuwindow() {
		// TODO Auto-generated method stub
		if (mPopupWindow == null) {
			mPopupWindow = new PopupWindow(customView,
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			mPopupWindow.setFocusable(true);
			mPopupWindow.setOutsideTouchable(true);
			mPopupWindow.setBackgroundDrawable(new BitmapDrawable());
			mPopupWindow.setOnDismissListener(this);
			customView.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					LinearLayout layout = (LinearLayout) customView
							.findViewById(R.id.id_meet_select_Lay);
					int bottom = layout.getBottom();
					int left = layout.getLeft();
					int right = layout.getRight();
					int top = layout.getTop();
					int y = (int) event.getY();
					int x = (int) event.getX();
					if (event.getAction() == MotionEvent.ACTION_UP) {
						if (y < top || y > bottom || x < left || x > right) {
							mPopupWindow.dismiss();
						}

					}

					return true;
				}
			});
			
		}
		// 为button初始化背景
	   setTextButtonBG();

	}
	

	/**
	 * 
	 */
	private void setTextButtonBG() {
		// TODO Auto-generated method stub
//	  Utils.toast(NOW_MEET_TAG+"");
		mSelectArrow.setImageDrawable(getResources().getDrawable(R.drawable.top_arrow));
		switch (NOW_MEET_TAG) {
		
		case MEET_ALL:
			initAllTextButton();
			getAll.setBackground(getResources().getDrawable(R.drawable.meet_unselected_tv_shape));
			break;
		case MEET_MY:
			initAllTextButton();
			getMyMeets.setBackground(getResources().getDrawable(R.drawable.meet_unselected_tv_shape));
			break;
		case MEET_GOING:
			initAllTextButton();
			getGoing.setBackground(getResources().getDrawable(R.drawable.meet_unselected_tv_shape));
			break;
		case MEET_WILL:
			initAllTextButton();
			getWill.setBackground(getResources().getDrawable(R.drawable.meet_unselected_tv_shape));
			break;
		case MEET_END:
			initAllTextButton();
			getEnd.setBackground(getResources().getDrawable(R.drawable.meet_unselected_tv_shape));
			break;

		default:
			break;
		}
	}

	private void initAllTextButton() {
		// TODO Auto-generated method stub
		getAll.setBackground(getResources().getDrawable(R.drawable.meet_select_onclick));
		getMyMeets.setBackground(getResources().getDrawable(R.drawable.meet_select_onclick));
		getGoing.setBackground(getResources().getDrawable(R.drawable.meet_select_onclick));
		getWill.setBackground(getResources().getDrawable(R.drawable.meet_select_onclick));
		getEnd.setBackground(getResources().getDrawable(R.drawable.meet_select_onclick));		
	}

	@Override
	public void onDismiss() {
		// TODO Auto-generated method stub
//		Utils.toast("dismiss");
		mSelectArrow.setImageDrawable(getResources().getDrawable(R.drawable.bottom_arrow));
	}

}
