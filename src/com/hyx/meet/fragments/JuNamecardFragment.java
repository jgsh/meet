/**
 * 
 */
package com.hyx.meet.fragments;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avos.avoscloud.AVCloud;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.FunctionCallback;
import com.avos.avoscloud.Session;
import com.hyx.meet.JuHYXWebviewActivity;
import com.hyx.meet.R;
import com.hyx.meet.Application.App;
import com.hyx.meet.adapter.JuNameCardAdapter;
import com.hyx.meet.custome.EnLetterView;
import com.hyx.meet.custome.xlist.XListView;
import com.hyx.meet.custome.xlist.XListView.IXListViewListener;
import com.hyx.meet.db.DBHelper;
import com.hyx.meet.db.DBNamecard;
import com.hyx.meet.entity.NameCard;
import com.hyx.meet.service.ChatService;
import com.hyx.meet.service.listener.MsgListener;
import com.hyx.meet.service.listener.NamecardListener;
import com.hyx.meet.utils.CharacterParser;
import com.hyx.meet.utils.Logger;
import com.hyx.meet.utils.NetAsyncTask;
import com.hyx.meet.utils.PinyinComparator;
import com.hyx.meet.utils.Utils;

/**
 * 名片夹
 * 
 * @author wuwei
 *
 */
public class JuNamecardFragment extends Fragment implements OnClickListener,
		OnItemLongClickListener, IXListViewListener, OnItemClickListener,
		MsgListener, NamecardListener {
	public static Set<MsgListener> msgListeners = new HashSet<MsgListener>();

	private static XListView mNameCardListview;
	private View mView;
	private static JuNameCardAdapter mAdapter;
	private static List<NameCard> mData;
	private static List<NameCard> nameCardsStatic = new ArrayList<NameCard>();
	private static Context Context;
	// private ProgressDialog mProgressDialog;

	private RelativeLayout mSearchBtnLay;
	private ImageView mSearchIconMid;
	private ImageView mSearchIconLeft;
	private TextView mSearchText;
	private EditText mSearchEdit;

	private ImageView mSearchClear;

	private static LinearLayout nothingLay;

	private static AVUser mUser;

	private DBHelper dbHelper;
	// private Map<String, Object> userObject;

	private static int TAG = 0;
	private final static int NAMECARD_SHOW = 0;
	static CharacterParser characterParser;
	static PinyinComparator pinyinComparator;
	
	private EnLetterView rightLetter;
	private TextView dialog;

	public JuNamecardFragment() {
		// TODO Auto-generated constructor stub

	}

	public JuNamecardFragment(Context context) {
		// TODO Auto-generated constructor stub
		this.Context = context;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		mView = inflater.inflate(R.layout.fragment_namecardfragment, container,
				false);

		mUser = AVUser.getCurrentUser();
		dbHelper = new DBHelper(App.ctx, App.DB_NAME, App.DB_VER);
		characterParser = CharacterParser.getInstance();
		pinyinComparator = new PinyinComparator();

		initView();
		loadNameCardsFromDB();
		refreshData();

		return mView;
	}

	/**
	 * 搜索排序
	 * 
	 * @param filterStr
	 */
	private void filterData(String filterStr) {
		List<NameCard> filterDateList = new ArrayList<NameCard>();
		if (TextUtils.isEmpty(filterStr)) {
			filterDateList = nameCardsStatic;
		} else {
			filterDateList.clear();
			for (NameCard sortModel : nameCardsStatic) {
				String name = sortModel.getName();
				if (name != null) {
					if (name.indexOf(filterStr.toString()) != -1
							|| characterParser.getSelling(name).startsWith(
									filterStr.toString())) {
						filterDateList.add(sortModel);
						Logger.e("name" + characterParser.getSelling(name)
								+ " filter" + filterStr);
					}
				}
			}
		}
		Collections.sort(filterDateList, pinyinComparator);

		mData.clear();
		mData.addAll(filterDateList);
		mAdapter.notifyDataSetChanged();

	}

	/**
	 * 给数据分组排序（按首字母顺序）
	 * 
	 * @param datas
	 */
	private static void fillFriendsData(List<NameCard> datas) {
		mData.clear();
		int total = datas.size();
		for (int i = 0; i < total; i++) {
			NameCard nameCard = datas.get(i);
			NameCard sortNameCard = new NameCard();
			// User user = datas.get(i);
			// User sortUser = new User();
			sortNameCard.setName(nameCard.getName());
			sortNameCard.setJobTitle(nameCard.getJobTitle());
			sortNameCard.setCompanyName(nameCard.getCompanyName());
			sortNameCard.setUserId(nameCard.getUserId());
			sortNameCard.setNamecardId(nameCard.getNamecardId());
			sortNameCard.setFirstMeeting(nameCard.getFirstMeeting());
			sortNameCard.setAvator(nameCard.getAvator());
			sortNameCard.setMobilePhone(nameCard.getMobilePhone());
			sortNameCard.setTelNumber(nameCard.getTelNumber());
			sortNameCard.setFax(nameCard.getFax());
			// sortUser.setAvatar(user.getAvatar());
			// sortUser.setUsername(user.getUsername());
			// sortUser.setObjectId(user.getObjectId());
			// //sortUser.(user.getContacts());
			String username = sortNameCard.getName();
			if (username != null) {
				String pinyin = characterParser.getSelling(sortNameCard
						.getName());
				String sortString = pinyin.substring(0, 1).toUpperCase();
				if (sortString.matches("[A-Z]")) {
					sortNameCard.setSortLetters(sortString.toUpperCase());
				} else {
					sortNameCard.setSortLetters("#");
				}
			} else {
				sortNameCard.setSortLetters("#");
			}
			mData.add(sortNameCard);
		}
		Collections.sort(mData, pinyinComparator);
		nameCardsStatic.clear();
		nameCardsStatic.addAll(mData);

	}

	/**
	 * 从数据库加载数据
	 */
	private void loadNameCardsFromDB() {
		// TODO Auto-generated method stub
		new getDataTask(App.ctx, false).execute();

	}

	/**
	 * 加载数据异步任务
	 * 
	 * @author ww
	 *
	 */
	class getDataTask extends NetAsyncTask {

		List<NameCard> nameCards;

		getDataTask(Context context, boolean showDialog) {
			// TODO Auto-generated constructor stub
			super(context, showDialog);
		}

		@Override
		protected void doInBack() throws Exception {
			// TODO Auto-generated method stub

			nameCards = DBNamecard.getNameCards(dbHelper);

		}

		@Override
		protected void onPost(Exception e) {
			// TODO Auto-generated method stub
			// mData.clear();
			if (e == null) {

				fillFriendsData(nameCards);
				// mData.addAll(nameCards);
				// Utils.toast("数据库"+nameCards.size()+"条记录");
				mAdapter.notifyDataSetChanged();
				if (nameCards.size() <= 0) {
					nothingLay.setVisibility(View.VISIBLE);
				}
			} else {
				Utils.toast("获取数据失败");
			}

		}

	}

	/**
	 * 初始化控件
	 */
	private void initView() {
		// TODO Auto-generated method stub

		mSearchBtnLay = (RelativeLayout) mView
				.findViewById(R.id.id_namecard_search_layBtn);
		mSearchEdit = (EditText) mView
				.findViewById(R.id.id_namecard_search_edit);
		mSearchText = (TextView) mView.findViewById(R.id.id_namecard_search_tv);
		mSearchIconLeft = (ImageView) mView
				.findViewById(R.id.id_namecard_search_icon_left);
		mSearchIconMid = (ImageView) mView
				.findViewById(R.id.id_namecard_search_icon_mid);
		mSearchClear = (ImageView) mView
				.findViewById(R.id.id_namecard_search_clear);

		// 监听搜索事件
		mSearchEdit.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				filterData(s.toString());

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		nothingLay = (LinearLayout) mView.findViewById(R.id.id_nothing);
		mNameCardListview = (XListView) mView.findViewById(R.id.namecardlv);
		mNameCardListview.setPullLoadEnable(false);
		mNameCardListview.setPullRefreshEnable(true);
		mNameCardListview.setXListViewListener(this);

		mNameCardListview.setOnItemLongClickListener(this);
		mNameCardListview.setOnItemClickListener(this);
		// mNameCardListview.setOnClickListener(this);
		mData = new ArrayList<NameCard>();
		// 填充适配器
		mAdapter = new JuNameCardAdapter(Context, mData);
		mNameCardListview.setAdapter(mAdapter);

		mSearchBtnLay.setOnClickListener(this);
		mSearchClear.setOnClickListener(this);
		
		initRightLetterView();

	}

	/**
	 *初始化右边导航
	 */
	private void initRightLetterView() {
		// TODO Auto-generated method stub
		rightLetter = (EnLetterView) mView.findViewById(R.id.id_namecard_right_letter);
	    dialog = (TextView) mView.findViewById(R.id.id_namecard_dialog);
	    rightLetter.setTextView(dialog);
	    rightLetter.setOnTouchingLetterChangedListener(new LetterListViewListener());
	}
	
  /**
   * 右边导航监听器
   * @author ww
   *
   */
	 private class LetterListViewListener implements
     EnLetterView.OnTouchingLetterChangedListener {

   @Override
   public void onTouchingLetterChanged(String s) {
     int position = mAdapter.getPositionForSection(s.charAt(0));
     if (position != -1) {
       mNameCardListview.setSelection(position);
     }
   }
 }

	/**
	 * 显示搜索框
	 */
	private void showSearchEdit() {
		mSearchIconMid.setVisibility(View.GONE);
		mSearchText.setVisibility(View.GONE);
		mSearchEdit.setVisibility(View.VISIBLE);
		mSearchIconLeft.setVisibility(View.VISIBLE);
		mSearchEdit.requestFocus();
		mSearchEdit.requestFocusFromTouch();
		mSearchEdit.setText("");

		mSearchClear.setVisibility(View.VISIBLE);
		showSoftInputView();
	}

	/**
	 * 隐藏搜索框
	 */
	private void hideSearchEdit() {
		// mSearchIconMid.setVisibility(View.VISIBLE);
		// mSearchText.setVisibility(View.VISIBLE);
		//
		// mSearchEdit.setVisibility(View.GONE);
		// mSearchIconLeft.setVisibility(View.GONE);
		// mSearchClear.setVisibility(View.GONE);
		mSearchEdit.clearFocus();

		hideSoftInputView();

	}

	void showSoftInputView() {
		InputMethodManager imm = (InputMethodManager) Context
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(mSearchEdit, InputMethodManager.SHOW_FORCED);

	}

	void hideSoftInputView() {
		InputMethodManager imm = (InputMethodManager) Context
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(mSearchEdit.getWindowToken(), 0);
		// Utils.hideSoftInputView(this);
	}

	/**
	 * listview点击事件
	 */
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		// Toast.makeText(Context, "第"+position+"个item click", 0).show();
		if (mSearchEdit.getVisibility() == View.VISIBLE) {
			// 隐藏搜索框
			hideSearchEdit();

		}
		// else {

		int is_open = 0;
		// 设置被点击ITEM的显示或隐藏
		LinearLayout optView = (LinearLayout) view
				.findViewById(R.id.id_namecard_opt_lay);
		if (optView.getVisibility() == View.VISIBLE) {
			is_open = 1;
		}

		// 初始化所有隐藏视图
		for (int i = 0; i < parent.getChildCount(); i++) {
			// if (i != position) {
			LinearLayout optViewAll = (LinearLayout) parent.getChildAt(i)
					.findViewById(R.id.id_namecard_opt_lay);
			// System.out.println(optViewAll);
			if (optViewAll != null) {
				optViewAll.setVisibility(View.GONE);
				// }
			}
		}
		// System.out.println("count->" + mAdapter.getCount() +
		// "countView->"
		// + mNameCardListview.getChildCount() + "position->" + position);

		if (is_open == 1) {
			optView.setVisibility(View.GONE);
			// optView.setLayoutAnimation(null);

		} else {
			Animation animation = AnimationUtils.loadAnimation(Context,
					R.anim.slide_top);
			// 得到一个LayoutAnimationController对象；
			LayoutAnimationController lac = new LayoutAnimationController(
					animation);
			// 设置控件显示的顺序；
			lac.setOrder(LayoutAnimationController.ORDER_REVERSE);
			// 设置控件显示间隔时间；
			lac.setDelay(1);
			// 为ListView设置LayoutAnimationController属性；
			optView.setLayoutAnimation(lac);
			optView.setVisibility(View.VISIBLE);

		}

	}

	/**
	 * listview长按事件
	 */
	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view,
			int position, long id) {
		// TODO Auto-generated method stub
		// String userObjectId = (String) mData.get(position - 1).getUserId();
		// Toast.makeText(Context,
		// "position->"+position+"--objectid->"+userObjectId, 10).show();
		// delete_nameCard(userObjectId, position);
		return false;
	}

	/**
	 * 初始化listView数据
	 */
	public static void refreshData() {
		// TODO Auto-generated method stub

		Map<String, Object> parameters = new HashMap<String, Object>();
		// 改为userId
		parameters.put("userId", mUser.getObjectId());

		AVCloud.callFunctionInBackground("getTwowayFriends", parameters,
				new FunctionCallback<Object>() {

					@Override
					public void done(Object data, AVException e) {
						// TODO Auto-generated method stub
						if (e == null) {

							// @SuppressWarnings("unchecked")
//							 Logger.d("namecards"+data.toString());
							@SuppressWarnings("unchecked")
							Map<String, Object> map = (Map<String, Object>) data;
							if ((Integer)map.get("errorCode") == 0) {
								// System.out.println(data.getClass().toString());
								@SuppressWarnings("unchecked")
								List<Map<String, Object>> userlist = null;
								userlist = (List<Map<String, Object>>) map
										.get("data");

								if (userlist.size() > 0) {
									// 从网上拉取数据并刷新数据库
									initFromNetWork(userlist);
									nothingLay.setVisibility(View.GONE);
								} else {
									// mProgressDialog.dismiss();
									mNameCardListview.stopRefresh();
									mData.clear();
									DBNamecard.deleteAll();
									DBNamecard.insertNameCards(mData);
									mAdapter.notifyDataSetChanged();
									Toast.makeText(App.ctx, "未获取到名片", 0).show();
									nothingLay.setVisibility(View.VISIBLE);
								}
							}
						
						} else {
							// mProgressDialog.dismiss();
							mNameCardListview.stopRefresh();
							Toast.makeText(App.ctx, "请检查网络设置", 0).show();

							// System.out.println("获取双向好友错误-》"+e.toString());
						}

					}
				});

	}

	private static void initFromNetWork(final List<Map<String, Object>> userlist) {
		// TODO Auto-generated method stub
		new NetAsyncTask(App.ctx, false) {
			List<NameCard> newNameCards = new ArrayList<NameCard>();

			@Override
			protected void onPost(Exception e) {
				if (e == null) {
					DBNamecard.deleteAll();
					int result = DBNamecard.insertNameCards(newNameCards);
					// Toast.makeText(Context, mData.toString(),
					// Toast.LENGTH_LONG).show();
					// System.out.println("namecards->"+mData.toString());

					fillFriendsData(newNameCards);
					mAdapter.notifyDataSetChanged();

				} else {
					Toast.makeText(App.ctx, "请检查网络", Toast.LENGTH_LONG).show();
				}
				// mProgressDialog.dismiss();
				mNameCardListview.stopRefresh();
				// TODO Auto-generated method stub
			}

			@Override
			protected void doInBack() throws Exception {
				// TODO Auto-generated method stub
				// mData.clear();
				newNameCards.clear();
				for (int i = 0; i < userlist.size(); i++) {
					final Map<String, Object> userInfo = userlist.get(i);
					AVObject nameCardIdOb = (AVObject) userInfo
							.get("nameCardId");
					String nameCardObjectId = nameCardIdOb.getObjectId();

					AVQuery<AVObject> query = new AVQuery<AVObject>(
							"Ju_NameCard");
					AVObject nameCardInfo = query.get(nameCardObjectId);
					if (nameCardInfo != null) {
						NameCard nameCard = new NameCard();
						nameCard.setUserId((String) userInfo.get("objectId"));
						nameCard.setNamecardId(nameCardInfo.getObjectId());
						nameCard.setName((String) userInfo.get("realName"));
						nameCard.setJobTitle(nameCardInfo.getString("jobTitle"));
						nameCard.setCompanyName(nameCardInfo
								.getString("companyName"));
						nameCard.setAvator(nameCardInfo.getString("avator"));
						nameCard.setIndustry(nameCardInfo.getString("industry"));
						nameCard.setAddress(nameCardInfo.getString("address"));
						nameCard.setMobilePhone((String) userInfo
								.get("username"));
						nameCard.setTelNumber((String) userInfo
								.get("username"));
						nameCard.setEmail(nameCardInfo.getString("email"));
						nameCard.setWeibo(nameCardInfo.getString("weibo"));
						nameCard.setWechat(nameCardInfo.getString("wechat"));
						nameCard.setQq(nameCardInfo.getString("qq"));
						nameCard.setHobby(nameCardInfo.getString("hobby"));
						nameCard.setBusinessMotto(nameCardInfo
								.getString("businessMotto"));
						nameCard.setFax(nameCardInfo.getString("fax"));
						nameCard.setFirstMeeting(userInfo
								.get("firstMeeting").toString());
						nameCard.setSex(nameCardInfo.getString("selectSex"));
						nameCard.setHistory1(nameCardInfo.getString("history1"));
						nameCard.setHistory2(nameCardInfo.getString("history2"));
						nameCard.setHistory3(nameCardInfo.getString("history3"));
//						Logger.d("[Namecard]->"+nameCard.toString());

						newNameCards.add(nameCard);
						//
					} else {
						// Toast.makeText(App.ctx, "未获取到名片", 0).show();
						nameCardsStatic.clear();
					}
				}

			}
		}.execute();
	}

	/**
	 * 点击事件
	 */
	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
//		case R.id.id_namecard_addFriendsBtn_Lay:
//			Intent addIntent = new Intent(Context, JuNameCardAddActivity.class);
//			startActivity(addIntent);
//			((Activity) Context).overridePendingTransition(R.anim.zoomin,
//					R.anim.zoomout_static);
//			break;
		case R.id.id_namecard_search_layBtn:
			if (mSearchEdit.getVisibility() == View.GONE) {
				// 显示搜索框
				showSearchEdit();
			}

			break;
		case R.id.id_namecard_search_clear:
			// 清除搜索框
			mSearchEdit.setText("");
			break;
		// case R.id.namecardlv:
		// hideSearchEdit();
		// break;
		// Intent searchIntent = new Intent(Context, JuNameCardSearch.class);
		// startActivity(searchIntent);
		// ((Activity) Context).overridePendingTransition(R.anim.zoomin,
		// R.anim.zoomout_static);
		default:
			break;
		}

	}

	/**
	 * 确认删除好友
	 */
	public static void delete_nameCard(final String objectId, final int position) {
		// TODO Auto-generated method stub
		if (position == -1) {
			delete(objectId, position);
		} else {

			AlertDialog.Builder builder = new AlertDialog.Builder(Context);
			// builder.setMessage("确认删除名片？");
			builder.setPositiveButton("删除名片",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.dismiss();
							delete(objectId, position);

						}
					});
			// builder.setNegativeButton("取消", new
			// DialogInterface.OnClickListener()
			// {
			//
			// @Override
			// public void onClick(DialogInterface dialog, int which) {
			// // TODO Auto-generated method stub
			// dialog.dismiss();
			//
			// }
			// });
			builder.create().show();
		}

	}

	/**
	 * 删除名片
	 * 
	 * @param <T>
	 * 
	 * @param objectId
	 * @param position
	 */
	private static void delete(final String objectId, final int position) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("touserId", objectId);
		parameters.put("userId", ChatService.getSelfId());
		AVCloud.callFunctionInBackground("deleteFriend", parameters,
				new FunctionCallback<Object>() {

					@Override
					public void done(Object result, AVException e) {
						// TODO Auto-generated method stub
						// Toast.makeText(Context, result.toString(), 0).show();
						if (e == null) {
							// unwatch删除对象
//							Session session = ChatService.getSession();
//							if (session.isWatching(objectId)) {
//								ChatService.unwatchById(objectId);
//								// Toast.makeText(App.ctx,
//								// "watched", 0).show();
//							} else {
//								// ChatService.watchById(objectId);
//								// Toast.makeText(App.ctx, "unwatched",
//								// 0).show();
//							}
							ChatService.delete_conversation(objectId);
							// 刷新会话
							for (MsgListener msgListener : msgListeners) {
								if (msgListener.onMessageUpdate(objectId)) {
//									break;
								}
							}

							// Toast.makeText(Context,
							// result.get("errorMessage").toString(), 0).show();
							if (position == -1) {
								refreshData();
							} else {
								mData.remove(position - 1);
								mAdapter.notifyDataSetChanged();
								if (mData.size() <= 0) {
									nothingLay.setVisibility(View.VISIBLE);
								}
							}

						} else {
							Toast.makeText(App.ctx, "请检查网络设置", 0).show();
							Logger.e(e.getMessage());

						}

					}
				});

	}

	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		// mProgressDialog.show();
		// 从网络上刷新信息
		hideSearchEdit();
		refreshData();
		// 后面应该改为调用此函数网络刷新，然后刷新数据库，再调用从数据库加载函数刷新界面

	}

	@Override
	public void onLoadMore() {
		// TODO Auto-generated method stub

	}

	public static void addMsgListener(MsgListener listener) {
		msgListeners.add(listener);
	}

	public static void removeMsgListener(MsgListener listener) {
		msgListeners.remove(listener);
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		JuMessageFragment.addMsgListener(this);
		JuHYXWebviewActivity.addNamecardListener(this);
	}
	
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		JuMessageFragment.removeMsgListener(this);
		JuHYXWebviewActivity.removeNamecardListener(this);
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		JuMessageFragment.removeMsgListener(this);
		JuHYXWebviewActivity.removeNamecardListener(this);
	}

	@Override
	public boolean onMessageUpdate(String otherId) {
		// TODO Auto-generated method stub
		hideSearchEdit();
		refreshData();
		// 刷新会话
		// for (MsgListener msgListener : msgListeners) {
		// if (msgListener.onMessageUpdate("0")) {
		// break;
		// }
		// }
		return true;
	}

	@Override
	public boolean onNameCardUpdate() {
		// TODO Auto-generated method stub
		hideSearchEdit();
		refreshData();
		return false;
	}

}
