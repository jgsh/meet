/**
 * 
 */
package com.hyx.meet.fragments;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.R.integer;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Space;
import android.widget.TextView;

import com.hyx.meet.JuHYXWebviewActivity;
import com.hyx.meet.R;
import com.hyx.meet.Application.App;
import com.hyx.meet.adapter.RecentMessageAdapter;
import com.hyx.meet.chat.JuChatActivity;
import com.hyx.meet.custome.xlist.XListView;
import com.hyx.meet.custome.xlist.XListView.IXListViewListener;
import com.hyx.meet.db.DBHelper;
import com.hyx.meet.db.DBMeet;
import com.hyx.meet.db.DBMsg;
import com.hyx.meet.entity.Conversation;
import com.hyx.meet.entity.MeetingDomain;
import com.hyx.meet.meet.JuGuidanceActivity;
import com.hyx.meet.qr.MipcaActivityCapture;
import com.hyx.meet.service.ChatService;
import com.hyx.meet.service.NotifyService;
import com.hyx.meet.service.UserService;
import com.hyx.meet.service.listener.MsgListener;
import com.hyx.meet.service.reciever.JuMSGHandlerV2;
import com.hyx.meet.service.reciever.JuMSGReciever;
import com.hyx.meet.service.reciever.JuPushReciever;
import com.hyx.meet.utils.JuUrl;
import com.hyx.meet.utils.Logger;
import com.hyx.meet.utils.NetAsyncTask;
import com.hyx.meet.utils.Utils;
import com.hyx.meet.entity.RoomType;

/**
 * 消息通知模块
 * 
 * @author ww
 *
 */
public class JuMessageFragment extends Fragment implements OnClickListener,
		IXListViewListener, OnItemClickListener, OnItemLongClickListener,
		MsgListener {

	public static Set<MsgListener> msgListeners = new HashSet<MsgListener>();

	private View mView;
	private LinearLayout mFriendsRequestBtn;
	private LinearLayout mSystemBtn;
	// private LinearLayout mMeetBaomingBtn;
	// private LinearLayout mMeetTixingBtn;
	// private LinearLayout mLyReplyBtn;
	private static Context Context;

	// private Button mQRScannerBtn;
//	private LinearLayout mQRScannerBtnLay;
	// private Button mMessageSearchBtn;
//	private RelativeLayout mMessageSearchBtnLay;

	private XListView mXListView;
	private RecentMessageAdapter adapter;

	private View mView2;
	private LinearLayout mHeaderView2;

	private TextView mMeet_notify_unread;
	private TextView mFriend_req_unread;

	private NotifyService notifyService;

	// private SharedPreferences system_notify_sp;

	// private final static String NEW_SYS_MESSAGE = "sys_message";// 系统通知信息
	private final static String MEET_NOTIFY_NUM = "meet_notify";// 小秘书新通知数量
	private final static String NEW_FRIEND_REQ_NUM = "new_friend_req";// 好友请求通知数量
	private final static String IS_LOGOUT = "logout";
	public boolean hidden = true;

	public JuMessageFragment() {
		// TODO Auto-generated constructor stu
	}

	public JuMessageFragment(Context context) {
		// TODO Auto-generated constructor stub
		this.Context = context;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		mView = inflater.inflate(R.layout.fragment_messagefragment, container,
				false);
		mView2 = inflater.inflate(R.layout.fragment_messagefragment_headerview,
				null);

		notifyService = new NotifyService(ChatService.getSelfId());
		// system_notify_sp = App.ctx.getSharedPreferences(NEW_SYS_MESSAGE,
		// App.ctx.MODE_PRIVATE);

		initView();
		onRefresh();
		return mView;

	}

	/**
	 * 初始化界面
	 */
	private void initView() {
		// TODO Auto-generated method stub
		mFriendsRequestBtn = (LinearLayout) mView2
				.findViewById(R.id.id_message_friends_requestBtn);
		mSystemBtn = (LinearLayout) mView2
				.findViewById(R.id.id_message_systemBtn);
		mMeet_notify_unread = (TextView) mView2
				.findViewById(R.id.id_meet_notify_unread);
		mFriend_req_unread = (TextView) mView2
				.findViewById(R.id.id_friend_req_unread);

//		mQRScannerBtnLay = (LinearLayout) mView
//				.findViewById(R.id.id_message_qrBtn_lay);
//		mMessageSearchBtnLay = (RelativeLayout) mView
//				.findViewById(R.id.id_message_searchBtn_lay);
		

		// mHeaderView2 = (LinearLayout)
		// mView2.findViewById(R.id.id_message_header_lay);

		mXListView = (XListView) mView.findViewById(R.id.id_message_convList);
		mXListView.setPullLoadEnable(false);
		mXListView.setPullRefreshEnable(false);
		mXListView.setXListViewListener(this);
		// mXListView.removeHeaderView(v)
		mXListView.addHeaderView(mView2);
		adapter = new RecentMessageAdapter(App.ctx);
		mXListView.setAdapter(adapter);
		mXListView.setOnItemClickListener(this);
		mXListView.setOnItemLongClickListener(this);

		// 添加点击事件
		mFriendsRequestBtn.setOnClickListener(this);
		// mLyReplyBtn.setOnClickListener(this);
		// mMeetBaomingBtn.setOnClickListener(this);
		// mMeetTixingBtn.setOnClickListener(this);
		mSystemBtn.setOnClickListener(this);

		// mQRScannerBtn.setOnClickListener(this);
//		mQRScannerBtnLay.setOnClickListener(this);
//		mMessageSearchBtnLay.setOnClickListener(this);

	}

	/**
	 * 获取点击事件
	 */
	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.id_message_friends_requestBtn:
			// 清除新好友通知数量
			clearNewNotify(NEW_FRIEND_REQ_NUM);

			Intent friendsRequest = new Intent(Context, JuHYXWebviewActivity.class);
			String url = "";
			if (JuUrl.MODE_STYLE.equals("prod")) {
				url = String.format(JuUrl.USER_REQUEST_PROD,
						ChatService.getSelfId());
			} else {
				url = String.format(JuUrl.USER_REQUEST_DEV,
						ChatService.getSelfId());
			}
			friendsRequest.putExtra("url", url);
			startActivity(friendsRequest);
			((Activity) Context).overridePendingTransition(R.anim.zoomin,
					R.anim.zoomout_static);
			// Toast.makeText(Context, "好友请求", 0).show();
			break;
//		case R.id.id_message_qrBtn_lay:
//			Intent qrScanIntent = new Intent(Context,
//					MipcaActivityCapture.class);
//			startActivity(qrScanIntent);
//			((Activity) Context).overridePendingTransition(R.anim.zoomin,
//					R.anim.zoomout_static);
//			break;
		case R.id.id_message_systemBtn:
			// 清除新会议通知数量
			clearNewNotify(MEET_NOTIFY_NUM);

			Intent intent = new Intent(Context, JuHYXWebviewActivity.class);
			String urlFormat = "";
			if (JuUrl.MODE_STYLE.equals("prod")) {
				urlFormat = String.format(JuUrl.SYSTEM_PROD,
						ChatService.getSelfId());
			} else {
				urlFormat = String.format(JuUrl.SYSTEM_DEV,
						ChatService.getSelfId());
			}

			intent.putExtra("url", urlFormat);
			startActivity(intent);
			break;
		// case R.id.id_message_ly_replyBtn:
		// break;
//		case R.id.id_message_searchBtn_lay:
			// Intent intent2 = new Intent(Context,JuGuidance.class);
			// startActivity(intent2);
//			break;
		default:
			break;
		}

	}

	/**
	 * 清除新消息通知数量
	 */
	private void clearNewNotify(String tag) {
		// TODO Auto-generated method stub
		// Editor editor = system_notify_sp.edit();
		// editor.putInt(tag, 0);
		// editor.commit();
		if (tag.equals(MEET_NOTIFY_NUM)) {
			if (mMeet_notify_unread.getVisibility() == View.VISIBLE) {
				mMeet_notify_unread.setVisibility(View.GONE);
			}
			notifyService.setMeetNotifyNum(0);

		} else {
			if (mFriend_req_unread.getVisibility() == View.VISIBLE) {
				mFriend_req_unread.setVisibility(View.GONE);
			}
			notifyService.setFriendNotifyNum(0);

		}
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		// TODO Auto-generated method stub
		this.hidden = hidden;
		// Utils.toast(""+hidden);
		if (!hidden) {
			onRefresh();
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		hidden = false;
		// Utils.toast(""+hidden);
		if (!hidden) {
			onRefresh();
			recieveNotify();
		}

		// GroupMsgReceiver.addMsgListener(this);
//		JuMSGReciever.addMsgListener(this);
		JuMSGHandlerV2.addMsgListener(this);
		JuPushReciever.addMsgListener(this);
		JuNamecardFragment.addMsgListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		 JuNamecardFragment.removeMsgListener(this);
//		 JuMSGReciever.removeMsgListener(this);
		 JuMSGHandlerV2.removeMsgListener(this);
		 JuPushReciever.removeMsgListener(this);
		// GroupMsgReceiver.removeMsgListener(this);
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
//		JuMSGReciever.removeMsgListener(this);
		JuMSGHandlerV2.removeMsgListener(this);
		JuPushReciever.removeMsgListener(this);
		JuNamecardFragment.removeMsgListener(this);
	}

	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		if (!hidden) {
//			updateMessageUnread();
			new GetDataTask(App.ctx, false).execute();
		}
	}

//	/**
//	 * 更新消息标记
//	 */
//	private void updateMessageUnread() {
//		// TODO Auto-generated method stub
//		DBHelper dbHelper = new DBHelper(App.ctx, App.DB_NAME, App.DB_VER);
//		SQLiteDatabase db = dbHelper.getReadableDatabase();
//		
//		int meet_notify_num = notifyService.getMeetNotifyNum();
//		int friend_req_num = notifyService.getFriendNotifyNum();
//		int chat_unread_num = DBMsg.getUnreadCountAll(db);
////		更新消息标记
//		JuMainActivity.updateMessageUnread(meet_notify_num+friend_req_num+chat_unread_num);
//		
//		
//	}

	@Override
	public void onLoadMore() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		Conversation recent = (Conversation) parent.getAdapter().getItem(
				position);
		//单聊
		if (recent.getMsg().getRoomType() == RoomType.Single) {
			if (recent.getToUser() != null) {
				Intent intent = new Intent(getActivity(), JuChatActivity.class);
				intent.putExtra("chatUserId", recent.getToUser().getUserId());
				intent.putExtra("toUserName", recent.getToUser().getName());
				startActivity(intent);
				((Activity) Context).overridePendingTransition(R.anim.zoomin,
						R.anim.zoomout_static);
			}
		} 
		//公众号
		else if (recent.getMsg().getRoomType() == RoomType.NOTIFY) {
			// Utils.toast("会议通知！");
			Intent intentNotify = new Intent(getActivity(),
					JuChatActivity.class);
			MeetingDomain meetingDomain = DBMeet.getMeetingById(recent.getMsg().getFromPeerId());

			intentNotify.putExtra("chatUserId", recent.getMsg().getOtherId());
			intentNotify.putExtra("toUserName", meetingDomain.getName());
			intentNotify.putExtra("roomType", RoomType.NOTIFY.getValue());
			startActivity(intentNotify);
			((Activity) Context).overridePendingTransition(R.anim.zoomin,
					R.anim.zoomout_static);

		}

	}

	class GetDataTask extends NetAsyncTask {
		List<Conversation> conversations;

		GetDataTask(Context cxt, boolean openDialog) {
			super(cxt, openDialog);
		}

		@Override
		protected void doInBack() throws Exception {
			//从数据库中获取
			conversations = ChatService.getConversationsAndCache();
		}

		@Override
		protected void onPost(Exception e) {
			// mXListView.stopRefresh();
			if (e != null) {
				// adapter.setDatas(conversations);
				// adapter.notifyDataSetChanged();
				Utils.toast(App.ctx, "请检查网络" + e.getMessage());
			} else {
				adapter.setDatas(conversations);
				adapter.notifyDataSetChanged();
			}
		}
	}

	// 新消息/推送通知到达更新
	@Override
	public boolean onMessageUpdate(String otherId) {
		// TODO Auto-generated method stub
		// 刷新系统有新消息(小秘书/新的会友)
		if (!hidden) {

			recieveNotify();
			// 刷新会话列表
			onRefresh();
		}

		return true;
	}

	/**
	 * 处理通知
	 */
	private void recieveNotify() {
		// TODO Auto-generated method stub
		// notifyService = new NotifyService(ChatService.getSelfId());
		int meet_notify_num = notifyService.getMeetNotifyNum();
		// system_notify_sp.getInt(MEET_NOTIFY_NUM, 0);
		int friend_req_num = notifyService.getFriendNotifyNum();
		// system_notify_sp.getInt(NEW_FRIEND_REQ_NUM, 0);

		if (meet_notify_num > 0 && meet_notify_num <100) {
			// 提示小秘书有新消息,刷新UI
			Logger.e("小秘书通知-》" + meet_notify_num + "条");
			if (mMeet_notify_unread.getVisibility() == View.GONE) {
				mMeet_notify_unread.setVisibility(View.VISIBLE);
			}
			mMeet_notify_unread.setText("" + meet_notify_num);
		}else if (meet_notify_num >= 100) {
			Logger.e("小秘书通知-》" + meet_notify_num + "条");
			if (mMeet_notify_unread.getVisibility() == View.GONE) {
				mMeet_notify_unread.setVisibility(View.VISIBLE);
			}
			mMeet_notify_unread.setText("N+");
		}

		if (friend_req_num > 0) {
			// 提示有新的好友请求,刷新UI
			Logger.e("新好友通知-》" + friend_req_num + "条");
			if (mFriend_req_unread.getVisibility() == View.GONE) {
				mFriend_req_unread.setVisibility(View.VISIBLE);
			}
			mFriend_req_unread.setText("" + friend_req_num);
		}else if (friend_req_num >= 100) {
			Logger.e("新好友通知-》" + friend_req_num + "条");
			if (mFriend_req_unread.getVisibility() == View.GONE) {
				mFriend_req_unread.setVisibility(View.VISIBLE);
			}
			mFriend_req_unread.setText("N+");
		}
		// 刷新名片列表
		for (MsgListener msgListener : msgListeners) {
			if (msgListener.onMessageUpdate("0")) {
//				break;
			}
		}

	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view,
			int position, long id) {
		// TODO Auto-generated method stub
		Conversation recent = (Conversation) parent.getAdapter().getItem(
				position);
		String convid = recent.getMsg().getConvid();
		deleteConversation(convid);

		return true;
	}

	private void deleteConversation(final String convid) {
		// TODO Auto-generated method stub
		AlertDialog.Builder builder = new AlertDialog.Builder(Context);
		// builder.setMessage("删除对话");
		builder.setPositiveButton("删除对话",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
						DBMsg.deleteConversation(convid);
						new GetDataTask(Context, false).execute();

					}
				});
		builder.create().show();
	}

	// 注册消息监听器
	public static void addMsgListener(MsgListener listener) {
		msgListeners.add(listener);
	}

	// remove消息监听器
	public static void removeMsgListener(MsgListener listener) {
		msgListeners.remove(listener);
	}

}
