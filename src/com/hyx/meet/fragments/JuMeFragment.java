/**
 * 
 */
package com.hyx.meet.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.GetCallback;
import com.hyx.meet.JuHYXWebviewActivity;
import com.hyx.meet.R;
import com.hyx.meet.namecard.JuMyNameCardShowActivity;
import com.hyx.meet.service.ChatService;
import com.hyx.meet.service.UserService;
import com.hyx.meet.setting.JuQRShowPopuActivity;
import com.hyx.meet.setting.JuSettingsActivity;
import com.hyx.meet.utils.JuUrl;
import com.hyx.meet.utils.PhotoUtil;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * 我 界面
 * 
 * @author wuwei
 * 
 */
public class JuMeFragment extends Fragment implements OnClickListener {

	private View mView;// 主布局
	private Context Context;// 上下文
	// private LinearLayout mToMyNameCardLay;
	private LinearLayout mToMyNewFeatureLay;
	private LinearLayout mToMyMeetingLay;
	private LinearLayout mToMySettingsLay;
	private ImageView mShowQrImgBtn;

	private LinearLayout mTitleLay;
	private static ImageView mHead;
	private TextView mName;
	private TextView mHome;
	private TextView mJob;
	private ImageView mMan;
	private ImageView mWoman;

	private static AVUser mCurrentUser;

	public JuMeFragment() {
	}

	public JuMeFragment(Context context) {
		// TODO Auto-generated constructor stub
		this.Context = context;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		mView = inflater
				.inflate(R.layout.fragment_mefragment, container, false);
		mCurrentUser = AVUser.getCurrentUser();

		initView();

		initData();

		return mView;
	}

	public static void refreshHead(String avator) {
		ImageLoader imageLoader = UserService.imageLoader;
		imageLoader.displayImage(avator, mHead, PhotoUtil.avatarImageOptions);

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// refreshHead(mCurrentUser.getString("avator"));
		initData();

	}

	public void initData() {
		// TODO Auto-generated method stub
		ImageLoader imageLoader = UserService.imageLoader;
		imageLoader.displayImage(mCurrentUser.getString("avator"), mHead,
				PhotoUtil.avatarImageOptions);

		mName.setText(mCurrentUser.getString("realName"));
		mCurrentUser.fetchInBackground("nameCardId",
				new GetCallback<AVObject>() {

					@Override
					public void done(AVObject userFetch, AVException e) {
						// TODO Auto-generated method stub
						if (e == null) {
							AVObject namecardAvObject = userFetch
									.getAVObject("nameCardId");
							// 公司名称
							mHome.setText(namecardAvObject
									.getString("companyName"));
							// 职位
							mJob.setText(namecardAvObject.getString("jobTitle"));
							// 性别
							if (namecardAvObject.getString("selectSex").equals(
									"0")) {
								// 男
								mMan.setVisibility(View.VISIBLE);
								mWoman.setVisibility(View.GONE);
							} else {
								// 女
								mWoman.setVisibility(View.VISIBLE);
								mMan.setVisibility(View.GONE);
							}
						}

					}
				});

	}

	private void initView() {
		// TODO Auto-generated method stub
		mToMyMeetingLay = (LinearLayout) mView
				.findViewById(R.id.id_me_to_my_createdMeet_Lay);
		mToMyNewFeatureLay = (LinearLayout) mView
				.findViewById(R.id.id_me_to_my_newFeature_Lay);
		mToMySettingsLay = (LinearLayout) mView
				.findViewById(R.id.id_me_to_settings_Lay);

		mTitleLay = (LinearLayout) mView
				.findViewById(R.id.id_me_title_linearLayout);
		mHead = (ImageView) mView.findViewById(R.id.id_me_head_icon);
		mName = (TextView) mView.findViewById(R.id.id_me_name);
		mHome = (TextView) mView.findViewById(R.id.id_me_home);
		mJob = (TextView) mView.findViewById(R.id.id_me_job);
		mMan = (ImageView) mView.findViewById(R.id.id_me_sex_man);
		mWoman = (ImageView) mView.findViewById(R.id.id_me_sex_woman);

		mShowQrImgBtn = (ImageView) mView.findViewById(R.id.id_me_qr_imgBtn);

		mTitleLay.setOnClickListener(this);
		mToMyMeetingLay.setOnClickListener(this);
		mToMySettingsLay.setOnClickListener(this);
		mToMyNewFeatureLay.setOnClickListener(this);
		mShowQrImgBtn.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.id_me_title_linearLayout:
			Intent me_titile_to_namecard = new Intent(Context,
					JuMyNameCardShowActivity.class);
			startActivity(me_titile_to_namecard);
			((Activity) Context).overridePendingTransition(R.anim.zoomin,
					R.anim.zoomout_static);
			break;
		case R.id.id_me_to_my_createdMeet_Lay:
			Intent me_to_meet = new Intent(Context, JuHYXWebviewActivity.class);
			String url = "";
			if (JuUrl.MODE_STYLE.equals("prod")) {
				url = String.format(JuUrl.MEETING_MANAGELIST_PROD,
						ChatService.getSelfId());

			} else {
				url = String.format(JuUrl.MEETING_MANAGELIST_DEV,
						ChatService.getSelfId());
			}
			me_to_meet.putExtra("url", url);
			startActivity(me_to_meet);
			((Activity) Context).overridePendingTransition(R.anim.zoomin,
					R.anim.zoomout_static);

			break;
		case R.id.id_me_to_settings_Lay:
			Intent settingIntent = new Intent(Context, JuSettingsActivity.class);
			startActivity(settingIntent);
			((Activity) Context).overridePendingTransition(R.anim.zoomin,
					R.anim.zoomout_static);
			break;
		case R.id.id_me_to_my_newFeature_Lay:
			goToNewFeature();
			break;
		case R.id.id_me_qr_imgBtn:
			Intent intent = new Intent(Context, JuQRShowPopuActivity.class);
			startActivity(intent);
			break;
		default:
			break;
		}

	}

	/**
	 * 跳转到新功能页面
	 * 
	 * @param v
	 */
	public void goToNewFeature() {
		Intent intent = new Intent(Context, JuHYXWebviewActivity.class);
		String url = "";
		if (JuUrl.MODE_STYLE.equals("prod")) {
			url = String
					.format(JuUrl.NEW_FEATURE_PROD, ChatService.getSelfId());
		} else {
			url = String.format(JuUrl.NEW_FEATURE_DEV, ChatService.getSelfId());
		}
		intent.putExtra("url", url);
		startActivity(intent);
		((Activity) Context).overridePendingTransition(R.anim.zoomin,
				R.anim.zoomout_static);

	}

}
