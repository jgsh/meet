package com.hyx.meet;


import com.hyx.meet.adapter.ChatMsgAdapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

public class JuImageBrowerActivity extends Activity {

	  String url;
	  String path;
	  ImageView imageView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ju_image_brower);
			imageView = (ImageView) findViewById(R.id.id_image_brower_imgv);
		    Intent intent = getIntent();
		    url = intent.getStringExtra("url");
		    path = intent.getStringExtra("path");
		    ChatMsgAdapter.displayImageByUri(imageView, path, url);
	}

}
