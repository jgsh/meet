
package com.hyx.meet.Application;

import java.io.File;
import java.io.FileOutputStream;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.Toast;

import com.avos.avoscloud.AVCloud;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVInstallation;
import com.avos.avoscloud.AVOSCloud;
import com.avos.avoscloud.PushService;
import com.avos.avoscloud.SaveCallback;
import com.avos.avoscloud.im.v2.AVIMClient;
import com.avos.avoscloud.im.v2.AVIMMessage;
import com.avos.avoscloud.im.v2.AVIMMessageManager;
import com.avos.avoscloud.im.v2.AVIMTypedMessage;
import com.avos.avoscloud.im.v2.AVIMTypedMessageHandler;
import com.avos.avoscloud.im.v2.callback.AVIMClientCallback;
import com.avos.avoscloud.im.v2.messages.AVIMTextMessage;
import com.hyx.meet.R;
import com.hyx.meet.meet.JuMainActivity;
import com.hyx.meet.service.ChatService;
import com.hyx.meet.service.NetworkStateService;
import com.hyx.meet.service.reciever.JuConversationHandlerV2;
import com.hyx.meet.service.reciever.JuMSGHandlerV2;
import com.hyx.meet.service.reciever.JuNetWorkHandlerV2;
import com.hyx.meet.setting.JuNewMessageSettingsActivity;
import com.hyx.meet.utils.Logger;
import com.hyx.meet.utils.PathUtils;
import com.hyx.meet.utils.PhotoUtil;
import com.hyx.meet.utils.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.utils.StorageUtils;

/**
 * Created by lzw on 14-5-29.
 */
public class App extends Application {
	public static final String DB_NAME = "hyx.db3";
	public static final int DB_VER = 6;
	public static final String MODE = "prod";
	public static boolean debug = false;
	public static App ctx;
	public static AVInstallation avInstallation;

	private final static String PREFS_NAME = "newMessage_setting";
	private final static String NO_MESSAGE = "noMessage";
	private final static String SOUND = "sound";
	private final static String SHAKE = "shake";
	

	@Override
	public void onCreate() {
		super.onCreate();
		ctx = this;

		// ChromeView.initialize(this);
		Utils.fixAsyncTaskBug();
		AVOSCloud.initialize(this,
				"ahjekkg29mdf3vlmqp80xysn6yar008b4j10ft9tf0gbt7sl",
				"nqw04nes7hbowj61e5097lvx1cj9c3fggb78hdli9zvwd914");
//		AVOSCloud.setDebugLogEnabled(true);
		//设置生产模式
		AVCloud.setProductionMode(true);
		
		PushService.setDefaultPushCallback(this, JuMainActivity.class);
		avInstallation = AVInstallation.getCurrentInstallation();
		//注册V2消息响应类
		registerIMV2Hadlers();
		//初始化imageloader
		initImageLoader(ctx);


	}
	
		


	/**
	 * 注册V2消息响应类
	 */
	private void registerIMV2Hadlers() {
		// TODO Auto-generated method stub
		AVIMClient.setClientEventHandler(new JuNetWorkHandlerV2());
		AVIMMessageManager.setConversationEventHandler(new JuConversationHandlerV2());
		AVIMMessageManager.registerMessageHandler(AVIMTypedMessage.class, new JuMSGHandlerV2());
	}


	/**
	 * 本地保存logo图片
	 */
//	private void saveLogo() {
//		// TODO Auto-generated method stub
//		File file = new File(PathUtils.getAppPath() + "logo.jpg");
//		// 如果图片不存在
//		if (!file.exists()) {
//			// 保存图片
//			Resources resources = getResources();
//			Bitmap bitmap = BitmapFactory.decodeResource(resources,
//					R.drawable.logo_big);
//			try {
//				FileOutputStream fileOutputStream = new FileOutputStream(file);
//				bitmap.compress(Bitmap.CompressFormat.JPEG, 100,
//						fileOutputStream);
//				fileOutputStream.close();
//			} catch (Exception e) {
//				// TODO: handle exception
//				Logger.e(e.getMessage());
//			}
//		}
//
//	}

	public static App getInstance() {
		return ctx;
	}

	/**
	 * 初始化ImageLoader
	 */
	public static void initImageLoader(Context context) {
		File cacheDir = StorageUtils.getOwnCacheDirectory(context, "hyx/Cache");
		ImageLoaderConfiguration config = PhotoUtil.getImageLoaderConfig(
				context, cacheDir);
		// Initialize ImageLoader with configuration.
		ImageLoader.getInstance().init(config);
	}

}
