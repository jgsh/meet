package com.hyx.meet.namecard;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.GetCallback;
import com.avos.avoscloud.SaveCallback;
import com.hyx.meet.R;
import com.hyx.meet.Application.App;
import com.hyx.meet.Application.SysApplication;
import com.hyx.meet.JuHYXWebviewActivity;
import com.hyx.meet.custome.SelectPicPopupWindow;
import com.hyx.meet.login.JuLoginActivity;
import com.hyx.meet.service.ChatService;
import com.hyx.meet.service.UserService;
import com.hyx.meet.setting.JuQRShowActivity;
import com.hyx.meet.utils.JuUrl;
import com.hyx.meet.utils.Logger;
import com.hyx.meet.utils.NetAsyncTask;
import com.hyx.meet.utils.PathUtils;
import com.hyx.meet.utils.PhotoUtil;
import com.hyx.meet.utils.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.LayoutInflater;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
/**
 * 我的个人资料
 * @author ww
 *
 */
public class JuMyNameCardShowActivity extends Activity implements
		OnClickListener {

	private static final int IMAGE_REQUEST = 0;
	private static final int TAKE_CAMERA_REQUEST = 2;
	private static final int PHOTO_REQUEST_CUT = 3;// 结果
	private static boolean is_camera = false;
	private String localCameraPath = PathUtils.getAvatarTmpPath();
	private String localSelectPath;

	private static ImageView mHead;
	private TextView mName;
	// private TextView mHome;
	// private ImageView mQR;

	private TextView mCompanyName;
	private TextView mJob;
	private TextView mIndustry;
	private TextView mAdrress;
	private TextView mPhone;
//	private TextView mFax;
	private TextView mEmail;
	// private TextView mTelNumber;
	private TextView mWeiboName;
	private TextView mWeChatName;
	private TextView mQQName;
	private TextView mHobby;
	private TextView mHistory;
	private TextView mHistory2;
	private TextView mHistory3;

//	private ImageView mCardImg;
	// private Button mEditBtn;
	private LinearLayout mBackBtnLay;
	// private Button mSettingBtn;
	// private RelativeLayout mSettingBtnLay;

	private RelativeLayout mEditRelativeLayout;
	private LinearLayout mHeaderLayout;

	SelectPicPopupWindow menuWindow;
	private static SysApplication sysApplication;

	private String nameCardId;
	private String mUserId;
	private String mNameString = " ";
	private String mAvatarString = " ";
	private String mJobString = "未知";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ju_my_name_card_show);
		sysApplication = SysApplication.getInstance();
		sysApplication.addActivity(JuMyNameCardShowActivity.this);
		initView();

		// try {
		// initData();
		// } catch (AVException e) {
		// // TODO Auto-generated catch block
		// // e.printStackTrace();
		// Toast.makeText(JuMyNameCardShowActivity.this, e.getMessage(),
		// Toast.LENGTH_LONG).show();
		// }
	}

	/**
	 * 初始化布局
	 */
	private void initView() {
		// TODO Auto-generated method stub
		// mMeListView = (ListView) findViewById(R.id.id_me_lv);
		mHead = (ImageView) findViewById(R.id.id_my_namecard_head_icon);
		// mQR = (ImageView) findViewById(R.id.id_my_namecard_qr_imgBtn);

		mName = (TextView) findViewById(R.id.id_my_namecard_name);
		mCompanyName = (TextView) findViewById(R.id.id_my_namecard_company);
		mJob = (TextView) findViewById(R.id.id_my_namecard_job);
		mIndustry = (TextView) findViewById(R.id.id_me_industry_tv);
		mAdrress = (TextView) findViewById(R.id.id_me_address);
		mPhone = (TextView) findViewById(R.id.id_me_phone_tv);
		// mFax = (TextView) findViewById(R.id.id_me_home);
		mEmail = (TextView) findViewById(R.id.id_me_email_tv);
		// mTelNumber = (TextView) findViewById(R.id.id_me_telnumber_tv);
		mWeiboName = (TextView) findViewById(R.id.id_me_weibo_tv);
		mWeChatName = (TextView) findViewById(R.id.id_me_weixin_tv);
		mQQName = (TextView) findViewById(R.id.id_me_qq_tv);
		mHobby = (TextView) findViewById(R.id.id_me_interst);
		mHistory = (TextView) findViewById(R.id.id_me_perence_tv);
		mHistory2 = (TextView) findViewById(R.id.id_me_perence_tv2);
		mHistory3 = (TextView) findViewById(R.id.id_me_perence_tv3);

		// mCardImg = (ImageView) findViewById(R.id.id_me_namecard_imageview);
		// mEditBtn = (Button) findViewById(R.id.id_me_editBtn);
		// mSettingBtnLay = (RelativeLayout)
		// findViewById(R.id.id_settings_btn_Lay);
		mEditRelativeLayout = (RelativeLayout) findViewById(R.id.id_my_namecard_editinfo_Lay);
		mHeaderLayout = (LinearLayout) findViewById(R.id.id_my_namecard_title_linearLayout);

		mBackBtnLay = (LinearLayout) findViewById(R.id.id_my_namecard_backBtn_Lay);
		// 设置按钮点击事件
		// mSettingBtnLay.setOnClickListener(this);
		// 设置编辑按钮点击事件
		mEditRelativeLayout.setOnClickListener(this);
		// 头像栏点击事件
		mHeaderLayout.setOnClickListener(this);

		// mQR.setOnClickListener(this);
		mBackBtnLay.setOnClickListener(this);
	}

	public static void refreshHead(String avator) {
		ImageLoader imageLoader = UserService.imageLoader;
		imageLoader.displayImage(avator, mHead, PhotoUtil.avatarImageOptions);
	}

	/**
	 * 初始化数据
	 */
	public void initData() throws AVException {
		// TODO Auto-generated method stub

		// mData = new ArrayList<Map<String,Object>>();
		AVUser currentUser = AVUser.getCurrentUser();
		mUserId = currentUser.getObjectId();
		// 头像
		final String avator = currentUser.getString("avator");
		mAvatarString = avator;
		ImageLoader imageLoader = UserService.imageLoader;
		imageLoader.displayImage(avator, mHead, PhotoUtil.avatarImageOptions);

		// Toast.makeText(Context, currentUser.getString("username"), 0).show();

		currentUser.fetchInBackground("nameCardId",
				new GetCallback<AVObject>() {

					@Override
					public void done(AVObject userAll, AVException e) {
						// TODO Auto-generated method stub

						if (e == null) {
							AVObject myCard = userAll.getAVObject("nameCardId");
							nameCardId = myCard.getObjectId();
							// 填充头像
							// if (myCard.getString("avator") != null) {
							// ImageLoader imageLoader =
							// UserService.imageLoader;
							// imageLoader.displayImage(myCard.getString("avator"),
							// mHead,PhotoUtil.avatarImageOptions);
							// }
							// 填充姓名
							if (myCard.getString("name") != null) {
								mName.setText(myCard.getString("name"));
								mNameString = myCard.getString("name");
							}
							// 填充家乡
							// if (myCard.getString("location") != null) {
							// mHome.setText(myCard.getString("location"));
							// }
							// {"公司","职务","行业","地址","手机","传真","邮箱"}
							// 填充公司
							if (myCard.getString("companyName") != null) {

								mCompanyName.setText(myCard
										.getString("companyName"));

							}
							// 填充职务
							if (myCard.getString("jobTitle") != null) {
								mJob.setText(myCard.getString("jobTitle"));
								mJobString = myCard.getString("jobTitle");

							}
							// 填充行业
							if (myCard.getString("industry") != null) {
								mIndustry.setText(myCard.getString("industry"));

							}
							// 填充地址
							if (myCard.getString("address") != null) {
								mAdrress.setText(myCard.getString("address"));

							}
							// 填充手机
							if (myCard.getString("mobilePhone") != null) {
								mPhone.setText(myCard.getString("mobilePhone"));

							}
							// 填充传真
							if (myCard.getString("fax") != null) {

							}
							// 填充邮箱
							if (myCard.getString("email") != null) {

								mEmail.setText(myCard.getString("email"));
							}
							// 填充兴趣
							if (myCard.getString("hobby") != null) {

								mHobby.setText(myCard.getString("hobby"));
							}
							// // 填充电话
							// if (myCard.getString("telNumber") != null) {
							//
							// mTelNumber.setText(myCard
							// .getString("telNumber"));
							// }
							// 填充微博
							if (myCard.getString("weibo") != null) {

								mWeiboName.setText(myCard.getString("weibo"));
							}
							// 填充微信
							if (myCard.getString("wechat") != null) {

								mWeChatName.setText(myCard.getString("wechat"));
							}
							// 填充QQ
							if (myCard.getString("qq") != null) {

								mQQName.setText(myCard.getString("qq"));
							}
							
							mHistory.setText(myCard.getString("history1"));
							mHistory2.setText(myCard.getString("history2"));
							mHistory3.setText(myCard.getString("history3"));

						} else {
							// System.out.println(e.getMessage());
							Toast.makeText(JuMyNameCardShowActivity.this,
									"请检查网络设置", Toast.LENGTH_LONG).show();

						}

					}
				});

	}

//	private JSONArray getJsonByStr(String jsonString) {
//		JSONArray jsonArray = null;
//		try {
//			jsonArray = new JSONArray(jsonString);
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return jsonArray;
//
//	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		// case R.id.id_me_editBtn:
		// Intent editIntent = new
		// Intent(JuMyNameCardShowActivity.this,JuEditInfo.class);
		// startActivity(editIntent);
		// overridePendingTransition(R.anim.zoomin, R.anim.zoomout_static);
		// break;
		case R.id.id_my_namecard_editinfo_Lay:
			Intent editIntentRalativeLayout = new Intent(
					JuMyNameCardShowActivity.this, JuHYXWebviewActivity.class);
			String url = "";
			if (JuUrl.MODE_STYLE.equals("prod")) {
				url =  String.format(JuUrl.USER_EDIT_PROD,
						ChatService.getSelfId());
			}else {
				 url = String.format(JuUrl.USER_EDIT_DEV,
							ChatService.getSelfId());
			}
			editIntentRalativeLayout.putExtra("url", url);
			startActivity(editIntentRalativeLayout);
			break;
		case R.id.id_my_namecard_title_linearLayout:
			menuWindow = new SelectPicPopupWindow(
					JuMyNameCardShowActivity.this, itemsOnClick);
			// 显示窗口
			View mView = LayoutInflater.from(JuMyNameCardShowActivity.this)
					.inflate(R.layout.activity_ju_my_name_card_show, null);
			menuWindow.showAtLocation(mView, Gravity.BOTTOM
					| Gravity.CENTER_HORIZONTAL, 0, 0); // 设置layout在PopupWindow中显示的位置
			break;
		case R.id.id_my_namecard_backBtn_Lay:
			this.finish();
			overridePendingTransition(R.anim.zoomout, R.anim.zoomin_static);
			break;
		default:
			break;
		}

	}

	// 为弹出窗口实现监听类
	private OnClickListener itemsOnClick = new OnClickListener() {

		public void onClick(View v) {

			menuWindow.dismiss();

			int id = v.getId();
			switch (id) {
			case R.id.id_me_uploadHead_camera:
				// Toast.makeText(Context, "拍照上传", 0).show();
				selectImageFromCamera();
				break;
			case R.id.id_me_uploadHead_native:
				// Toast.makeText(Context, "从相册选取", 0).show();
				selectImageFromLocal();
				break;
			case R.id.id_me_qr_show:
				Intent mIntent = new Intent(JuMyNameCardShowActivity.this,
						JuQRShowActivity.class);
				startActivity(mIntent);
				break;
			case R.id.id_me_share_my_namecard:
				if (!mAvatarString.isEmpty() && !mNameString.isEmpty()
						&& !mJobString.isEmpty()) {
					Map<String, Object> params = new HashMap<String, Object>();
					params.put("avatar", mAvatarString);
					params.put("title", mNameString + "，" + mJobString);
					params.put("userId", mUserId);
					UserService.shareNameCard(params,
							JuMyNameCardShowActivity.this);

				}

				break;
			default:
				break;
			}

		}

	};

	private void selectImageFromLocal() {
		// TODO Auto-generated method stub
		Intent intent;

		intent = new Intent(Intent.ACTION_PICK, null);
		intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
				"image/*");

		// intent = new Intent(Intent.ACTION_GET_CONTENT);
		// intent.setType("image/*");

		startActivityForResult(intent, IMAGE_REQUEST);
	}

	private void selectImageFromCamera() {
		// TODO Auto-generated method stub
		Intent openCameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		Uri imageUri = Uri.fromFile(new File(localCameraPath));
		openCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
		startActivityForResult(openCameraIntent, TAKE_CAMERA_REQUEST);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case IMAGE_REQUEST:
				is_camera = false;
				localSelectPath = parsePathByReturnData(data);
				startPhotoZoom(data.getData(), 300, IMAGE_REQUEST);
				// sendImageByPath(localSelectPath);
				break;
			case TAKE_CAMERA_REQUEST:
				is_camera = true;
				startPhotoZoom(Uri.fromFile(new File(localCameraPath)), 150,
						TAKE_CAMERA_REQUEST);
				// sendImageByPath(localCameraPath);
				break;
			case PHOTO_REQUEST_CUT:
				// sendImageByPath(parsePathByReturnData(data));
				if (is_camera) {
					sendImageByPath(localCameraPath);
				} else {
					sendImageByPath(localSelectPath);
				}
				break;
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	private void startPhotoZoom(Uri uri, int size, int type) {
		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setDataAndType(uri, "image/*");
		// crop为true是设置在开启的intent中设置显示的view可以剪裁
		intent.putExtra("crop", "true");

		// aspectX aspectY 是宽高的比例
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);

		// outputX,outputY 是剪裁图片的宽高
		intent.putExtra("outputX", size);
		intent.putExtra("outputY", size);
		intent.putExtra("return-data", true);
		if (type == IMAGE_REQUEST) {
			intent.putExtra("output", Uri.fromFile(new File(localSelectPath))); // 专入目标文件
		} else {
			intent.putExtra("output", Uri.fromFile(new File(localCameraPath))); // 专入目标文件
		}

		startActivityForResult(intent, PHOTO_REQUEST_CUT);
	}

	private void sendImageByPath(String localSelectPath) {
		// TODO Auto-generated method stub
		final String objectId = Utils.uuid();// 随机获得文件名
		final String newPath = PathUtils.getAvatarPath(objectId);
		// PhotoUtil.simpleCompressImage(localSelectPath,newPath);
		PhotoUtil.compressImage(localSelectPath, newPath);
		updateAdvator(newPath, objectId);

	}

	/**
	 * 更新个人信息
	 * 
	 * @param newPath
	 */
	private void updateAdvator(final String newPath, final String objectId) {
		// TODO Auto-generated method stub
		new NetAsyncTask(App.ctx, false) {
			String uploadUrl;

			@Override
			protected void doInBack() throws Exception {
				uploadUrl = uploadAdvator(newPath, objectId);
			}

			@Override
			protected void onPost(Exception e) {
				if (e != null) {
					e.printStackTrace();
				} else {
					try {
						updateUserAdvator(uploadUrl);
					} catch (AVException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}

				}
			}

		}.execute();

	}

	/**
	 * 更新名片头像地址
	 * 
	 * @param uploadUrl
	 * @throws AVException
	 */
	protected void updateNameCardAdvator(final String uploadUrl)
			throws AVException {
		// TODO Auto-generated method stub
		String tableName = "Ju_NameCard";
		AVQuery<AVObject> query = new AVQuery<AVObject>(tableName);

		query.getInBackground(nameCardId, new GetCallback<AVObject>() {
			@Override
			public void done(AVObject nameCard, AVException e) {
				// TODO Auto-generated method stub
				if (e == null) {
					nameCard.put("avator", uploadUrl);
					nameCard.saveInBackground(new SaveCallback() {
						@Override
						public void done(AVException e) {
							if (e == null) {
								refreshHead(uploadUrl);
								Toast.makeText(JuMyNameCardShowActivity.this,
										"更新头像成功", 0).show();
							} else {
								Toast.makeText(JuMyNameCardShowActivity.this,
										"namecard->" + e.getMessage(), 0)
										.show();
								;
							}
						}
					});

				} else {

				}

			}
		});

	}

	/**
	 * 更新用户头像地址
	 * 
	 * @param uploadUrl2
	 * @throws AVException
	 */
	private void updateUserAdvator(final String uploadUrl) throws AVException {
		// TODO Auto-generated method stub
		AVUser avUser = AVUser.getCurrentUser();
		avUser.put("avator", uploadUrl);
		avUser.saveInBackground(new SaveCallback() {

			@Override
			public void done(AVException e) {
				// TODO Auto-generated method stub
				if (e == null) {
					try {
						updateNameCardAdvator(uploadUrl);
					} catch (AVException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				} else {
					// 强制登录
					sysApplication.exit();
					Intent intent = new Intent(JuMyNameCardShowActivity.this,
							JuLoginActivity.class);
					startActivity(intent);
					Logger.e(e.getCode() + e.getMessage());
					Toast.makeText(JuMyNameCardShowActivity.this,
							"user->" + e.getMessage(), 0).show();
				}
			}
		});

	}

	/**
	 * 上传头像
	 * 
	 * @param newPath
	 * @throws IOException
	 * @throws FileNotFoundException
	 * @throws AVException
	 */
	private String uploadAdvator(String newPath, String objectId)
			throws FileNotFoundException, IOException, AVException {
		// TODO Auto-generated method stub
		AVFile file = AVFile.withAbsoluteLocalPath(objectId, newPath);
		file.save();
		String url = file.getUrl();
		return url;
	}

	private String parsePathByReturnData(Intent data) {
		if (data == null) {
			return null;
		}
		String localSelectPath = null;
		Uri selectedImage = data.getData();
		if (selectedImage != null) {
			Cursor cursor = getContentResolver().query(selectedImage, null,
					null, null, null);
			cursor.moveToFirst();
			int columnIndex = cursor.getColumnIndex("_data");
			localSelectPath = cursor.getString(columnIndex);
			cursor.close();
		}
		return localSelectPath;
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try {
			initData();
		} catch (AVException e) {
			// TODO Auto-generated catch block
			Toast.makeText(JuMyNameCardShowActivity.this, e.getMessage(),
					Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}
	}

}

// if (myCard.getString("history") != null) {
// // mHistory.setText(myCard.getString("history"));
// JSONArray historyJson = getJsonByStr(myCard
// .getString("history"));
// String history = "";
// if (historyJson != null) {
// for (int i = 0; i < historyJson.length(); i++) {
// try {
// Logger.d(historyJson.getString(i));
// if (i < historyJson.length() - 1
// && !historyJson
// .getString(i)
// .isEmpty()
// && !historyJson
// .getString(i)
// .equals("")) {
// if (!historyJson.getString(
// i + 1).equals("")) {
// history = history
// + historyJson
// .getString(i)
// + "\n";
//
// } else {
// history = history
// + historyJson
// .getString(i);
// }
// } else {
// history = history
// + historyJson
// .getString(i);
// }
// } catch (JSONException e1) {
// // TODO Auto-generated catch block
// e1.printStackTrace();
// }
// }
//
// mHistory.setText(history);
//
// } else {
// mHistory.setText(history);
// }
// }
//
