package com.hyx.meet.namecard;

import java.util.HashMap;
import java.util.Map;

import com.avos.avoscloud.AVCloud;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.FunctionCallback;
import com.avos.avoscloud.GetCallback;
import com.avos.avoscloud.Session;
import com.hyx.meet.R;
import com.hyx.meet.login.JuLoginActivity;
import com.hyx.meet.service.ChatService;
import com.hyx.meet.service.UserService;
import com.hyx.meet.utils.Logger;
import com.hyx.meet.utils.PhotoUtil;
import com.hyx.meet.utils.UrlDecodeUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 添加好友界面
 * 
 * @author ww
 *
 */
public class JuNameCardAddByQRActivity extends Activity implements
		OnClickListener {

	private Button mTopTxtBtn;
	// private Button mBackBtn;
	private LinearLayout mBackBtnLay;

	private ImageView mAddAvator;
	private TextView mAddName;
	private TextView mAddJob;
	private TextView mAddCompany;
	private Button mAddBtn;
	
	private TextView mAddPhone;
	private TextView mAddIndustry;
	private TextView mAddAdress;
	private TextView mAddHistroy1;
	private TextView mAddHistroy2;
	private TextView mAddHistroy3;
	

	private String addUserId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ju_name_card_add_by_qr);

		initView();
		checkIsFromUrl();
	}

	/**
	 * 检测是不是外部链接进来的
	 */
	private void checkIsFromUrl() {
		// TODO Auto-generated method stub
		Intent i_getvalue = getIntent();
		String action = i_getvalue.getAction();

		if (Intent.ACTION_VIEW.equals(action)) {
			// 从外部链接进来
			String requestUrl = i_getvalue.getDataString();
			String[] urlParameters = UrlDecodeUtils
					.getUrlParameters(requestUrl);
			String urlAction = urlParameters[2];
			if (AVUser.getCurrentUser() != null) {
				if (urlAction.equals("addFriend") && urlParameters.length == 4) {
					// 初始化用户信息
					initData(urlParameters[3]);
				}

			} else {
				// 标志从外部链接报名动作true，输入用户名和密码再报名
				Toast.makeText(JuNameCardAddByQRActivity.this,
						"请先登录!" + requestUrl, 0).show();
				Intent intent = new Intent(JuNameCardAddByQRActivity.this,
						JuLoginActivity.class);
				startActivity(intent);
				this.finish();
			}
		} else {
			// 初始化用户信息
			initData(i_getvalue.getStringExtra("result"));

		}

	}

	/**
	 * 初始化数据
	 */
	private void initData(String userId) {
		// TODO Auto-generated method stub
		addUserId = userId;

		AVQuery<AVUser> avQuery = new AVQuery<AVUser>("_User");
		avQuery.getInBackground(addUserId, new GetCallback<AVUser>() {

			@Override
			public void done(AVUser addUser, AVException e) {
				// TODO Auto-generated method stub
				if (e == null) {
					if (addUser.getString("avator") != null) {
						ImageLoader imageLoader = UserService.imageLoader;
						imageLoader.displayImage(addUser.getString("avator"),
								mAddAvator, PhotoUtil.avatarImageOptions);
					}

					if (addUser.getString("realName") != null) {
						mAddName.setText(addUser.getString("realName"));
					}
					if (addUser.getString("companyName") != null) {
						mAddCompany.setText(addUser.getString("companyName"));
					}
					if (addUser.getString("jobTitle") != null) {
						mAddJob.setText(addUser.getString("jobTitle"));
					}
					if (addUser.getString("username") != null) {
						mAddPhone.setText(addUser.getString("username"));
					}
					addUser.fetchInBackground("nameCardId",new GetCallback<AVObject>() {

						@Override
						public void done(AVObject userInfo, AVException e) {
							// TODO Auto-generated method stub
							if (e == null) {
								AVObject namecard = userInfo.getAVObject("nameCardId");
								if (namecard.getString("industry") != null) {
									mAddIndustry.setText(namecard.getString("industry"));
								}
								
								if (namecard.getString("location") != null) {
									mAddAdress.setText(namecard.getString("location"));
								}
								
								if (namecard.getString("history1") != null) {
									mAddHistroy1.setText(namecard.getString("history1"));
								}
								
								if (namecard.getString("history2") != null) {
									mAddHistroy2.setText(namecard.getString("history2"));
								}
								
								if (namecard.getString("history3") != null) {
									mAddHistroy3.setText(namecard.getString("history3"));
								}
							}else {
								Logger.d(e.getMessage());								
							}
							
						}
					});
				} else {
					Logger.d(e.getMessage());
				}

			}
		});

	}

	/**
	 * 初始化界面
	 */
	private void initView() {
		// TODO Auto-generated method stub
		mBackBtnLay = (LinearLayout) findViewById(R.id.id_common_top_backBtn_Lay);
		mTopTxtBtn = (Button) findViewById(R.id.id_common_top_txtBtn);
		mTopTxtBtn.setText("交换名片");

		mAddName = (TextView) findViewById(R.id.id_qr_add_name);
		mAddCompany = (TextView) findViewById(R.id.id_qr_add_company);
		mAddJob = (TextView) findViewById(R.id.id_qr_add_job);
		mAddBtn = (Button) findViewById(R.id.id_qr_add_Btn);
		mAddAvator = (ImageView) findViewById(R.id.id_qr_add_avatar);
		mAddPhone = (TextView) findViewById(R.id.id_qr_add_phone);
		mAddIndustry = (TextView) findViewById(R.id.id_qr_add_industry);
		mAddAdress = (TextView) findViewById(R.id.id_qr_add_address);
		mAddHistroy1 = (TextView) findViewById(R.id.id_qr_add_histroy1);
		mAddHistroy2 = (TextView) findViewById(R.id.id_qr_add_histroy2);
		mAddHistroy3 = (TextView) findViewById(R.id.id_qr_add_histroy3);

		mBackBtnLay.setOnClickListener(this);
		mAddBtn.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.id_common_top_backBtn_Lay) {
			this.finish();
		} else if (id == R.id.id_qr_add_Btn) {
			addFriends(addUserId);
			Intent intent = new Intent(JuNameCardAddByQRActivity.this,
					JuLoginActivity.class);
			startActivity(intent);
			this.finish();

		} else {
		}

	}

	/**
	 * 添加好友
	 */
	private void addFriends(final String addUserId) {
		// TODO Auto-generated method stub
//		AVCloud.setProductionMode(false);
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("touserId", addUserId);
		parameters.put("userId", ChatService.getSelfId());
		parameters.put("source", "扫码添加");
		AVCloud.callFunctionInBackground("requestAddFriend", parameters,
				new FunctionCallback<Map<String, Object>>() {

					@Override
					public void done(Map<String, Object> result, AVException e) {
						// TODO Auto-generated method stub
						if (e == null) {
//							Session session = ChatService.getSession();
//							if (session.isWatching(addUserId)) {
//								// Toast.makeText(App.ctx, "watched", 0).show();
//							} else {
//								ChatService.watchById(addUserId);
//								// Toast.makeText(App.ctx, "unwatched",
//								// 0).show();
//							}

							Toast.makeText(JuNameCardAddByQRActivity.this,
									result.get("errorMessage").toString(), 0)
									.show();
							if ((Integer) result.get("errorCode") == 0) {
								mAddBtn.setText("已发送");
								mAddBtn.setTextColor(Color.GRAY);
								mAddBtn.setOnClickListener(null);
							}

						} else {
							Toast.makeText(JuNameCardAddByQRActivity.this,
									getString(R.string.pleaseCheckNetwork), 0)
									.show();
							Logger.e(e.getMessage());
						}
					}

				});

	}

}
