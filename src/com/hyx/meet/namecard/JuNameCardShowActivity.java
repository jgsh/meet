package com.hyx.meet.namecard;

import java.util.HashMap;
import java.util.Map;



import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.GetCallback;
import com.hyx.meet.Application.SysApplication;
import com.hyx.meet.custome.namecardOptPopupWindow;
import com.hyx.meet.custome.sweet_dialog.SweetAlertDialog;
import com.hyx.meet.db.DBNamecard;
import com.hyx.meet.entity.NameCard;
import com.hyx.meet.service.UserService;

import com.hyx.meet.utils.PhotoUtil;
import com.hyx.meet.utils.Utils;
import com.hyx.meet.R;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * 他人名片展示
 * 
 * @author ww
 *
 */
public class JuNameCardShowActivity extends Activity implements OnClickListener {

	private namecardOptPopupWindow optPopupWindow;

	private Button mTopTxtBtn;
	// private Button mBackBtn;
	private LinearLayout mBackBtnLay;
	// private Button mMenuBtn;
	private RelativeLayout mMenuBtnLay;

	// private LinearLayout mDeleteNameCard;

	private String otherId;// 用户id
	private String otherNameCardId;// 用户名片id
	private String name;
	private String otherNumber;

	private ImageView mHead;
	private TextView mName;
	private TextView mCompany;
	// private TextView mBeizhu;

	// private TextView mCompany;
	private TextView mJobTitle;
	private TextView mIndustry;
	private TextView mPerence;
	private TextView mPerence2;
	private TextView mPerence3;

	private TextView mAddress;
	private TextView mPhone;
	// private TextView mTelNumber;
	private TextView mEmail;

	private TextView mWeibo;
	private TextView mWechat;
	private TextView mQQ;
	private TextView mInterest;

	private Intent mIntent;

	private String avatarString = " ";// 初始
	private String nameString = "未知";
	private String jobString = "未知";// 初始
	private ImageView mMan;
	private ImageView mWoman;
	private RelativeLayout mToChat;
	private RelativeLayout mToCall;
	private RelativeLayout mToSMS;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ju_name_card_show);
		SysApplication.getInstance().addActivity(JuNameCardShowActivity.this);

		initView();
		mIntent = getIntent();
		initData(mIntent);

	}

	/**
	 * 初始化界面
	 */
	private void initView() {
		// TODO Auto-generated method stub
		mTopTxtBtn = (Button) findViewById(R.id.id_namecard_show_topname_tv);
		mBackBtnLay = (LinearLayout) findViewById(R.id.id_card_show_backBtn_Lay);
		mMenuBtnLay = (RelativeLayout) findViewById(R.id.id_card_show_menuBtn_Lay);
		mToChat = (RelativeLayout) findViewById(R.id.id_namecard_message);
		mToCall = (RelativeLayout) findViewById(R.id.id_namecard_call);
		mToSMS = (RelativeLayout) findViewById(R.id.id_namecard_sms);
		// mDeleteNameCard = (LinearLayout)
		// findViewById(R.id.id_delete_in_namecard_show);

		mHead = (ImageView) findViewById(R.id.id_other_head_icon);
		mName = (TextView) findViewById(R.id.id_other_name);
		mCompany = (TextView) findViewById(R.id.id_other_company_tv);
		// mBeizhu = (TextView) findViewById(R.id.id_other_beizhu_edit);
		// mCompany = (TextView) findViewById(R.id.id_other_company_tv);
		mJobTitle = (TextView) findViewById(R.id.id_other_job);
		mIndustry = (TextView) findViewById(R.id.id_other_industry_tv);
		// 从业经历
		mPerence = (TextView) findViewById(R.id.id_other_perence_tv);
		mPerence2 = (TextView) findViewById(R.id.id_other_perence_tv2);
		mPerence3= (TextView) findViewById(R.id.id_other_perence_tv3);
		mAddress = (TextView) findViewById(R.id.id_other_address);
		mPhone = (TextView) findViewById(R.id.id_other_phone_tv);
		// mTelNumber = (TextView) findViewById(R.id.id_other_telnumber_tv);
		mEmail = (TextView) findViewById(R.id.id_other_email_tv);
		mWechat = (TextView) findViewById(R.id.id_other_weixin_tv);
		mWeibo = (TextView) findViewById(R.id.id_other_weibo_tv);
		mQQ = (TextView) findViewById(R.id.id_other_qq_tv);
		mInterest = (TextView) findViewById(R.id.id_other_interst);
		mMan = (ImageView) findViewById(R.id.id_other_man);
		mWoman = (ImageView) findViewById(R.id.id_other_woman);

		// mBeizhu.setOnClickListener(this);
		mBackBtnLay.setOnClickListener(this);
		mMenuBtnLay.setOnClickListener(this);
		mToCall.setOnClickListener(this);
		mToSMS.setOnClickListener(this);
		mToChat.setOnClickListener(this);
		// mDeleteNameCard.setOnClickListener(this);
	}

	/**
	 * 初始化数据
	 * 
	 * @param intent
	 */
	private void initData(Intent intent) {
		// TODO Auto-generated method stub
//		name = intent.getStringExtra("name");
//		otherId = intent.getStringExtra("otherId");
		otherNameCardId = intent.getStringExtra("otherNameCardId");
		
		NameCard nameCardFromDb = DBNamecard.getNameCardByNameCardId(otherNameCardId);
		name = nameCardFromDb.getName();
		otherId = nameCardFromDb.getUserId();
//		Logger.e("[shownamecard] otherId->"+otherId);

		// 顶部栏名字显示
		mTopTxtBtn.setText(name);

		// 加载数据
		AVQuery<AVObject> query = new AVQuery<AVObject>("Ju_NameCard");
		query.getInBackground(otherNameCardId, new GetCallback<AVObject>() {

			@Override
			public void done(AVObject nameCardOb, AVException e) {
				// TODO Auto-generated method stub
				if (e == null) {

					// 填充头像
					if (nameCardOb.getString("avator") != null) {
						ImageLoader imageLoader = UserService.imageLoader;
						imageLoader.displayImage(
								nameCardOb.getString("avator"), mHead,
								PhotoUtil.avatarImageOptions);
						avatarString = nameCardOb.getString("avator");
					}
					if (nameCardOb.getString("selectSex").equals("0")) {
						// 男
						mMan.setVisibility(View.VISIBLE);
						mWoman.setVisibility(View.GONE);
					} else {
						// 女
						mMan.setVisibility(View.GONE);
						mWoman.setVisibility(View.VISIBLE);

					}
					// 填充姓名
					if (nameCardOb.getString("name") != null) {
						mName.setText(nameCardOb.getString("name"));
						nameString = nameCardOb.getString("name");
					}
					// 填充所在地
					if (nameCardOb.getString("location") != null) {
						// mHome.setText(nameCardOb.getString("location"));
					}
					// {"公司","职务","行业","地址","手机","传真","邮箱"}
					// 填充公司
					if (nameCardOb.getString("companyName") != null) {

						mCompany.setText(nameCardOb.getString("companyName"));

					}
					// 填充职务
					if (nameCardOb.getString("jobTitle") != null) {
						mJobTitle.setText(nameCardOb.getString("jobTitle"));
						jobString = nameCardOb.getString("jobTitle");

					}
					// 填充行业
					if (nameCardOb.getString("industry") != null) {
						mIndustry.setText(nameCardOb.getString("industry"));

					}
					// 填充地址
					if (nameCardOb.getString("address") != null) {
						mAddress.setText(nameCardOb.getString("address"));

					}
					// 填充手机
					if (nameCardOb.getString("mobilePhone") != null) {
						mPhone.setText(nameCardOb.getString("mobilePhone"));
						otherNumber = nameCardOb.getString("mobilePhone");

					}
					// 填充传真
					if (nameCardOb.getString("fax") != null) {

					}
					// 填充邮箱
					if (nameCardOb.getString("email") != null) {

						mEmail.setText(nameCardOb.getString("email"));
					}
					// 填充兴趣
					if (nameCardOb.getString("hobby") != null) {

						mInterest.setText(nameCardOb.getString("hobby"));
					}
					// 填充电话
					// if (nameCardOb.getString("telNumber") != null) {
					//
					// mTelNumber.setText(nameCardOb.getString("telNumber"));
					// }
					// 填充微博
					if (nameCardOb.getString("weibo") != null) {

						mWeibo.setText(nameCardOb.getString("weibo"));
					}
					// 填充微信
					if (nameCardOb.getString("wechat") != null) {

						mWechat.setText(nameCardOb.getString("wechat"));
					}
					// 填充QQ
					if (nameCardOb.getString("qq") != null) {

						mQQ.setText(nameCardOb.getString("qq"));
					}
					
					mPerence.setText(nameCardOb.getString("history1"));
					mPerence2.setText(nameCardOb.getString("history2"));
					mPerence3.setText(nameCardOb.getString("history3"));

					

				} else {
					Toast.makeText(JuNameCardShowActivity.this, "请检查网络设置",
							Toast.LENGTH_LONG).show();
				}

			}
		});

	}

//	private JSONArray getJsonByStr(String jsonString) {
//		JSONArray jsonArray = null;
//		try {
//			jsonArray = new JSONArray(jsonString);
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return jsonArray;
//
//	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			JuNameCardShowActivity.this.finish();
			overridePendingTransition(R.anim.zoomin_static, R.anim.zoomout);

		}

		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.id_namecard_opt_lay) {
		} else if (id == R.id.id_card_show_backBtn_Lay) {
			this.finish();
			overridePendingTransition(R.anim.zoomin_static, R.anim.zoomout);
		} else if (id == R.id.id_card_show_menuBtn_Lay) {
			optPopupWindow = new namecardOptPopupWindow(
					JuNameCardShowActivity.this, itemsOnClick);
			// 显示窗口
			View mView = LayoutInflater.from(JuNameCardShowActivity.this)
					.inflate(R.layout.activity_ju_guidance, null);
			optPopupWindow.showAtLocation(mView, Gravity.BOTTOM
					| Gravity.CENTER_HORIZONTAL, 0, 0); // 设置layout在PopupWindow中显示的位置

		}else if (id == R.id.id_namecard_call) {
			UserService.toCall(JuNameCardShowActivity.this, otherNumber);			
		}else if (id == R.id.id_namecard_message) {
			UserService.goToChat(JuNameCardShowActivity.this, name,otherId);
		}else if (id == R.id.id_namecard_sms) {
			UserService.toSMS(JuNameCardShowActivity.this, otherNumber);
		}

	}

	// 为弹出窗口实现监听类
	private OnClickListener itemsOnClick = new OnClickListener() {

		public void onClick(View v) {

			optPopupWindow.dismiss();

			int id = v.getId();
			switch (id) {
			case R.id.id_namecard_opt_share:
				if (!avatarString.isEmpty() && !nameString.isEmpty()
						&& !jobString.isEmpty()) {
					Map<String, Object> params = new HashMap<String, Object>();
					params.put("avatar", avatarString);
					params.put("title", nameString + "，" + jobString);
					params.put("userId", otherId);
					UserService.shareNameCard(params,
							JuNameCardShowActivity.this);

				}
				break;
			case R.id.id_namecard_opt_del:
				confirm();
				break;
			case R.id.id_namecard_opt_pullBlack:
				Utils.toast("已拉黑");
				break;
			default:
				break;

			}
		}
	};

	/**
	 * 确认删除名片
	 */
	private void confirm() {
		// TODO Auto-generated method stub
		new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
	    .setTitleText("提示")
	    .setContentText("确认删除好友？")
	    .setCancelText("取消")
	    .setConfirmText("删除")
	    .showCancelButton(true)
	    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
	        @Override
	        public void onClick(SweetAlertDialog sDialog) {
//	            sDialog.cancel();
	            sDialog.dismiss();
	        }
	    })
	    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
			
			@Override
			public void onClick(SweetAlertDialog sweetAlertDialog) {
				// TODO Auto-generated method stub
				sweetAlertDialog.dismiss();
				mIntent.putExtra("is_delete", 1);
				mIntent.putExtra("userId", otherId);
				setResult(RESULT_OK, mIntent);
				JuNameCardShowActivity.this.finish();
			}
		})
	    .show();

//		AlertDialog.Builder builder = new AlertDialog.Builder(this);
//		builder.setMessage("删除名片");
//		builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
//
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				// TODO Auto-generated method stub
//				dialog.dismiss();
//				mIntent.putExtra("is_delete", 1);
//				mIntent.putExtra("userId", otherId);
//				setResult(RESULT_OK, mIntent);
//				JuNameCardShowActivity.this.finish();
//
//			}
//		});
//		builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
//
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				// TODO Auto-generated method stub
//				dialog.dismiss();
//
//			}
//		});
//		builder.create().show();

	}

}
