package com.hyx.meet.qr;

import java.io.IOException;
import java.util.Vector;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.hyx.meet.JuHYXWebviewActivity;
import com.hyx.meet.R;
import com.hyx.meet.namecard.JuNameCardAddByQRActivity;
import com.hyx.meet.qr.camera.CameraManager;
import com.hyx.meet.qr.decoding.CaptureActivityHandler;
import com.hyx.meet.qr.decoding.InactivityTimer;
import com.hyx.meet.qr.view.ViewfinderView;
import com.hyx.meet.service.MeetingService;
import com.hyx.meet.utils.JuUrl;
import com.hyx.meet.utils.Logger;
import com.hyx.meet.utils.UrlDecodeUtils;
import com.hyx.meet.utils.Utils;

/**
 * Initial the camera
 * 
 * @author Ryan.Tang
 */
public class MipcaActivityCapture extends Activity implements Callback,
		OnClickListener {

	private CaptureActivityHandler handler;
	private ViewfinderView viewfinderView;
	private boolean hasSurface;
	private Vector<BarcodeFormat> decodeFormats;
	private String characterSet;
	private InactivityTimer inactivityTimer;
	private MediaPlayer mediaPlayer;
	private boolean playBeep;
	private static final float BEEP_VOLUME = 0.10f;
	private boolean vibrate;

	private TextView mResultTxt;
	private Button mTopTxtBtn;
	private LinearLayout mBackBtnLay;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_capture);
		// ViewUtil.addTopView(getApplicationContext(), this,
		// R.string.scan_card);
		CameraManager.init(getApplication());
		viewfinderView = (ViewfinderView) findViewById(R.id.viewfinder_view);

//		mResultTxt = (TextView) findViewById(R.id.id_qr_resultTxt);
		mTopTxtBtn = (Button) findViewById(R.id.id_common_top_txtBtn);
		mTopTxtBtn.setText("扫一扫");

		mBackBtnLay = (LinearLayout) findViewById(R.id.id_common_top_backBtn_Lay);
		mBackBtnLay.setOnClickListener(this);

		hasSurface = false;
		inactivityTimer = new InactivityTimer(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
		SurfaceHolder surfaceHolder = surfaceView.getHolder();
		if (hasSurface) {
			initCamera(surfaceHolder);
		} else {
			surfaceHolder.addCallback(this);
			surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		}
		decodeFormats = null;
		characterSet = null;

		playBeep = true;
		AudioManager audioService = (AudioManager) getSystemService(AUDIO_SERVICE);
		if (audioService.getRingerMode() != AudioManager.RINGER_MODE_NORMAL) {
			playBeep = false;
		}
		initBeepSound();
		vibrate = true;

	}

	@Override
	protected void onPause() {
		super.onPause();
		if (handler != null) {
			handler.quitSynchronously();
			handler = null;
		}
		CameraManager.get().closeDriver();
	}

	@Override
	protected void onDestroy() {
		inactivityTimer.shutdown();
		super.onDestroy();
	}

	/**
	 * 二维码回调处理
	 * 
	 * @param result
	 * @param barcode
	 */
	public void handleDecode(Result result, Bitmap barcode) {
		inactivityTimer.onActivity();
		playBeepSoundAndVibrate();
		// 扫码结果
		String resultString = result.getText();

		Intent fromIntent = getIntent();
		int needResult = fromIntent.getIntExtra("needResult", 0);
		
		if (!Utils.isNetworkConnected(this)) {
			resultString = "network";
		}

		if (resultString.equals("")) {
			Toast.makeText(MipcaActivityCapture.this, "扫码失败",
					Toast.LENGTH_SHORT).show();
		} else if (resultString.equals("network")) {
			Utils.toast(getString(R.string.pleaseCheckNetwork));			
		}else {
			if (needResult == 1) {
				// 直接返回结果
				fromIntent.putExtra("qrResult", toJsonString(resultString));
				setResult(RESULT_OK, fromIntent);
			} else {
				Logger.d("[qr result]"+resultString);
				String[] parameterts = UrlDecodeUtils
						.getUrlParameters(resultString);
				Uri uri=Uri.parse(resultString);			
				//判断是否是会友行返回的二维码数据
				if (resultString.startsWith("qrcode://")) {
					if (parameterts.length >= 4) {
						// 扫描添加好友
						if (parameterts[2].equals("nameCard")
								&& parameterts[3].equals("addFriend")) {
							Intent toIntent = new Intent(
									MipcaActivityCapture.this,
									JuNameCardAddByQRActivity.class);
							toIntent.putExtra("result", parameterts[4]);
							startActivity(toIntent);
						}
						// 扫码会议签到
						if (parameterts[2].equals("meeting")) {
							// 会议签到
							MeetingService.checkMeetingIn(uri.getQueryParameter("meetingId"),
									uri.getQueryParameter("userId"),"qrcode");

						}
					}
				}else if(uri.getLastPathSegment().equals("meetingShowDetail.html")){
					// 会议报名
//					Utils.toast(uri.getQueryParameter("meetingId"));
					Intent intent = new Intent(Intent.ACTION_VIEW,uri);
					startActivity(intent);
//					Intent intent = new Intent(MipcaActivityCapture.this,JsSdkDemo.class);
////					String url = resultString;
//					String url  = String.format(JuUrl.getMeetingSignIn(), uri.getQueryParameter("meetingId"));
//					intent.putExtra("url", url);
//					startActivity(intent);					
//					MeetingService.enterMeeting(uri.getQueryParameter("meetingId"), "扫码报名");

				}
			}

		}
		MipcaActivityCapture.this.finish();
	}

	/**
	 * 封装返回数据
	 * 
	 * @param resultString
	 */
	private String toJsonString(String resultString) {
		// TODO Auto-generated method stub
		JSONObject result = new JSONObject();
		try {
			result.put("data", resultString);
			result.put("errorMessage", "success");
			result.put("errorCode", 0);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result.toString();
	}

	private void initCamera(SurfaceHolder surfaceHolder) {
		try {
			CameraManager.get().openDriver(surfaceHolder);
		} catch (IOException ioe) {
			return;
		} catch (RuntimeException e) {
			return;
		}
		if (handler == null) {
			handler = new CaptureActivityHandler(this, decodeFormats,
					characterSet);
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {

	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		if (!hasSurface) {
			hasSurface = true;
			initCamera(holder);
		}

	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		hasSurface = false;

	}

	public ViewfinderView getViewfinderView() {
		return viewfinderView;
	}

	public Handler getHandler() {
		return handler;
	}

	public void drawViewfinder() {
		viewfinderView.drawViewfinder();

	}

	private void initBeepSound() {
		if (playBeep && mediaPlayer == null) {
			// The volume on STREAM_SYSTEM is not adjustable, and users found it
			// too loud,
			// so we now play on the music stream.
			setVolumeControlStream(AudioManager.STREAM_MUSIC);
			mediaPlayer = new MediaPlayer();
			mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mediaPlayer.setOnCompletionListener(beepListener);

			AssetFileDescriptor file = getResources().openRawResourceFd(
					R.raw.beep);
			try {
				mediaPlayer.setDataSource(file.getFileDescriptor(),
						file.getStartOffset(), file.getLength());
				file.close();
				mediaPlayer.setVolume(BEEP_VOLUME, BEEP_VOLUME);
				mediaPlayer.prepare();
			} catch (IOException e) {
				mediaPlayer = null;
			}
		}
	}

	private static final long VIBRATE_DURATION = 200L;

	private void playBeepSoundAndVibrate() {
		if (playBeep && mediaPlayer != null) {
			mediaPlayer.start();
		}
		if (vibrate) {
			Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
			vibrator.vibrate(VIBRATE_DURATION);
		}
	}

	/**
	 * When the beep has finished playing, rewind to queue up another one.
	 */
	private final OnCompletionListener beepListener = new OnCompletionListener() {
		public void onCompletion(MediaPlayer mediaPlayer) {
			mediaPlayer.seekTo(0);
		}
	};

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v.getId() == R.id.id_common_top_backBtn_Lay) {
			this.finish();
		}

	}

}