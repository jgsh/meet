package com.hyx.meet.meet;

import java.util.ArrayList;
import java.util.List;

import com.hyx.meet.custome.sweet_dialog.SweetAlertDialog;
import com.hyx.meet.entity.JuContact;
import com.hyx.meet.service.JuShareService;
import com.hyx.meet.service.UserService;
import com.hyx.meet.service.listener.ActivityFinshListener;
import com.hyx.meet.setting.JuSettingsActivity;
import com.hyx.meet.utils.PhotoUtil;
import com.hyx.meet.utils.Utils;
import com.hyx.meet.R;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.InputType;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ImageSpan;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * 分享会议BY短信界面
 * 
 * @author ww
 *
 */
public class JuShareMeetBySMSMainActivity extends Activity implements
		OnClickListener {
	TextView mSelectEditText;
	EditText mAddEditText;
	LinearLayout mAddBtn;
	LinearLayout mBackLay;
	RelativeLayout mConfirmLay;
	static final int SELECT_CONTACTS = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ju_share_meet_by_smsmain);

		initView();
	}

	private void initView() {
		// TODO Auto-generated method stub
		mSelectEditText = (TextView) findViewById(R.id.id_jushareMeet_mSelectedEdit);
		// 屏蔽父控件的点击事件
		mSelectEditText.setMovementMethod(LinkMovementMethod.getInstance());

		mAddEditText = (EditText) findViewById(R.id.id_jushareMeet_mAddEdit);
		mAddBtn = (LinearLayout) findViewById(R.id.id_jushareMeet_mAddBtnLay);
		mBackLay = (LinearLayout) findViewById(R.id.id_shareMeet_BySMS_backBtn_Lay);
		mConfirmLay = (RelativeLayout) findViewById(R.id.id_shareMeet_BySMS_confirm_Lay);

		// 软键盘回车键监听事件
		mAddEditText.setImeOptions(EditorInfo.IME_ACTION_SEND);
		mAddEditText
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {

					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						// 当actionId == XX_SEND 或者 XX_DONE时都触发
						// 或者event.getKeyCode == ENTER 且 event.getAction ==
						// ACTION_DOWN时也触发
						// 注意，这是一定要判断event != null。因为在某些输入法上会返回null。
						if (actionId == EditorInfo.IME_ACTION_SEND
								|| actionId == EditorInfo.IME_ACTION_DONE
								|| (event != null
										&& KeyEvent.KEYCODE_ENTER == event
												.getKeyCode() && KeyEvent.ACTION_DOWN == event
										.getAction())) {
							// 处理事件
							if (!mAddEditText.getText().equals("") && isMobileNO()) {
								addInputNumbers(getInputNumbers());
							}

						}
						return false;
					}

				});

		mAddBtn.setOnClickListener(this);
		mBackLay.setOnClickListener(this);
		mConfirmLay.setOnClickListener(this);
	}

	/**
	 * 添加选中的号码
	 * 
	 * @param str
	 */
	private void insertEditText(String str, final int position) {
		Bitmap bm = PhotoUtil.createBitmap(JuShareMeetBySMSMainActivity.this,
				str);
		if (bm != null) {
			// 图片替换名字
			ImageSpan imageSpan = new ImageSpan(
					JuShareMeetBySMSMainActivity.this, bm);
			SpannableString spannableString = new SpannableString("[name]"
					+ str + "[/name]");
			spannableString.setSpan(imageSpan, 0,
					("[name]" + str + "[/name]").length(),
					Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			// 添加点击事件删除发送人
			ClickableSpan clickableSpan = new ClickableSpan() {

				@Override
				public void onClick(View widget) {
					// TODO Auto-generated method stub
					deleteContact(position);
				}
			};
			spannableString.setSpan(clickableSpan, 0,
					("[name]" + str + "[/name]").length(),
					Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

			// int index = mSelectEditText.getSelectionStart();
			mSelectEditText.append(spannableString);
			// Editable editable = mSelectEditText.getEditableText();
			// editable.append(spannableString);
			// if (index < 0 || index >= editable.length()) {
			// editable.append(spannableString);
			// } else {
			// editable.insert(editable.length(), spannableString);
			// }

		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.id_jushareMeet_mAddBtnLay:
			Intent intent = new Intent(JuShareMeetBySMSMainActivity.this,
					JuShareMeetBySMSActivity.class);
			startActivityForResult(intent, SELECT_CONTACTS);
			break;
		case R.id.id_shareMeet_BySMS_backBtn_Lay:
			finish();
			break;
		case R.id.id_shareMeet_BySMS_confirm_Lay:
			Intent intent2 = getIntent();
			if (isMobileNO()) {
				List<String> sendNumbers = getNumbers();
				if (sendNumbers.size() > 0) {
					// Logger.d("numbers->"+getNumbers().toString());
					sendMakeSure(intent2.getStringExtra("meetingId"));
				} else {
					// 一个号码都没添加
					Utils.showCommonFailDialog(this, "请至少添加一个号码!");
				}

			}
			// Logger.d("name->"+getNames().toString()+"----------"+"number->"
			// +getNumbers().toString());
			// Logger.d("inputContacts"+getInputNumbers().toString());

		default:
			break;
		}

	}

	/**
	 * 
	 * @param stringExtra
	 */
	private void sendMakeSure(final String meetingId) {
		// TODO Auto-generated method stub
		SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this,
				SweetAlertDialog.WARNING_TYPE)
				.setTitleText("确认发送")
				.setContentText("确认进行短信分享?")
				.setConfirmText("发送")
				.showCancelButton(true)
				.setCancelText("取消")
				.setCancelClickListener(
						new SweetAlertDialog.OnSweetClickListener() {
							@Override
							public void onClick(SweetAlertDialog sDialog) {
								// sDialog.cancel();
								sDialog.dismiss();
							}
						})
				.setConfirmClickListener(
						new SweetAlertDialog.OnSweetClickListener() {

							@Override
							public void onClick(
									SweetAlertDialog sweetAlertDialog) {
								// TODO Auto-generated method stub
								JuShareService.inviteMeetingBySMS(getNumbers(),
										meetingId,
										JuShareMeetBySMSMainActivity.this,new ActivityFinshListener() {
											
											@Override
											public void finish() {
												// TODO Auto-generated method stub
												myFinishActivity();
											}

											
										});
								sweetAlertDialog.dismiss();
							}
						});
		sweetAlertDialog.show();
	}

	/**
	 * 关闭页面
	 */
	private void myFinishActivity() {
		// TODO Auto-generated method stub
		this.finish();
	}
	
	/**
	 * 判断是否有不符合格式的号码
	 * 
	 * @return
	 */
	private boolean isMobileNO() {
		boolean isMobile = true;
		List<String> list = getInputNumbers();
		for (int i = 0; i < list.size(); i++) {
			// 如果有不符合格式的号码
			// if (list.size() == 1 && list.get(i).equals("")) {
			// // 排除没有输入号码
			// } else {
			if (!Utils.isMobileNO(list.get(i))) {
				isMobile = false;
				Utils.showCommonFailDialog(this, "sorry,号码" + list.get(i)
						+ "格式不正确!");
				break;
			}
			// }

		}
		return isMobile;
	}

	/**
	 * 获取选择联系人的号码列表
	 * 
	 * @return
	 */
	private List<String> getNumbers() {
		// TODO Auto-generated method stub
		numberList.clear();
		for (int i = 0; i < mSelectContacts.size(); i++) {
			numberList.add(mSelectContacts.get(i).getPhoneNumber());
		}

		if (getInputNumbers().size() > 0) {
			numberList.addAll(getInputNumbers());
		}

		return numberList;
	}

	// /**
	// * 获取选择的联系人的姓名
	// *
	// * @return
	// */
	// private List<String> getNames() {
	// // TODO Auto-generated method stub
	// nameList.clear();
	// for (int i = 0; i < mSelectContacts.size(); i++) {
	// nameList.add(mSelectContacts.get(i).getName());
	// }
	//
	// return nameList;
	// }

	List<String> numberList = new ArrayList<String>();
	List<String> nameList = new ArrayList<String>();
	List<JuContact> mSelectContacts = new ArrayList<JuContact>();// 选择的联系人 对象列表

	@SuppressWarnings("unchecked")
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (resultCode == RESULT_OK) {
			if (requestCode == SELECT_CONTACTS) {
				// 清空edit
				mSelectEditText.setText("");
				// 接收选择的联系人，并且确保联系人唯一性
				addContacts((List<JuContact>) data
						.getSerializableExtra("contacts"));
			}
		}
	}

	/**
	 * 增加选中的联系人
	 * 
	 * @param serializableExtra
	 */
	private void addContacts(List<JuContact> serializableExtra) {
		// TODO Auto-generated method stub
		filterContacts(serializableExtra);
		// Logger.d("contacts"+mSelectContacts.toString());
		for (int i = 0; i < mSelectContacts.size(); i++) {
			insertEditText(mSelectContacts.get(i).getName(), i);
		}

	}

	/**
	 * 
	 * @param numbers
	 */
	private void addInputNumbers(List<String> numbers) {
		List<JuContact> inputNumbers = new ArrayList<JuContact>();
		for (int i = 0; i < numbers.size(); i++) {
			JuContact juContact = new JuContact();
			juContact.setName(numbers.get(i));
			juContact.setPhoneNumber(numbers.get(i));
			inputNumbers.add(juContact);
		}
		//过滤号码
		filterContacts(inputNumbers);
		mAddEditText.setText("");
		refreshSelectContacts();
	}

	/**
	 * 删除选中的联系人
	 * 
	 * @param serializableExtra
	 */
	private void deleteContact(final int position) {
		// TODO Auto-generated method stub
		// Utils.toast(mSelectContacts.get(position).getName());
		new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
				.setTitleText("提示")
				.setContentText("取消对该联系人的发送？")
				.setCancelText("取消")
				.setConfirmText("确认")
				.showCancelButton(true)
				.setCancelClickListener(
						new SweetAlertDialog.OnSweetClickListener() {
							@Override
							public void onClick(SweetAlertDialog sDialog) {
								// sDialog.cancel();
								sDialog.dismiss();
							}
						})
				.setConfirmClickListener(
						new SweetAlertDialog.OnSweetClickListener() {

							@Override
							public void onClick(
									SweetAlertDialog sweetAlertDialog) {
								// TODO Auto-generated method stub
								mSelectContacts.remove(position);
								refreshSelectContacts();
								sweetAlertDialog.dismiss();

							}
						}).show();

	}

	/**
	 * 刷新已选的联系人
	 */
	private void refreshSelectContacts() {
		mSelectEditText.setText("");
		for (int i = 0; i < mSelectContacts.size(); i++) {
			insertEditText(mSelectContacts.get(i).getName(), i);
		}

	}

	/**
	 * 过滤选中的联系人
	 * 
	 * @param serializableExtra
	 */
	private void filterContacts(List<JuContact> serializableExtra) {
		// TODO Auto-generated method stub
		for (int i = 0; i < serializableExtra.size(); i++) {
			boolean same = false;// 标志本对象是否重复

			// 若存在相同号码，怎避免重复加入
			for (int j = 0; j < mSelectContacts.size(); j++) {
				if (mSelectContacts.get(j).getPhoneNumber()
						.equals(serializableExtra.get(i).getPhoneNumber())) {
					same = true;// 重复
					break;
				}

			}

			// 若不重复
			if (!same) {
				mSelectContacts.add(serializableExtra.get(i));
			}
		}

	}

	/**
	 * 过滤手动输入的号码
	 * 
	 * @return
	 */
	private List<String> getInputNumbers() {
		List<String> list = new ArrayList<String>();
		String str = mAddEditText.getText().toString();
		if (!str.equals("")) {
			// 替换中英文分号和逗号
			str = str.replaceAll("，", ",");
			str = str.replaceAll(";", ",");
			str = str.replaceAll("；", ",");

			String[] strs = {};
			strs = str.split(",");

			for (int i = 0; i < strs.length; i++) {
				boolean is_same = false;
				//过滤相同号码
				for (int j = 0; j < list.size(); j++) {
					//存在相同号码
					if (list.get(j).equals(strs[i].trim())) {
						is_same = true;
						break;
					}
				}
				if (!is_same) {
					list.add(strs[i].trim());	
				}
			}

		}
		return list;
	}
	

}



