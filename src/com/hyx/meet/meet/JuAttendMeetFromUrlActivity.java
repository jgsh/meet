package com.hyx.meet.meet;

import com.avos.avoscloud.AVUser;
import com.hyx.meet.R;
import com.hyx.meet.R.layout;
import com.hyx.meet.login.JuLoginActivity;
import com.hyx.meet.namecard.JuNameCardAddByQRActivity;
import com.hyx.meet.service.MeetingService;
import com.hyx.meet.utils.Logger;
import com.hyx.meet.utils.UrlDecodeUtils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 处理外部链接跳进来报名会议
 * @author ww
 *
 */
public class JuAttendMeetFromUrlActivity extends Activity implements OnClickListener {
	
	private TextView mCancle;
	private TextView mCommit;
	private EditText mEditText;
	private String meetId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ju_attend_meet_from_url);
		initView();
		check_isFromExtra();
	}

	private void initView() {
		// TODO Auto-generated method stub
		mCancle = (TextView) findViewById(R.id.id_attendMeet_cancle);
		mCommit = (TextView) findViewById(R.id.id_attendMeet_commit);
		mEditText = (EditText) findViewById(R.id.id_attendMeet_ly_Edit);
		
		mCancle.setOnClickListener(this);
		mCommit.setOnClickListener(this);
	}
	
	/**
	 * 检测是不是外部链接进来
	 */
	private boolean check_isFromExtra() {
		// TODO Auto-generated method stub
		Intent i_getvalue = getIntent();  
		String action = i_getvalue.getAction();  
		  
		if(Intent.ACTION_VIEW.equals(action)){
			String requestUrl = i_getvalue.getDataString();
			String[] urlParameters = UrlDecodeUtils.getUrlParameters(requestUrl);
			String urlAction = urlParameters[2];
		   	if (AVUser.getCurrentUser() != null) {
		   		if (urlAction.equals("enterMeeting") && urlParameters.length == 4) {
//					报名
		   			meetId = urlParameters[3];
		   			Logger.d(meetId+"|"+requestUrl);
//		   			enterMeeting(urlParameters[3]);
//					Toast.makeText(JuLoginActivity.this, urlParameters[3], 0).show();
			   		return true;					
				}

			}else{
//				标志从外部链接报名动作true，输入用户名和密码再报名
				Toast.makeText(JuAttendMeetFromUrlActivity.this, "请先登录!"+requestUrl, 0).show();
				Intent intent = new Intent(JuAttendMeetFromUrlActivity.this,JuLoginActivity.class);
				startActivity(intent);
				return true;
			}
		}
		return false;
	}

	
	/**
	 * 报名
	 * @param string
	 */
	private void enterMeeting(String ID) {
		// TODO Auto-generated method stub
		MeetingService.enterMeeting(ID, mEditText.getText().toString());
//		String name = "未知";
//		String mobilePhoneNumber = "";
//		if (!AVUser.getCurrentUser().getString("realName").isEmpty()) {
//			name = AVUser.getCurrentUser().getString("realName");
//		}
//		
//		if (!AVUser.getCurrentUser().getUsername().isEmpty()) {
//			mobilePhoneNumber = AVUser.getCurrentUser().getUsername();
//		}
//				
//		MeetingService.enteMeetingById(ID,mEditText.getText().toString(),name,mobilePhoneNumber);
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.id_attendMeet_cancle:
			this.finish();
			break;
		case R.id.id_attendMeet_commit:
			enterMeeting(meetId);
			Intent intent = new Intent(JuAttendMeetFromUrlActivity.this,
					JuLoginActivity.class);
			startActivity(intent);
			this.finish();

			break;

		default:
			break;
		}
		
	}

}
