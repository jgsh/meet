package com.hyx.meet.meet;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.R.integer;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsoluteLayout;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.RelativeLayout.LayoutParams;

import com.avos.avoscloud.AVUser;
import com.hyx.meet.JuHYXWebviewActivity;
import com.hyx.meet.R;
import com.hyx.meet.Application.App;
import com.hyx.meet.Application.SysApplication;
import com.hyx.meet.avobject.User;
import com.hyx.meet.fragments.JuMeFragment;
import com.hyx.meet.fragments.JuMeetingFragment;
import com.hyx.meet.fragments.JuMessageFragment;
import com.hyx.meet.fragments.JuNamecardFragment;
import com.hyx.meet.service.CacheService;
import com.hyx.meet.service.ChatService;
import com.hyx.meet.service.JuChatServiceV2;
import com.hyx.meet.service.JuUpdateService;
import com.hyx.meet.service.NetworkStateService;
import com.hyx.meet.service.NotifyService;
import com.hyx.meet.service.UserService;
import com.hyx.meet.service.listener.MsgListener;
import com.hyx.meet.service.reciever.JuMSGHandlerV2;
import com.hyx.meet.service.reciever.JuMSGReciever;
import com.hyx.meet.service.reciever.JuPushReciever;
import com.hyx.meet.utils.AndroidConfig;
import com.hyx.meet.utils.JuUnZipUtil;
import com.hyx.meet.utils.JuUrl;
import com.hyx.meet.utils.Logger;
import com.hyx.meet.utils.PathUtils;
import com.hyx.meet.utils.PhotoUtil;

public class JuMainActivity extends FragmentActivity implements MsgListener {

	private static JuMessageFragment juMessageFragment;
	private JuMeetingFragment juMeetingFragment;
	private JuNamecardFragment juNamecardFragment;
	private JuMeFragment juMeFragment;
	private Fragment[] fragments;
	private int index;

	private final static int INDEX_MEET = 0;
	private final static int INDEX_MESSAGE = 1;
	private final static int INDEX_NAMECARD = 2;
	private final static int INDEX_ME = 3;

	// private RelativeLayout[] tab_containers;
	private ImageView[] mTabs;
	TextView messageText;
	TextView meetText;
	TextView namecardText;
	TextView meText;

	private static TextView mUnread;
	private static int WIDTH;
	private static int HEIGHT;

	// private static boolean is_alive = false;

	// ��ǰfragment��index
	private int currentTabIndex;

	private final static String NEW_SYS_MESSAGE = "sys_message";// 系统通知信息
	private final static String MEET_NOTIFY_NUM = "meet_notify";// 小秘书新通知数量
	private final static String NEW_FRIEND_REQ_NUM = "new_friend_req";// 好友请求通知数量
	private final static String IS_LOGOUT = "logout";

	private NotifyService NotifyService;

	// private Drawable meet;
	// private Drawable namecard;
	// private Drawable message;
	private ImageView mGuideOpt;
	private AbsoluteLayout mGuideOptLay;
	private ImageView mClear;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		SysApplication.getInstance().addActivity(JuMainActivity.this);
		NotifyService = new NotifyService(ChatService.getSelfId()); // 启动监测网络监听器
		startNetworkService();

		// 获取屏幕的宽度和高度
		WIDTH = AndroidConfig.getScreenWidth(this);
		HEIGHT = AndroidConfig.getScreenHeight(this);

		initView();
		juMessageFragment = new JuMessageFragment(JuMainActivity.this);
		juMeetingFragment = new JuMeetingFragment(JuMainActivity.this);
		juNamecardFragment = new JuNamecardFragment(JuMainActivity.this);
		juMeFragment = new JuMeFragment(JuMainActivity.this);

		fragments = new Fragment[] { juMeetingFragment, juMessageFragment,
				juNamecardFragment, juMeFragment };
		getSupportFragmentManager().beginTransaction().add(
				R.id.fragment_container, juMessageFragment);

		getSupportFragmentManager().beginTransaction()
				.add(R.id.fragment_container, juMeetingFragment)
				.show(juMeetingFragment).commit();

		// 注册当前用户对象
		CacheService.registerUserCache(User.curUser());
		// 登陆聊天服务器
//		ChatService.openSession(this);
		JuChatServiceV2.openChat();
		// 启动异机登陆检测
		checkOneLog();
		// 检测是否有App更新
		JuUpdateService.checkUpdate(false, JuMainActivity.this,
				JuMainActivity.this);
		// 检测是否网页更新
		JuUnZipUtil juUnZipUtil = new JuUnZipUtil();
		juUnZipUtil.unZipToHYX(PathUtils.getZipDir(), JuMainActivity.this,
				true, false);

	}

	/**
	 * 启动异地登陆检测并订阅自己频道
	 */
	private void checkOneLog() {
		// TODO Auto-generated method stub
		// PushService.subscribe(App.ctx, ChatService.getSelfId(),
		// JuMainActivity.class);
		// AVInstallation.getCurrentInstallation().saveInBackground();
		// SharedPreferences sp = App.ctx.getSharedPreferences(NEW_SYS_MESSAGE,
		// App.ctx.MODE_PRIVATE);
		// 如果这台设备上的账号本身就处于被登出但是未退出状态，就不发送登出通知了
		if (!NotifyService.getIsLogout()) {
			UserService.sendCheckOneLog(AVUser.getCurrentUser());
			Logger.e("[MainActivity] sendCheckOneLog" + "yes");
		}

	}

	/**
	 * 启动监测网络监听器
	 */
	private void startNetworkService() {
		// TODO Auto-generated method stub

		Intent i = new Intent(App.ctx, NetworkStateService.class);
		startService(i);
	}

	private final static int NAMECARD_SHOW = 0;

	// @SuppressWarnings("static-access")
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case NAMECARD_SHOW:
				int is_delete = data.getIntExtra("is_delete", 0);
				Logger.e("is_delete" + is_delete);
				if (is_delete == 1) {
					// 删除名片
					juNamecardFragment.delete_nameCard(
							data.getStringExtra("userId"), -1);
					// delete_nameCard(data.getStringExtra("userId"), -1);
				}
				break;

			default:
				break;
			}
		}
	}

	public void initView() {

		mTabs = new ImageView[4];
		mTabs[0] = (ImageView) findViewById(R.id.id_btn_meet);
		mTabs[1] = (ImageView) findViewById(R.id.btn_message);
		mTabs[2] = (ImageView) findViewById(R.id.btn_namecard);
		mTabs[3] = (ImageView) findViewById(R.id.btn_me);
		// �ѵ�һ��tab��Ϊѡ��״̬
		mTabs[0].setSelected(true);
		messageText = (TextView) findViewById(R.id.text_message);
		meetText = (TextView) findViewById(R.id.text_meet);
		namecardText = (TextView) findViewById(R.id.text_namecard);
		meText = (TextView) findViewById(R.id.text_me);
		mUnread = (TextView) findViewById(R.id.id_message_unread);
		// 操作引导图
		// meet = getResources().getDrawable(R.drawable.firstuse_meetlist);
		// message = getResources().getDrawable(R.drawable.firstuse_message);
		// namecard = getResources().getDrawable(R.drawable.firstuse_namecard);
		//
		mGuideOpt = (ImageView) findViewById(R.id.id_guide_opt_img);
		mGuideOptLay = (AbsoluteLayout) findViewById(R.id.id_guide_opt_lay);
		// mClear = (ImageView) findViewById(R.id.id_guide_opt_clear_round);

		openGuideOpt(INDEX_MEET);

	}

	/**
	 * 更新消息标记
	 * 
	 * @param num
	 */
	public static void updateMessageUnread(int num) {
		if (num > 0 && num < 100) {
			mUnread.setText("" + num);
			mUnread.setVisibility(View.VISIBLE);
		} else if (num >= 100) {
			mUnread.setText("N+");
			mUnread.setVisibility(View.VISIBLE);
		} else {
			mUnread.setVisibility(View.GONE);
		}
	}

	public static void OnRefreshMessage() {
		juMessageFragment.onRefresh();
	}

	/**
	 * button点击事件
	 * 
	 * @param view
	 */

	@SuppressLint("ResourceAsColor")
	public void onTabClicked(View view) {
		int id = view.getId();
		if (id == R.id.btn_container_conversation) {
			// juMessageFragment.hidden = false;
			// OnRefreshMessage();
			index = 1;
			// mGuideOpt.setImageDrawable(message);
			openGuideOpt(INDEX_MESSAGE);
		} else if (id == R.id.btn_container_b1) {
			// mGuideOpt.setImageDrawable(meet);
			openGuideOpt(INDEX_MEET);
			index = 0;
		} else if (id == R.id.btn_container_address_list) {
			// mGuideOpt.setImageDrawable(namecard);
			openGuideOpt(INDEX_NAMECARD);
			index = 2;
		} else if (id == R.id.btn_container_setting) {
			index = 3;
			openGuideOpt(INDEX_ME);
			// juMeFragment.initData();
		}
		if (currentTabIndex != index) {
			FragmentTransaction trx = getSupportFragmentManager()
					.beginTransaction();
			trx.hide(fragments[currentTabIndex]);
			if (!fragments[index].isAdded()) {
				trx.add(R.id.fragment_container, fragments[index]);
			}
			trx.show(fragments[index]).commit();
		}
		mTabs[currentTabIndex].setSelected(false);
		mTabs[index].setSelected(true);
		currentTabIndex = index;

		if (view.getId() == R.id.btn_container_conversation) {

			messageText.setTextColor(this.getResources().getColor(
					R.color.txtcolor_selected));
		} else {
			messageText.setTextColor(this.getResources().getColor(
					R.color.txtcolor));
		}
		if (view.getId() == R.id.btn_container_b1) {

			meetText.setTextColor(this.getResources().getColor(
					R.color.txtcolor_selected));
		} else {
			meetText.setTextColor(this.getResources()
					.getColor(R.color.txtcolor));
		}
		if (view.getId() == R.id.btn_container_address_list) {

			namecardText.setTextColor(this.getResources().getColor(
					R.color.txtcolor_selected));
		} else {
			namecardText.setTextColor(this.getResources().getColor(
					R.color.txtcolor));
		}
		if (view.getId() == R.id.btn_container_setting) {

			meText.setTextColor(this.getResources().getColor(
					R.color.txtcolor_selected));

		} else {
			meText.setTextColor(this.getResources().getColor(R.color.txtcolor));
		}

	}

	/**
	 * 获取版本信息
	 * 
	 * @return
	 */
	private String getVersion() {
		try {
			PackageInfo info = this.getPackageManager().getPackageInfo(
					this.getPackageName(), 0);
			int versionCode = info.versionCode;
			return versionCode + "";
		} catch (Exception e) {
		}
		return "";
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		JuPushReciever.addMsgListener(this);
//		JuMSGReciever.addMsgListener(this); 
		JuMSGHandlerV2.addMsgListener(this);
		if (NotifyService.getIsLogout()) {
			// UserService.sendCheckOneLog(AVUser.getCurrentUser());
			UserService.logoutConfirm(this);

		}
		// MeetingService.getMessages();
		updateMessageUnread(ChatService.getMessageUnread());
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		JuPushReciever.removeMsgListener(this);
//		JuMSGReciever.removeMsgListener(this);
		JuMSGHandlerV2.removeMsgListener(this);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		// ChatService.closeSession();
		JuPushReciever.removeMsgListener(this);
//		JuMSGReciever.removeMsgListener(this);
		JuMSGHandlerV2.removeMsgListener(this);
		// 设置下次拉取消息时间戳
		setGetMessageTimeStamp();

		// 取消订阅
		// PushService.unsubscribe(App.ctx, ChatService.getSelfId());
		// AVInstallation.getCurrentInstallation().saveInBackground();
	}

	/**
	 * 设置下次拉取消息时间戳
	 */
	private void setGetMessageTimeStamp() {
		// TODO Auto-generated method stub
		long nowTimeLong = System.currentTimeMillis();
		long change = 8 * 60 * 60 * 1000;// 当前时间与开始时间格式上差八个小时
		nowTimeLong = nowTimeLong + change;
		Date date = new Date(nowTimeLong);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'"
				+ "HH:mm:ss" + ".000Z");

		String loginTimeStr = dateFormat.format(date);
		// 设置下次拉取消息时间戳
		new NotifyService(ChatService.getSelfId()).setLoginTime(loginTimeStr);
	}

	@Override
	public boolean onMessageUpdate(String otherId) {
		// TODO Auto-generated method stub
		// SharedPreferences sp = App.ctx.getSharedPreferences(NEW_SYS_MESSAGE,
		// App.ctx.MODE_PRIVATE);
		// 收到登出通知后，强迫用户登出
		updateMessageUnread(ChatService.getMessageUnread());
		if (NotifyService.getIsLogout()) {
			// UserService.sendCheckOneLog(AVUser.getCurrentUser());
			UserService.logoutConfirm(this);

		}

		return true;
	}

	private int mBackKeyPressedTimes = 0;

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (mBackKeyPressedTimes == 0) {
			Toast.makeText(this, "再按一次退出程序 ", Toast.LENGTH_SHORT).show();
			mBackKeyPressedTimes = 1;
			new Thread() {
				@Override
				public void run() {
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					} finally {
						mBackKeyPressedTimes = 0;
					}
				}
			}.start();
			return;
		} else {
			JuMainActivity.this.finish();
		}

		super.onBackPressed();
	}

	/**
	 * 发布会议
	 */
	public void createMeeting(View v) {
		Intent intent = new Intent(JuMainActivity.this,
				JuHYXWebviewActivity.class);
		String url = "";
		if (JuUrl.MODE_STYLE.equals("prod")) {
			url = String.format(JuUrl.MEETING_EDIT_GUIDE_PROD,
					ChatService.getSelfId());
		} else {
			url = String.format(JuUrl.MEETING_EDIT_GUIDE_DEV,
					ChatService.getSelfId());
		}
		intent.putExtra("url", url);
		startActivity(intent);

	}

	/*********************************** 引导页操作 ****************************************/

	int method = 0;

	/**
	 * 显示操作引导页
	 * 
	 * @param drawable
	 */
	public void openGuideOpt(int type) {
		// 0 meet 1 message 2 namecard 3me 4 meetdetail 5 exchangeNamecard
		SharedPreferences sp = getSharedPreferences("first_use",
				MODE_WORLD_READABLE);

		boolean isFirstUse = sp.getBoolean(getVersion() + type, true);
		if (isFirstUse) {
			Editor editor = sp.edit();
			editor.putBoolean(getVersion() + type, false);
			editor.commit();
			if (mGuideOptLay.getVisibility() == View.GONE) {
				mGuideOptLay.setVisibility(View.VISIBLE);
				mGuideOpt.setVisibility(View.VISIBLE);
			}

			if (type == 0) {
				method = 0;// 步骤
				changeClear(0, 0, 0, 0, this, mGuideOptLay);
				mGuideOpt.setImageResource(R.drawable.firstuse_meeting_first_1);
				// 会议
				mGuideOpt.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (method == 0) {// 会议分类
							changeClear(
									WIDTH
											/ 2
											- PhotoUtil.sp2px(
													JuMainActivity.this, 150)
											/ 2, 0, PhotoUtil.sp2px(
											JuMainActivity.this, 150),
									PhotoUtil.sp2px(JuMainActivity.this, 50),
									JuMainActivity.this, mGuideOptLay);
							mGuideOpt
									.setImageResource(R.drawable.firstuse_meeting_fenlei_2);

						} else if (method == 1) {// 扫一扫
							changeClear(
									WIDTH
											- PhotoUtil.sp2px(
													JuMainActivity.this, 50),
									0,
									PhotoUtil.sp2px(JuMainActivity.this, 50),
									PhotoUtil.sp2px(JuMainActivity.this, 50),
									JuMainActivity.this, mGuideOptLay);
							mGuideOpt
									.setImageResource(R.drawable.firstuse_meeting_sao_3);
						} else if (method == 2) {// 海报
							changeClear(0,
									PhotoUtil.sp2px(JuMainActivity.this, 50),
									WIDTH,
									PhotoUtil.sp2px(JuMainActivity.this, 200),
									JuMainActivity.this, mGuideOptLay);
							mGuideOpt
									.setImageResource(R.drawable.firstuse_meeting_img_4);
						} else if (method == 3) {// 会议邀请
							changeClear(
									PhotoUtil.sp2px(JuMainActivity.this, 10),
									PhotoUtil.sp2px(JuMainActivity.this, 340),
									PhotoUtil.sp2px(JuMainActivity.this, 150),
									PhotoUtil.sp2px(JuMainActivity.this, 40),
									JuMainActivity.this, mGuideOptLay);
							mGuideOpt
									.setImageResource(R.drawable.firstuse_meeting_invite_5);
						} else if (method == 4) {// 会议签到
							changeClear(
									WIDTH
											/ 2
											+ PhotoUtil.sp2px(
													JuMainActivity.this, 10),
									PhotoUtil.sp2px(JuMainActivity.this, 340),
									PhotoUtil.sp2px(JuMainActivity.this, 150),
									PhotoUtil.sp2px(JuMainActivity.this, 40),
									JuMainActivity.this, mGuideOptLay);
							mGuideOpt
									.setImageResource(R.drawable.firstuse_meeting_signin_6);
						} else {
							mGuideOptLay.setVisibility(View.GONE);
							mGuideOpt.setVisibility(View.GONE);
						}

						method++;
					}
				});

			} else if (type == 1) {// 消息界面
				method = 0;// 步骤
				// 小秘书
				changeClear(PhotoUtil.sp2px(JuMainActivity.this, 10),
						PhotoUtil.sp2px(JuMainActivity.this, 58),
						PhotoUtil.sp2px(JuMainActivity.this, 150),
						PhotoUtil.sp2px(JuMainActivity.this, 50),
						JuMainActivity.this, mGuideOptLay);
				mGuideOpt
						.setImageResource(R.drawable.firstuse_message_secret_1);
				mGuideOpt.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (method == 0) {// 新的会友
							changeClear(
									PhotoUtil.sp2px(JuMainActivity.this, 10),
									PhotoUtil.sp2px(JuMainActivity.this, 108),
									PhotoUtil.sp2px(JuMainActivity.this, 150),
									PhotoUtil.sp2px(JuMainActivity.this, 50),
									JuMainActivity.this, mGuideOptLay);
							mGuideOpt
									.setImageResource(R.drawable.firstuse_message_friend_2);
						} else if (method == 1) {// 会议服务号
							changeClear(0,
									PhotoUtil.sp2px(JuMainActivity.this, 166),
									WIDTH,
									PhotoUtil.sp2px(JuMainActivity.this, 80),
									JuMainActivity.this, mGuideOptLay);
							mGuideOpt
									.setImageResource(R.drawable.firstuse_message_notify_3);
						} else {
							mGuideOptLay.setVisibility(View.GONE);
							mGuideOpt.setVisibility(View.GONE);
						}
						method++;

					}
				});
			} else if (type == 2) {// 名片夹
				method = 0;// 步骤
				// 头像
				changeClear(PhotoUtil.sp2px(JuMainActivity.this, 5),
						PhotoUtil.sp2px(JuMainActivity.this, 120),
						PhotoUtil.sp2px(JuMainActivity.this, 80),
						PhotoUtil.sp2px(JuMainActivity.this, 80),
						JuMainActivity.this, mGuideOptLay);
				mGuideOpt.setImageResource(R.drawable.first_namecard_1);
				mGuideOpt.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (method == 0) {// 操作
							changeClear(
									PhotoUtil.sp2px(JuMainActivity.this, 75),
									PhotoUtil.sp2px(JuMainActivity.this, 120),
									PhotoUtil.sp2px(JuMainActivity.this, 200),
									PhotoUtil.sp2px(JuMainActivity.this, 80),
									JuMainActivity.this, mGuideOptLay);
							mGuideOpt
									.setImageResource(R.drawable.first_namecard_2);
						} else {
							mGuideOptLay.setVisibility(View.GONE);
							mGuideOpt.setVisibility(View.GONE);
						}
						method++;
					}
				});
			} else if (type == 3) {// 我的
				method = 0;// 步骤
				// 头像
				changeClear(PhotoUtil.sp2px(JuMainActivity.this, 5),
						PhotoUtil.sp2px(JuMainActivity.this, 50),
						PhotoUtil.sp2px(JuMainActivity.this, 80),
						PhotoUtil.sp2px(JuMainActivity.this, 80),
						JuMainActivity.this, mGuideOptLay);
				mGuideOpt.setImageResource(R.drawable.firstuse_me_img_1);
				mGuideOpt.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (method == 0) {// 二维码
							changeClear(
									WIDTH
											- PhotoUtil.sp2px(
													JuMainActivity.this, 50),
									PhotoUtil.sp2px(JuMainActivity.this, 70),
									PhotoUtil.sp2px(JuMainActivity.this, 40),
									PhotoUtil.sp2px(JuMainActivity.this, 40),
									JuMainActivity.this, mGuideOptLay);
							mGuideOpt
									.setImageResource(R.drawable.firstuse_me_qr_2);
						} else if (method == 1) {// 发布会以
							changeClear(
									PhotoUtil.sp2px(JuMainActivity.this, 5),
									PhotoUtil.sp2px(JuMainActivity.this, 155),
									PhotoUtil.sp2px(JuMainActivity.this, 150),
									PhotoUtil.sp2px(JuMainActivity.this, 40),
									JuMainActivity.this, mGuideOptLay);
							mGuideOpt
									.setImageResource(R.drawable.firstuse_me_creat_3);

						} else {
							mGuideOptLay.setVisibility(View.GONE);
							mGuideOpt.setVisibility(View.GONE);
						}
						method++;

					}
				});
			}
		}
	}

	/**
	 * 改变透明框的位置
	 * 
	 * @param i
	 * @param j
	 * @param k
	 * @param l
	 */
	private void changeClear(int toLeft, int toTop, int width, int height,
			Activity context, AbsoluteLayout mGuideOptLay) {
		// TODO Auto-generated method stub
		final Drawable clearDrawable = getResources().getDrawable(
				R.drawable.clear_round);
		mGuideOptLay.removeAllViews();
		ImageView clear = new ImageView(context);
		View leftView = new View(context);
		View rightView = new View(context);
		View topView = new View(context);
		View bottomView = new View(context);
		// 添加透明区域
		AbsoluteLayout.LayoutParams clearLp = new AbsoluteLayout.LayoutParams(
				width, height, toLeft, toTop);

		clear.setLayoutParams(clearLp);
		clear.setScaleType(ScaleType.FIT_XY);
		clear.setImageDrawable(clearDrawable);
		clear.setId(1);
		mGuideOptLay.addView(clear);

		ImageView clearView = (ImageView) findViewById(mGuideOptLay.getChildAt(
				0).getId());
		int clearRight = toLeft + width;
		int clearBottom = toTop + height;

		// 添加周围背景
		AbsoluteLayout.LayoutParams leftLp = new AbsoluteLayout.LayoutParams(
				toLeft, height, 0, toTop);
		leftView.setLayoutParams(leftLp);
		leftView.setBackgroundColor(Color.parseColor("#99000000"));
		mGuideOptLay.addView(leftView);

		AbsoluteLayout.LayoutParams rightLp = new AbsoluteLayout.LayoutParams(
				WIDTH - clearRight, height, clearRight, toTop);
		rightView.setLayoutParams(rightLp);
		rightView.setBackgroundColor(Color.parseColor("#99000000"));
		mGuideOptLay.addView(rightView);

		AbsoluteLayout.LayoutParams topLp = new AbsoluteLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, clearBottom - height, 0, 0);
		topView.setLayoutParams(topLp);
		topView.setBackgroundColor(Color.parseColor("#99000000"));
		mGuideOptLay.addView(topView);

		AbsoluteLayout.LayoutParams bottomLp = new AbsoluteLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, HEIGHT - clearBottom, 0, clearBottom);
		bottomView.setLayoutParams(bottomLp);
		bottomView.setBackgroundColor(Color.parseColor("#99000000"));
		mGuideOptLay.addView(bottomView);

	}

}
