package com.hyx.meet.meet;

import java.net.URISyntaxException;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.location.LocationManagerProxy;
import com.amap.api.location.LocationProviderProxy;
import com.amap.api.maps.AMap;
import com.amap.api.maps.AMap.InfoWindowAdapter;
import com.amap.api.maps.AMap.OnInfoWindowClickListener;
import com.amap.api.maps.AMap.OnMapClickListener;
import com.amap.api.maps.AMap.OnMarkerClickListener;
import com.amap.api.maps.AMapOptions;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.LocationSource;
import com.amap.api.maps.MapView;
import com.amap.api.maps.LocationSource.OnLocationChangedListener;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.Circle;
import com.amap.api.maps.model.CircleOptions;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.LatLngBounds;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;
import com.amap.api.maps.model.MyLocationStyle;
import com.amap.api.maps.overlay.DrivingRouteOverlay;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.route.BusRouteResult;
import com.amap.api.services.route.DrivePath;
import com.amap.api.services.route.DriveRouteResult;
import com.amap.api.services.route.RouteSearch;
import com.amap.api.services.route.RouteSearch.DriveRouteQuery;
import com.amap.api.services.route.RouteSearch.OnRouteSearchListener;
import com.amap.api.services.route.WalkRouteResult;
import com.hyx.meet.R;
import com.hyx.meet.R.drawable;
import com.hyx.meet.R.id;
import com.hyx.meet.R.layout;
import com.hyx.meet.R.string;
import com.hyx.meet.custome.gotoMapPopupWindow;
import com.hyx.meet.namecard.JuMyNameCardShowActivity;
import com.hyx.meet.service.ChatService;
import com.hyx.meet.service.MeetingService;
import com.hyx.meet.setting.JuQRShowActivity;
import com.hyx.meet.utils.Logger;
import com.hyx.meet.utils.Utils;

import android.R.bool;
import android.R.integer;
import android.app.Activity;
import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
/**
 * 地图导航/会议签到
 * @author ww
 *
 */
public class JuGuidanceActivity extends Activity implements OnClickListener,
		LocationSource, AMapLocationListener, OnMarkerClickListener,
		InfoWindowAdapter, OnInfoWindowClickListener, OnRouteSearchListener,
		OnMapClickListener {

	MapView mMapView;
	private LinearLayout mBackBtn;
	private Button mTopTxt;
	private RelativeLayout mGotoTarget;
	private TextView mGotoTargetTxt;
	private ImageView mGotoTargetImageview;
	AMap aMap;

	private OnLocationChangedListener mListener;
	private LocationManagerProxy mAMapLocationManager;

	private PendingIntent mPendingIntent;
	private Circle mCircle;
	private boolean is_inTarget = false;
	private int type;// 0为导航 1为位置签到
	private String meetingId;// 要签到的会议Id

	public static final String GEOFENCE_BROADCAST_ACTION = "com.location.apis.geofencedemo.broadcast";

	private RouteSearch mRouteSearch;
	private int drivingMode = RouteSearch.DrivingDefault;// 驾车默认模式
	private DriveRouteResult driveRouteResult;// 驾车模式查询结果

	gotoMapPopupWindow mGotoMapPopupWindow;

	private double targetLatitude;// 目的的纬度30.31854, 120.347
	private double targetLongtitude;// 目的地经度
	private LatLonPoint targetLatLonPoint;
	private String targetName;
	private String targetAddress;

	private double nowLatitude;// 目前位置纬度
	private double nowLongtitude;// 目前位置经度
	private LatLonPoint nowLatLongpoint;

	private int firstLocation = 1;// 首先定位到目的地

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ju_guidance);
		mMapView = (MapView) findViewById(R.id.map_guidance);
		mMapView.onCreate(savedInstanceState);

		initView();

		init(getIntent());
	}

	private void initView() {
		// TODO Auto-generated method stub
		mBackBtn = (LinearLayout) findViewById(R.id.id_common_top_backBtn_Lay);
		mBackBtn.setOnClickListener(this);

		mGotoTarget = (RelativeLayout) findViewById(R.id.id_common_top_right_txt_Lay);
		mGotoTarget.setOnClickListener(this);
		mGotoTarget.setVisibility(View.VISIBLE);

		mGotoTargetTxt = (TextView) findViewById(R.id.id_common_top_right_txt);
		mGotoTargetTxt.setVisibility(View.VISIBLE);
		
		mGotoTargetImageview = (ImageView) findViewById(R.id.id_gotoTarget_Icon);
		mGotoTargetImageview.setOnClickListener(this);
		mGotoTargetImageview.setVisibility(View.VISIBLE);

		mTopTxt = (Button) findViewById(R.id.id_common_top_txtBtn);
//		mTopTxt.setText("导航");
	}

	/**
	 * 初始化地图对象
	 */
	private void init(Intent intent) {
		// TODO Auto-generated method stub
		type = intent.getIntExtra("type", 0);
		if (type == 1) {
			// 会议签到
			mGotoTargetTxt.setText("会议签到");
			meetingId = intent.getStringExtra("meetingId");
		}
		targetAddress = intent.getStringExtra("address");
		// targetName = intent.getStringExtra("name");
		targetLatitude = intent.getDoubleExtra("latitude", 30.31854);
		targetLongtitude = intent.getDoubleExtra("longitude", 120.347);
		if (aMap == null) {
			aMap = mMapView.getMap();
			aMap.setMapType(AMap.MAP_TYPE_NORMAL);
			setUpMap();
		}

	}

	/**
	 * 地图设置
	 */
	private void setUpMap() {
		// TODO Auto-generated method stub
		targetLatLonPoint = new LatLonPoint(targetLatitude, targetLongtitude);
		// nowLatLongpoint = new LatLonPoint(nowLatitude, nowLongtitude);
		// 设置地图监听器
		setUpMapListener();
		// 电子围栏回调intent
		Intent intent = new Intent(GEOFENCE_BROADCAST_ACTION);
		mPendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0,
				intent, 0);

		// 注册reciever
		IntentFilter intentFilter = new IntentFilter(
				ConnectivityManager.CONNECTIVITY_ACTION);
		intentFilter.addAction(GEOFENCE_BROADCAST_ACTION);
		registerReceiver(mGeoFenceReciever, intentFilter);

		// 自定义系统蓝点
		MyLocationStyle myLocationStyle = new MyLocationStyle();
		// 自定义定位蓝点图标
		myLocationStyle.myLocationIcon(BitmapDescriptorFactory
				.fromResource(R.drawable.location_arrow));
		// 自定义精度范围的圆形边框颜色
		myLocationStyle.strokeColor(Color.WHITE);
		// 自定义精度范围的圆形边框宽度
		myLocationStyle.strokeWidth(1);
		aMap.setMyLocationStyle(myLocationStyle);

		// 设置地图的一些属性
		aMap.getUiSettings().setZoomControlsEnabled(false);
		aMap.getUiSettings().setLogoPosition(
				AMapOptions.LOGO_POSITION_BOTTOM_RIGHT);
		aMap.setLocationSource(this);
		aMap.getUiSettings().setMyLocationButtonEnabled(true);
		aMap.setMyLocationEnabled(true);
		aMap.setMyLocationType(AMap.LOCATION_TYPE_MAP_FOLLOW);
		// aMap.stopAnimation();
		// 设置缩放级别
		aMap.moveCamera(CameraUpdateFactory.zoomTo(16));
		// 设置目的地标记
		setTargetMarker();

	}

	private BroadcastReceiver mGeoFenceReciever = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			// 接受广播
			if (intent.getAction().equals(GEOFENCE_BROADCAST_ACTION)) {
				Bundle bundle = intent.getExtras();
				try {
					int status = bundle.getInt("status");
					if (status == 0) {
//						Utils.toast("不在区域");
					} else {
						is_inTarget = true;
//						Utils.toast("在区域");
					}

				} catch (Exception e) {
					// TODO: handle exception
					Logger.e("[JuGuidece]->" + e.getMessage());
				}
			}
		}

	};

	/**
	 * 设置地图监听器
	 */
	private void setUpMapListener() {
		// TODO Auto-generated method stub
		// 地图点击监听器
		aMap.setOnMapClickListener(this);
		// 标记点击监听器
		aMap.setOnMarkerClickListener(this);
		aMap.setInfoWindowAdapter(this);
		aMap.setOnInfoWindowClickListener(this);
		// 路径导航监听器
		mRouteSearch = new RouteSearch(this);
		mRouteSearch.setRouteSearchListener(this);
		// 区域视图移动监听器

	}

	/**
	 * 设置目的地标记
	 */
	private void setTargetMarker() {
		// TODO Auto-generated method stub
		MarkerOptions markerOptions = new MarkerOptions();
		// 设置marker的图标样式
		// markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.target_marker));
		// 设置marker点击之后显示的标题
		markerOptions.title(targetAddress);
		// 设置marker的坐标
		markerOptions.position(new LatLng(targetLatitude, targetLongtitude));
		// 设置marker的可见性
		markerOptions.visible(true);
		// 设置marker是否可以被拖拽
		markerOptions.draggable(false);
		markerOptions.snippet("这是地址信息");
		// 将marker添加到地图上去
		Marker marker = aMap.addMarker(markerOptions);
		marker.showInfoWindow();
		aMap.moveCamera(CameraUpdateFactory.changeLatLng(new LatLng(
				targetLatitude, targetLongtitude)));
		setGeoFenceAlert(new LatLng(targetLatitude, targetLongtitude));
	}

	/**
	 * 判断当前位置是否在目的地范围内
	 */
	// private boolean is_BoungsContain(LatLng nowLatLng) {
	// mAMapLocationManager.addGeoFenceAlert(nowLatLng.latitude,
	// nowLatLng.longitude, 100, 6 * 1000, null);
	// return false;
	// }

	// ***********************************marker回调方法***************************//
	@Override
	public boolean onMarkerClick(Marker marker) {
		// TODO Auto-generated method stub、
		return false;
	}

	@Override
	public View getInfoWindow(Marker marker) {
		// TODO Auto-generated method stub
		View infoWindow = getLayoutInflater().inflate(
				R.layout.marker_infowindow, null);
		TextView info = (TextView) infoWindow.findViewById(R.id.id_marker_info);
		LinearLayout gotoNavi = (LinearLayout) infoWindow
				.findViewById(R.id.id_marker_goto_target_lay);
		String infoString = marker.getTitle();
		if (TextUtils.isEmpty(infoString)) {
			infoString = "没有地址信息";
		}
		info.setText(infoString);
		gotoNavi.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// Utils.toast("开始导航");
				mGotoMapPopupWindow = new gotoMapPopupWindow(JuGuidanceActivity.this,
						itemsOnClick);
				// 显示窗口
				View mView = LayoutInflater.from(JuGuidanceActivity.this).inflate(
						R.layout.activity_ju_guidance, null);
				mGotoMapPopupWindow.showAtLocation(mView, Gravity.BOTTOM
						| Gravity.CENTER_HORIZONTAL, 0, 0); // 设置layout在PopupWindow中显示的位置

			}
		});

		// marker.showInfoWindow();

		return infoWindow;

	}

	@Override
	public void onInfoWindowClick(Marker marker) {
		// TODO Auto-generated method stub

	}

	@Override
	public View getInfoContents(Marker arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	// 为弹出窗口实现监听类
	private OnClickListener itemsOnClick = new OnClickListener() {

		public void onClick(View v) {

			mGotoMapPopupWindow.dismiss();

			int id = v.getId();
			switch (id) {
			case R.id.id_goto_baidu:
				// Utils.toast("百度地图");
				try {
					Intent intent = Intent
							.getIntent("intent://map/direction?origin=latlng:"
									+ nowLatitude
									+ ","
									+ nowLongtitude
									+ "&destination=latlng:"
									+ targetLatitude
									+ ","
									+ targetLongtitude
									+ "&mode=driving&src=huiyouxing#Intent;scheme=bdapp;package=com.baidu.BaiduMap;end");
					startActivity(intent);
				} catch (URISyntaxException e1) {
					// TODO Auto-generated catch block
					Utils.toast("请安装百度地图");
					Logger.e(e1.getMessage());

				} catch (Exception e) {
					// TODO: handle exception
					Logger.e(e.getMessage());
					Utils.toast("请安装百度地图");

				}
				break;
			case R.id.id_goto_gaode:
				// Utils.toast("高德地图");
				Intent gaodeIntent = new Intent(
						"android.intent.action.VIEW",
						android.net.Uri
								.parse("androidamap://navi?sourceApplication=huiyouxing&"
										+ "lat="
										+ targetLatitude
										+ "&lon="
										+ targetLongtitude + "&dev=1&style=2"));
				gaodeIntent.setPackage("com.autonavi.minimap");
				try {
					startActivity(gaodeIntent);
				} catch (Exception e) {
					// TODO: handle exception
					Logger.e(e.getMessage());
					Utils.toast("请安装高德地图");
				}
				break;
			case R.id.id_goto_local:
				// Utils.toast("本地导航");
				final RouteSearch.FromAndTo fromAndTo = new RouteSearch.FromAndTo(
						nowLatLongpoint, targetLatLonPoint);
				DriveRouteQuery query = new DriveRouteQuery(fromAndTo,
						drivingMode, null, null, "");// 第一个参数表示路径规划的起点和终点，第二个参数表示驾车模式，第三个参数表示途经点，第四个参数表示避让区域，第五个参数表示避让道路
				mRouteSearch.calculateDriveRouteAsyn(query);// 异步路径规划驾车模式查询

				break;
			default:
				break;
			}

		}

	};

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.id_common_top_backBtn_Lay:
			this.finish();
			break;
		case R.id.id_common_top_right_txt_Lay:
			if (type == 0) {
				// 到目的地
				aMap.moveCamera(CameraUpdateFactory.changeLatLng(new LatLng(
						targetLatitude, targetLongtitude)));
			} else {
				// 会议签到【方法】
				if (is_inTarget) {
					MeetingService.checkMeetingIn(meetingId, ChatService.getSelfId(),"map");
				}else {
					Utils.toast("不在范围内不能签到!");	
				}
				
			}

			break;
		case R.id.id_gotoTarget_Icon:
			aMap.moveCamera(CameraUpdateFactory.changeLatLng(new LatLng(
					targetLatitude, targetLongtitude)));
			break;

		default:
			break;
		}
	}

	/**
	 * 激活定位
	 */
	@Override
	public void activate(OnLocationChangedListener listener) {
		// TODO Auto-generated method stub
		mListener = listener;
		if (mAMapLocationManager == null) {
			mAMapLocationManager = LocationManagerProxy.getInstance(this);
			// 此方法为每隔固定时间会发起一次定位请求，为了减少电量消耗或网络流量消耗，
			// 注意设置合适的定位时间的间隔，并且在合适时间调用removeUpdates()方法来取消定位请求
			// 在定位结束后，在合适的生命周期调用destroy()方法     
			// 其中如果间隔时间为-1，则定位只定一次
			mAMapLocationManager.requestLocationData(
					LocationProviderProxy.AMapNetwork, -1, 1, this);
			// 设置GPS获取位置
			mAMapLocationManager.setGpsEnable(true);

		}

	}

	/**
	 * 停止定位
	 */
	@Override
	public void deactivate() {
		// TODO Auto-generated method stub
		mListener = null;
		if (mAMapLocationManager != null) {
			mAMapLocationManager.removeUpdates(this);
			mAMapLocationManager.destroy();
		}
		mAMapLocationManager = null;
	}

	// ***********************************重写父类方法***************************//
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		mMapView.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		mMapView.onPause();
		// 删除定位
		deactivate();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		mMapView.onSaveInstanceState(outState);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		mMapView.onDestroy();
		unregisterReceiver(mGeoFenceReciever);
	}

	// ***********************************OnLocationChangedListener（定位）回调方法***************************//
	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	/**
	 * 定位成功后回调函数
	 */
	@Override
	public void onLocationChanged(AMapLocation aMapLocation) {
		// TODO Auto-generated method stub
		if (mListener != null && aMapLocation != null) {
			if (aMapLocation.getAMapException().getErrorCode() == 0) {
				mListener.onLocationChanged(aMapLocation);// 显示系统小蓝点
			}

			nowLatitude = aMapLocation.getLatitude();
			nowLongtitude = aMapLocation.getLongitude();
			nowLatLongpoint = new LatLonPoint(nowLatitude, nowLongtitude);
			if (firstLocation == 1) {
				aMap.moveCamera(CameraUpdateFactory.changeLatLng(new LatLng(
						targetLatitude, targetLongtitude)));
				firstLocation = 0;
			}
			// aMap.setMyLocationRotateAngle(aMapLocation.getBearing());
		}

	}

	// ***********************************路径导航回调方法***************************//

	@Override
	public void onBusRouteSearched(BusRouteResult arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onDriveRouteSearched(DriveRouteResult result, int rCode) {
		// TODO Auto-generated method stub
		if (rCode == 0) {
			if (result != null && result.getPaths() != null
					&& result.getPaths().size() > 0) {
				driveRouteResult = result;
				DrivePath drivePath = driveRouteResult.getPaths().get(0);
				// aMap.clear();//清理之前的图标
				DrivingRouteOverlay drivingRouteOverlay = new DrivingRouteOverlay(
						this, aMap, drivePath, driveRouteResult.getStartPos(),
						driveRouteResult.getTargetPos());
				drivingRouteOverlay.removeFromMap();
				drivingRouteOverlay.addToMap();
				drivingRouteOverlay.zoomToSpan();

			} else {
				Utils.toast("没有查询结果");
			}

		} else {
			Utils.toast(getString(R.string.pleaseCheckNetwork));
		}

	}

	@Override
	public void onWalkRouteSearched(WalkRouteResult arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onMapClick(LatLng latLng) {
		// TODO Auto-generated method stub

		// try {
		// Intent intent = new Intent(GEOFENCE_BROADCAST_ACTION);
		// mPendingIntent.send(getApplicationContext(),0,intent);
		// } catch (CanceledException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
	}

	/**
	 * 设置电子围栏位置
	 * 
	 * @param latLng
	 */
	private void setGeoFenceAlert(LatLng latLng) {
		mAMapLocationManager.removeGeoFenceAlert(mPendingIntent);
		if (mCircle != null) {
			mCircle.remove();
		}
		mAMapLocationManager.addGeoFenceAlert(latLng.latitude,
				latLng.longitude, 100, 6 * 1000, mPendingIntent);

		CircleOptions circleOptions = new CircleOptions();
		circleOptions.center(latLng).radius(100)
				.fillColor(Color.argb(180, 224, 171, 10))
				.strokeColor(Color.RED);

		mCircle = aMap.addCircle(circleOptions);
	}

}
