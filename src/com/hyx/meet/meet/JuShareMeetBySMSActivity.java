package com.hyx.meet.meet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.hyx.meet.R;
import com.hyx.meet.adapter.JuCheckContactAdapter;
import com.hyx.meet.custome.EnLetterView;
import com.hyx.meet.entity.JuContact;
import com.hyx.meet.service.JuShareService;
import com.hyx.meet.utils.CharacterParser;
import com.hyx.meet.utils.PinyinComparator;
import com.hyx.meet.utils.Utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * 选择通讯录联系人分享会议
 * 
 * @author ww
 *
 */
public class JuShareMeetBySMSActivity extends Activity implements OnItemClickListener,
		OnClickListener {

	ListView mContactListView;
	JuCheckContactAdapter mAdapter;
	private static  List<JuContact> mData;
	ArrayList<String> mSelectedNumber;
	ArrayList<String> mSelectedName;
	ArrayList<JuContact> mSelectContacts;
	LinearLayout mBackLayout;
	RelativeLayout mConfirm;
	LinearLayout mCheckAll;
	ImageView mCheckAllImg;
	TextView mCheckedNum;
	static boolean is_checkAll = false;
	private EnLetterView rightLetter;
	private TextView dialog;
	static CharacterParser characterParser;
	static PinyinComparator pinyinComparator;
	Intent fromIntent;
	static final int SELECT_CONTACTS = 0;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ju_share_meet_by_sms);
		characterParser = CharacterParser.getInstance();
		pinyinComparator = new PinyinComparator();
		
		fromIntent = getIntent();

		initView();
		// getContacts();
		fillFriendsData(JuShareService.getContact());
	}

	/**
	 * 初始化视图
	 */
	private void initView() {
		// TODO Auto-generated method stub
		mSelectedNumber = new ArrayList<String>();
		mSelectedName = new ArrayList<String>();
		mSelectContacts = new ArrayList<JuContact>();

		mContactListView = (ListView) findViewById(R.id.id_contact_list);
		mData = new ArrayList<JuContact>();
		mAdapter = new JuCheckContactAdapter(this, mData);
		mContactListView.setAdapter(mAdapter);

		mConfirm = (RelativeLayout) findViewById(R.id.id_checkContact_confirm_Lay);
		mBackLayout = (LinearLayout) findViewById(R.id.id_checkContact_backBtn_Lay);

		mCheckAll = (LinearLayout) findViewById(R.id.id_contact_checkAll);
		mCheckAllImg = (ImageView) findViewById(R.id.id_contaxt_checkAll_icon);
		mCheckedNum = (TextView) findViewById(R.id.id_contact_checked_num);

		mContactListView.setOnItemClickListener(this);
		mCheckAll.setOnClickListener(this);
		mConfirm.setOnClickListener(this);
		mBackLayout.setOnClickListener(this);
		
		initRightLetterView();
	}

	/**
	 * 初始化右边导航
	 */
	private void initRightLetterView() {
		// TODO Auto-generated method stub
		rightLetter = (EnLetterView) findViewById(R.id.id_checkContatc_right_letter);
		dialog = (TextView) findViewById(R.id.id_checkContact_dialog);
		rightLetter.setTextView(dialog);
		rightLetter
				.setOnTouchingLetterChangedListener(new LetterListViewListener());
	}

	/**
	 * 右边导航监听器
	 * 
	 * @author ww
	 *
	 */
	private class LetterListViewListener implements
			EnLetterView.OnTouchingLetterChangedListener {

		@Override
		public void onTouchingLetterChanged(String s) {
			int position = mAdapter.getPositionForSection(s.charAt(0));
			if (position != -1) {
				mContactListView.setSelection(position);
			}
		}
	}
	
	
	/**
	 * 给数据分组排序（按首字母顺序）
	 * 
	 * @param datas
	 */
	private   void fillFriendsData(List<JuContact> datas) {
		mData.clear();
		int total = datas.size();
		for (int i = 0; i < total; i++) {
			JuContact juContact = datas.get(i);
//			JuContact sortJuContact = new JuContact();
			String username = juContact.getName();
			if (username != null) {
				String pinyin = characterParser.getSelling(juContact
						.getName());
				String sortString = pinyin.substring(0, 1).toUpperCase();
				if (sortString.matches("[A-Z]")) {
					juContact.setSortLetters(sortString.toUpperCase());
				} else {
					juContact.setSortLetters("#");
				}
			} else {
				juContact.setSortLetters("#");
			}
			mData.add(juContact);
		}
		Collections.sort(mData, new Comparator<JuContact>() 
			{
			public int compare(JuContact o1, JuContact o2) {
			    if (o1.getSortLetters().equals("@")
			        || o2.getSortLetters().equals("#")) {
			      return -1;
			    } else if (o1.getSortLetters().equals("#")
			        || o2.getSortLetters().equals("@")) {
			      return 1;
			    } else {
			      return o1.getSortLetters().compareTo(o2.getSortLetters());
			    }
			  }

		});
		mAdapter.setDatas(mData);
		

	}


	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
				if (mAdapter.getItem(position).is_checked()) {
			mSelectContacts.remove(mAdapter.getItem(position));
			mAdapter.getItem(position).set_checked(false);
			mAdapter.notifyDataSetChanged();
			mSelectedName.remove(mAdapter.getItem(position).getName());
			mSelectedNumber.remove(mAdapter.getItem(position).getPhoneNumber());
		} else {
			//避免短号
			if ((mAdapter.getItem(position).getPhoneNumber().replace(" ", "")).length() <= 6) {
				Utils.showCommonFailDialog(this,"很抱歉，系统无法识别此号码!");
			}else {
				mAdapter.getItem(position).set_checked(true);
				mAdapter.notifyDataSetChanged();
				mSelectContacts.add(mAdapter.getItem(position));
				mSelectedName.add(mAdapter.getItem(position).getName());
				mSelectedNumber.add(mAdapter.getItem(position).getPhoneNumber());
			}
			
		}
		//设置已选人数
		setCheckedNum();
	}

	private void setCheckedNum() {
		// TODO Auto-generated method stub
		mCheckedNum.setText("已选" + mSelectContacts.size() + "位联系人");
	}
	
	

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {
		case R.id.id_contact_checkAll:
			if (is_checkAll) {
				cancel_checkAll();				
			} else {
				checkAll();
			}
			break;
		case R.id.id_checkContact_backBtn_Lay:
			finish();
			break;
		case R.id.id_checkContact_confirm_Lay:
			if (mSelectContacts.size()>100) {
				Utils.showCommonFailDialog(this, "一次最多选择100个号码!");
			}else {
				Intent intent  = new Intent();
				intent.putStringArrayListExtra("contacts_number", mSelectedNumber);
				intent.putStringArrayListExtra("contacts_name", mSelectedName);
				intent.putParcelableArrayListExtra("contacts", (ArrayList<? extends Parcelable>) mSelectContacts);
				setResult(RESULT_OK, intent);
				this.finish();

			}
			break;

		default:
			break;
		}

	}

	private void checkAll() {
		// TODO Auto-generated method stub
		//短号识别
		boolean flag = true;
		for (int i = 0; i < mAdapter.getCount(); i++) {
			if ((mAdapter.getItem(i).getPhoneNumber().replaceAll(" ", "")).length() <= 6) {
				flag = false;
				break;
			}
			
		}
		if (flag) {
			is_checkAll = true;
			// 全选
			mSelectContacts.clear();
			mSelectedNumber.clear();
			mSelectedName.clear();
			mCheckAllImg.setImageResource(R.drawable.common_icon_checkmark);
			for (int i = 0; i < mAdapter.getCount(); i++) {
				mAdapter.getItem(i).set_checked(true);
				mSelectContacts.add(mAdapter.getItem(i));
				mSelectedNumber.add(mAdapter.getItem(i).getPhoneNumber());
				mSelectedName.add(mAdapter.getItem(i).getName());
			}
			// 刷新列表
			mAdapter.notifyDataSetChanged();
			setCheckedNum();	
		}else {
			Utils.showCommonFailDialog(this,"很抱歉，号码中有系统无法识别的号码!");
		}

		
	}

	private void cancel_checkAll() {
		// TODO Auto-generated method stub
		is_checkAll = false;
		mSelectContacts.clear();
		mSelectedNumber.clear();
		mSelectedName.clear();
		// 取消全选
		mCheckAllImg.setImageDrawable(null);
		for (int i = 0; i < mAdapter.getCount(); i++) {
			mAdapter.getItem(i).set_checked(false);
		}
		// 刷新列表
		mAdapter.notifyDataSetChanged();
		setCheckedNum();
	}

}
