package com.hyx.meet.meet;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;




import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.location.LocationManagerProxy;
import com.amap.api.location.LocationProviderProxy;
import com.amap.api.maps.AMap;
import com.amap.api.maps.AMap.CancelableCallback;
import com.amap.api.maps.AMap.OnCameraChangeListener;
import com.amap.api.maps.CameraUpdate;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.LocationSource;
import com.amap.api.maps.MapView;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.MyLocationStyle;
import com.amap.api.services.core.AMapException;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.core.PoiItem;
import com.amap.api.services.geocoder.GeocodeAddress;
import com.amap.api.services.geocoder.GeocodeQuery;
import com.amap.api.services.geocoder.GeocodeResult;
import com.amap.api.services.geocoder.GeocodeSearch;
import com.amap.api.services.geocoder.GeocodeSearch.OnGeocodeSearchListener;
import com.amap.api.services.geocoder.RegeocodeAddress;
import com.amap.api.services.geocoder.RegeocodeQuery;
import com.amap.api.services.geocoder.RegeocodeResult;
import com.amap.api.services.help.Inputtips;
import com.amap.api.services.help.Inputtips.InputtipsListener;
import com.amap.api.services.help.Tip;
import com.amap.api.services.poisearch.PoiItemDetail;
import com.amap.api.services.poisearch.PoiResult;
import com.amap.api.services.poisearch.PoiSearch;
import com.amap.api.services.poisearch.PoiSearch.OnPoiSearchListener;
import com.amap.api.services.poisearch.PoiSearch.Query;
import com.amap.api.services.poisearch.PoiSearch.SearchBound;
import com.hyx.meet.R;
import com.hyx.meet.adapter.JuPoisAdapter;
import com.hyx.meet.entity.LocationPoi;
import com.hyx.meet.utils.Logger;
import com.hyx.meet.utils.Utils;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.ListView;

/**
 * 选取地址
 * @author ww
 *
 */
public class JuGetLocationActivity extends Activity implements LocationSource,
		AMapLocationListener, OnGeocodeSearchListener, OnCameraChangeListener,
		OnPoiSearchListener, OnItemClickListener, OnClickListener {

	private MapView mMapview;
	private AMap aMap;
	private ListView mListView;
	private JuPoisAdapter mAdapter;
	private List<LocationPoi> mData;

	private AutoCompleteTextView mSearchLocationEdText;
	private LinearLayout mCancleBtn;
	private LinearLayout mConfirmBtn;
	private LinearLayout mSearchBtn;

	// 所在位置经纬度
	private double mylatitude;
	private double mylongtitude;
	private GeocodeSearch geocodeSearch;
	private PoiSearch poiSearch;
	private Query query;
	private LatLonPoint mylatLonPoint;
	// 地图中心点城市
	private String middleCity = "杭州";
	private LatLonPoint middleLatLonPoint;

	private static  LocationPoi selectLocation;
	private static boolean  is_regeolocation = true;

	// private PoiItem commomPoiItem;

	private OnLocationChangedListener mListener;
	private LocationManagerProxy mAMapLocationManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ju_get_location);
		mMapview = (MapView) findViewById(R.id.map);
		mMapview.onCreate(savedInstanceState);

		initView();

		init();
	}

	private void initView() {
		// TODO Auto-generated method stub
		mCancleBtn = (LinearLayout) findViewById(R.id.id_cancel_getLocation);
		mConfirmBtn = (LinearLayout) findViewById(R.id.id_confirm_getLocation);
		mSearchBtn = (LinearLayout) findViewById(R.id.id_search_location);
		mSearchLocationEdText = (AutoCompleteTextView) findViewById(R.id.id_location_search_editview);
		mListView = (ListView) findViewById(R.id.id_location_poislist);

		mData = new ArrayList<LocationPoi>();
		mAdapter = new JuPoisAdapter(JuGetLocationActivity.this, mData);
		mListView.setAdapter(mAdapter);
		mListView.setOnItemClickListener(this);

		mCancleBtn.setOnClickListener(this);
		mConfirmBtn.setOnClickListener(this);
		mSearchBtn.setOnClickListener(this);
		mSearchLocationEdText.setOnClickListener(this);

		mSearchLocationEdText.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				String newText = s.toString();
				if (newText.equals("")) {
					mSearchBtn.setVisibility(View.GONE);
					mConfirmBtn.setVisibility(View.VISIBLE);
				} else {
					mSearchBtn.setVisibility(View.VISIBLE);
					mConfirmBtn.setVisibility(View.GONE);
				}
				Inputtips inputtips = new Inputtips(JuGetLocationActivity.this,
						new InputtipsListener() {

							@Override
							public void onGetInputtips(List<Tip> tips, int rCode) {
								// TODO Auto-generated method stub
								List<String> list = new ArrayList<String>();
								for (int i = 0; i < tips.size(); i++) {
									list.add(tips.get(i).getName() + "["
											+ tips.get(i).getDistrict() + "]");
								}

								ArrayAdapter<String> adapter = new ArrayAdapter<String>(
										getApplicationContext(),
										R.layout.route_inputs, list);
								mSearchLocationEdText.setAdapter(adapter);
								adapter.notifyDataSetChanged();
																
								

							}
						});
				// 发送输入提示请求
				try {
					inputtips.requestInputtips(newText, "");
				} catch (AMapException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

	}

	// 初始化
	private void init() {
		// TODO Auto-generated method stub
		if (aMap == null) {
			aMap = mMapview.getMap();
			aMap.setMapType(AMap.MAP_TYPE_NORMAL);
			setUpMap();
		}
		// 查询附近兴趣点
		// regeosearch();

	}

	/**
	 * 查询附近兴趣点[查询]
	 */
	private void regeosearch(LatLonPoint latLonPoint) {
		// TODO Auto-generated method stub

		// 第一个参数表示一个Latlng，第二参数表示范围多少米，第三个参数表示是火系坐标系还是GPS原生坐标系
		RegeocodeQuery query = new RegeocodeQuery(latLonPoint, 200,
				GeocodeSearch.GPS);
		geocodeSearch.getFromLocationAsyn(query);
	}

	/***
	 * 查询附近兴趣点
	 */
	private void geosearch(String query, String city) {
		GeocodeQuery query2 = new GeocodeQuery(query, city);
		geocodeSearch.getFromLocationNameAsyn(query2);
	}

	// 设置地图
	private void setUpMap() {
		// TODO Auto-generated method stub
		geocodeSearch = new GeocodeSearch(this);
		geocodeSearch.setOnGeocodeSearchListener(this);

		aMap.setOnCameraChangeListener(this);

		// 自定义系统蓝点
		MyLocationStyle myLocationStyle = new MyLocationStyle();
		// 自定义定位蓝点图标
		myLocationStyle.myLocationIcon(BitmapDescriptorFactory
				.fromResource(R.drawable.location_arrow));
		// 自定义精度范围的圆形边框颜色
		myLocationStyle.strokeColor(Color.WHITE);
		// 自定义精度范围的圆形边框宽度
		myLocationStyle.strokeWidth(1);
		aMap.setMyLocationStyle(myLocationStyle);

		aMap.getUiSettings().setZoomControlsEnabled(false);
		aMap.setLocationSource(this);
		aMap.getUiSettings().setMyLocationButtonEnabled(true);
		aMap.setMyLocationEnabled(true);
		aMap.setMyLocationType(AMap.LOCATION_TYPE_LOCATE);
//		设置缩放级别
		aMap.moveCamera(CameraUpdateFactory.zoomTo(16));

	}

	// ***********************************重写父类方法***************************//
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		mMapview.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		mMapview.onPause();
		// 删除定位
		deactivate();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		mMapview.onSaveInstanceState(outState);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		mMapview.onDestroy();
	}

	/**
	 * 激活定位
	 * 
	 * @param arg0
	 */
	@Override
	public void activate(OnLocationChangedListener listener) {
		// TODO Auto-generated method stub
		mListener = listener;
		if (mAMapLocationManager == null) {
			mAMapLocationManager = LocationManagerProxy.getInstance(this);
			// 此方法为每隔固定时间会发起一次定位请求，为了减少电量消耗或网络流量消耗，
			// 注意设置合适的定位时间的间隔，并且在合适时间调用removeUpdates()方法来取消定位请求
			// 在定位结束后，在合适的生命周期调用destroy()方法     
			// 其中如果间隔时间为-1，则定位只定一次
			mAMapLocationManager.requestLocationData(
					LocationProviderProxy.AMapNetwork, -1, 1, this);
			// 设置GPS获取位置
			mAMapLocationManager.setGpsEnable(true);

		}

	}

	/**
	 * 停止定位
	 */
	@Override
	public void deactivate() {
		// TODO Auto-generated method stub
		mListener = null;
		if (mAMapLocationManager != null) {
			mAMapLocationManager.removeUpdates(this);
			mAMapLocationManager.destroy();
		}
		mAMapLocationManager = null;
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)定位成功后回调函数
	 * 
	 * @see
	 * com.amap.api.location.AMapLocationListener#onLocationChanged(com.amap
	 * .api.location.AMapLocation)
	 */
	@Override
	public void onLocationChanged(AMapLocation aMapLocation) {
		// TODO Auto-generated method stub
		if (mListener != null && aMapLocation != null) {
			if (aMapLocation.getAMapException().getErrorCode() == 0) {
				mListener.onLocationChanged(aMapLocation);// 显示系统小蓝点
			}

			// 所在位置的经纬度
			mylatitude = aMapLocation.getLatitude();
			mylongtitude = aMapLocation.getLongitude();
			mylatLonPoint = new LatLonPoint(mylatitude, mylongtitude);

		}

	}

	// 地理编码回调方法
	@Override
	public void onGeocodeSearched(GeocodeResult geocodeResult, int code) {
		// TODO Auto-generated method stub
		if (code == 0) {
			if (geocodeResult != null
					&& geocodeResult.getGeocodeAddressList() != null
					&& geocodeResult.getGeocodeAddressList().size() > 0) {

				GeocodeAddress geocodeAddress = geocodeResult
						.getGeocodeAddressList().get(0);
				LatLonPoint latLonPoint = geocodeAddress.getLatLonPoint();
				LatLng latLng = new LatLng(latLonPoint.getLatitude(), latLonPoint.getLongitude());
				moveCamera(CameraUpdateFactory.changeLatLng(latLng), null,true);
				
				mSearchBtn.setVisibility(View.GONE);
				mConfirmBtn.setVisibility(View.VISIBLE);
				mSearchLocationEdText.clearFocus();


			} else {
				Utils.toast("无查询结果");
			}
		} else {
			Utils.toast("请检查网络");

		}

	}

	// 逆地理编码回调方法
	@Override
	public void onRegeocodeSearched(RegeocodeResult result, int rCode) {
		// TODO Auto-generated method stub
		if (rCode == 0) {
			if (result != null && result.getRegeocodeAddress() != null
					&& result.getRegeocodeAddress().getFormatAddress() != null) {
				RegeocodeAddress regeocodeAddress = result.getRegeocodeAddress();
				String addressName = regeocodeAddress.getFormatAddress();
//				当前选择的地址
				LocationPoi locationPoi  = new LocationPoi();
				locationPoi.setName("[位置]");
				locationPoi.setLatitude(middleLatLonPoint.getLatitude());
				locationPoi.setLongtitude(middleLatLonPoint.getLongitude());
				locationPoi.setAddress(addressName);
				locationPoi.setDistrict(addressName);
				
				selectLocation = locationPoi;
				
				middleCity = result.getRegeocodeAddress().getCityCode();
				query = new PoiSearch.Query("", "", middleCity);
				query.setPageSize(25);
				query.setPageNum(0);
				;
				poiSearch = new PoiSearch(this, query);
				poiSearch.setBound(new SearchBound(middleLatLonPoint, 1000));

				poiSearch.setOnPoiSearchListener(this);
				poiSearch.searchPOIAsyn();
				
			} else {
				Utils.toast("查询不到该位置");
			}
		} else {
			Utils.toast("请检查网络");
		}

	}

	/**
	 * 转换对象
	 * @param poiItem
	 * @return
	 */
	private LocationPoi poiItem2LocationPoi(PoiItem poiItem) {
		// TODO Auto-generated method stub
		LocationPoi locationPoi = new LocationPoi();
		locationPoi.setName(poiItem.toString());
		locationPoi.setAddress(poiItem.getSnippet());
		locationPoi.setDistrict(poiItem.getProvinceName()
				+ poiItem.getCityName() + poiItem.getAdName()+poiItem.getSnippet());

		locationPoi.setLatitude(poiItem.getLatLonPoint().getLatitude());
		locationPoi.setLongtitude(poiItem.getLatLonPoint().getLongitude());

		return locationPoi;

	}

	@Override
	public void onCameraChange(CameraPosition cameraPosition) {
		// TODO Auto-generated method stub

	}

	/**
	 * 可是区域移动后回调函数
	 */
	@Override
	public void onCameraChangeFinish(CameraPosition cameraPosition) {
		// TODO Auto-generated method stub
		LatLng latLng = cameraPosition.target;
		double latitude = latLng.latitude;
		double longtitude = latLng.longitude;
		middleLatLonPoint = new LatLonPoint(latitude, longtitude);
		if (is_regeolocation) {
			regeosearch(middleLatLonPoint);			
		}else {
			is_regeolocation = true;
		}

	}

	@Override
	public void onPoiItemDetailSearched(PoiItemDetail arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPoiSearched(PoiResult result, int rCode) {
		// TODO Auto-generated method stub
		if (rCode == 0) {
			if (result != null && result.getQuery() != null) {
				if (result.getQuery().equals(query)) {
					List<PoiItem> poiItems = result.getPois();
					if (poiItems != null && poiItems.size() > 0) {
						mData.clear();
						//当前位置
//						selectLocation.setIs_select(true);
						mData.add(selectLocation);
						for (Iterator iterator = poiItems.iterator(); iterator
								.hasNext();) {
							PoiItem poiItem = (PoiItem) iterator.next();
							Logger.d("poisaddress->"
									+ poiItem.getProvinceName()
									+ poiItem.getCityName()
									+ poiItem.getAdName()
									+ poiItem.getSnippet());

							LocationPoi locationPoi = new LocationPoi();
							locationPoi = poiItem2LocationPoi(poiItem);

							mData.add(locationPoi);

						}
						mAdapter.notifyDataSetChanged();
						mListView.setSelection(0);
					}
				}

			} else {
				Utils.toast("没有搜索结果");
			}
		} else {
			Utils.toast("网络错误");

		}

	}

	/**
	 * 移动可视区域
	 * 
	 * @param update
	 * @param callback
	 */
	public void moveCamera(CameraUpdate update, CancelableCallback callback,boolean is_regeolocation) {
		this.is_regeolocation = is_regeolocation;
		aMap.moveCamera(update);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		LocationPoi locationPoi = mAdapter.getItem(position);
//		当前选择的地址
		selectLocation = locationPoi;
		double latitude = locationPoi.getLatitude();
		double longtitude = locationPoi.getLongtitude();
		LatLng latLng = new LatLng(latitude, longtitude);
		// 根据中心点改变可视区域
		moveCamera(CameraUpdateFactory.changeLatLng(latLng), null,false);
		view.setSelected(true);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.id_cancel_getLocation:
			this.finish();
			break;
		case R.id.id_confirm_getLocation:
//			Utils.toast(selectLocation.getAddress());
			Intent intent = getIntent();
			Bundle bundle = new Bundle();
			bundle.putString("location", selectLocation.toJsonString());
			intent.putExtras(bundle);
			setResult(RESULT_OK, intent);
			this.finish();
			break;
		case R.id.id_search_location:
			geosearch(mSearchLocationEdText.getText().toString(), "");
			break;
		case R.id.id_location_search_editview:

			break;

		default:
			break;
		}
	}

}
