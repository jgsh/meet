package com.hyx.meet.chat;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.view.ViewPager;
import android.text.Selection;
import android.text.Spannable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.Group;
import com.avos.avoscloud.im.v2.AVIMClient;
import com.avos.avoscloud.im.v2.AVIMConversation;
import com.avos.avoscloud.im.v2.callback.AVIMConversationCreatedCallback;
import com.hyx.meet.Application.App;
import com.hyx.meet.adapter.ChatMsgAdapter;
import com.hyx.meet.adapter.EmotionGridAdapter;
import com.hyx.meet.adapter.EmotionPagerAdapter;
import com.hyx.meet.avobject.ChatGroup;
import com.hyx.meet.avobject.User;
import com.hyx.meet.custome.EmotionEditText;
import com.hyx.meet.custome.RecordButton;
import com.hyx.meet.custome.xlist.XListView;
import com.hyx.meet.db.DBHelper;
import com.hyx.meet.db.DBMsg;
import com.hyx.meet.entity.Msg;
import com.hyx.meet.entity.MsgBuilder;
import com.hyx.meet.entity.RoomType;
import com.hyx.meet.entity.SendCallback;
import com.hyx.meet.R;
import com.hyx.meet.launcher.JuLauncherActivity;
import com.hyx.meet.login.JuLoginActivity;
import com.hyx.meet.meet.JuMainActivity;
import com.hyx.meet.service.CacheService;
import com.hyx.meet.service.ChatService;
import com.hyx.meet.service.JuChatServiceV2;
import com.hyx.meet.service.MeetingService;
import com.hyx.meet.service.MsgAgent;
import com.hyx.meet.service.UserService;
import com.hyx.meet.service.listener.MsgListener;
import com.hyx.meet.service.reciever.JuMSGHandlerV2;
import com.hyx.meet.service.reciever.JuPushReciever;
import com.hyx.meet.utils.AVOSUtils;
import com.hyx.meet.utils.ChatUtils;
import com.hyx.meet.utils.EmotionUtils;
import com.hyx.meet.utils.Logger;
import com.hyx.meet.utils.NetAsyncTask;
import com.hyx.meet.utils.PathUtils;
import com.hyx.meet.utils.PhotoUtil;
import com.hyx.meet.utils.SimpleTextWatcher;
import com.hyx.meet.utils.Utils;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;
/**
 * 聊天界面
 * @author ww
 *
 */
public class JuChatActivity extends Activity implements OnClickListener,
		MsgListener, XListView.IXListViewListener, OnItemClickListener {

	private static final int IMAGE_REQUEST = 0;
	public static final int LOCATION_REQUEST = 1;
	private static final int TAKE_CAMERA_REQUEST = 2;
	public static final int PAGE_SIZE = 20;
	static String RETRY_ACTION = "com.avoscloud.chat.RETRY_CONNECT";
	private String localCameraPath = PathUtils.getTmpPath();

	public static JuChatActivity ctx;

	private Button mTopTxtBtn;
	private EmotionEditText contentEdit;
	private RecordButton recordBtn;
	private Button sendBtn, turnToAudioBtn;
	private ViewPager emotionPager;

	LinearLayout chatBottomLayout;
	MsgAgent msgAgent;
	User curUser;
	String fromUsername;
	String chatUserId;// 当前用户聊天对象id
	DBHelper dbHelper;

	View chatTextLayout, chatAudioLayout, chatAddLayout, chatEmotionLayout;
	View turnToTextBtn, addImageBtn, showAddBtn, addLocationBtn,
			showEmotionBtn, addCameraBtn;

	private String audioId;
	public static RoomType roomType;
	public static final String CHAT_USER_ID = "chatUserId";
	public static final String GROUP_ID = "groupId";
	public static final String ROOM_TYPE = "roomType";
	User chatUser;
	Group group;
	int msgSize;

	private ChatMsgAdapter adapter;
	private List<Msg> msgs = new ArrayList<Msg>();
	private XListView xListView;
	// private Button mBackBtn;
	private LinearLayout mBackBtnLay;

	AVIMConversation mAvImConversation;
	AudioManager am;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ju_chat);
		am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		ctx = this;
		check_isLogin();

	}

	int ConversationType_OneOne = 0; // 两个人之间的单聊
	int ConversationType_Group = 1; // 多人之间的群聊

	/**
	 * 创建对话
	 * 
	 * @param avimClient
	 */
	private void creatConversation(AVIMClient avimClient, List<String> userIds) {
		// TODO Auto-generated method stub
		// 我们给对话增加一个自定义属性 type，表示单聊还是群聊
		// 常量定义：
		Map<String, Object> attr = new HashMap<String, Object>();
		attr.put("type", ConversationType_OneOne);

		avimClient.createConversation(userIds, attr,
				new AVIMConversationCreatedCallback() {

					@Override
					public void done(AVIMConversation avimConversation,
							AVException e) {
						// TODO Auto-generated method stub
						if (e != null) {
							JuChatActivity.this.finish();
						} else {
							if (null != avimConversation) {
								mAvImConversation = avimConversation;
								// loadMsgsFromDB(true);// 初始化聊天记录
							} else {
								// JuChatActivity.this.finish();
								// loadMsgsFromDB(true);
								Utils.toast("网络连接失败!");
								Logger.d("crateConversation" + e.getMessage());
							}
						}

					}
				});

	}

	/**
	 * 检查是否已登陆过 ww
	 */
	private void check_isLogin() {
		// TODO Auto-generated method stub
		if (AVUser.getCurrentUser() != null) {
			initView();
			initByIntent(getIntent());
		} else {
			Intent intent = new Intent(JuChatActivity.this,
					JuLoginActivity.class);
			startActivity(intent);
			JuChatActivity.this.finish();

		}

	}

	/**
	 * 初始化各种控件以及数据
	 */
	private void initByIntent(Intent intent) {
		// TODO Auto-generated method stub
		mTopTxtBtn.setText(intent.getStringExtra("toUserName"));

		initEmotionPager();// 初始化表情菜单
		setEditTextChangeListener();// 添加输入框监听事件

		setSoftInputMode();
		ChatService.cancelNotification(ctx);// 取消通知
		initListView();// 初始化listview
		initData(intent);//初始化聊天数据
		ChatService.cancelNotification(ctx);// 取消通知
		initRecordBtn();// 初始化录音按钮
		// Logger.d(EmotionUtils.emotionCodes.length + " len");

	}

	private void initListView() {
		// TODO Auto-generated method stub
		adapter = new ChatMsgAdapter(ctx, msgs);
		adapter.setDatas(msgs);
		xListView.setAdapter(adapter);
		xListView.setPullRefreshEnable(true);
		xListView.setPullLoadEnable(false);
		xListView.setXListViewListener(this);
		xListView.setOnScrollListener(new PauseOnScrollListener(
				UserService.imageLoader, true, true));
		xListView.setOnItemClickListener(this);

	}

	public void initData(Intent intent) {
		curUser = User.curUser();
		fromUsername = curUser.getString("realName");
		dbHelper = new DBHelper(ctx, App.DB_NAME, App.DB_VER);
		int roomTypeInt = intent.getIntExtra(ROOM_TYPE,
				RoomType.Single.getValue());
		roomType = RoomType.fromInt(roomTypeInt);
		msgSize = PAGE_SIZE;
		if (roomType == RoomType.Single) {
			chatUserId = intent.getStringExtra(CHAT_USER_ID);
			if (Utils.isNetworkConnected(this)) {
				// Session session = ChatService.getSession();
				// if (session.isWatching(chatUserId)) {
				// // Toast.makeText(App.ctx, "watched", 0).show();
				// Logger.d("watched");
				//
				// } else {
				// ChatService.watchById(chatUserId);
				//
				// }
				// 创建对话
				List<String> chatUserIds = new ArrayList<String>();
				chatUserIds.add(chatUserId);
				chatUserIds.add(ChatService.getSelfId());
				//打开会话
				creatConversation(JuChatServiceV2.getAvimClient(), chatUserIds);

			} else {
				Utils.toast("网络连接失败");
			}
			loadMsgsFromDB(true);// 初始化聊天记录
			msgAgent = new MsgAgent(roomType, chatUserId, fromUsername);
		} else if (roomType == RoomType.NOTIFY) {
			chatUserId = intent.getStringExtra(CHAT_USER_ID);
			msgAgent = new MsgAgent(roomType, chatUserId, fromUsername);
			loadMsgsFromDB(true);// 初始化聊天记录
		} else {
			// String groupId = intent.getStringExtra(GROUP_ID);
			// Session session = ChatService.getSession();

			// group = session.getGroup(groupId);
			// ChatGroup chatGroup = CacheService.lookupChatGroup(groupId);
			// CacheService.setCurrentChatGroup(chatGroup);
			// msgAgent = new MsgAgent(roomType, groupId);
		}
	}

	public static Intent getUserChatIntent(Context ctx, String userId,
			String username) {
		Intent intent = new Intent(ctx, JuChatActivity.class);
		intent.putExtra(CHAT_USER_ID, userId);
		intent.putExtra(ROOM_TYPE, RoomType.Single.getValue());
		intent.putExtra("toUserName", username);
		intent.putExtra("openChat", 1);
		return intent;
	}

	public static Intent getNotifyChatIntent(Context ctx, String userId,
			String username) {
		Intent intent = new Intent(ctx, JuChatActivity.class);
		intent.putExtra(CHAT_USER_ID, userId);
		intent.putExtra(ROOM_TYPE, RoomType.NOTIFY.getValue());
		intent.putExtra("toUserName", username);
		intent.putExtra("openChat", 1);
		return intent;
	}

	public static Intent getNotifyCommonIntent(Context ctx) {
		Intent intent = new Intent(ctx, JuLauncherActivity.class);
		return intent;
	}

	private void setEditTextChangeListener() {
		// TODO Auto-generated method stub
		contentEdit.addTextChangedListener(new SimpleTextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (s.length() > 0) {
					sendBtn.setEnabled(true);
					showSendBtn();
				} else {
					sendBtn.setEnabled(false);
					showTurnToRecordBtn();
				}
				super.onTextChanged(s, start, before, count);
			}
		});

	}

	private void showTurnToRecordBtn() {
		sendBtn.setVisibility(View.GONE);
		turnToAudioBtn.setVisibility(View.VISIBLE);
	}

	private void showSendBtn() {
		sendBtn.setVisibility(View.VISIBLE);
		turnToAudioBtn.setVisibility(View.GONE);
	}

	private void initRecordBtn() {
		// TODO Auto-generated method stub
		setNewRecordPath();
		recordBtn
				.setOnFinishedRecordListener(new RecordButton.RecordEventListener() {
					@Override
					public void onFinishedRecord(final String audioPath,
							final double secs) {
						final String objectId = audioId;
						if (isStateFine()) {

							msgAgent.createAndSendMsg2(
									new MsgAgent.MsgBuilderHelper() {
										@Override
										public void specifyType(
												MsgBuilder msgBuilder) {
											msgBuilder.audio2(objectId, secs);
											// Utils.toast("录音时长"+secs);
										}
									}, sendCallback, mAvImConversation);

						} else {
							Utils.toast(getResources().getString(
									R.string.pleaseCheckNetwork));
						}
						setNewRecordPath();
					}

					@Override
					public void onStartRecord() {
					}
				});

	}

	public void setNewRecordPath() {
		audioId = com.hyx.meet.utils.Utils.uuid();
		String audioPath = com.hyx.meet.utils.PathUtils
				.getAudioFilePath(audioId);
		recordBtn.setSavePath(audioPath);
	}

	/**
	 * 检查网络状态
	 * 
	 * @return
	 */
	public boolean isStateFine() {
		if (mAvImConversation != null) {
			return true;
		} else {
			return false;
		}
	}

	private void initEmotionPager() {
		// TODO Auto-generated method stub
		List<View> views = new ArrayList<View>();
		for (int i = 0; i < 2; i++) {
			views.add(getEmotionGridView(i));
		}
		EmotionPagerAdapter pagerAdapter = new EmotionPagerAdapter(views);
		emotionPager.setAdapter(pagerAdapter);

	}

	private View getEmotionGridView(int pos) {
		LayoutInflater inflater = LayoutInflater.from(ctx);
		View emotionView = inflater.inflate(R.layout.chat_emotion_gridview,
				null);
		GridView gridView = (GridView) emotionView
				.findViewById(R.id.id_emotion_gridview);
		final EmotionGridAdapter emotionGridAdapter = new EmotionGridAdapter(
				ctx);
		List<String> pageEmotions;
		if (pos == 0) {
			pageEmotions = EmotionUtils.emotionTexts1;
		} else {
			pageEmotions = EmotionUtils.emotionTexts2;
		}
		emotionGridAdapter.setDatas(pageEmotions);
		gridView.setAdapter(emotionGridAdapter);
		gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				String emotionText = (String) parent.getAdapter().getItem(
						position);
				int start = contentEdit.getSelectionStart();
				StringBuffer sb = new StringBuffer(contentEdit.getText());
				sb.replace(contentEdit.getSelectionStart(),
						contentEdit.getSelectionEnd(), emotionText);
				contentEdit.setText(sb.toString());
				CharSequence info = contentEdit.getText();
				if (info instanceof Spannable) {
					Spannable spannable = (Spannable) info;
					Selection.setSelection(spannable,
							start + emotionText.length());
				}
			}
		});
		return gridView;
	}

	/**
	 * 初始化界面
	 */
	private void initView() {
		// TODO Auto-generated method stub

		xListView = (XListView) findViewById(R.id.id_chat_listview);
		mBackBtnLay = (LinearLayout) findViewById(R.id.id_common_top_backBtn_Lay);
		mBackBtnLay.setOnClickListener(this);

		mTopTxtBtn = (Button) findViewById(R.id.id_common_top_txtBtn);
		contentEdit = (EmotionEditText) findViewById(R.id.id_chat_textEdit);
		recordBtn = (RecordButton) findViewById(R.id.id_chat_recordBtn);
		emotionPager = (ViewPager) findViewById(R.id.id_chat_emotionPager);

		sendBtn = (Button) findViewById(R.id.id_chat_sendBtn);
		turnToAudioBtn = (Button) findViewById(R.id.id_chat_turnToAudioBtn);
		turnToTextBtn = findViewById(R.id.id_chat_turnToTextBtn);
		showAddBtn = findViewById(R.id.showAddBtn);
		showEmotionBtn = findViewById(R.id.showEmotionBtn);
		addLocationBtn = findViewById(R.id.addLocationBtn);
		// 隐藏位置发送图标
		addLocationBtn.setVisibility(View.GONE);
		addImageBtn = findViewById(R.id.addImageBtn);
		addCameraBtn = findViewById(R.id.addCameraBtn);

		chatTextLayout = findViewById(R.id.chatTextLayout);
		chatAudioLayout = findViewById(R.id.chatRecordLayout);
		chatBottomLayout = (LinearLayout) findViewById(R.id.bottomLayout);
		chatAddLayout = findViewById(R.id.chatAddLayout);
		chatEmotionLayout = findViewById(R.id.chatEmotionLayout);

		contentEdit.setOnClickListener(this);
		sendBtn.setOnClickListener(this);
		turnToAudioBtn.setOnClickListener(this);
		turnToTextBtn.setOnClickListener(this);
		showAddBtn.setOnClickListener(this);
		showEmotionBtn.setOnClickListener(this);
		addLocationBtn.setOnClickListener(this);
		addImageBtn.setOnClickListener(this);
		addCameraBtn.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.id_chat_sendBtn) {
			// Toast.makeText(JuChatActivity.this,
			// contentEdit.getText().toString(), 0).show();
			if (Utils.isNetworkConnected(this) && roomType == RoomType.Single) {
				sendText();
			} else if (Utils.isNetworkConnected(this)
					&& roomType == RoomType.NOTIFY) {
				Utils.toast(getString(R.string.not_public));
			} else {
				Utils.toast(getString(R.string.pleaseCheckNetwork));
			}
		} else if (id == R.id.id_chat_turnToAudioBtn) {
			// Toast.makeText(JuChatActivity.this, "发送语音", 0).show();
			showAudioLayout();
		} else if (id == R.id.id_chat_turnToTextBtn) {
			showTextLayout();
		} else if (id == R.id.showEmotionBtn) {
			toggleEmotionLayout();
		} else if (id == R.id.showAddBtn) {
			toggleBottomAddLayout();
		} else if (id == R.id.addCameraBtn) {
			if (Utils.isNetworkConnected(this) && roomType == RoomType.Single) {
				selectImageFromCamera();
			} else if (Utils.isNetworkConnected(this)
					&& roomType == RoomType.NOTIFY) {
				Utils.toast(getString(R.string.not_public));
			} else {
				Utils.toast(getString(R.string.pleaseCheckNetwork));
			}
		} else if (id == R.id.addImageBtn) {
			if (Utils.isNetworkConnected(this) && roomType == RoomType.Single) {
				selectImageFromLocal();
			} else if (Utils.isNetworkConnected(this)
					&& roomType == RoomType.NOTIFY) {
				Utils.toast(getString(R.string.not_public));
			} else {
				Utils.toast(getString(R.string.pleaseCheckNetwork));
			}

		} else if (id == R.id.addLocationBtn) {
		} else if (id == R.id.id_common_top_backBtn_Lay) {
			Intent intent = new Intent(JuChatActivity.this,
					JuMainActivity.class);
			startActivity(intent);
			this.finish();
		} else if (id == R.id.id_chat_textEdit) {
			hideBottomLayoutAndScrollToLast();
		}

	}

	private void hideBottomLayoutAndScrollToLast() {
		hideBottomLayout();
		scrollToLast();
	}

	public void selectImageFromLocal() {
		Intent intent;
		intent = new Intent(Intent.ACTION_GET_CONTENT);
		intent.setType("image/*");
		startActivityForResult(intent, IMAGE_REQUEST);
	}

	public void selectImageFromCamera() {
		Intent openCameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		Uri imageUri = Uri.fromFile(new File(localCameraPath));
		openCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
		startActivityForResult(openCameraIntent, TAKE_CAMERA_REQUEST);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case IMAGE_REQUEST:
				String localSelectPath = parsePathByReturnData(data);
				sendImageByPath(localSelectPath);
				break;
			case TAKE_CAMERA_REQUEST:
				sendImageByPath(localCameraPath);
				break;
			case LOCATION_REQUEST:
				sendLocationByReturnData(data);
				break;
			}
		}
		hideBottomLayout();
		super.onActivityResult(requestCode, resultCode, data);
	}

	private void sendImageByPath(String localSelectPath) {
		final String objectId = Utils.uuid();// 随机获得文件名
		final String newPath = PathUtils.getChatFilePath(objectId);
		// PhotoUtil.simpleCompressImage(localSelectPath,newPath);
		PhotoUtil.compressImage(localSelectPath, newPath);
		if (isStateFine()) {
			msgAgent.createAndSendMsg2(new MsgAgent.MsgBuilderHelper() {
				@Override
				public void specifyType(MsgBuilder msgBuilder) {
					msgBuilder.image2(objectId);
				}
			}, sendCallback, mAvImConversation);
		} else {
			Utils.toast(getResources().getString(R.string.pleaseCheckNetwork));
		}
	}

	private String parsePathByReturnData(Intent data) {
		if (data == null) {
			return null;
		}
		String localSelectPath = null;
		Uri selectedImage = data.getData();
		if (selectedImage != null) {
			Cursor cursor = getContentResolver().query(selectedImage, null,
					null, null, null);
			cursor.moveToFirst();
			int columnIndex = cursor.getColumnIndex("_data");
			localSelectPath = cursor.getString(columnIndex);
			cursor.close();
		}
		return localSelectPath;
	}

	private void hideBottomLayout() {
		hideAddLayout();
		chatEmotionLayout.setVisibility(View.GONE);
	}

	private void sendLocationByReturnData(Intent data) {
		final double latitude = data.getDoubleExtra("x", 0);
		final double longitude = data.getDoubleExtra("y", 0);
		final String address = data.getStringExtra("address");
		if (address != null && !address.equals("")) {
			if (isStateFine()) {
				msgAgent.createAndSendMsg2(new MsgAgent.MsgBuilderHelper() {
					@Override
					public void specifyType(MsgBuilder msgBuilder) {
						msgBuilder.location(address, latitude, longitude);
					}
				}, sendCallback, mAvImConversation);
			}
		} else {
			Utils.toast(ctx,
					getResources().getString(R.string.cannotGetYourAddressInfo));
		}
	}

	private void toggleBottomAddLayout() {
		if (chatAddLayout.getVisibility() == View.VISIBLE) {
			hideAddLayout();
		} else {
			chatEmotionLayout.setVisibility(View.GONE);
			hideSoftInputView();
			showAddLayout();
		}
	}

	private void showAddLayout() {
		chatAddLayout.setVisibility(View.VISIBLE);
	}

	private void toggleEmotionLayout() {
		// TODO Auto-generated method stub
		if (chatEmotionLayout.getVisibility() == View.VISIBLE) {
			chatEmotionLayout.setVisibility(View.GONE);
		} else {
			chatEmotionLayout.setVisibility(View.VISIBLE);
			hideAddLayout();
			showTextLayout();
			hideSoftInputView();
		}
	}

	private void hideAddLayout() {
		chatAddLayout.setVisibility(View.GONE);
	}

	private void showTextLayout() {
		chatTextLayout.setVisibility(View.VISIBLE);
		chatAudioLayout.setVisibility(View.GONE);
		chatTextLayout.requestFocus();
	}

	private void showAudioLayout() {
		// TODO Auto-generated method stub
		chatTextLayout.setVisibility(View.GONE);
		chatAudioLayout.setVisibility(View.VISIBLE);
		chatEmotionLayout.setVisibility(View.GONE);
		hideSoftInputView();
	}

	void setSoftInputMode() {
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}

	void hideSoftInputView() {
		InputMethodManager imm = (InputMethodManager) getSystemService(JuChatActivity.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(contentEdit.getWindowToken(), 0);
		// Utils.hideSoftInputView(this);
	}

	private void sendText() {
		// TODO Auto-generated method stub
		final String content = contentEdit.getText().toString();
		if (TextUtils.isEmpty(content) == false) {
			if (isStateFine()) {
				msgAgent.createAndSendMsg2(new MsgAgent.MsgBuilderHelper() {
					@Override
					public void specifyType(MsgBuilder msgBuilder) {
						msgBuilder.text2(content);
					}
				}, new DefaultSendCallback() {
					@Override
					public void onSuccess(Msg msg) {
						super.onSuccess(msg);
						contentEdit.setText("");
					}
				}, mAvImConversation);
			} else {
				Utils.toast(getResources().getString(
						R.string.pleaseCheckNetwork));
			}
		}

	}

	class DefaultSendCallback implements SendCallback {
		@Override
		public void onError(Exception e) {
			e.printStackTrace();
			// Utils.toast(e.getMessage());
			loadMsgsFromDB(false);
		}

		@Override
		public void onStart(Msg msg) {
			loadMsgsFromDB(false);
		}

		@Override
		public void onSuccess(Msg msg) {
			loadMsgsFromDB(false);
		}
	}

	private SendCallback sendCallback = new DefaultSendCallback();

	public void loadMsgsFromDB(boolean showDialog) {
		new GetDataTask(ctx, showDialog, true).execute();
	}

	class GetDataTask extends NetAsyncTask {
		List<Msg> msgs;
		boolean scrollToLast = true;

		GetDataTask(Context cxt, boolean openDialog, boolean scrollToLast) {
			super(cxt, openDialog);
			this.scrollToLast = scrollToLast;
		}

		@Override
		protected void doInBack() throws Exception {
			String convid;
			if (roomType == RoomType.Single) {
				//会话ID
				convid = AVOSUtils.convid(
						AVUser.getCurrentUser().getObjectId(), chatUserId);
				// mAvImConversation.getConversationId();
				// ChatService.getPeerId(chatUser)
			} else if (roomType == RoomType.NOTIFY) {
				convid = AVOSUtils.convid(
						AVUser.getCurrentUser().getObjectId(), chatUserId);
			} else {
				convid = mAvImConversation.getConversationId();
			}
			msgs = DBMsg.getMsgs(dbHelper, convid, msgSize);
			DBMsg.markMsgsAsHaveRead(msgs);//刷新数据库消息标记已阅读
			// 如果是会议通知，（发送）标记会议已阅读
			if (roomType == RoomType.NOTIFY) {
				//标记阅读
				MeetingService.markPushAsRead(msgs);
			}
			// ChatService.cacheUserOrChatGroup(msgs);
		}

		@Override
		protected void onPost(Exception e) {
			if (e == null) {
				ChatUtils.stopRefresh(xListView);
				addMsgsAndRefresh(msgs, scrollToLast);
			} else {
				Utils.toast("获取数据失败");
				Logger.d("[JuChatActivity] GetDataTask->" + e.getMessage());
			}
		}
	}

	public void addMsgsAndRefresh(List<Msg> msgs, boolean scrollToLast) {
		int lastN = adapter.getCount();
		int newN = msgs.size();
		this.msgs = msgs;
		adapter.setDatas(this.msgs);
		adapter.notifyDataSetChanged();
		if (scrollToLast) {
			scrollToLast();
		} else {
			xListView.setSelection(newN - lastN - 1);
			if (lastN == newN) {
				Utils.toast("聊天记录加载完毕");
			}
		}
		if (newN < PAGE_SIZE) {
			xListView.setPullRefreshEnable(false);
		} else {
			xListView.setPullRefreshEnable(true);
		}
	}

	public void scrollToLast() {
		// Logger.d("scrollToLast");
		xListView.smoothScrollToPosition(xListView.getCount() - 1);
		xListView.setSelection(xListView.getCount() - 1);
	}

	public void resendMsg(final Msg resendMsg) {
		MsgAgent.resendMsg2(resendMsg, sendCallback, fromUsername,
				mAvImConversation);
	}

	@Override
	public boolean onMessageUpdate(String otherId) {
		// TODO Auto-generated method stub
		if (otherId.equals(currentChatId())) {
			loadMsgsFromDB(false);
			return true;
		}
		return false;
	}

	private String currentChatId() {
		// TODO Auto-generated method stub
		if (roomType == RoomType.Single) {
			return chatUserId;
			// chatUser.getObjectId();
		} else if (roomType == RoomType.NOTIFY) {
			return chatUserId;
		} else {
			return getChatGroup().getObjectId();
		}

	}

	public ChatGroup getChatGroup() {
		return CacheService.getCurrentChatGroup();
	}

	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				msgSize += PAGE_SIZE;
				new GetDataTask(ctx, false, false).execute();
			}
		}, 1000);

	}

	@Override
	public void onLoadMore() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onResume() {
		super.onResume();
		if (AVUser.getCurrentUser() != null) {
			if (roomType == RoomType.Single) {
				// JuMSGReciever.addMsgListener(this);
				JuMSGHandlerV2.addMsgListener(this);
			} else if (roomType == RoomType.NOTIFY) {
				JuPushReciever.addMsgListener(this);
			} else {
				// GroupMsgReceiver.addMsgListener(this);
			}

		}
		// initActionBar();
	}

	@Override
	public void onPause() {
		super.onPause();
		if (AVUser.getCurrentUser() != null) {
			if (roomType == RoomType.Single) {
				// JuMSGReciever.removeMsgListener(this);
				JuMSGHandlerV2.removeMsgListener(this);
			} else if (roomType == RoomType.NOTIFY) {
				JuPushReciever.removeMsgListener(this);
			} else {
				// GroupMsgReceiver.removeMsgListener(this);
			}

		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		switch (keyCode) {
		case KeyEvent.KEYCODE_VOLUME_UP:
			am.adjustStreamVolume(AudioManager.STREAM_MUSIC,
					AudioManager.ADJUST_RAISE, AudioManager.FLAG_PLAY_SOUND
							| AudioManager.FLAG_SHOW_UI);
			return true;
		case KeyEvent.KEYCODE_VOLUME_DOWN:
			am.adjustStreamVolume(AudioManager.STREAM_MUSIC,
					AudioManager.ADJUST_LOWER, AudioManager.FLAG_PLAY_SOUND
							| AudioManager.FLAG_SHOW_UI);
			return true;
		case KeyEvent.KEYCODE_BACK:
			Intent intent = new Intent(JuChatActivity.this,
					JuMainActivity.class);
			startActivity(intent);
			JuChatActivity.this.finish();
			overridePendingTransition(R.anim.zoomin_static, R.anim.zoomout);
			return true;
		default:
			break;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub

	}

}
