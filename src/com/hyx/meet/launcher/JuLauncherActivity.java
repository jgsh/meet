package com.hyx.meet.launcher;

import com.hyx.meet.login.JuLoginActivity;
import com.hyx.meet.utils.JuUnZipUtil;
import com.hyx.meet.utils.PathUtils;
import com.hyx.meet.R;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

public class JuLauncherActivity extends Activity {

	public static final String CHAT_USER_ID = "chatUserId";
	public static final String GROUP_ID = "groupId";
	public static final String ROOM_TYPE = "roomType";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		ImageView iv = new ImageView(getApplicationContext());
		iv.setImageResource(R.drawable.welcome_home);
		iv.setScaleType(ScaleType.CENTER_CROP);
		setContentView(iv);

		new Thread() {
			public void run() {
				try {
					Thread.sleep(1500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				SharedPreferences sp = getSharedPreferences("first_use",
						MODE_WORLD_READABLE);
				boolean isFirstUse = sp.getBoolean(getVersion(), true);
				if (isFirstUse) {
					Editor edit = sp.edit();
					edit.putBoolean(getVersion(), false);
					edit.commit();
					//引导页界面
					Intent intent = new Intent(getApplicationContext(),
							JuFirstUserActivity.class);
					startActivity(intent);
					finish();
				} else {
					// Intent fromIntent = getIntent();
					// int openChat = fromIntent.getIntExtra("openChat", 0);
					//登陆界面
					Intent intent = new Intent(getApplicationContext(),
							JuLoginActivity.class);
					// if (openChat == 1) {
					// if (AVUser.getCurrentUser() != null) {
					// intent = new Intent(getApplicationContext(),
					// JuChatActivity.class);
					// intent.putExtra(CHAT_USER_ID,
					// fromIntent.getStringExtra(CHAT_USER_ID));
					// intent.putExtra(ROOM_TYPE,
					// fromIntent.getIntExtra(ROOM_TYPE,
					// RoomType.Single.getValue()));
					// intent.putExtra("toUserName",
					// fromIntent.getStringExtra("toUserName"));
					//
					// } else {
					// intent = new Intent(getApplicationContext(),
					// JuLoginActivity.class);
					//
					// }
					// } else {
					// intent = new Intent(getApplicationContext(),
					// JuLoginActivity.class);
					// }

					startActivity(intent);
					finish();
				}

			};
		}.start();
	}

	private String getVersion() {
		try {
			PackageInfo info = this.getPackageManager().getPackageInfo(
					this.getPackageName(), 0);
			int versionCode = info.versionCode;
			return versionCode + "";
		} catch (Exception e) {
		}
		return "";
	}

}
