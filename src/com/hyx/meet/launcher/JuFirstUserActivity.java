package com.hyx.meet.launcher;

import java.util.ArrayList;
import java.util.List;

import com.hyx.meet.R;
import com.hyx.meet.R.id;
import com.hyx.meet.R.layout;
import com.hyx.meet.login.JuLoginActivity;
import com.hyx.meet.utils.JuUnZipUtil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

public class JuFirstUserActivity extends Activity {

	private ViewPager mViewPager;

	private List<View> views ;
	private View view1;
	private View view2;
	private View view3;
	private View view4;
	private ViewPagerAdapter mViewPagerAdapter;
	private ImageView mImageView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ju_first_user);

		mViewPager = (ViewPager) findViewById(R.id.id_welcome_viewpager);
		LayoutInflater layoutInflater = LayoutInflater.from(this);
		view1 = layoutInflater.inflate(R.layout.welcome_one, null);
		view2 = layoutInflater.inflate(R.layout.welcome_two, null);
		view3 = layoutInflater.inflate(R.layout.welcome_three, null);
		view4 = layoutInflater.inflate(R.layout.welcome_four, null);
		mImageView = (ImageView) view4.findViewById(R.id.id_welcome_four);
		mImageView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(JuFirstUserActivity.this,
						JuLoginActivity.class);
				startActivity(intent);
				finish();
			}
		});
				
		views  = new ArrayList<View>();
		views.add(view1);
		views.add(view2);
		views.add(view3);
		views.add(view4);
		mViewPagerAdapter = new ViewPagerAdapter(views, this);
		mViewPager.setAdapter(mViewPagerAdapter);
		mViewPager.setCurrentItem(0);
		
		
		//复制assets中得缓存包到本地（暂时线上）
		JuUnZipUtil juUnZipUtil = new JuUnZipUtil();
//		juUnZipUtil.unZipToHYX(PathUtils.getZipDir(), JuLauncherActivity.this, false);
		juUnZipUtil.unZipFromAssets(JuFirstUserActivity.this, false);


	}

	public class ViewPagerAdapter extends PagerAdapter {

		List<View> viewsList;
		private Context context;

		ViewPagerAdapter(List<View> views, Context context) {
			this.viewsList = views;
			this.context = context;
		}

		@Override
		public void destroyItem(View container, int position, Object object) {
			// TODO Auto-generated method stub
			((ViewPager) container).removeView(viewsList.get(position));
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return viewsList.size();
		}

		@Override
		public int getItemPosition(Object object) {
			// TODO Auto-generated method stub
			return super.getItemPosition(object);
		}
		
		public List<View> getViews() {
			return viewsList;
		}
		
		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			// TODO Auto-generated method stub

			View item = viewsList.get(position);
			if (item.getParent() == null) {
				container.addView(item,0);
			}else {
				((ViewGroup)item.getParent()).removeView(item);
				container.addView(item,0);
			}

			return item;
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			// TODO Auto-generated method stub
			return (arg0 == arg1);
		}

	}

}
