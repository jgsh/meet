package com.hyx.meet.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract.CommonDataKinds.Phone;

import com.avos.avoscloud.AVCloud;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.FunctionCallback;
import com.hyx.meet.Application.App;
import com.hyx.meet.custome.sweet_dialog.SweetAlertDialog;
import com.hyx.meet.entity.JuContact;
import com.hyx.meet.service.listener.ActivityFinshListener;
import com.hyx.meet.utils.Logger;
import com.hyx.meet.utils.Utils;

/**
 * 分享服务层
 * @author ww
 *
 */
public class JuShareService {
	
	/**
	 * 获取手机通讯录联系人
	 * @param context
	 * @return
	 */
	public static List<JuContact> getContact(){
		List<JuContact> contacts = new ArrayList<JuContact>();
		//获得内容解析者
		ContentResolver resolver = App.ctx.getContentResolver();
		//查询条目：联系人ID，联系人姓名，联系人号码
		Cursor phoneCursor = resolver.query(Phone.CONTENT_URI, new String[]{Phone._ID,Phone.DISPLAY_NAME,Phone.NUMBER}, null, null, null);
		if (phoneCursor != null) {
			while(phoneCursor.moveToNext()){
				//获得联系人号码
				String phoneNumber = phoneCursor.getString(2);
				if (phoneNumber == null) {
					continue;
				}
				String phoneName = phoneCursor.getString(1);
				String phoneId = phoneCursor.getString(0);
				JuContact contact = new JuContact(phoneNumber,phoneName,phoneId);
				contacts.add(contact);				
				
			}
		}
		return contacts;
	}

	/**
	 * 短信邀请会议接口
	 * @param mobilePhoneNumber
	 * @param meetingId
	 */
	public static void inviteMeetingBySMS(List<String> mobilePhoneNumber,String meetingId,final Context context,final ActivityFinshListener listener){
		Logger.d(getContact().toString());
		final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE)
		.setTitleText("发送中");
		sweetAlertDialog.setCancelable(false);
		sweetAlertDialog.show();

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("mobilePhoneNumber", mobilePhoneNumber);
		params.put("userId", ChatService.getSelfId());
		params.put("meetingId", meetingId);
		AVCloud.callFunctionInBackground("meetingSmsShare", params, new FunctionCallback<Map<String, Object>>() {

			@Override
			public void done(Map<String, Object> result, AVException e) {
				// TODO Auto-generated method stub
				sweetAlertDialog.dismiss();
				if (e == null) {
					Logger.d("[MeetingService] inviteMeetingBySMS : "+result.toString());
					//跳转到发送结果状态界面
					if ((Integer)result.get("errorCode") == 0) {
						Utils.showShareBySMSCommonSuccessDialog(context, (String)result.get("errorMessage"),listener);
					}else {
						Utils.showCommonFailDialog(context, (String)result.get("errorMessage"));
					}
				}else {
					Logger.e("[MeetingService] inviteMeetingBySMS : "+e.getMessage());
					Utils.showCommonFailDialog(context, "发送失败");
				}
				
			}
		});
	}

}
