package com.hyx.meet.service.listener;

/**
 *	关闭activity监听器
 * @author ww
 *
 */
public interface ActivityFinshListener {
	void finish();
}
