package com.hyx.meet.service;

import java.io.FileNotFoundException;
import java.io.IOException;

import android.app.PendingIntent.OnFinished;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.im.v2.AVIMConversation;
import com.avos.avoscloud.im.v2.AVIMTypedMessage;
import com.avos.avoscloud.im.v2.callback.AVIMConversationCallback;
import com.avos.avoscloud.im.v2.messages.AVIMAudioMessage;
import com.avos.avoscloud.im.v2.messages.AVIMImageMessage;
import com.avos.avoscloud.im.v2.messages.AVIMTextMessage;
import com.hyx.meet.Application.App;
import com.hyx.meet.db.DBMsg;
import com.hyx.meet.entity.Msg;
import com.hyx.meet.entity.Msg.Type;
import com.hyx.meet.entity.MsgBuilder;
import com.hyx.meet.entity.RoomType;
import com.hyx.meet.entity.SendCallback;
import com.hyx.meet.service.reciever.JuMSGHandlerV2;
import com.hyx.meet.utils.JuAudioEncorder;
import com.hyx.meet.utils.Logger;
import com.hyx.meet.utils.NetAsyncTask;
import com.hyx.meet.utils.PathUtils;
import com.hyx.meet.utils.Utils;

/**
 * Created by lzw on 14/11/23.
 */
public class MsgAgent {
	RoomType roomType;
	String toId;
	String fromUsername;

	public MsgAgent(RoomType roomType, String toId, String fromUsername) {
		this.roomType = roomType;
		this.toId = toId;
		this.fromUsername = fromUsername;
	}

	public interface MsgBuilderHelper {
		void specifyType(MsgBuilder msgBuilder);
	}

	// public void createAndSendMsg(MsgBuilderHelper msgBuilderHelper,
	// final SendCallback callback) {
	// final MsgBuilder builder = new MsgBuilder();
	// builder.target(roomType, toId, fromUsername);
	// msgBuilderHelper.specifyType(builder);
	// final Msg msg = builder.preBuild();
	// DBMsg.insertMsg(msg);
	// callback.onStart(msg);
	// uploadAndSendMsg(msg, callback);
	// }
	//
	// public void uploadAndSendMsg(final Msg msg, final SendCallback callback)
	// {
	// new NetAsyncTask(App.ctx, false) {
	// String uploadUrl;
	//
	// @Override
	// protected void doInBack() throws Exception {
	// uploadUrl = MsgBuilder.uploadMsg(msg);
	// }
	//
	// @Override
	// protected void onPost(Exception e) {
	// if (e != null) {
	// e.printStackTrace();
	// DBMsg.updateStatus(msg.getObjectId(), Msg.Status.SendFailed);
	// callback.onError(e);
	// } else {
	// if (uploadUrl != null) {
	// DBMsg.updateContent(msg.getObjectId(), uploadUrl);
	// }
	// sendMsg(msg);
	// callback.onSuccess(msg);
	// }
	// }
	// }.execute();
	// }
	//
	// public Msg sendMsg(Msg msg) {
	// AVMessage avMsg = msg.toAVMessage();
	// Session session = ChatService.getSession();
	// if (roomType == RoomType.Single) {
	// session.sendMessage(avMsg);
	// } else {
	// Group group = session.getGroup(toId);
	// group.sendMessage(avMsg);
	// }
	// return msg;
	// }
	//
	// public static void resendMsg(Msg msg, SendCallback sendCallback,
	// String fromUsername) {
	// String toId;
	// if (msg.getRoomType() == RoomType.Group) {
	// String groupId = msg.getConvid();
	// toId = groupId;
	// } else {
	// toId = msg.getToPeerId();
	// msg.setRequestReceipt(true);
	// }
	// DBMsg.updateStatus(msg.getObjectId(), Msg.Status.SendStart);
	// sendCallback.onStart(msg);
	// MsgAgent msgAgent = new MsgAgent(msg.getRoomType(), toId, fromUsername);
	// msgAgent.uploadAndSendMsg(msg, sendCallback);
	// }

	/****************************** V2 ********************/
	/**
	 * 
	 * @param msgBuilderHelper
	 * @param callback
	 * @param avimConversation
	 */
	public void createAndSendMsg2(MsgBuilderHelper msgBuilderHelper,
			final SendCallback callback, AVIMConversation avimConversation) {
		final MsgBuilder builder = new MsgBuilder();
		builder.target2(roomType, toId, fromUsername);
		msgBuilderHelper.specifyType(builder);
		final Msg msg = builder.preBuild();
		// msg.setConvid(avimConversation.getConversationId());
		DBMsg.insertMsg(msg);
		callback.onStart(msg);
		uploadAndSendMsg2(msg, callback, avimConversation);
	}

	/**
	 * 
	 * @param msg
	 * @param callback
	 * @param avimConversation
	 */
	public void uploadAndSendMsg2(final Msg msg, final SendCallback callback,
			final AVIMConversation avimConversation) {
		new NetAsyncTask(App.ctx, false) {

			@Override
			protected void doInBack() throws Exception {
					// TODO Auto-generated catch block	
				if (msg.getType() == Msg.Type.Audio2) {
					//转码
					 //将wav文件转成amr
					JuAudioEncorder juAudioEncorder = new JuAudioEncorder();
					juAudioEncorder.wav2mp3(PathUtils.getTmpPath()+".wav", msg.getAudioPath2());
				}
				
			}

			@Override
			protected void onPost(Exception e) {
				if (e != null) {
					e.printStackTrace();
					DBMsg.updateStatus(msg.getObjectId(), Msg.Status.SendFailed);
					callback.onError(e);
				}else {
						new Thread(){
							@Override
							public void run() {
								try {
									sendMsg2(msg, avimConversation);
								} catch (FileNotFoundException e) {
									// TODO Auto-generated catch block
									DBMsg.updateStatus(msg.getObjectId(), Msg.Status.SendFailed);
									Logger.e("fileNotfound---"+e.getMessage());
								} catch (IOException e) {
									// TODO Auto-generated catch block
									DBMsg.updateStatus(msg.getObjectId(), Msg.Status.SendFailed);
									Logger.e("IOException---"+e.getMessage());
								}	
							};
							
						}.start();
					

					callback.onSuccess(msg);
				}
				
				
			}
		}.execute();
	}

	AVIMTypedMessage avimTypedMessage;
	AVIMTextMessage avimTextMessage;
	AVIMImageMessage avimImageMessage;
	AVIMAudioMessage avimAudioMessage;
	Type type;

	/**
	 * 
	 * @param msg
	 * @param avimConversation
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public Msg sendMsg2(final Msg msg, final AVIMConversation avimConversation)
			throws FileNotFoundException, IOException {
		if (msg.getType() == Msg.Type.Text2) {
			avimTextMessage = msg.getTextMessage();
			type = Type.Text2;
		} else if (msg.getType() == Msg.Type.Image2) {
			avimImageMessage = msg.getImageMessage();
			type = type.Image2;
		} else if (msg.getType() == Msg.Type.Audio2) {
			avimAudioMessage = msg.getAudioMessage();
			type = type.Audio2;
		}

		if (roomType == RoomType.Single) {
			if (type == Type.Text2) {
				avimConversation.sendMessage(avimTextMessage,
						new AVIMConversationCallback() {
							@Override
							public void done(AVException e) {
								// TODO Auto-generated method stub
								if (e != null) {
									Logger.d("发送失败" + e.getMessage());
									JuChatServiceV2.onMessageFailure(msg,
											JuMSGHandlerV2.msgListeners,
											avimConversation);
								} else {
//									Utils.toast("发送成功");
									JuChatServiceV2.onMessageSent(msg,
											JuMSGHandlerV2.msgListeners,
											avimConversation);
								}
							}
						});
			} else if (type == Type.Image2) {
				avimConversation.sendMessage(avimImageMessage,
						new AVIMConversationCallback() {
							@Override
							public void done(AVException e) {
								// TODO Auto-generated method stub
								if (e != null) {
									Logger.d("发送失败" + e.getMessage());
									JuChatServiceV2.onMessageFailure(msg,
											JuMSGHandlerV2.msgListeners,
											avimConversation);
								} else {
//									Utils.toast("发送成功");
									// 保存图片文件url地址
									DBMsg.updateContent(msg.getObjectId(),
											avimImageMessage.getFileUrl());
									JuChatServiceV2.onMessageSent(msg,
											JuMSGHandlerV2.msgListeners,
											avimConversation);
								}
							}
						});
			} else if (type == Type.Audio2) {
				avimConversation.sendMessage(avimAudioMessage,
						new AVIMConversationCallback() {
							@Override
							public void done(AVException e) {
								// TODO Auto-generated method stub
								if (e != null) {
									Logger.d("发送失败" + e.getMessage());
									JuChatServiceV2.onMessageFailure(msg,
											JuMSGHandlerV2.msgListeners,
											avimConversation);
								} else {
//									Utils.toast("发送成功");
									Logger.d(avimAudioMessage.getFileMetaData().toString());
									// 保存录音文件url地址
									DBMsg.updateContent(msg.getObjectId(),
											avimAudioMessage.getFileUrl());
									JuChatServiceV2.onMessageSent(msg,
											JuMSGHandlerV2.msgListeners,
											avimConversation);
								}
							}
						});

			}

		}
		return msg;
	}

	/**
	 * 
	 * @param msg
	 * @param sendCallback
	 * @param fromUsername
	 */
	public static void resendMsg2(Msg msg, SendCallback sendCallback,
			String fromUsername, AVIMConversation avimConversation) {
		String toId;
		if (msg.getRoomType() == RoomType.Group) {
			String groupId = msg.getConvid();
			toId = groupId;
		} else {
			toId = msg.getToPeerId();
			// msg.setRequestReceipt(true);
		}
		DBMsg.updateStatus(msg.getObjectId(), Msg.Status.SendStart);
		sendCallback.onStart(msg);
		MsgAgent msgAgent = new MsgAgent(msg.getRoomType(), toId, fromUsername);
		msgAgent.uploadAndSendMsg2(msg, sendCallback, avimConversation);
	}

}
