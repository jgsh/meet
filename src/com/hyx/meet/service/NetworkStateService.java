package com.hyx.meet.service;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVInstallation;
import com.avos.avoscloud.AVOSCloud;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.PushService;
import com.avos.avoscloud.SaveCallback;
import com.hyx.meet.R;
import com.hyx.meet.Application.App;
import com.hyx.meet.meet.JuMainActivity;
import com.hyx.meet.utils.Logger;
import com.hyx.meet.utils.Utils;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.widget.Toast;

/**
 * 检测网络状态
 * @author ww
 *
 */
public class NetworkStateService extends Service {

	private static final String tag="mNetWorkReciever";
	private ConnectivityManager connectivityManager;
    private NetworkInfo info;
    
    private BroadcastReceiver mNetWorkReciever = new BroadcastReceiver(){
    	
    	   public void onReceive(Context context, Intent intent) {
    		   String action = intent.getAction();  
               if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {  
                   Logger.d(tag+ "->网络状态已经改变");  
                   connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);  
                   info = connectivityManager.getActiveNetworkInfo();    
                   if(info != null && info.isAvailable()) {  
                       String name = info.getTypeName();  
                       Logger.d(tag+ "->当前网络名称：" + name);  
                       registerRetry();
                       //doSomething()  
                   } else {  
                       Logger.d(tag+ "->没有可用网络");  
                       Utils.toast(getString(R.string.pleaseCheckNetwork));
                     //doSomething()  
                   }  
               }  

    	   }

    	   /**
    	    * 重新注册leancloud
    	    */
		private void registerRetry() {
			// TODO Auto-generated method stub
			AVOSCloud.initialize(App.ctx, "ahjekkg29mdf3vlmqp80xysn6yar008b4j10ft9tf0gbt7sl", "nqw04nes7hbowj61e5097lvx1cj9c3fggb78hdli9zvwd914");
		    AVOSCloud.setDebugLogEnabled(true);
		    PushService.setDefaultPushCallback(App.ctx, JuMainActivity.class);
//		    if (AVUser.getCurrentUser() != null) {
//				PushService.subscribe(App.ctx, ChatService.getSelfId(), JuMainActivity.class);							
//			}
		    AVInstallation.getCurrentInstallation().saveInBackground(new SaveCallback() {
				
				@Override
				public void done(AVException e) {
					// TODO Auto-generated method stub
					if (e==null) {
						// 保存成功
			            String installationId = AVInstallation.getCurrentInstallation().getInstallationId();
//						Toast.makeText(ctx, installationId, 0).show();
					}else {
						Toast.makeText(App.ctx, getString(R.string.pleaseCheckNetwork), 0).show();
					}
					
				}
			});
			
		};
    };
    
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	@Override
	public void onCreate() {
		super.onCreate();
		IntentFilter mFilter = new IntentFilter();
		mFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
		registerReceiver(mNetWorkReciever, mFilter);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(mNetWorkReciever);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return super.onStartCommand(intent, flags, startId);
	}

}
