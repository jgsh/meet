package com.hyx.meet.service;

import java.util.HashMap;
import java.util.Map;

import com.avos.avoscloud.AVCloud;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.FunctionCallback;
import com.hyx.meet.R;
import com.hyx.meet.Application.App;
import com.hyx.meet.custome.sweet_dialog.SweetAlertDialog;
import com.hyx.meet.namecard.JuNameCardShowActivity;
import com.hyx.meet.utils.Logger;
import com.hyx.meet.utils.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;

public class JuUpdateService {
	private static final int LAST_VERSION = 2;
	// private static final String PROMTED_UPDATE = "promtedUpdate";
	private static final String UPDATE_URL = "https://fir.im/gsy8";
	static Activity activity;

	// static JuUpdateService updateService;
	// SharedPreferences pref;
	// SharedPreferences.Editor editor;

	/**
	 * 构造方法
	 * 
	 * @param activity
	 */
	// public JuUpdateService(Activity activity) {
	// // TODO Auto-generated constructor stub
	// this.activity = activity;
	// pref = PreferenceManager.getDefaultSharedPreferences(activity);
	// editor = pref.edit();
	// }

	/**
	 * 获取更新App类实例
	 * 
	 * @param ctx
	 * @return
	 */
	// public static JuUpdateService getInstance(Activity ctx) {
	// if (updateService == null) {
	// updateService = new JuUpdateService(ctx);
	// }
	// return updateService;
	// }

	/**
	 * 打开下载app的url
	 * 
	 * @param url
	 */
	public static void openUrlInBrowser(String url) {
		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		activity.startActivity(intent);
	}
	
	public static String getVersionName(Context ctx) {
	    String versionName = null;
	    try {
	      versionName = ctx.getPackageManager().getPackageInfo(
	          ctx.getPackageName(), 0).versionName;
	    } catch (PackageManager.NameNotFoundException e) {
	      // TODO Auto-generated catch block
	      e.printStackTrace();
	    }
	    return versionName;
	  }


	/**
	 * 检测是否有更新
	 * 
	 * @return
	 */
	public static void checkUpdate(final boolean is_toast,
			final Activity juactivity,final Context context) {
		activity = juactivity;
		Map<String, Object> parameters = new HashMap<String, Object>();
//		if (!App.MODE.equals("dev")) {
//		} else {
//			AVCloud.setProductionMode(false);
//		}
		parameters.put("platform", "android");
		AVCloud.callFunctionInBackground("appVerLatest", parameters,
				new FunctionCallback<Map<String, Object>>() {

					@Override
					public void done(final Map<String, Object> result, AVException e) {
						// TODO Auto-generated method stub
						if (e == null) {
							if ((Integer) result.get("version")== Utils.getVersionCodeInt()) {
								if (is_toast) {
									Utils.toast("已是最新版本");
								}
//								AlertDialog.Builder builder = new AlertDialog.Builder(
//										activity);
//								builder.setTitle(R.string.haveNewVersion)
//										.setMessage("当前版本：V"+getVersionName(activity)+"\n最新版本：V"+result.get("versionName").toString())
//										.setPositiveButton(
//												R.string.installNewVersion,
//												new DialogInterface.OnClickListener() {
//
//													@Override
//													public void onClick(
//															DialogInterface dialog,
//															int which) {
//														openUrlInBrowser(result.get("fileurl").toString());
//													}
//												})
//										.setNegativeButton(
//												R.string.laterNotify, null)
//										.show();
								Logger.d("[UpdateService checkUpdate->] nowCode"
										+Utils.getVersionCodeInt() );

							} else  if(	(Integer) result.get("version") > Utils.getVersionCodeInt()){
//								Logger.d("[UpdateService checkUpdate->]"
//										+ "版本更新");
								// 弹出更新App通知
								new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE)
							    .setTitleText("版本更新")
							    .setContentText("当前版本：V"+getVersionName(activity)+"\n最新版本：V"+result.get("versionName").toString())
							    .setCancelText("稍后提醒")
							    .setConfirmText("前往下载")
							    .showCancelButton(true)
							    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
							        @Override
							        public void onClick(SweetAlertDialog sDialog) {
							            sDialog.dismiss();
							        }
							    })
							    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
									
									@Override
									public void onClick(SweetAlertDialog sweetAlertDialog) {
										// TODO Auto-generated method stub
										sweetAlertDialog.dismiss();
										openUrlInBrowser(result.get("fileurl").toString());
																			
									}
								}).show();
//								AlertDialog.Builder builder = new AlertDialog.Builder(
//										activity);
//								builder.setTitle(R.string.haveNewVersion)
//										.setMessage("当前版本：V"+getVersionName(activity)+"\n最新版本：V"+result.get("versionName").toString())
//										.setPositiveButton(
//												R.string.installNewVersion,
//												new DialogInterface.OnClickListener() {
//
//													@Override
//													public void onClick(
//															DialogInterface dialog,
//															int which) {
//														openUrlInBrowser(result.get("fileurl").toString());
//													}
//												})
//										.setNegativeButton(
//												R.string.laterNotify, null)
//										.show();
							}

						} else {
							Logger.e("[UpdateService checkUpdate->]"
									+ e.getMessage());
						}

					}
				});
	}

}
