package com.hyx.meet.service;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import android.R.bool;
import android.R.integer;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.avos.avoscloud.AVCloud;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVMessage;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.FunctionCallback;
import com.avos.avoscloud.Group;
import com.avos.avoscloud.Session;
import com.avos.avoscloud.SessionManager;
import com.hyx.meet.Application.App;
import com.hyx.meet.adapter.ChatMsgAdapter.MsgViewType;
import com.hyx.meet.avobject.User;
import com.hyx.meet.chat.JuChatActivity;
import com.hyx.meet.db.DBHelper;
import com.hyx.meet.db.DBMeet;
import com.hyx.meet.db.DBMsg;
import com.hyx.meet.db.DBNamecard;
import com.hyx.meet.entity.Conversation;
import com.hyx.meet.entity.Msg;
import com.hyx.meet.entity.NameCard;
import com.hyx.meet.entity.RoomType;
import com.hyx.meet.meet.JuMainActivity;
import com.hyx.meet.service.listener.MsgListener;
import com.hyx.meet.setting.JuNewMessageSettingsActivity;
import com.hyx.meet.utils.AVOSUtils;
import com.hyx.meet.utils.Logger;
import com.hyx.meet.utils.NetAsyncTask;
import com.hyx.meet.utils.Utils;

/**
 * Created by ww on 15-1-5.
 */
public class ChatService {
	private static final int REPLY_NOTIFY_ID = 1;
	private static final long NOTIFY_PEROID = 1000;
	static long lastNotifyTime = 0;

	// private static DBHelper dbHelper = new DBHelper(App.ctx, App.DB_NAME,
	// App.DB_VER);;

	public static <T extends AVUser> String getPeerId(T user) {
		return user.getObjectId();
	}

	public static String getSelfId() {
		return getPeerId(User.curUser());
	}

//	public static <T extends AVUser> void withUsersToWatch(List<T> users,
//			boolean watch) {
//		List<String> peerIds = new ArrayList<String>();
//		for (AVUser user : users) {
//			peerIds.add(getPeerId(user));
//		}
//		String selfId = getPeerId(User.curUser());
//		Session session = SessionManager.getInstance(selfId);
//		if (watch) {
//			session.watchPeers(peerIds);
//		} else {
//			session.unwatchPeers(peerIds);
//		}
//	}

	public static void delete_conversation(String toUserId) {
		String convid = AVOSUtils.convid(getSelfId(), toUserId);
		DBMsg.deleteConversation(convid);
	}

//	public static void unwatchById(final String toUserId) {
//		Session session = SessionManager.getInstance(getSelfId());
//		List<String> list = new ArrayList<String>();
//		list.add(toUserId);
//		session.unwatchPeers(list);
//		// Toast.makeText(App.ctx, "doUnwatchSuccess", 0).show();
//		Logger.d("doUnwatchSuccess");
//
//	}
//
//	/**
//	 * watch函数一点作用
//	 * 
//	 * @param toUserId
//	 */
//	public static void watchById(final String toUserId) {
//		Session session = SessionManager.getInstance(getSelfId());
//		List<String> list = new ArrayList<String>();
//		list.add(toUserId);
//		session.watchPeers(list);
//		// Toast.makeText(App.ctx, "doWatchSuccess", 0).show();
//		Logger.d("doWatchSuccess");
//
//	}

//	public static <T extends AVUser> void withUserToWatch(T user, boolean watch) {
//		List<T> users = new ArrayList<T>();
//		users.add(user);
//		withUsersToWatch(users, watch);
//	}
//
//	public static Session getSession() {
//		return SessionManager.getInstance(getPeerId(User.curUser()));
//	}
//
//	public static void openSession(Activity activity) {
//		
//		Session session = getSession();
//		if (session.isOpen() == false) {
//			getFriendsFromAvcloud(session);
//		}
		// List<String> friendIds = new ArrayList<String>();
		// friendIds = getFriendIdsFromAvcloud(getSelfId());
		// session.open(friendIds);
		// session.setSignatureFactory(new SignatureFactory());
		// if (session.isOpen() == false) {
		// session.open(new LinkedList<String>());
		// }
//	}

	/**
	 * 获取双向好友的ids
	 * 
	 * @param selfId
	 * @return
	 */
//	public static ArrayList<String> getFriendIdsFromAvcloud(
//			List<Map<String, Object>> friends) {
//		// TODO Auto-generated method stub
//
//		List<String> friendsIds = new ArrayList<String>();
//		for (Map<String, Object> friend : friends) {
//
//			friendsIds.add(friend.get("objectId").toString());
//
//		}
//
//		return (ArrayList<String>) friendsIds;
//	}

	/**
	 * 获取双向好友
	 * 
	 * @param selfId
	 * @todo 放到cloudService中
	 * @return
	 */
//	public static void getFriendsFromAvcloud(final Session session) {
//		// TODO Auto-generated method stub
////		AVCloud.setProductionMode(false); // 调用测试环境云代码
//		Map<String, Object> params = new HashMap<String, Object>();
//		params.put("userId", getSelfId());
//		AVCloud.callFunctionInBackground("getTwowayFriends", params,
//				new FunctionCallback<Map<String, Object>>() {
//
//					@SuppressWarnings("unchecked")
//					@Override
//					public void done(Map<String, Object> data, AVException e) {
//						// TODO Auto-generated method stub
//						if (e == null) {
//
//							// @SuppressWarnings("unchecked")
//							// System.out.println("data->" + data.toString());
//							// System.out.println(data.getClass().toString());
//							@SuppressWarnings("unchecked")
//							List<Map<String, Object>> userlist = null;
//							userlist = (List<Map<String, Object>>) data
//									.get("data");
//							if (userlist.size() > 0) {
//								List<String> friendsIds = new ArrayList<String>();
//								friendsIds = getFriendIdsFromAvcloud(userlist);
//								session.open(friendsIds);
//
//								// System.out.println("friendIds->"+friendsIds.toString());
//
//								CacheService
//										.registerBatchUser(mapToUsers(userlist));
//								CacheService.setFriends(userlist);
//								CacheService.setFriendIds(friendsIds);
//							}
//
//						} else {
//
//							// System.out.println(e.getMessage());
//
//						}
//
//					}
//
//				});
//
//	}

	/**
	 * 转换格式
	 * 
	 * @param friends
	 * @return
	 */
	public static ArrayList<User> mapToUsers(List<Map<String, Object>> friends) {
		// TODO Auto-generated method stub
		ArrayList<User> users = new ArrayList<User>();
		for (Map<String, Object> map : friends) {
			User user = Utils.mapToUser(map);
			// System.out.println("map->"+map.toString());
			users.add(user);
		}

		return users;

	}

	/**
	 * 获取会话列表
	 * @return
	 * @throws AVException
	 */
	public static List<Conversation> getConversationsAndCache()
			throws AVException {
		List<Msg> msgs = DBMsg.getRecentMsgs(User.curUserId());
		boolean is_conflict = false;//设置是否串号标志
		// cacheUserOrChatGroup(msgs);
		ArrayList<Conversation> conversations = new ArrayList<Conversation>();
		DBHelper dbHelper = new DBHelper(App.ctx, App.DB_NAME, App.DB_VER);
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		for (Msg msg : msgs) {
			Conversation conversation = new Conversation();
			if (msg.getRoomType() == RoomType.Single) {
				String chatUserId = msg.getOtherId();
				// UserService.cacheUserIfNone(chatUserId);
				if (chatUserId.equals(getSelfId()) 
						|| (!msg.getFromPeerId().equals(getSelfId())&&
								!msg.getToPeerId().equals(getSelfId()))) {
					continue;
				}
				NameCard nameCard = DBNamecard.getNameCardByUserId(dbHelper,
						chatUserId);

				//聊天人的名片
				conversation.setToUser(nameCard);
				// Logger.d(CacheService.lookupUser(chatUserId).toString());
			} else {
				conversation.setToChatGroup(CacheService.lookupChatGroup(msg
						.getConvid()));
			}
			int unreadCount = DBMsg.getUnreadCount(db, msg.getConvid());
			conversation.setMsg(msg);
			conversation.setUnreadCount(unreadCount);//未读消息
			if (msg.getRoomType() == RoomType.Single) {
				if (DBNamecard.haveNameCardByUserId(msg.getOtherId())) {
					conversations.add(conversation);
				}else {
					//标记串号信息（刚登录时数据库没有名片夹的记录时）已阅读,并设置串号标志为true
					List<Msg> markMsgs = new ArrayList<Msg>();
					markMsgs.add(msg);
					DBMsg.markMsgsAsHaveRead(markMsgs);					
//					DBMsg.deleteConversation(msg.getConvid());
					is_conflict = true;
				}

			} else if(msg.getRoomType() == RoomType.NOTIFY){
				//本地有无此会议
				if (DBMeet.haveMeeting(msg.getFromPeerId())) {
					conversations.add(conversation);					
				}else {
					//删除串号信息,并设置串号标志为true
					DBMsg.deleteConversation(msg.getConvid());
					is_conflict = true;
				}
			}
		}
		db.close();
		if (is_conflict) {
			//因为刷新总消息条数是在刷新列表之前，所以如果串号，则还要在强制刷新总消息条数标志
			JuMainActivity.updateMessageUnread(getMessageUnread());		
		}
		return conversations;
	}

	public static void cacheUserOrChatGroup(List<Msg> msgs) throws AVException {
		Set<String> userIds = new HashSet<String>();
		Set<String> groupIds = new HashSet<String>();
		for (Msg msg : msgs) {
			if (msg.getRoomType() == RoomType.Single) {
				userIds.add(msg.getToPeerId());
			} else if (msg.getRoomType() == RoomType.NOTIFY) {

			} else {
				String groupId = msg.getConvid();
				groupIds.add(groupId);
			}
			userIds.add(msg.getFromPeerId());
		}
		if (userIds.size() > 0) {
			CacheService.cacheUserAndGet(new ArrayList<String>(userIds));
		}
		// GroupService.cacheChatGroups(new ArrayList<String>(groupIds));
	}

	//
//	public static void closeSession() {
//		Session session = ChatService.getSession();
//		session.close();
//	}

	//
	// public static Group getGroupById(String groupId) {
	// return getSession().getGroup(groupId);
	// }
	//
//	会议通知数量存储表（与userId关联）
//	private final static String NEW_SYS_MESSAGE = "sys_message";// 系统通知信息
//	private final static String MEET_NOTIFY_NUM = "meet_notify";// 小秘书新通知数量
//	private final static String NEW_FRIEND_REQ_NUM = "new_friend_req";// 好友请求通知数量
//	private final static String IS_LOGOUT = "logout";
// 新消息设置存储表（与userId关联）
	private final static String PREFS_NAME = "newMessage_setting_";// 新消息设置信息
	private final static String NO_MESSAGE = "noMessage";
	private final static String SOUND = "sound";
	private final static String SHAKE = "shake";

	public static void notifyMsg(Context context, Msg msg, boolean is_group)
			throws JSONException {
		if (System.currentTimeMillis() - lastNotifyTime < NOTIFY_PEROID) {
			return;
		} else {
			lastNotifyTime = System.currentTimeMillis();
		}
		int icon = context.getApplicationInfo().icon;
		CharSequence notifyContent = msg.getNotifyContent();
		String content = msg.getContent();
		String username;
		if (msg.getRoomType() == RoomType.NOTIFY) {
			if (msg.getType() == Msg.Type.Board) {
				username = "会议通知";
			} else if (msg.getType() == Msg.Type.Invite) {
				username = "会议邀请";
			} else if (msg.getType() == Msg.Type.FriednReq
					|| msg.getType() == Msg.Type.FriednDel) {
				username = "好友(关注/取消关注)";
			} else if (msg.getType() == Msg.Type.MAudit) {
				username = "会议审核请求";
			} else if (msg.getType() == Msg.Type.Logout) {
				username = "登出请求";
			} else {
				username = "新消息";
			}
		} else {
			username = msg.getFromName();
			if (msg.getType() == Msg.Type.Image || msg.getType() == Msg.Type.Image2) {
				content = "[图片]";
			}else if (msg.getType() == Msg.Type.Audio2) {
				content = "[语音]";
			}
		}
		Intent intent;
		if (!is_group) {
			if (msg.getRoomType() == RoomType.Single) {
				intent = JuChatActivity.getUserChatIntent(context,
						msg.getFromPeerId(), username);
			} else {
				if (msg.getType() == Msg.Type.Board) {
					intent = JuChatActivity.getNotifyChatIntent(context,
							msg.getFromPeerId(), username);
				} else {
					intent = JuChatActivity.getNotifyCommonIntent(context);
				}

			}
		} else {
			intent = null;
			// intent = JuChatActivity.getGroupChatIntent(context,
			// group.getGroupId());
		}
		// why Random().nextInt()
		// http://stackoverflow.com/questions/13838313/android-onnewintent-always-receives-same-intent
		PendingIntent pend = PendingIntent.getActivity(context,
				new Random().nextInt(), intent, 0);
		Notification.Builder builder = new Notification.Builder(context);
//		Logger.e("notifyContent" + notifyContent.toString());
		builder.setContentIntent(pend).setSmallIcon(icon)
				.setWhen(System.currentTimeMillis())
				.setTicker(username + ":" + content)
				.setContentTitle("会友行")
				.setContentText(username + ":" + content)
				.setAutoCancel(true);
		NotificationManager man = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = builder.getNotification();
		SharedPreferences sp = (App.ctx).getSharedPreferences(PREFS_NAME+ChatService.getSelfId(),
				(App.ctx).MODE_PRIVATE);
		// 获取用户设置信息
		boolean is_sound = sp.getBoolean(SOUND, true);
		boolean is_shake = sp.getBoolean(SHAKE, true);
		boolean is_nomessage = sp.getBoolean(NO_MESSAGE, false);
		// PreferenceMap preferenceMap =
		// PreferenceMap.getCurUserPrefDao(context);
		if (is_sound) {
			notification.defaults |= Notification.DEFAULT_SOUND;
		}
		if (is_shake) {
			notification.defaults |= Notification.DEFAULT_VIBRATE;
		}
		// if (preferenceMap.isVibrateNotify()) {
		// notification.defaults |= Notification.DEFAULT_VIBRATE;
		// }
		man.notify(REPLY_NOTIFY_ID, notification);
	}

	/**
	 * 公众号接受消息
	 * 
	 * @param context
	 * @param pushData
	 * @param listeners
	 */
	public static void onRecive(Context context, org.json.JSONObject pushData,
			Set<MsgListener> listeners) {
		final Msg msg = Msg.fromPushData(pushData);
		String selfId = getSelfId();
		String convid = AVOSUtils.convid(selfId, msg.getFromPeerId());
		msg.setToPeerId(selfId);
		msg.setRoomType(RoomType.NOTIFY);
		msg.setStatus(Msg.Status.SendReceived);
		msg.setReadStatus(Msg.ReadStatus.Unread);
		msg.setConvid(convid);
		handleReceivedMeetingPush(context, msg, listeners);

	}

	/**
	 * 处理接收的公众号推送
	 * 
	 * @param context
	 * @param msg
	 * @param listeners
	 */
	public static void handleReceivedMeetingPush(final Context context,
			final Msg msg, final Set<MsgListener> listeners) {
		// 更新系统通知数量
//		SharedPreferences sp = App.ctx.getSharedPreferences(NEW_SYS_MESSAGE,
//				App.ctx.MODE_PRIVATE);
//		Editor editor = sp.edit();
		NotifyService notifyService = new NotifyService(getSelfId());
		int meet_notify_num = notifyService.getMeetNotifyNum();
//				sp.getInt(MEET_NOTIFY_NUM, 0);
		int friend_req_num = notifyService.getFriendNotifyNum();
//				sp.getInt(NEW_FRIEND_REQ_NUM, 0);

		if (msg.getType() == Msg.Type.MAudit
				|| msg.getType() == Msg.Type.Invite  ) {
//			msg.getType() == Msg.Type.FriednDel
			notifyService.setMeetNotifyNum(++meet_notify_num);
//			editor.putInt(MEET_NOTIFY_NUM, ++meet_notify_num);
//			editor.commit();
		} else if (msg.getType() == Msg.Type.FriednReq) {
			if (msg.getConfirm() == 0) {
//				好友添加请求
				if (msg.getFromPeerId().equals(getSelfId())) {
					notifyService.setFriendNotifyNum(++friend_req_num);									
				}
			}
//			editor.putInt(NEW_FRIEND_REQ_NUM, ++friend_req_num);
//			editor.commit();
		} else if (msg.getType() == Msg.Type.Logout) {
			if (User.curUser() != null) {
				// Utils.toast("登出token-》"+msg.getFromPeerId());
				if (!msg.getObjectId().equals(User.curUser().getUuid())
						&& msg.getFromPeerId().equals(
								User.curUser().getObjectId())) {
					// 当userid相同但是口令不同时
					Logger.d("您在另一台设备上已登录");
					notifyService.setIsLogout(true);
//					editor.putBoolean(IS_LOGOUT, true);
//					editor.commit();
				} else {
					// Utils.toast("单机登陆");
					Logger.d("单机登陆");
				}
			}

		} else if (msg.getType() == Msg.Type.FriednDel) {
			
		}else {
			DBMsg.insertMsg(msg);

		}
		String otherId = getOtherId(msg.getFromPeerId(), null);
		boolean done = false;
		for (MsgListener listener : listeners) {
			if (listener.onMessageUpdate(otherId)) {
				done = true;
//				break;
			}
		}
		if (!done) {
			if (User.curUser() != null && msg.getType() != Msg.Type.Logout) {
				notifyMsg(context, msg, false);
			}
		}
	}
	
	
	/**
	 * 更新消息标记
	 */
	public  static int getMessageUnread() {
		// TODO Auto-generated method stub
		NotifyService notifyService = new NotifyService(getSelfId());
		DBHelper dbHelper = new DBHelper(App.ctx, App.DB_NAME, App.DB_VER);
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		
		int meet_notify_num = notifyService.getMeetNotifyNum();
		int friend_req_num = notifyService.getFriendNotifyNum();
		int chat_unread_num = DBMsg.getUnreadCountAll(db);
//		更新消息标记
		return meet_notify_num+friend_req_num+chat_unread_num;
		
		
	}

	/**
	 * 处理接受的消息
	 * @param context
	 * @param avMsg
	 * @param listeners
	 * @param group
	 */
//	public static void onMessage(Context context, AVMessage avMsg,
//			Set<MsgListener> listeners, Group group) {
//		final Msg msg = Msg.fromAVMessage(avMsg);
//		String convid;
//		String selfId = getSelfId();
//		if (group == null) {
//			msg.setToPeerId(selfId);
//			convid = AVOSUtils.convid(selfId, msg.getFromPeerId());
//			msg.setRoomType(RoomType.Single);
//			
//		} else {
//			convid = group.getGroupId();
//			msg.setRoomType(RoomType.Group);
//		}
//		msg.setStatus(Msg.Status.SendReceived);
//		msg.setReadStatus(Msg.ReadStatus.Unread);
//		msg.setConvid(convid);
//		if (!selfId.equals(msg.getFromPeerId())) {
//			handleReceivedMsg(context, msg, listeners, group);			
//		}
//	}

//	public static void handleReceivedMsg(final Context context, final Msg msg,
//			final Set<MsgListener> listeners, final Group group) {
//		new NetAsyncTask(context, false) {
//			@Override
//			protected void doInBack() throws Exception {
//				if (msg.getType() == Msg.Type.Audio) {
//					File file = new File(msg.getAudioPath());
//					String url = msg.getContent();
//					// Utils.downloadFileIfNotExists(url, file);
//				}
//				if (group != null) {
//					// GroupService.cacheChatGroupIfNone(group.getGroupId());
//				}
//				String fromId = msg.getFromPeerId();
//				// String fromUsername = getFromUsername(fromId);
//				UserService.cacheUserIfNone(fromId);
//			}
//
//			@Override
//			protected void onPost(Exception e) {
//				if (e != null) {
//					Utils.toast(context, "网络异常");
//				} else {
//					DBMsg.insertMsg(msg);
//					String otherId = getOtherId(msg.getFromPeerId(), group);
//					boolean done = false;
//					for (MsgListener listener : listeners) {
//						if (listener.onMessageUpdate(otherId)) {
//							done = true;
////							break;
//						}
//					}
//					if (!done) {
//						if (User.curUser() != null) {
//							// PreferenceMap preferenceMap =
//							// PreferenceMap.getCurUserPrefDao(context);
//							// if (preferenceMap.isNotifyWhenNews()) {
//							// notifyMsg(context, msg, group);
//							// }
//							if (getSelfId().equals(otherId)) {
//							} else {
//								// Utils.toast("有新消息");
//								notifyMsg(context, msg, false);
//							}
//						}
//					}
//				}
//			}
//		}.execute();
//	}

	private static String getFromUsername(String fromId) {
		return null;
	}

	public static String getOtherId(String otherId, Group group) {
		assert otherId != null || group != null;
		if (group != null) {
			return group.getGroupId();
		} else {
			return otherId;
		}
	}

	//
//	public static void onMessageSent(AVMessage avMsg,
//			Set<MsgListener> listeners, Group group) {
//		Msg msg = Msg.fromAVMessage(avMsg);
//		DBMsg.updateStatusAndTimestamp(msg.getObjectId(),
//				Msg.Status.SendSucceed, msg.getTimestamp());
//		String otherId = getOtherId(msg.getToPeerId(), group);
//		for (MsgListener msgListener : listeners) {
//			if (msgListener.onMessageUpdate(otherId)) {
//				break;
//			}
//		}
//	}
//
//	//
//	public static void onMessageFailure(AVMessage avMsg,
//			Set<MsgListener> msgListeners, Group group) {
//		Msg msg = Msg.fromAVMessage(avMsg);
//		DBMsg.updateStatus(msg.getObjectId(), Msg.Status.SendFailed);
//		String otherId = getOtherId(msg.getToPeerId(), group);
//		for (MsgListener msgListener : msgListeners) {
//			if (msgListener.onMessageUpdate(otherId)) {
//				break;
//			}
//		}
//	}

	//
	// public static void onMessageError(Throwable throwable, Set<MsgListener>
	// msgListeners) {
	// String errorMsg = throwable.getMessage();
	// Logger.d("error " + errorMsg);
	// if (errorMsg != null && errorMsg.startsWith("{")) {
	// AVMessage avMsg = new AVMessage(errorMsg);
	// //onMessageFailure(avMsg, msgListeners, group);
	// }
	// }
	//
	// public static List<User> findGroupMembers(ChatGroup chatGroup) throws
	// AVException {
	// List<String> members = chatGroup.getMembers();
	// return CacheService.findUsers(members);
	// }
	//
	public static void cancelNotification(Context ctx) {
		Utils.cancelNotification(ctx, REPLY_NOTIFY_ID);
	}

	//
//	public static void onMessageDelivered(AVMessage avMsg,
//			Set<MsgListener> listeners) {
//		Msg msg = Msg.fromAVMessage(avMsg);
//		DBMsg.updateStatus(msg.getObjectId(), Msg.Status.SendReceived);
//		String otherId = msg.getToPeerId();
//		for (MsgListener listener : listeners) {
//			if (listener.onMessageUpdate(otherId)) {
//				break;
//			}
//		}
//	}
	//
	// public static boolean isSessionPaused() {
	// return MsgReceiver.isSessionPaused();
	// }
}
