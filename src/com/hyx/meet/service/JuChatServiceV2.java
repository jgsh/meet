package com.hyx.meet.service;

import java.io.File;
import java.util.Set;

import android.content.Context;
import android.content.Intent;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVMessage;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.Group;
import com.avos.avoscloud.im.v2.AVIMClient;
import com.avos.avoscloud.im.v2.AVIMConversation;
import com.avos.avoscloud.im.v2.AVIMMessage;
import com.avos.avoscloud.im.v2.AVIMTypedMessage;
import com.avos.avoscloud.im.v2.callback.AVIMClientCallback;
import com.hyx.meet.Application.App;
import com.hyx.meet.Application.SysApplication;
import com.hyx.meet.avobject.User;
import com.hyx.meet.chat.JuChatActivity;
import com.hyx.meet.db.DBMsg;
import com.hyx.meet.entity.Msg;
import com.hyx.meet.entity.RoomType;
import com.hyx.meet.service.listener.MsgListener;
import com.hyx.meet.utils.AVOSUtils;
import com.hyx.meet.utils.Logger;
import com.hyx.meet.utils.NetAsyncTask;
import com.hyx.meet.utils.Utils;

/***
 * IM聊天V2service
 * 
 * @author ww
 *
 */
public class JuChatServiceV2 {
	public static final String CHAT_USER_ID = "chatUserId";
	public static final String GROUP_ID = "groupId";
	public static final String ROOM_TYPE = "roomType";
	public static final String AVIMCONVERSATION = "conversation";

	
	public static boolean chat_online = false;
	/**
	 * 登陆聊天服务器
	 */
	public static  void openChat() {
		// TODO Auto-generated method stub
		getAvimClient().open(new AVIMClientCallback() {
			
			@Override
			public void done(AVIMClient avimClient, AVException e) {
				// TODO Auto-generated method stub
				if (e == null) {
					chat_online = true;
					Logger.d("聊天登录成功!");
				}else {
					chat_online = false;
					Logger.d("聊天登录失败!"+e.getMessage());

				}
			}
		});
	}
	
	/**
	 * 登陆聊天服务器
	 */
	public static  void closeChat(final Intent intent,final NotifyService notifyService,final Context context) {
		// TODO Auto-generated method stub
		getAvimClient().close(new AVIMClientCallback() {
			
			@Override
			public void done(AVIMClient avimClient, AVException e) {
				// TODO Auto-generated method stub
				if (e == null) {
					chat_online = false;
					context.startActivity(intent);
					// 用户对象销毁
					AVUser.logOut();
					// 设置是否登出为false
					notifyService.setIsLogout(false);
					//清除登录时间
					notifyService.setLoginTime("2015-01-01T09:29:41.000Z");
										
					// 系统退出
					SysApplication.getInstance().exit();

					Logger.d("聊天退出成功!");
				}else {
					chat_online = true;
					Logger.d("聊天退出失败!"+e.getMessage());

				}
			}
		});
	}


	/**
	 * 获取聊天客户端实例
	 * @return
	 */
	public static AVIMClient getAvimClient(){
		return AVIMClient.getInstance(ChatService.getSelfId());
	}
	

	
	/**
	 * V2接收消息处理
	 * 
	 * @param message
	 * @param conversation
	 * @param client
	 * @param msgListeners
	 */
	public static void onMessage(AVIMTypedMessage message,
			AVIMConversation conversation, AVIMClient client,
			Set<MsgListener> msgListeners) {
		final Msg msg = Msg.fromAVIMMessage(message,conversation);
		String selfId = ChatService.getSelfId();
		String convid ;
		boolean is_group = false;
		if (conversation.getMembers().size() > 2) {
			// 群聊
			convid = conversation.getConversationId();
			is_group = true;
		} else {
			// 单聊
			convid = AVOSUtils.convid(selfId, msg.getFromPeerId());
			msg.setToPeerId(selfId);
			msg.setRoomType(RoomType.Single);

		}
		msg.setStatus(Msg.Status.SendReceived);
		msg.setReadStatus(Msg.ReadStatus.Unread);
		msg.setConvid(convid);
		handleReceivedMsg(App.ctx, msg, msgListeners, is_group);

	}

	/**
	 * 处理V2的聊天消息
	 * 
	 * @param ctx
	 * @param msg
	 * @param msgListeners
	 */
	private static void handleReceivedMsg(final Context context, final Msg msg,
			final Set<MsgListener> msgListeners, final boolean is_group) {
		// TODO Auto-generated method stub
		new NetAsyncTask(context, false) {
			@Override
			protected void doInBack() throws Exception {
				if (msg.getType() == Msg.Type.Audio || msg.getType() == Msg.Type.Audio2) {
					File file = new File(msg.getAudioPath2());
					String url = msg.getContent();
					Utils.downloadFileIfNotExists(url, file);
				}
				if (is_group) {
					// GroupService.cacheChatGroupIfNone(group.getGroupId());
				}
//				String fromId = msg.getAvimMessage().getFrom();
				// UserService.cacheUserIfNone(fromId);
			}

			@Override
			protected void onPost(Exception e) {
				if (e != null) {
					Utils.toast(context, "网络异常");
				} else {
					DBMsg.insertMsg(msg);
					String otherId = msg.getFromPeerId();
					boolean done = false;
					for (MsgListener listener : msgListeners) {
						if (listener.onMessageUpdate(otherId)) {
							done = true;
						}
					}
					if (!done) {
						if (User.curUser() != null) {

							if (ChatService.getSelfId().equals(otherId)) {
							} else {
								 Utils.toast("有新消息");
								ChatService.notifyMsg(context.getApplicationContext(), msg, is_group);
							}
						}
					}
				}
			}
		}.execute();
	}

	/**
	 * 
	 * @param avimMessage
	 * @param listeners
	 * @param avimConversation
	 */
	public static void onMessageSent(Msg msg,
			Set<MsgListener> listeners, AVIMConversation avimConversation) {
		DBMsg.updateStatusAndTimestamp(msg.getObjectId(),
				Msg.Status.SendSucceed, msg.getTimestamp());
		String otherId = ChatService.getOtherId(msg.getToPeerId(), null);
		for (MsgListener msgListener : listeners) {
			if (msgListener.onMessageUpdate(otherId)) {
				break;
			}
		}
	}

	/**
	 * 
	 * @param avimTypedMessage
	 * @param msgListeners
	 * @param avimConversation
	 */
	public static void onMessageFailure(Msg msg,
			Set<MsgListener> msgListeners, AVIMConversation avimConversation) {
//		Msg msg = Msg.fromAVIMMessage(avimTypedMessage,avimConversation);
		DBMsg.updateStatus(msg.getObjectId(), Msg.Status.SendFailed);
		String otherId = ChatService.getOtherId(msg.getToPeerId(), null);
		for (MsgListener msgListener : msgListeners) {
			if (msgListener.onMessageUpdate(otherId)) {
				break;
			}
		}
	}

}
