package com.hyx.meet.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;

import android.widget.Toast;

import com.avos.avoscloud.AVCloud;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVInstallation;

import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.FunctionCallback;
import com.avos.avoscloud.PushService;
import com.hyx.meet.Application.App;
import com.hyx.meet.db.DBMeet;
import com.hyx.meet.db.DBMsg;
import com.hyx.meet.entity.MeetingDomain;
import com.hyx.meet.entity.Msg;
import com.hyx.meet.entity.RoomType;
import com.hyx.meet.meet.JuMainActivity;
import com.hyx.meet.utils.AVOSUtils;
import com.hyx.meet.utils.Logger;

/**
 * 会议服务层
 * @author ww
 *
 */
public class MeetingService {
	private final static int MEET_ALL = 0;// 0 全部
	private final static int MEET_MY = 1;// 0 我发布的会议
	private final static int MEET_GOING = 2;// 0 正在进行
	private final static int MEET_WILL = 3;// 0 即将开始
	private final static int MEET_END = 4;// 0 圆满结束
	
	
	/**
	 * 过滤会议
	 * 
	 * @param meeting
	 */
	public static List<MeetingDomain> filterMeetings(int state) {
		long nowTime = System.currentTimeMillis();// 当前时间
		long change = 8 * 60 * 60 * 1000;// 当前时间与开始时间格式上差少了八个小时
		nowTime = nowTime + change;

		List<MeetingDomain> all = DBMeet.getInformationAll();
		List<MeetingDomain> returnData = new ArrayList<MeetingDomain>();
		switch (state) {
		case MEET_ALL:
			returnData.addAll(all);
			break;
		case MEET_GOING:
			for (int i = 0; i < all.size(); i++) {
				MeetingDomain temp = all.get(i);
				long startTime  = temp.getStartTime();
				long endTime  = temp.getEndTime();
				if (nowTime > startTime && nowTime < endTime) {
					returnData.add(temp);
				}
			}
			break;
		case MEET_MY:
			for (int i = 0; i < all.size(); i++) {
				MeetingDomain temp = all.get(i);
				String meetOwenrId  = temp.getHostObjectId();//发布者ID
				if (ChatService.getSelfId().equals(meetOwenrId)) {
					returnData.add(temp);
				}
			}
			break;
		case MEET_WILL:
			for (int i = 0; i < all.size(); i++) {
				MeetingDomain temp = all.get(i);
				long startTime  = temp.getStartTime();
				long endTime  = temp.getEndTime();
				if (nowTime < startTime && nowTime < endTime) {
					returnData.add(temp);
				}
			}
			break;
		case MEET_END:
			for (int i = 0; i < all.size(); i++) {
				MeetingDomain temp = all.get(i);
				long endTime  = temp.getEndTime();
				long startTime  = temp.getStartTime();
				if (nowTime > endTime && nowTime > startTime) {
					returnData.add(temp);
				}
			}
			break;

		default:
			returnData.addAll(all);
			break;
		}
		return returnData;
	}

	/**
	 * 报名
	 * 
	 * @param string
	 */
	public static void enterMeeting(String ID, String message) {
		// TODO Auto-generated method stub
		String name = "未知";
		String mobilePhoneNumber = "";
		if (!AVUser.getCurrentUser().getString("realName").isEmpty()) {
			name = AVUser.getCurrentUser().getString("realName");
		}

		if (!AVUser.getCurrentUser().getUsername().isEmpty()) {
			mobilePhoneNumber = AVUser.getCurrentUser().getUsername();
		}

		enteMeetingById(ID, message, name, mobilePhoneNumber);
	}

	/**
	 * 报名会议
	 * 
	 * @param meetingId
	 */
	public static void enteMeetingById(final String meetingId, String message,
			String name, String mobilePhoneNumber) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("meetingId", meetingId);
		parameters.put("userId", ChatService.getSelfId());
		parameters.put("message", message);
		parameters.put("name", name);
		parameters.put("mobilePhoneNumber", mobilePhoneNumber);
		// AVCloud.setProductionMode(false);
		AVCloud.callFunctionInBackground("attendMeeting", parameters,
				new FunctionCallback<Map<String, Object>>() {

					@Override
					public void done(Map<String, Object> result, AVException e) {
						// TODO Auto-generated method stub
						if (e == null) {
							Toast.makeText(App.ctx,
									result.get("errorMessage").toString(),
									Toast.LENGTH_LONG).show();
							// 订阅会议频道
							PushService.subscribe(App.ctx, meetingId,
									JuMainActivity.class);
							AVInstallation.getCurrentInstallation()
									.saveInBackground();

						} else {
							Logger.d("[MeetingService]报名会议-》" + e.getMessage());
							// Toast.makeText(App.ctx,
							// e.getMessage()+"code->"+e.getCode()+e.getLocalizedMessage(),
							// Toast.LENGTH_LONG).show();
						}

					}
				});

	}

	/**
	 * 订阅会议频道
	 * 
	 * @param meetingIds
	 */
	public static void subcribeByIDs(List<MeetingDomain> meetings) {
		// List<String> meetingIds = new ArrayList<String>();
		AVInstallation.getCurrentInstallation().removeAll("channels",
				App.avInstallation.getList("channels"));
		Logger.d("[subcribeByIDs] remove"
				+ App.avInstallation.getList("channels"));
		for (Iterator iterator = meetings.iterator(); iterator.hasNext();) {
			MeetingDomain meet = (MeetingDomain) iterator.next();
			// meetingIds.add(meet.getObjectid());
			// 订阅会议频道
			PushService.subscribe(App.ctx, meet.getObjectid(),
					JuMainActivity.class);
			// AVInstallation.getCurrentInstallation().addUnique("channels",meet.getObjectid());
			// Logger.e("subcribeByIDs->"+meet.getObjectid());
			// Logger.d("[subcribeByIDs]subcribeByIDs "+App.avInstallation.getList("channels"));
		}
		PushService.subscribe(App.ctx, ChatService.getSelfId(),
				JuMainActivity.class);
		Logger.d("[subcribeByIDs] add" + App.avInstallation.getList("channels"));
		// meetingIds.add(ChatService.getSelfId());
		// App.avInstallation.addAllUnique("channels",meetingIds);
		// App.avInstallation.saveInBackground();
		// App.avInstallation.put("channels", meetingIds);
		// App.avInstallation.addAllUnique("channels", meetingIds);
		// AVInstallation.getCurrentInstallation().saveInBackground(new
		// SaveCallback() {
		//
		// @Override
		// public void done(AVException e) {
		// // TODO Auto-generated method stub
		// if (e == null) {
		// // App.avInstallation.saveInBackground();
		// Logger.d("[subcribeByIDs] saveSuccess!"+App.avInstallation.getList("channels"));
		// }else {
		// Logger.e("subcribeByIDs Error"+e.getMessage());
		// }
		// }
		// });

	}

	/**
	 * 退订会议频道
	 * 
	 * @param meetingIds
	 */
	public static void unsubcribeMeetingByIDs(List<MeetingDomain> meetings) {
		for (Iterator iterator = meetings.iterator(); iterator.hasNext();) {
			MeetingDomain meet = (MeetingDomain) iterator.next();
			// 订阅会议频道
			PushService.unsubscribe(App.ctx, meet.getObjectid());
		}

	}

	/**
	 * 会议签到
	 * 
	 * @param meetingId
	 * @param userId
	 */
	public static void checkMeetingIn(final String meetingId,
			final String userId,final String way) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("meetingId", meetingId);
		parameters.put("userId", userId);
		parameters.put("way", way);
		parameters.put("hostObjectId", ChatService.getSelfId());
		// AVCloud.setProductionMode(false);
		AVCloud.callFunctionInBackground("meetingCheckIn", parameters,
				new FunctionCallback<Map<String, Object>>() {

					@Override
					public void done(Map<String, Object> result, AVException e) {
						// TODO Auto-generated method stub
						if (e == null) {
							Toast.makeText(App.ctx,
									result.get("errorMessage").toString(),
									Toast.LENGTH_LONG).show();
//							get("errorMessage")

						} else {
							Logger.d("[MeetingService]位置签到" + e.getMessage());
							// Toast.makeText(App.ctx,
							// e.getMessage()+"code->"+e.getCode()+e.getLocalizedMessage(),
							// Toast.LENGTH_LONG).show();
						}

					}
				});

	}

	/**
	 * 标记会议通知已阅读（发送到服务器）
	 * 
	 * @param msgs
	 */
	public static void markPushAsRead(List<Msg> msgs) {
		JSONArray jsonArray = new JSONArray();
		for (Iterator iterator = msgs.iterator(); iterator.hasNext();) {
			Msg msg = (Msg) iterator.next();
			String pushId = msg.getPushId();
			if (!msg.getPushId().isEmpty()) {
				// Logger.e("pushId[meetingService]-> "+pushId);
				jsonArray.put(pushId);
			}

		}
		// pushId集合
		// String pushIds = jsonArray.toString();
		// 发送标记已阅读
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("pushIds", jsonArray);
		parameters.put("userId", ChatService.getSelfId());
		// AVCloud.setProductionMode(false);
		AVCloud.callFunctionInBackground("markPushAsRead", parameters,
				new FunctionCallback<Object>() {

					@Override
					public void done(Object result, AVException e) {
						// TODO Auto-generated method stub
						if (e == null) {
							Logger.d("markPushAsRead(result) [meetingservice]->"
									+ result.toString());
						} else {
							Logger.d("markPushAsRead(result) [meetingservice]->"
									+ e.getMessage());
						}
					}
				});

	}

	/**
	 * 主动拉取消息
	 */
	public static void getMessages() {
		Map<String, Object> params = new HashMap<String, Object>();
		// JSONObject params = new JSONObject();
		JSONArray meetingIds = new JSONArray();
		JSONArray type = new JSONArray();
		String timeStamp;
		String sort = "asc";

		List<MeetingDomain> meetList = DBMeet.getInformationAll();
		for (int i = 0; i < meetList.size(); i++) {
			meetingIds.put(meetList.get(i).getObjectid());
		}
		type.put("5");
		timeStamp = new NotifyService(ChatService.getSelfId()).getLoginTime();
		// Logger.e("logintime"+timeStamp);
		// Logger.e("channelIds"+meetingIds.toString());

		params.put("channelIds", meetingIds);
		params.put("type", type);
		params.put("timeStamp", timeStamp);
		params.put("sort", sort);

		// if (App.MODE.equals("prod")) {
		//
		// } else {
		// AVCloud.setProductionMode(false);
		// }
		AVCloud.callFunctionInBackground("getPushMessages", params,
				new FunctionCallback<Map<String, Object>>() {

					@Override
					public void done(Map<String, Object> result, AVException e) {
						// TODO Auto-generated method stub
						if (e == null) {
							hanldGetMessages(result);
							// Logger.e("[meetingService]getMessages->"+result.toString());
						} else {
							Logger.e("[meetingService]getMessages->"
									+ e.getMessage());
						}

					}
				});

	}

	/**
	 * 处理主动拉取消息
	 * 
	 * @param result
	 */
	public static void hanldGetMessages(Map<String, Object> result) {
		// TODO Auto-generated method stub
		if (result.get("errorMessage").toString().equals("success")) {
			List<Map<String, Object>> dataList = (List<Map<String, Object>>) result
					.get("data");
			for (int i = 0; i < dataList.size(); i++) {
				Map<String, Object> message = dataList.get(i);
				// 转换数据格式
				Msg msg = Msg.fromGetMessages(message);
				String selfId = ChatService.getSelfId();
				String convid = AVOSUtils.convid(selfId, msg.getFromPeerId());
				msg.setToPeerId(selfId);
				msg.setRoomType(RoomType.NOTIFY);
				msg.setStatus(Msg.Status.SendReceived);
				msg.setReadStatus(Msg.ReadStatus.Unread);
				msg.setConvid(convid);
				// 插入数据库
				DBMsg.insertMsg(msg);
			}
			JuMainActivity.updateMessageUnread(ChatService.getMessageUnread());
		}

	}

	/**
	 * 设置下次拉取消息时间戳
	 */
	public static void setGetMessageTimeStamp() {
		// TODO Auto-generated method stub
		long nowTimeLong = System.currentTimeMillis();
		long change = 8 * 60 * 60 * 1000;// 当前时间与开始时间格式上差八个小时
		nowTimeLong = nowTimeLong + change;
		Date date = new Date(nowTimeLong);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'"
				+ "HH:mm:ss" + ".000Z");

		String loginTimeStr = dateFormat.format(date);
		// 设置下次拉取消息时间戳
		new NotifyService(ChatService.getSelfId()).setLoginTime(loginTimeStr);
	}

}
