package com.hyx.meet.service;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.R.integer;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.AbsoluteLayout;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout.LayoutParams;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.framework.Platform.ShareParams;
import cn.sharesdk.onekeyshare.OnekeyShare;
import cn.sharesdk.onekeyshare.ShareContentCustomizeCallback;

import com.avos.avoscloud.AVCloud;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVGeoPoint;
import com.avos.avoscloud.AVInstallation;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVRelation;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.FollowCallback;
import com.avos.avoscloud.FunctionCallback;
import com.avos.avoscloud.PushService;
import com.avos.avoscloud.SaveCallback;
import com.hyx.meet.R;
import com.hyx.meet.Application.App;
import com.hyx.meet.Application.C;
import com.hyx.meet.Application.SysApplication;
import com.hyx.meet.avobject.User;
import com.hyx.meet.chat.JuChatActivity;
import com.hyx.meet.custome.sweet_dialog.SweetAlertDialog;
import com.hyx.meet.db.DBMeet;
import com.hyx.meet.entity.MeetingDomain;
import com.hyx.meet.login.JuLoginActivity;
import com.hyx.meet.setting.JuSettingsActivity;
import com.hyx.meet.utils.AndroidConfig;
import com.hyx.meet.utils.JuUrl;
import com.hyx.meet.utils.Logger;
import com.hyx.meet.utils.PhotoUtil;
import com.hyx.meet.utils.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.tencent.open.Util;

/**
 * Created by lzw on 14-9-15.
 */
public class UserService {
	public static final int ORDER_UPDATED_AT = 1;
	public static final int ORDER_DISTANCE = 0;
	public static ImageLoader imageLoader = ImageLoader.getInstance();

	/**
	 * 分享名片
	 * 
	 * @param params
	 */
	public static void shareNameCard(Map<String, Object> params, Context context) {
		ShareSDK.initSDK(context);
		OnekeyShare oks = new OnekeyShare();
		String source = URLEncoder.encode("来自名片分享");

		// Logger.e("shareParams->" + params.toString());

		String shareurl = String.format(JuUrl.NAMECARD_SHARE_PROD,
				params.get("userId"), source);// 分享链接
		// Logger.e("[userService]shareurl->"+shareurl);
		String title = "姓名，职位";
		String imageUrl = "头像";

		String description = "简要概述";
		String content = App.ctx.getString(R.string.share_namecard_describ);
		String defaultContent = "默认内容";

		if (!params.get("title").toString().isEmpty()) {
			title = params.get("title").toString();
		}
		if (!params.get("avatar").toString().isEmpty()) {
			imageUrl = params.get("avatar").toString();
		}
		// 关闭sso授权
		oks.disableSSOWhenAuthorize();

		// 分享时Notification的图标和文字
		oks.setNotification(R.drawable.logo_big,
				App.ctx.getString(R.string.app_name));
		// title标题，印象笔记、邮箱、信息、微信、人人网和QQ空间使用
		oks.setTitle(title);
		// titleUrl是标题的网络链接，仅在人人网和QQ空间使用
		oks.setTitleUrl(shareurl);
		// text是分享文本，所有平台都需要这个字段
		oks.setText(content);
		// imagePath是图片的本地路径，Linked-In以外的平台都支持此参数
		// File file = new File(PathUtils.getAppPath()+"1.png");
		// if (file.exists() == true) {
		// Log.i("file", "file is  exist");
		// }
		// oks.setImagePath(PathUtils.getAppPath() + "logo.jpg");//
		// 确保SDcard下面存在此张图片
		// url仅在微信（包括好友和朋友圈）中使用
		oks.setUrl(shareurl);
		oks.setImageUrl(imageUrl);

		// comment是我对这条分享的评论，仅在人人网和QQ空间使用
		oks.setComment(defaultContent);
		// site是分享此内容的网站名称，仅在QQ空间使用
		oks.setSite("会友行");
		// siteUrl是分享此内容的网站地址，仅在QQ空间使用
		oks.setSiteUrl(shareurl);

		oks.setShareContentCustomizeCallback(new ShareContentCustomizeCallback() {

			@Override
			public void onShare(Platform platform, ShareParams paramsToShare) {
				// TODO Auto-generated method stub

			}
		});

		// 启动分享GUI
		oks.show(context);

	}

	/**
	 * 退出
	 */
	public static void logExit() {

		// 系统退出
		SysApplication.getInstance().exit();
	}

	/**
	 * 跳转到聊天界面
	 * 
	 * @param context
	 */
	public static void goToChat(Context context, String toName, String toUserId) {
		Intent intentMessage = new Intent(context, JuChatActivity.class);
		intentMessage.putExtra("toUserName", toName);
		intentMessage.putExtra("chatUserId", toUserId);
		((Activity) context).startActivity(intentMessage);
		((Activity) context).overridePendingTransition(R.anim.zoomin,
				R.anim.zoomout_static);
	}

	/**
	 * 跳转到打电话界面
	 * 
	 * @param context
	 */
	public static void toCall(Context context, String toNumber) {
		if (toNumber.isEmpty()) {
			Toast.makeText(context, "电话号码为空", 0).show();

		} else {
			Intent intent = new Intent();

			intent.setAction(Intent.ACTION_DIAL);
			intent.setData(Uri.parse("tel:" + toNumber));
			((Activity) context).startActivity(intent);
			((Activity) context).overridePendingTransition(R.anim.zoomin,
					R.anim.zoomout_static);

		}

	}

	/**
	 * 跳转到打电话界面
	 * 
	 * @param context
	 */
	public static void toSMS(Context context, String toNumber) {
		if (toNumber.isEmpty()) {
			Toast.makeText(context, "电话号码为空", 0).show();

		} else {
			Intent intent = new Intent();

			intent.setAction(Intent.ACTION_SENDTO);
			intent.setData(Uri.parse("smsto:" + toNumber));
			((Activity) context).startActivity(intent);
			((Activity) context).overridePendingTransition(R.anim.zoomin,
					R.anim.zoomout_static);
		}
	}

	/**
	 * 取消该用户的所有订阅
	 */
	public static void unsubscribe(final DialogInterface dialog,
			final Context context) {

		// 取消用户订阅
		PushService.unsubscribe(App.ctx, ChatService.getSelfId());
		// 退订该用户会议
		List<MeetingDomain> meetings = DBMeet.getInformationAll();
		MeetingService.unsubcribeMeetingByIDs(meetings);
		// 提交更改
		AVInstallation.getCurrentInstallation().saveInBackground(
				new SaveCallback() {

					@Override
					public void done(AVException e) {
						// TODO Auto-generated method stub
						if (e == null) {
							dialog.dismiss();

							Intent intent = new Intent(context,
									JuLoginActivity.class);
							NotifyService notifyService = new NotifyService(
									ChatService.getSelfId());
							// 退出登录
							JuChatServiceV2.closeChat(intent, notifyService,
									context);

						} else {
							Utils.toast(context
									.getString(R.string.pleaseCheckNetwork));
						}
					}
				});
		;
	}

	// private final static String NEW_SYS_MESSAGE = "sys_message";// 系统通知信息
	// private final static String MEET_NOTIFY_NUM = "meet_notify";// 小秘书新通知数量
	// private final static String NEW_FRIEND_REQ_NUM = "new_friend_req";//
	// 好友请求通知数量
	// private final static String IS_LOGOUT = "logout";

	public static void logoutConfirm(final Context context) {
		// final AlertDialog.Builder builder = new AlertDialog.Builder(context);
		// builder.setMessage("您的账号在其他设备登录");
		// builder.setTitle("异常提醒");
		// builder.setPositiveButton("退出", new DialogInterface.OnClickListener()
		// {
		// @Override
		// public void onClick(DialogInterface dialog, int which) {
		// // dialog.dismiss();
		//
		// unsubscribe(dialog,context);
		//
		//
		// }
		// });
		// builder.setCancelable(false);
		// builder.create().show();
		SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context,
				SweetAlertDialog.ERROR_TYPE)
				.setTitleText("异常提醒")
				.setContentText("您的账号在其他设备登录")
				.setConfirmText("退出")
				.showCancelButton(false)
				.setConfirmClickListener(
						new SweetAlertDialog.OnSweetClickListener() {

							@Override
							public void onClick(
									SweetAlertDialog sweetAlertDialog) {
								// TODO Auto-generated method stub
								unsubscribe(sweetAlertDialog, context);

							}
						});
		sweetAlertDialog.setCancelable(false);
		sweetAlertDialog.show();

	}

	/**
	 * 检测是否单机登陆
	 * 
	 * @param user
	 */
	public static void sendCheckOneLog(AVUser user) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("userId", user.getObjectId());
		parameters.put("token", user.getUuid());
		// AVCloud.setProductionMode(false);
		AVCloud.callFunctionInBackground("singleLogout", parameters,
				new FunctionCallback<Object>() {

					@Override
					public void done(Object arg0, AVException arg1) {
						// TODO Auto-generated method stub

					}
				});

	}

	public static User findUser(String id) throws AVException {
		AVQuery<User> q = User.getQuery(User.class);
		q.setCachePolicy(AVQuery.CachePolicy.NETWORK_ELSE_CACHE);
		User user = q.get(id);
		user.setUsername(user.getString("realName"));
		user.setAvatorUrl(user.getString("avator"));
		return user;
	}

	public static List<User> findFriends() throws AVException {
		User curUser = User.curUser();
		AVRelation<User> relation = curUser.getRelation(User.FRIENDS);
		relation.setTargetClass("_User");
		AVQuery<User> query = relation.getQuery(User.class);
		query.setCachePolicy(AVQuery.CachePolicy.NETWORK_ELSE_CACHE);
		List<User> users = query.find();
		return users;
	}

	public static void displayAvatar(String imageUrl, ImageView avatarView) {
		imageLoader.displayImage(imageUrl, avatarView,
				PhotoUtil.avatarImageOptions);
	}

	public static List<User> searchUser(String searchName, int skip)
			throws AVException {
		AVQuery<User> q = User.getQuery(User.class);
		q.whereContains(User.USERNAME, searchName);
		q.limit(C.PAGE_SIZE);
		q.skip(skip);
		User user = User.curUser();
		List<String> friendIds = new ArrayList<String>(
				CacheService.getFriendIds());
		friendIds.add(user.getObjectId());
		q.whereNotContainedIn(C.OBJECT_ID, friendIds);
		q.orderByDescending(C.UPDATED_AT);
		q.setCachePolicy(AVQuery.CachePolicy.NETWORK_ELSE_CACHE);
		List<User> users = q.find();
		CacheService.registerBatchUser(users);
		return users;
	}

	// public static List<User> findNearbyPeople(int skip, int orderType) throws
	// AVException {
	// PreferenceMap preferenceMap = PreferenceMap.getCurUserPrefDao(App.ctx);
	// AVGeoPoint geoPoint = preferenceMap.getLocation();
	// if (geoPoint == null) {
	// Logger.i("geo point is null");
	// return new ArrayList<User>();
	// }
	// AVQuery<User> q = AVObject.getQuery(User.class);
	// User user = User.curUser();
	// q.whereNotEqualTo(C.OBJECT_ID, user.getObjectId());
	// if (orderType == ORDER_DISTANCE) {
	// q.whereNear(User.LOCATION, geoPoint);
	// } else {
	// q.orderByDescending(C.UPDATED_AT);
	// }
	// q.skip(skip);
	// q.setCachePolicy(AVQuery.CachePolicy.NETWORK_ELSE_CACHE);
	// q.limit(C.PAGE_SIZE);
	// List<User> users = q.find();
	// CacheService.registerBatchUser(users);
	// return users;
	// }
	//
	public static void saveSex(User.Gender gender, SaveCallback saveCallback) {
		User user = User.curUser();
		user.setGender(gender);
		user.saveInBackground(saveCallback);
	}

	public static List<String> transformIds(List<? extends AVObject> objects) {
		List<String> ids = new ArrayList<String>();
		for (AVObject o : objects) {
			ids.add(o.getObjectId());
		}
		return ids;
	}

	public static User signUp(String name, String password) throws AVException {
		User user = new User();
		user.setUsername(name);
		user.setPassword(password);
		user.signUp();
		return user;
	}

	public static void cacheUserIfNone(String userId) throws AVException {
		if (CacheService.lookupUser(userId) == null) {
			CacheService.registerUserCache(findUser(userId));
		}
	}

	public static void saveAvatar(String path) throws IOException, AVException {
		User user = User.curUser();
		final AVFile file = AVFile.withAbsoluteLocalPath(user.getUsername(),
				path);
		file.save();
		user.setAvatar(file);

		user.save();
		user.fetch();
	}

	public static void updateUserInfo() {
		User user = User.curUser();
		if (user != null) {
			AVInstallation installation = AVInstallation
					.getCurrentInstallation();
			if (installation != null) {
				user.setInstallation(installation);
				user.saveInBackground();
			}
		}
	}

	// public static void updateUserLocation() {
	// PreferenceMap preferenceMap = PreferenceMap.getCurUserPrefDao(App.ctx);
	// AVGeoPoint lastLocation = preferenceMap.getLocation();
	// if (lastLocation != null) {
	// final User user = User.curUser();
	// final AVGeoPoint location = user.getLocation();
	// if (location == null || !Utils.doubleEqual(location.getLatitude(),
	// lastLocation.getLatitude())
	// || !Utils.doubleEqual(location.getLongitude(),
	// lastLocation.getLongitude())) {
	// user.setLocation(lastLocation);
	// user.saveInBackground(new SaveCallback() {
	// @Override
	// public void done(AVException e) {
	// if (e != null) {
	// e.printStackTrace();
	// } else {
	// Logger.v("lastLocation save " + user.getLocation());
	// }
	// }
	// });
	// }
	// }
	// }

	public static void addFriendById(String addUserId) {

		AVUser.getCurrentUser().followInBackground(addUserId,
				new FollowCallback<AVObject>() {

					@Override
					public void done(AVObject object, AVException e) {
						// TODO Auto-generated method stub
						if (e == null) {
							Utils.toast("已发送请求，等待验证");
						} else if (e.getCode() == AVException.DUPLICATE_VALUE) {
							Utils.toast("已发送请求,请不要重复添加");
						} else {
							Utils.toast(e.getMessage());
						}
					}
				});

	}

	/**
	 * 改变透明框的位置
	 * 
	 * @param i
	 * @param j
	 * @param k
	 * @param l
	 */
	public static void changeClear(int toLeft, int toTop, int width,
			int height, Activity context, AbsoluteLayout mGuideOptLay) {
		// TODO Auto-generated method stub
		DisplayMetrics metrics = new DisplayMetrics();
		context.getWindowManager().getDefaultDisplay().getMetrics(metrics);
		int WIDTH = metrics.widthPixels;
		int HEIGHT = metrics.heightPixels;
		final Drawable clearDrawable = context.getResources().getDrawable(
				R.drawable.clear_round);

		mGuideOptLay.removeAllViews();
		ImageView clear = new ImageView(context);
		View leftView = new View(context);
		View rightView = new View(context);
		View topView = new View(context);
		View bottomView = new View(context);
		// 添加透明区域
		AbsoluteLayout.LayoutParams clearLp = new AbsoluteLayout.LayoutParams(
				width, height, toLeft, toTop);

		clear.setLayoutParams(clearLp);
		clear.setScaleType(ScaleType.FIT_XY);
		clear.setImageDrawable(clearDrawable);
		clear.setId(1);
		mGuideOptLay.addView(clear);

		// ImageView clearView = (ImageView)
		// findViewById(mGuideOptLay.getChildAt(
		// 0).getId());
		int clearRight = toLeft + width;
		int clearBottom = toTop + height;

		// 添加周围背景
		AbsoluteLayout.LayoutParams leftLp = new AbsoluteLayout.LayoutParams(
				toLeft, height, 0, toTop);
		leftView.setLayoutParams(leftLp);
		leftView.setBackgroundColor(Color.parseColor("#99000000"));
		mGuideOptLay.addView(leftView);

		AbsoluteLayout.LayoutParams rightLp = new AbsoluteLayout.LayoutParams(
				WIDTH - clearRight, height, clearRight, toTop);
		rightView.setLayoutParams(rightLp);
		rightView.setBackgroundColor(Color.parseColor("#99000000"));
		mGuideOptLay.addView(rightView);

		AbsoluteLayout.LayoutParams topLp = new AbsoluteLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, clearBottom - height, 0, 0);
		topView.setLayoutParams(topLp);
		topView.setBackgroundColor(Color.parseColor("#99000000"));
		mGuideOptLay.addView(topView);

		AbsoluteLayout.LayoutParams bottomLp = new AbsoluteLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, HEIGHT - clearBottom, 0, clearBottom);
		bottomView.setLayoutParams(bottomLp);
		bottomView.setBackgroundColor(Color.parseColor("#99000000"));
		mGuideOptLay.addView(bottomView);

	}

}
