package com.hyx.meet.service.reciever;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import com.avos.avoscloud.AVUser;
import com.hyx.meet.Application.App;
import com.hyx.meet.meet.JuMainActivity;
import com.hyx.meet.namecard.JuNameCardShowActivity;
import com.hyx.meet.service.ChatService;
import com.hyx.meet.service.listener.MsgListener;
import com.hyx.meet.utils.Logger;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class JuPushReciever extends BroadcastReceiver {

	public static Set<MsgListener> msgListeners = new HashSet<MsgListener>();

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		System.out.println("recieverAction->" + intent.getAction());
		
		if (intent.getAction().equals("com.huiyouxing.openChatView")) {
			System.out.println("openChatView->meeting通告"+intent.getExtras().getString("com.avos.avoscloud.Data"));			
			try {
				String Data = intent.getExtras().getString("com.avos.avoscloud.Data");
				JSONObject jsonObject = new JSONObject(Data);
				if (AVUser.getCurrentUser() != null) {
					ChatService.onRecive(context, jsonObject, msgListeners);					
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//			处理消息
		}
		
	}

	public static void addMsgListener(MsgListener listener) {
		msgListeners.add(listener);
	}

	public static void removeMsgListener(MsgListener listener) {
		msgListeners.remove(listener);
	}

}
