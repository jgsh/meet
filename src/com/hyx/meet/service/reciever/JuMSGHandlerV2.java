package com.hyx.meet.service.reciever;

import java.util.HashSet;
import java.util.Set;

import com.avos.avoscloud.im.v2.AVIMClient;
import com.avos.avoscloud.im.v2.AVIMConversation;
import com.avos.avoscloud.im.v2.AVIMMessage;
import com.avos.avoscloud.im.v2.AVIMMessageHandler;
import com.avos.avoscloud.im.v2.AVIMMessageManager;
import com.avos.avoscloud.im.v2.AVIMTypedMessage;
import com.avos.avoscloud.im.v2.AVIMTypedMessageHandler;
import com.hyx.meet.service.JuChatServiceV2;
import com.hyx.meet.service.listener.MsgListener;
import com.hyx.meet.utils.Logger;

/**
 * leacloudV2 MSG消息接受回调类
 * 
 * @author ww
 *
 */
public class JuMSGHandlerV2 extends AVIMTypedMessageHandler<AVIMTypedMessage> {

	public static Set<MsgListener> msgListeners = new HashSet<MsgListener>();

	/**
	 * 接收消息的回调
	 */
	@Override
	public void onMessage(AVIMTypedMessage message,
			AVIMConversation conversation, AVIMClient client) {
		// TODO Auto-generated method stub
		super.onMessage(message, conversation, client);
//		Logger.d("有新消息到达"+message.getContent());
		JuChatServiceV2.onMessage(message, conversation, client,msgListeners);
	}
	/**
	 * 消息被接收的回调
	 */
	@Override
	public void onMessageReceipt(AVIMTypedMessage message,
			AVIMConversation conversation, AVIMClient client) {
		// TODO Auto-generated method stub
		super.onMessageReceipt(message, conversation, client);
	}
	public static void addMsgListener(MsgListener listener) {
		msgListeners.add(listener);
	}

	public static void removeMsgListener(MsgListener listener) {
		msgListeners.remove(listener);
	}

}
