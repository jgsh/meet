package com.hyx.meet.service.reciever;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.content.Context;

import com.avos.avoscloud.AVMessage;
import com.avos.avoscloud.AVMessageReceiver;
import com.avos.avoscloud.Session;
import com.hyx.meet.service.ChatService;
import com.hyx.meet.service.listener.MsgListener;
import com.hyx.meet.utils.Logger;

public class JuMSGReciever extends AVMessageReceiver {

//	public static StatusListener statusListener;
	  public static Set<String> onlineIds = new HashSet<String>();
	  public static Set<MsgListener> msgListeners = new HashSet<MsgListener>();
	  private static boolean sessionPaused = true;

	@Override
	public void onError(Context context, Session session, Throwable arg2) {
		// TODO Auto-generated method stub
		Logger.d("onError");
//		System.out.println("errorMsg->"+arg2.getMessage());

	}

	/**
	 * 处理接受到的消息，一条新消息到达
	 */
	@Override
	public void onMessage(Context context, Session session, AVMessage avMsg) {
		// TODO Auto-generated method stub
		Logger.d("onMessage");
//		ChatService.onMessage(context, avMsg, msgListeners, null);
		

	}

	/**
	 * 消息回执
	 */
	@Override
	public void onMessageDelivered(Context context, Session session, AVMessage arg2) {
		// TODO Auto-generated method stub
		Logger.d("onMessageDelivered");
	}

	@Override
	public void onMessageFailure(Context context, Session session, AVMessage avMsg) {
		// TODO Auto-generated method stub
		Logger.d("onMessageFailure");
//		System.out.println("onMessage->"+avMsg.getMessage());
//	    ChatService.onMessageFailure(avMsg, msgListeners, null);
		
	}

	@Override
	public void onMessageSent(Context context, Session session, AVMessage avMsg) {
		// TODO Auto-generated method stub

		Logger.d("onMessageSent");
//	    ChatService.onMessageSent(avMsg, msgListeners, null);

	}

	@Override
	public void onPeersUnwatched(Context context, Session session, List<String> arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPeersWatched(Context context, Session session, List<String> arg2) {
		// TODO Auto-generated method stub

	}
	

	@Override
	public void onSessionOpen(Context context, Session session) {
		// TODO Auto-generated method stub
		 	Logger.d("onSessionOpen");
			System.out.println("sessionOpen->用户成功登陆条提案服务器");
			sessionPaused = false;
	}

	@Override
	public void onSessionPaused(Context context, Session session) {
		// TODO Auto-generated method stub
		Logger.d("onSessionPaused");
		System.out.println("sessionOpen->pause");
		sessionPaused = true;

	}

	@Override
	public void onSessionResumed(Context context, Session session) {
		// TODO Auto-generated method stub
		Logger.d("onSessionResumed");
		System.out.println("sessionOpen->resumed");
		sessionPaused = false;

	}

	@Override
	public void onStatusOffline(Context context, Session session, List<String> arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusOnline(Context context, Session session, List<String> arg2) {
		// TODO Auto-generated method stub

	}
	
	public static boolean isSessionPaused() {
	    return sessionPaused;
	  }
	
	public static void setSessionPaused(boolean sessionPaused) {
		JuMSGReciever.sessionPaused = sessionPaused;
	}
	
//	public static void registerStatusListener(StatusListener listener) {
//	    statusListener = listener;
//	  }

//	  public static void unregisterSatutsListener() {
//	    statusListener = null;
//	  }

	  public static void addMsgListener(MsgListener listener) {
	    msgListeners.add(listener);
	  }

	  public static void removeMsgListener(MsgListener listener) {
	    msgListeners.remove(listener);
	  }

	@Override
	public void onSessionClose(Context arg0, Session arg1) {
		// TODO Auto-generated method stub
		
	}

//	  public static List<String> getOnlineIds() {
//	    return new ArrayList<String>(onlineIds);
//	  }

}
