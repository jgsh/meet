package com.hyx.meet.service.reciever;

import com.avos.avoscloud.im.v2.AVIMClient;
import com.avos.avoscloud.im.v2.AVIMClientEventHandler;

/**
 * 网络事件响应接口
 * @author ww
 *
 */
public class JuNetWorkHandlerV2 extends AVIMClientEventHandler {

	/**
	 *网络连接断开事件发生，此时聊天服务不可用 
	 */
	@Override
	public void onConnectionPaused(AVIMClient client) {
		// TODO Auto-generated method stub

	}

	/**
	 * 网络连接恢复正常，此时聊天服务变得可用
	 */
	@Override
	public void onConnectionResume(AVIMClient client) {
		// TODO Auto-generated method stub

	}

}
