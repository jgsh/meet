package com.hyx.meet.service;

import com.hyx.meet.Application.App;

import android.R.bool;
import android.R.integer;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * 推送消息数量存储表
 * @author ww
 *
 */
public class NotifyService {

	private final static String NEW_SYS_MESSAGE = "sys_message_";// 系统通知信息
	private final static String MEET_NOTIFY_NUM = "meet_notify";// 小秘书新通知数量
	private final static String NEW_FRIEND_REQ_NUM = "new_friend_req";// 好友请求通知数量
	private final static String IS_LOGOUT = "logout";
	private final static String NOTIFY_SUM = "notify_sum";
	private final static String LOGIN_TIME = "login_time";

	private static SharedPreferences sp ;
	private static Editor editor ;
	
	public NotifyService(String userId) {
		// TODO Auto-generated constructor stub
		this.sp = App.ctx.getSharedPreferences(
				NEW_SYS_MESSAGE+userId, App.ctx.MODE_PRIVATE);
		this.editor =  sp.edit();
	}

	/**
	 * 获取系统通知存储表
	 * 
	 * @return
	 */
	public static SharedPreferences getSystemSP(String userId) {
		return sp;
	}
	
	/**
	 * 获取登录时间戳
	 * @return
	 */
	public static String getLoginTime(){
		return sp.getString(LOGIN_TIME, "2015-01-01T09:29:41.000Z");
	}
	
	/**
	 * 设置登录时间戳
	 * @param login_time
	 */
	public static void setLoginTime(String login_time){
		editor.putString(LOGIN_TIME, login_time);
		editor.commit();
	}

	/**
	 * 获取会议通知数量
	 * @return
	 */
	public static int getMeetNotifyNum(){
		return sp.getInt(MEET_NOTIFY_NUM, 0);
	}
	
	/**
	 * 获取好友关注/取消关注 通知数量
	 * @return
	 */
	public static int getFriendNotifyNum(){
		return sp.getInt(NEW_FRIEND_REQ_NUM, 0);
	}
	
	/**
	 * 获取是否登出
	 * @return
	 */
	public static boolean getIsLogout(){
		return sp.getBoolean(IS_LOGOUT, false);
	}
	
	/**
	 * 设置会议通知数量
	 * @param num
	 */
	public static void setMeetNotifyNum(int num){
		editor.putInt(MEET_NOTIFY_NUM, num);
		editor.commit();
	}

	/**
	 * 设置好友通知  数量
	 * @param num
	 */
	public static void setFriendNotifyNum(int num){
		editor.putInt(NEW_FRIEND_REQ_NUM, num);
		editor.commit();
	}
	
	/**
	 * 设置是否登出
	 * @param is_logout
	 */
	public static void setIsLogout(boolean is_logout){
		editor.putBoolean(IS_LOGOUT, is_logout);
		editor.commit();
	}
	
	/**
	 * 设置消息总数量
	 * @param num
	 */
	public static void  setNotifySum(int num){
		editor.putInt(NOTIFY_SUM, num);
		editor.commit();
	}
	
	/**
	 * 获取消息总数量
	 * @return
	 */
	public static int  getNotifySum(){
		return sp.getInt(NOTIFY_SUM, 0);
	}



}
