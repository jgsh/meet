package com.hyx.meet.utils;


import java.util.Comparator;

import com.hyx.meet.entity.NameCard;

public class PinyinComparator implements Comparator<NameCard> {
  public int compare(NameCard o1, NameCard o2) {
    if (o1.getSortLetters().equals("@")
        || o2.getSortLetters().equals("#")) {
      return -1;
    } else if (o1.getSortLetters().equals("#")
        || o2.getSortLetters().equals("@")) {
      return 1;
    } else {
      return o1.getSortLetters().compareTo(o2.getSortLetters());
    }
  }

}
