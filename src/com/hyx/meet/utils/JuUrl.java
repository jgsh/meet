
package com.hyx.meet.utils;

/**
 * 会友行url地址类
 * 
 * @author ww
 *
 */
public class JuUrl {
	// dev.juyx.avosapps.com

	public final static String MODE_STYLE = "dev";
//	public final static String PROD_MODE = "http://www.huiyouxing.com/";// 生产模式
	public final static String PROD_MODE = "https://juyx.avosapps.com/";// 生产模式

	// public final static String PROD_MODE =
	// "http://dev.juyx.avosapps.com/";//生产模式
	// public final static String DEV_MODE = "file:///android_asset/";//开发模式
	public final static String DEV_MODE = "file:///mnt/sdcard/hyx/www/";// 开发模式

	public final static String DEV_HEADER = DEV_MODE + "comm/header.html";
	public final static String DEV_FOOTER = DEV_MODE + "comm/footer.html";

	// 会议详情(生产/开发)
	public final static String MEETING_DETAIL_PROD = PROD_MODE
			+ "meetingShowDetail.html?meetingId=%s&userId=%s&layout=true&share=false";
	public final static String MEETING_DETAIL_DEV = DEV_MODE
			+ "meetingShowDetail.html?meetingId=%s&userId=%s&layout=true&share=false";
	// 报名会议(生产/开发)
	public final static String MEETING_SIGNIN_PROD = PROD_MODE
			+ "meetingShowDetail.html?weixin=true&layout=true&meetingId=%s";
	public final static String MEETING_SIGNIN_DEV = DEV_MODE
			+ "meetingShowDetail.html?weixin=true&layout=true&meetingId=%s";
	// 发布会议（生产/开发）
	public final static String MEETING_EDIT_PROD = PROD_MODE
			+ "meetingEdit.html?meetingId=0&userId=%s&layout=true";
	public final static String MEETING_EDIT_DEV = DEV_MODE
			+ "meetingEdit.html?meetingId=0&userId=%s&layout=true";
	// 内管理会议列表
	public final static String MEETING_MANAGELIST_PROD = PROD_MODE
			+ "meetingShowList.html?&userId=%s&layout=true&mlist=true";
	public final static String MEETING_MANAGELIST_DEV = DEV_MODE
			+ "meetingShowList.html?&userId=%s&layout=true&mlist=true";
	// 个人资料编辑页
	public final static String USER_EDIT_PROD = PROD_MODE
			+ "userEdit.html?userId=%s&layout=true";
	public final static String USER_EDIT_DEV = DEV_MODE
			+ "userEdit.html?userId=%s&layout=true";
	// 用户注册
	public final static String USER_REGISTER_PROD = PROD_MODE
			+ "userSignupPhoneNumber.html?layout=true";
	public final static String USER_REGISTER_DEV = DEV_MODE
			+ "userSignupPhoneNumber.html?layout=true";
	// 忘记密码
	public final static String USER_PASSWARD_PROD = PROD_MODE
			+ "userPasswordPhoneNumber.html?layout=true";
	public final static String USER_PASSWARD_DEV = DEV_MODE
			+ "userPasswordPhoneNumber.html?layout=true";
	// 好有请求列表
	public final static String USER_REQUEST_PROD = PROD_MODE
			+ "contactsReq.html?userId=%s&layout=true";
	public final static String USER_REQUEST_DEV = DEV_MODE
			+ "contactsReq.html?userId=%s&layout=true";
	// 小秘书
	public final static String SYSTEM_PROD = PROD_MODE
			+ "userNotification.html?userId=%s&layout=true";
	public final static String SYSTEM_DEV = DEV_MODE
			+ "userNotification.html?userId=%s&layout=true";
	// 发布会以引导页面
	public final static String MEETING_EDIT_GUIDE_PROD = PROD_MODE
			+ "meetingEditGuide.html?layout=true&userId=%s";
	public final static String MEETING_EDIT_GUIDE_DEV = DEV_MODE
			+ "meetingEditGuide.html?layout=true&userId=%s";

	// 新功能统计页面
	public final static String NEW_FEATURE_PROD = PROD_MODE
			+ "newFeature.html?userId=%s&layout=true";
	public final static String NEW_FEATURE_DEV = DEV_MODE
			+ "newFeature.html?userId=%s&layout=true";

	// 名片分享链接
	public final static String NAMECARD_SHARE_PROD = PROD_MODE
			+ "contactExchange.html?layout=true&touserId=%s&source=%s&share=true";
	// 版本介绍
	public final static String VERSION_PROD = PROD_MODE
			+ "versionHistory.html?layout=true";
	public final static String VERSION_DEV = DEV_MODE
			+ "versionHistory.html?layout=true";

	// 邀请函
	public final static String INVITING_PROD = PROD_MODE
			+ "meetingQRCheckIn.html?meetingId=%s&userId=%s&layout=true";
	public final static String INVITING_DEV = DEV_MODE
			+ "meetingQRCheckIn.html?meetingId=%s&userId=%s&layout=true";

	/**
	 * 获取会议报名链接
	 * 
	 * @return
	 */
	public static String getMeetingSignIn() {
		if (MODE_STYLE.equals("prod")) {
			return MEETING_SIGNIN_PROD;
		} else {
			return MEETING_SIGNIN_DEV;
		}
	}
	
	/**
	 * 获取邀请函链接
	 * 
	 * @return
	 */
	public static String getInviting() {
		if (MODE_STYLE.equals("prod")) {
			return INVITING_PROD;
		} else {
			return INVITING_DEV;
		}
	}


}
