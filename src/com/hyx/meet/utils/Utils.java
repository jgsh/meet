/**
 * 
 */
package com.hyx.meet.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.hyx.meet.Application.App;
import com.hyx.meet.avobject.User;
import com.hyx.meet.custome.JsBridge.BridgeWebView;
import com.hyx.meet.custome.sweet_dialog.SweetAlertDialog;
import com.hyx.meet.service.listener.ActivityFinshListener;

/**
 * 会有行工具类
 * 
 * @author ww
 *
 */
public class Utils {

	public static void showProcessDialog(Context context, String Message) {
		new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE)
				.setTitleText(Message).show();
	}

	public static void showCommonSuccessDialog(Context context, String Message) {
		new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
				.setTitleText("提示")
				.setContentText(Message)
				.setConfirmText("我知道了")
				.showCancelButton(false)
				.setConfirmClickListener(
						new SweetAlertDialog.OnSweetClickListener() {

							@Override
							public void onClick(
									SweetAlertDialog sweetAlertDialog) {
								// TODO Auto-generated method stub
								sweetAlertDialog.dismiss();
							}
						}).show();
	}
	
	/**
	 * 短信分享成功界面
	 * @param context
	 * @param Message
	 */
	public static void showShareBySMSCommonSuccessDialog(Context context, String Message,final ActivityFinshListener listener) {
		new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
				.setTitleText("提示")
				.setContentText(Message)
				.setConfirmText("我知道了")
				.showCancelButton(false)
				.setConfirmClickListener(
						new SweetAlertDialog.OnSweetClickListener() {

							@Override
							public void onClick(
									SweetAlertDialog sweetAlertDialog) {
								// TODO Auto-generated method stub
								sweetAlertDialog.dismiss();
								listener.finish();
							}
						}).show();
	}


	public static void showCommonFailDialog(Context context, String Message) {
		new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
				.setTitleText("提示")
				.setContentText(Message)
				.setConfirmText("我知道了")
				.showCancelButton(false)
				.setConfirmClickListener(
						new SweetAlertDialog.OnSweetClickListener() {

							@Override
							public void onClick(
									SweetAlertDialog sweetAlertDialog) {
								// TODO Auto-generated method stub
								sweetAlertDialog.dismiss();
							}
						}).show();
	}

	public static void showCommonDialog(Context context, String Message) {
		new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE)
				.setTitleText("提示")
				.setContentText(Message)
				.setConfirmText("我知道了")
				.showCancelButton(false)
				.setConfirmClickListener(
						new SweetAlertDialog.OnSweetClickListener() {

							@Override
							public void onClick(
									SweetAlertDialog sweetAlertDialog) {
								// TODO Auto-generated method stub
								sweetAlertDialog.dismiss();
							}
						}).show();
	}

	public static Drawable getMeetingDetail() {
		// return
		// App.ctx.getResources().getDrawable(R.drawable.firstuse_detail);
		return null;
	}

	/**
	 * 判断网络是否连接正常
	 * 
	 * @param context
	 * @return
	 */
	public static boolean isNetworkConnected(Context context) {
		if (context != null) {
			ConnectivityManager mConnectivityManager = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetworkInfo = mConnectivityManager
					.getActiveNetworkInfo();
			if (mNetworkInfo != null) {
				return mNetworkInfo.isAvailable();
			}
		}
		return false;
	}

	public static User mapToUser(Map<String, Object> map) {

		User user = new User();
		user.setObjectId((String) map.get("objectId"));
		user.setUsername((String) map.get("realName"));
		user.setMobilePhoneNumber((String) map.get("mobilePhoneNumber"));
		user.setAvatorUrl((String) map.get("avator"));

		return user;

	}

	public static void toast(String s) {
		toast(App.ctx, s);
	}

	public static void toast(Context context, String str) {
		Toast.makeText(context, str, Toast.LENGTH_SHORT).show();
	}

	public static void fixAsyncTaskBug() {
		new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params) {
				return null;
			}
		}.execute();
	}

	public static boolean isListNotEmpty(Collection<?> collection) {
		if (collection != null && collection.size() > 0) {
			return true;
		}
		return false;
	}

	/**
	 * 电话号码格式的判定
	 * 
	 * @param mobiles
	 * @return
	 */
	public static boolean isMobileNO(String mobiles) {
//		^((+86)|(86))?
		Pattern p = Pattern
				.compile("^((\\+86)|(86))?((13[0-9])|(17\\d)|(14\\d)|(15[^4,\\D])|(18[0-9]))\\d{8}$");
		Matcher m = p.matcher(mobiles);
		return m.matches();
	}

	public static String uuid() {
		// return UUID.randomUUID().toString().substring(0, 24);
		return myUUID();
	}

	public static String myUUID() {
		StringBuilder sb = new StringBuilder();
		int start = 48, end = 58;
		appendChar(sb, start, end);
		appendChar(sb, 65, 90);
		appendChar(sb, 97, 123);
		String charSet = sb.toString();
		StringBuilder sb1 = new StringBuilder();
		for (int i = 0; i < 24; i++) {
			int len = charSet.length();
			int pos = new Random().nextInt(len);
			sb1.append(charSet.charAt(pos));
		}
		return sb1.toString();
	}

	public static void appendChar(StringBuilder sb, int start, int end) {
		int i;
		for (i = start; i < end; i++) {
			sb.append((char) i);
		}
	}

	public static void hideSoftInputView(Activity activity) {
		if (activity.getWindow().getAttributes().softInputMode != WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN) {
			InputMethodManager manager = (InputMethodManager) activity
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			View currentFocus = activity.getCurrentFocus();
			if (currentFocus != null) {
				manager.hideSoftInputFromWindow(currentFocus.getWindowToken(),
						InputMethodManager.HIDE_NOT_ALWAYS);
			}
		}
	}

	public static String md5(String string) {
		byte[] hash = null;
		try {
			hash = string.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("Huh,UTF-8 should be supported?", e);
		}
		return computeMD5(hash);
	}

	public static String computeMD5(byte[] input) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(input, 0, input.length);
			byte[] md5bytes = md.digest();

			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < md5bytes.length; i++) {
				String hex = Integer.toHexString(0xff & md5bytes[i]);
				if (hex.length() == 1)
					hexString.append('0');
				hexString.append(hex);
			}
			return hexString.toString();
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	public static ProgressDialog showSpinnerDialog(Activity activity) {
		// activity = modifyDialogContext(activity);

		ProgressDialog dialog = new ProgressDialog(activity);
		dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		dialog.setCancelable(true);
		dialog.setMessage("努力加载中...");
		if (activity.isFinishing() == false) {
			dialog.show();
		}
		return dialog;
	}

	public static void cancelNotification(Context ctx, int notifyId) {
		String ns = Context.NOTIFICATION_SERVICE;
		NotificationManager nMgr = (NotificationManager) ctx
				.getSystemService(ns);
		nMgr.cancel(notifyId);
	}

	/**
	 * 获取应用版本号
	 * 
	 * @return
	 */
	public static String getVersionCode() {
		try {
			PackageInfo info = App.ctx.getPackageManager().getPackageInfo(
					App.ctx.getPackageName(), 0);
			int versionCode = info.versionCode;
			return versionCode + "";
		} catch (Exception e) {
		}
		return "";
	}

	/**
	 * 获取版本号 int
	 * 
	 * @return
	 */
	public static int getVersionCodeInt() {
		try {
			PackageInfo info = App.ctx.getPackageManager().getPackageInfo(
					App.ctx.getPackageName(), 0);
			int versionCode = info.versionCode;
			return versionCode;
		} catch (Exception e) {
		}
		return 1;
	}

	/**
	 * 获取应用版本名称
	 * 
	 * @return
	 */
	public static String getVersionName() {
		try {
			PackageInfo info = App.ctx.getPackageManager().getPackageInfo(
					App.ctx.getPackageName(), 0);
			String versionName = info.versionName;
			return versionName;
		} catch (Exception e) {
		}
		return "";
	}

	/**
	 * 从sd卡获取html字符串（header+网页+footer）
	 * 
	 * @param webview
	 * @param url
	 * @return
	 */
	public static void getWWWFromSD(BridgeWebView webView, String url) {
		new getWWWFromSDTask(webView, url, App.ctx, false).execute();

	}
	/**
	 * 下载文件到指定目录
	 * @param url
	 * @param toFile
	 * @throws IOException
	 */
	public static void downloadFileIfNotExists(String url, File toFile)
			throws IOException {
		if (toFile.exists()) {
		} else {
			downloadFile(url, toFile);
		}
	}

	public static void downloadFile(String url, File toFile) throws IOException {
		toFile.createNewFile();
		FileOutputStream outputStream = new FileOutputStream(toFile);
		InputStream inputStream = inputStreamFromUrl(url);
		inputToOutput(outputStream, inputStream);
	}

	public static InputStream inputStreamFromUrl(String url)
			throws IOException, ClientProtocolException {
		DefaultHttpClient client = new DefaultHttpClient();
		HttpGet get = new HttpGet(url);
		HttpResponse response = client.execute(get);
		HttpEntity entity = response.getEntity();
		InputStream stream = entity.getContent();
		return stream;
	}

	public static void inputToOutput(FileOutputStream outputStream,
			InputStream inputStream) throws IOException {
		byte[] buffer = new byte[1024];
		int len;
		while ((len = inputStream.read(buffer)) != -1) {
			outputStream.write(buffer, 0, len);
		}
		outputStream.close();
		inputStream.close();
	}



}

/**
 * 获取网页文件字符串任务类
 * 
 * @author ww
 *
 */
class getWWWFromSDTask extends NetAsyncTask {

	BridgeWebView mWebView;
	String mUrl;
	String mContent;

	protected getWWWFromSDTask(BridgeWebView webView, String url, Context ctx,
			boolean openDialog) {
		super(ctx, openDialog);
		// TODO Auto-generated constructor stub
		mWebView = webView;
		mUrl = url;
	}

	@Override
	protected void doInBack() throws Exception {
		// TODO Auto-generated method stub
		// 获取文件的路径
		String headerPath = PathUtils.getWWWDir() + "comm/header.html";
		String footerPath = PathUtils.getWWWDir() + "comm/footer.html";
		Uri uri = Uri.parse(mUrl);
		String path = PathUtils.getWWWDir() + uri.getLastPathSegment();
		// 取得文件输入流
		File header = new File(headerPath);
		File footer = new File(footerPath);
		File mid = new File(path);
		FileInputStream headerInputStream = new FileInputStream(header);
		FileInputStream footerInputStream = new FileInputStream(footer);
		FileInputStream midInputStream = new FileInputStream(mid);
		// 拼接字符串
		mContent = fromInputStream(headerInputStream, "UTF-8")
				+ fromInputStream(midInputStream, "UTF-8")
				+ fromInputStream(footerInputStream, "UTF-8");
		// FileOutputStream out = new FileOutputStream(new
		// File(PathUtils.getWWWDir()+"temp.html") );
		// OutputStreamWriter outputStreamWriter = new OutputStreamWriter(out);
		// outputStreamWriter.write(mContent);
		// outputStreamWriter.flush();
		// outputStreamWriter.close();
		FileWriter writer = new FileWriter(new File(PathUtils.getWWWDir()
				+ "temp.html"));
		writer.write(mContent);
		writer.flush();
		writer.close();
	}

	@Override
	protected void onPost(Exception e) {
		// TODO Auto-generated method stub
		// 加载拼接后的网页
		if (e == null) {
			Uri uri = Uri.parse(mUrl);
			String url = mUrl.replace(uri.getLastPathSegment(), "temp.html");
			mWebView.load(url, null);
		} else {
			Utils.toast("加载失败");
			Logger.d("[Util getWWWFromSDTask]" + e.getMessage());
		}
		// mWebView.load(mUrl, mContent);
		// Logger.d("[Util getWWWFromSDTask] String:"+url+":"+mUrl);

	}

	/**
	 * 输入流转换为字符串
	 * 
	 * @param fileInputStream
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	protected String fromInputStream(FileInputStream fileInputStream,
			String encode) throws UnsupportedEncodingException {
		InputStreamReader reader = new InputStreamReader(fileInputStream,
				"UTF-8");
		// ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		StringBuffer sb = new StringBuffer();
		String result = "";

		try {
			while (reader.ready()) {
				sb.append((char) reader.read());
			}
			reader.close();
			fileInputStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		result = new String(sb.toString().getBytes("UTF-8"), "UTF-8");
		return result;
	}

}
