package com.hyx.meet.utils;

import android.app.Activity;
import android.util.DisplayMetrics;

public class AndroidConfig {
	
	/**
	 * 获取屏幕高度
	 * @param context
	 * @return
	 */
	public static int getScreenHeight(Activity context){
		
		DisplayMetrics metrics = new DisplayMetrics();
		context.getWindowManager().getDefaultDisplay().getMetrics(metrics);;
		
		
		return metrics.heightPixels;
		
	}
	
	/**
	 * 获取屏幕宽度
	 * @param context
	 * @return
	 */
	public static int getScreenWidth(Activity context){
		
		DisplayMetrics metrics = new DisplayMetrics();
		context.getWindowManager().getDefaultDisplay().getMetrics(metrics);;
		
		
		return metrics.widthPixels;
	}
		
		

}
