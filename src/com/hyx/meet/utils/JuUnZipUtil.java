package com.hyx.meet.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import com.avos.avoscloud.AVCloud;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.FunctionCallback;
import com.hyx.meet.Application.App;
import com.hyx.meet.custome.sweet_dialog.SweetAlertDialog;
import com.hyx.meet.service.UserService;
import com.hyx.meet.setting.JuSettingsActivity;

import android.R.integer;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.AssetManager;

/**
 * 解压zip文件到可读写目录中
 * 
 * @author ww
 *
 */
public class JuUnZipUtil {

	// {
	// "errorCode": 0,
	// "errorMessage": "获取版本成功",
	// "data": {
	// "version": 2, // int 类型；网页版本号，区别于 app 版本号
	// "zipUrl": "http://ac-ahjekkg2.clouddn.com/218747c621163a6e.zip", //
	// 网页压缩包地址
	// "zipSize": "2.02M", // 压缩包大小
	// "isForceUpdate":0 // 是否强制更新，0否；1是。
	// }
	// }

	// static String mZipout;
	// static String mWwwout;
	public static SharedPreferences sp;
	public static Editor editor;
	public static String VERSION_CODE = "wwwVersionCode";
	public static String VERSION_CODE_CACHE = "wwwVersionCodeCache";
	public static String SP_NAME = "wwwVersion";
	public static final int ORIGIN_VERSIONCODE = 11;

	public JuUnZipUtil() {
		this.sp = App.ctx.getSharedPreferences(SP_NAME, App.ctx.MODE_PRIVATE);
		this.editor = this.sp.edit();
	}

	/**
	 * 线上下载解压zip到指定目录
	 * 
	 * @param in
	 * @param out
	 * @param context
	 * @param isReplaceAll
	 */
	public static void unZipToHYX(final String zipout, final Context context,
			final boolean openDialog,final boolean showToast) {
		// mZipout = zipout;
		// mWwwout = wwwout;
//		if (App.MODE.equals("prod")) {
//
//		} else {
//			AVCloud.setProductionMode(false);
//		}
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("platform", "android");
		AVCloud.callFunctionInBackground("verLatest", parameters,
				new FunctionCallback<Map<String, Object>>() {

					@Override
					public void done(Map<String, Object> result, AVException e) {
						// TODO Auto-generated method stub
						if (e == null) {
							if ((Integer) result.get("errorCode") == 0) {
								Logger.d("[JuUnzip] unZipToHYX:"
										+ result.toString());
								Map<String, Object> data = new HashMap<String, Object>();
								data = (Map<String, Object>) result.get("data");
								int newVersion = (Integer) data.get("version");
								int isForceUpdate = (Integer) data
										.get("isForceUpdate");
								String zipUrl = data.get("zipUrl").toString();
								String zipSize = data.get("zipSize").toString();
								Logger.d("[JuUnzip] unZipToHYX:"
										+ "newVersion:" + newVersion
										+ ",zipUrl:" + zipUrl);
								// 如果版本更新了，就更新包
								if (sp.getInt(VERSION_CODE, ORIGIN_VERSIONCODE) < newVersion
										|| !PathUtils.check(PathUtils
												.getAppPath() + "www")) {
									new DownLoaderTask(zipUrl, zipout, context,
											openDialog).execute();
									// 记录这次的缓存版本
									editor.putInt(VERSION_CODE_CACHE,
											newVersion);
									editor.commit();

								} else {
									if (showToast) {
//										Utils.toast("已是最新网页版本");
										new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
									    .setTitleText("提示")
									    .setContentText("已是最新版本")
									    .setConfirmText("我知道了")
									    .showCancelButton(false)
									    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
											
											@Override
											public void onClick(SweetAlertDialog sweetAlertDialog) {
												// TODO Auto-generated method stub
												sweetAlertDialog.dismiss();
											}
										})
									    .show();
									}
								}

							}
						}

					}
				});

	}

	/**
	 * 从assets解压zip缓存包到本地可写目录[用户首次安装调用的函数]
	 * 
	 * @param context
	 * @param openDialog
	 */
	public static void unZipFromAssets(final Context context,
			final boolean openDialog) {
		editor.putInt(VERSION_CODE_CACHE, ORIGIN_VERSIONCODE);
		editor.commit();
		new CopyFromAssestsTask(context, openDialog).execute();
	}

}

/***
 * 从assets文件夹中解压zip到可写目录任务
 * 
 * @author ww
 *
 */
class CopyFromAssestsTask extends NetAsyncTask {

	Context mContext;
	boolean openDialog;

	protected CopyFromAssestsTask(Context ctx, boolean openDialog) {
		super(ctx, openDialog);
		// TODO Auto-generated constructor stub
		this.mContext = ctx;
		this.openDialog = openDialog;
	}

	@Override
	protected void doInBack() throws Exception {
		// TODO Auto-generated method stub
		byte[] buffer = new byte[1024];
		FileOutputStream output;
		try {
			output = new FileOutputStream(new File(PathUtils.getZipPath()));

			AssetManager assetManager = App.ctx.getAssets();

			BufferedInputStream in = new BufferedInputStream(
					assetManager.open("www.zip"), 1024);
			BufferedOutputStream out = new BufferedOutputStream(output, 1024);
			int n = 0;
			while ((n = in.read(buffer, 0, 1024)) != -1) {
				out.write(buffer, 0, n);
			}
			out.flush();
			out.close();
			in.close();
			output.close();
			// assetManager.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	protected void onPost(Exception e) {
		// TODO Auto-generated method stub
		if (e == null) {
			new UnZipTask(PathUtils.getZipPath(), PathUtils.getAppPath(),
					mContext, openDialog).execute();
		} else {
		}
	}

}

/**
 * 线上下载zip任务
 * 
 * @author ww
 *
 */
class DownLoaderTask extends NetAsyncTask {

	String url;
	String out;
	private URL mUrl;
	private File mFile;
	FileOutputStream mOutputStream;
	Context mContext;
	int length = 0;
	int downLength = 0;

	protected DownLoaderTask(String url, String out, Context ctx,
			boolean openDialog) {
		super(ctx, openDialog);
		// TODO Auto-generated constructor stub
		this.url = url;
		this.out = out;
		this.mContext = ctx;
		try {
			mUrl = new URL(url);
			String fileName = "www.zip";
			mFile = new File(out, fileName);
			Logger.d("[JuUnzip] DownLoaderTask" + "out=" + out + "," + " name="
					+ fileName + ",mUrl.getFile()=" + mUrl.getFile());
		} catch (MalformedURLException e) {
			// TODO: handle exception
			Logger.e("[JuUnzip] DownLoaderTask" + e.getMessage());
		}
	}

	@Override
	protected void doInBack() throws Exception {
		// TODO Auto-generated method stub
		downLoad();
	}

	@Override
	protected void onPost(Exception e) {
		// TODO Auto-generated method stub
		if (e == null) {
			// 解压zip到hyx目录
			if (length == downLength) {

			}
			new UnZipTask(out + "www.zip", PathUtils.getAppPath(), mContext,
					openDialog).execute();
			Logger.d("[unzip downlodPost] 文件大小与下载大小" + downLength + "/"
					+ length);
		} else {

		}
	}

	/**
	 * 下载zip函数
	 */
	public void downLoad() {
		// TODO Auto-generated method stub
		URLConnection connection = null;
		try {
			connection = mUrl.openConnection();
			length = connection.getContentLength();
			mOutputStream = new FileOutputStream(mFile);
			copy(connection.getInputStream(), mOutputStream);
			// mOutputStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void copy(InputStream input, FileOutputStream output) {
		// TODO Auto-generated method stub
		byte[] buffer = new byte[1024];
		BufferedInputStream in = new BufferedInputStream(input, 1024);
		BufferedOutputStream out = new BufferedOutputStream(output, 1024);

		int n = 0;
		try {
			while ((n = in.read(buffer)) > 0) {
				out.write(buffer, 0, n);
				downLength += n;
			}
			out.flush();
			out.close();
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

class UnZipTask extends NetAsyncTask {

	private final File mInput;
	private final File mOutput;
	Context mContext;

	protected UnZipTask(String in, String out, Context ctx, boolean openDialog) {
		super(ctx, openDialog);
		// TODO Auto-generated constructor stub
		mInput = new File(in);
		mOutput = new File(out);
		mContext = ctx;
	}

	@Override
	protected void doInBack() throws Exception {
		// TODO Auto-generated method stub
		uuzip();
	}

	@Override
	protected void onPost(Exception e) {
		// TODO Auto-generated method stub
		// 更新当前的网页版本
		JuUnZipUtil juUnZipUtil = new JuUnZipUtil();
		juUnZipUtil.editor.putInt(juUnZipUtil.VERSION_CODE,
				juUnZipUtil.sp.getInt(juUnZipUtil.VERSION_CODE_CACHE, juUnZipUtil.ORIGIN_VERSIONCODE));
		juUnZipUtil.editor.commit();
	}

	private void uuzip() {
		// TODO Auto-generated method stub
		Enumeration<ZipEntry> entries;
		ZipFile zip = null;
		try {
			zip = new ZipFile(mInput);
			entries = (Enumeration<ZipEntry>) zip.entries();
			while (entries.hasMoreElements()) {
				ZipEntry entry = (ZipEntry) entries.nextElement();
				if (entry.isDirectory()) {
					continue;
				}
				File destination = new File(mOutput, entry.getName());
				if (!destination.getParentFile().exists()) {
					// Logger.e("[JuUnzip] UnZipTask : make="
					// + destination.getParentFile().getAbsolutePath());
					destination.getParentFile().mkdirs();
				}
				FileOutputStream outputStream = new FileOutputStream(
						destination);
				copy(zip.getInputStream(entry), outputStream);
				outputStream.close();
			}
			zip.close();
		} catch (ZipException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void copy(InputStream input, FileOutputStream output) {
		// TODO Auto-generated method stub
		byte[] buffer = new byte[1024];
		BufferedInputStream in = new BufferedInputStream(input, 1024);
		BufferedOutputStream out = new BufferedOutputStream(output, 1024);
		int n = 0;
		try {
			while ((n = in.read(buffer, 0, 1024)) != -1) {
				out.write(buffer, 0, n);
			}
			out.flush();
			out.close();
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
