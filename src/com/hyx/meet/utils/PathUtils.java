package com.hyx.meet.utils;

import android.os.Environment;

import java.io.File;

/**
 * Created by ww on 15-01-16.
 */
public class PathUtils {
	public static String getSDcardDir() {
		return Environment.getExternalStorageDirectory().getPath() + "/";
	}

	public static String checkAndMkdirs(String dir) {
		File file = new File(dir);
		if (file.exists() == false) {
			file.mkdirs();
		}
		return dir;
	}
	
	public static boolean check(String dir) {
		File file = new File(dir);
		return file.exists();
	}


	public static String getAppPath() {
		String dir = getSDcardDir() + "hyx/";
		return checkAndMkdirs(dir);
	}

	/**
	 * wwwzip路径
	 * @return
	 */
	public static String getZipDir() {
		String dir = getAppPath() + "wwwzip/";
		return checkAndMkdirs(dir);
	}

	public static String getZipPath() {
		return getZipDir() + "www.zip";
	}
	
	/**
	 * www网页文件路径
	 * @return
	 */
	public static String getWWWDir() {
		String dir = getAppPath() + "www/";
		return checkAndMkdirs(dir);
	}
	
	/**
	 * 删除网页缓存
	 */
	public static void deleteWWWDir(){
		deleteFilesByDirectory(new File(getWWWDir()));
	}

	


	/**
	 * 获取头像路径
	 * 
	 * @return
	 */
	public static String getAvatarDir() {
		String dir = getAppPath() + "avatar/";
		return checkAndMkdirs(dir);
	}

	/**
	 * 获取头像缓存绝对路径
	 * 
	 * @return
	 */
	public static String getAvatarTmpPath() {
		return getAvatarDir() + "tmp";
	}

	/**
	 * 获取头像绝对路径
	 * 
	 * @return
	 */
	public static String getAvatarPath(String id) {
		return getAvatarDir() + id;
	}

	/**
	 * 聊天文件路径
	 * 
	 * @return
	 */
	public static String getChatFileDir() {
		String dir = getAppPath() + "files/";
		return checkAndMkdirs(dir);
	}

	/**
	 * 聊天文件绝对路径
	 * 
	 * @return
	 */

	public static String getChatFilePath(String id) {
		String dir = getChatFileDir();
		String path = dir + id;
		return path;
	}
	
	/**
	 * 音频文件路径
	 * 
	 * @return
	 */
	public static String getAudioFilePath(String id) {
		String dir = getAppPath() + "Audios/";
		String path = checkAndMkdirs(dir)+id;
		return path;
	}


	/**
	 * 录音缓存绝对路径
	 * 
	 * @return
	 */
	public static String getRecordTmpPath() {
		return getChatFileDir() + "tmp";
	}

	public static String getUUIDFilePath() {
		return getChatFilePath(Utils.uuid());
	}

	/**
	 * 缓存绝对路径
	 * 
	 * @return
	 */
	public static String getTmpPath() {
		return getAppPath() + "tmp";
	}

	/**
	 * 大图存储文件夹
	 * 
	 * @return
	 */
	public static String getImageBigDir() {
		String dir = getAppPath() + "bigImages/";
		return checkAndMkdirs(dir);
	}

	/**
	 * 大图存路径
	 * 
	 * @return
	 */

	public static String getImageBigPath(String id) {
		String dir = getImageBigDir();
		String path = dir + id;
		return path;
	}

	/**
	 * 大图存路径
	 * 
	 * @return
	 */

	// public static String getUUIDBigImAGEPath() {
	// return getImageBigPath(Utils.uuid());
	// }

	/************************* 删除缓存 ***************************/
	/**
	 * 清除缓存和文件
	 * 
	 * @return
	 */
	public static boolean clearFilesAndCache() {
		deleteFilesByDirectory(new File(getAvatarDir()));
		deleteFilesByDirectory(new File(getChatFileDir()));
		deleteFilesByDirectory(new File(getAppPath() + "Cache/"));
		return true;
	}

	/**
	 * * 删除方法 这里只会删除某个文件夹下的文件，如果传入的directory是个文件，将不做处理 * *
	 * 
	 * @param directory
	 */
	private static void deleteFilesByDirectory(File directory) {
		if (directory != null && directory.exists() && directory.isDirectory()) {
			for (File item : directory.listFiles()) {
				item.delete();
			}
		}
	}

}
