package com.hyx.meet.utils;

import org.ocpsoft.prettytime.PrettyTime;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by lzw on 14-6-28.
 */
public class TimeUtils {
  public static PrettyTime prettyTime = new PrettyTime();

  public static String getDate(Date date) {
    SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
    return format.format(date);
  }
  
  public static String getTime(Date date){
	  SimpleDateFormat format = new SimpleDateFormat("HH:mm");
	    return format.format(date); 
  }
  
  public static String getDateSimple(Date date){
	  SimpleDateFormat format = new SimpleDateFormat("MM-dd HH:mm");
	    return format.format(date); 
  }

  public static String getDateByTimeStamp(long timestamp){
//	    long gap = System.currentTimeMillis() - timestamp;
	    return getDateSimple(new Date(timestamp));
//        if (gap<24*60*60*1000) {
//			return getTime(new Date(timestamp));
//		}else if(gap >=24*60*60*1000 && gap<48*60*60*1000) {
//			return "昨天";
//		}else {
//			return getDateSimple(new Date(timestamp));
//		}
  }

  public static String millisecs2DateString(long timestamp) {
    long gap = System.currentTimeMillis() - timestamp;
    if (gap < 1000 * 60 * 60 * 24) {
      return "今天 "+getTime(new Date(timestamp));
    } else {
      return getDate(new Date(timestamp));
    }
  }

  public static boolean haveTimeGap(long lastTime, long time) {
    int gap = 1000 * 60 * 3;
    return time - lastTime > gap;
  }
}
