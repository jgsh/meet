/**
 * 
 */
package com.hyx.meet.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * 类说明
 * @author joy
 *
 */
public class ErrorCode {

	
	private static Map<String,Object> Error;
	public ErrorCode()
	{
	    Error=new HashMap<String,Object>();
	    Error.put("000","操作成功");
	    Error.put("001","系统错误");
	    Error.put("100","密匙验证错误");
	    Error.put("101","请用post方式访问");
	    Error.put("102","短信验证码发送失败");
	    Error.put("103","获取天翼平台信任码失败");
	    Error.put("104","短信验证码未发送或者已经过期");
	    Error.put("105","短信验证码验证错误");
	    Error.put("106","密码不能小于6位");
	    Error.put("107","注册用户名不能为空");
		Error.put("108","用户已经存在");
		Error.put("109","登陆用户名不能为空");
		Error.put("110","用户不存在");
	    Error.put("111","用户名和密码不匹配");
	    Error.put("112","参数不完整");
	    Error.put("113","创建用户名片失败");
	    Error.put("114","会议创建者不能加入自己的会议");
	    Error.put("115","会议已经关闭报名");
	    Error.put("116","你已经报名会议");
	    Error.put("117","用户未审核通过，不能进行签到");
	    Error.put("118","不能重复签到");
	    Error.put("211", "用户不存在!");
	    Error.put("210", "密码错误!");
		
	}
	
	public static String getError(int  errorcode)
	{
		return Error.get(errorcode+"").toString();
	}
	
	
}
