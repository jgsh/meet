/**
 * 
 */
package com.hyx.meet.login;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.LogInCallback;
import com.hyx.meet.meet.JuMainActivity;
import com.hyx.meet.service.NotifyService;
import com.hyx.meet.utils.ErrorCode;
import com.hyx.meet.utils.JuUrl;
import com.hyx.meet.JuHYXWebviewActivity;
import com.hyx.meet.R;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * 登陆界面
 * 
 * @author joy
 * 
 */
public class JuLoginActivity extends Activity {

	private EditText userName;
	private EditText passWord;
	String username;
	String password;
	Button login1;

	private ProgressDialog mProgressDialog;

	// private static boolean is_fromExtra = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_layout);
		// SysApplication.getInstance().addActivity(JuLoginActivity.this);
		// check_isFromExtra();
		// ww 检查是否登陆过
		check_isLogin();

		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage("正在登录...");
		mProgressDialog.setCancelable(false);

		userName = (EditText) findViewById(R.id.userName);
		passWord = (EditText) findViewById(R.id.passWord);
		login1 = (Button) findViewById(R.id.login1);

		/**
		 * 给输入电话号添加点击时间，够11位就可以点击按钮
		 */
		userName.addTextChangedListener(new TextWatcher() {

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// if (userName.getText().toString().length() == 11) {
				// login1.setBackgroundResource(R.drawable.yzm_complete);
				//
				// } else {
				// login1.setBackgroundResource(R.drawable.yzm);
				// }

			}

		});

	}

	/**
	 * 检测是不是外部链接进来
	 */
	// private boolean check_isFromExtra() {
	// // TODO Auto-generated method stub
	// Intent i_getvalue = getIntent();
	// String action = i_getvalue.getAction();
	//
	// if(Intent.ACTION_VIEW.equals(action)){
	// String requestUrl = i_getvalue.getDataString();
	// String[] urlParameters = UrlDecodeUtils.getUrlParameters(requestUrl);
	// String urlAction = urlParameters[2];
	// if (AVUser.getCurrentUser() != null) {
	// if (urlAction.equals("enterMeeting") && urlParameters.length == 4) {
	// // 报名
	// enteMeeting(urlParameters[3]);
	// // Toast.makeText(JuLoginActivity.this, urlParameters[3], 0).show();
	// return true;
	// }
	//
	// }else{
	// // 标志从外部链接报名动作true，输入用户名和密码再报名
	// Toast.makeText(JuLoginActivity.this, "请先登录!"+requestUrl, 0).show();
	// is_fromExtra = true;
	// return true;
	// }
	// }
	// return false;
	// }

	// /**
	// * 报名
	// * @param string
	// */
	// private void enteMeeting(String ID) {
	// // TODO Auto-generated method stub
	// MeetingService.enteMeetingById(ID);
	// }

	/**
	 * 检查是否已登陆过 ww
	 */
	private void check_isLogin() {
		// TODO Auto-generated method stub
		if (AVUser.getCurrentUser() != null) {
			// check_isFromExtra();
			Intent intent = new Intent(JuLoginActivity.this,
					JuMainActivity.class);
			startActivity(intent);
			// ww修改的
			JuLoginActivity.this.finish();
		}
		// }else {
		// check_isFromExtra();
		// }

	}

	public void login(View v) {

		if (TextUtils.isEmpty(userName.getText().toString())) {
			Toast.makeText(JuLoginActivity.this, "用户名不能为空", Toast.LENGTH_SHORT)
					.show();
			return;

		}
		if (TextUtils.isEmpty(passWord.getText().toString())) {
			Toast.makeText(JuLoginActivity.this, "密码不能为空", Toast.LENGTH_SHORT)
					.show();
			return;

		}

		else {

			// 只有满足上面的所有条件，按钮才可被点击
			login1.setClickable(true);
			String username = userName.getText().toString();
			String password = passWord.getText().toString();
			mProgressDialog.show();
			loginmain(username, password);
		}

	}

	public void loginmain(String username, String password) {

		AVUser.logInInBackground(username, password,
				new LogInCallback<AVUser>()

				{

					@Override
					public void done(AVUser user, AVException e) {
						// TODO Auto-generated method stub
						mProgressDialog.dismiss();
						if (e == null) {
							if (user != null) {
								Toast.makeText(JuLoginActivity.this, "登录成功",
										Toast.LENGTH_SHORT).show();
								NotifyService notifyService = new NotifyService(
										user.getObjectId());
								notifyService
										.setLoginTime("2015-01-01T09:29:41.000Z");
								Intent intent = new Intent(
										JuLoginActivity.this,
										JuMainActivity.class);
								startActivity(intent);
								// ww修改的
								JuLoginActivity.this.finish();

							} else {
								Toast.makeText(JuLoginActivity.this, "用户不存在",
										Toast.LENGTH_SHORT).show();
							}
						} else {
							if (e.getCode() == 211 || e.getCode() == 210) {
								Toast.makeText(JuLoginActivity.this,
										new ErrorCode().getError(e.getCode()),
										0).show();
							}
						}

					}

				});
	}

	public void jump(View v) {

		Intent intent = new Intent(this, JuHYXWebviewActivity.class);
		String url ;
		if (JuUrl.MODE_STYLE.equals("prod")) {
			url = JuUrl.USER_REGISTER_PROD;

		}else {
			url = JuUrl.USER_REGISTER_DEV;
		}
		intent.putExtra("url", url);
		startActivity(intent);
		// //ww修改的
		// JuLoginActivity.this.finish();

	}

	public void forget(View v) {

		Intent intent = new Intent(this, JuHYXWebviewActivity.class);
		String url ;
		if (JuUrl.MODE_STYLE.equals("prod")) {
			url = JuUrl.USER_PASSWARD_PROD;

		}else {
			url = JuUrl.USER_PASSWARD_DEV;
		}
		intent.putExtra("url", url);
		startActivity(intent);

	}

	public void hide(View v) {
		((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
				.hideSoftInputFromWindow(JuLoginActivity.this.getCurrentFocus()
						.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	}
}
