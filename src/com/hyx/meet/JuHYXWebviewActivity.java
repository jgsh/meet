package com.hyx.meet;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.onekeyshare.OnekeyShare;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.hyx.meet.Application.App;
import com.hyx.meet.custome.JsBridge.BridgeHandler;
import com.hyx.meet.custome.JsBridge.BridgeWebView;
import com.hyx.meet.custome.JsBridge.CallBackFunction;
import com.hyx.meet.custome.JsBridge.DefaultHandler;
import com.hyx.meet.db.DBMeet;
import com.hyx.meet.entity.MeetingDomain;
import com.hyx.meet.meet.JuGetLocationActivity;
import com.hyx.meet.meet.JuGuidanceActivity;
import com.hyx.meet.meet.JuMainActivity;
import com.hyx.meet.meet.JuShareMeetBySMSActivity;
import com.hyx.meet.meet.JuShareMeetBySMSMainActivity;
import com.hyx.meet.namecard.JuNameCardShowActivity;
import com.hyx.meet.qr.MipcaActivityCapture;
import com.hyx.meet.service.ChatService;
import com.hyx.meet.service.JuShareService;
import com.hyx.meet.service.UserService;
import com.hyx.meet.service.listener.MeetingListener;
import com.hyx.meet.service.listener.NamecardListener;
import com.hyx.meet.utils.AndroidConfig;
import com.hyx.meet.utils.JuUrl;
import com.hyx.meet.utils.Logger;
import com.hyx.meet.utils.NetAsyncTask;
import com.hyx.meet.utils.PathUtils;
import com.hyx.meet.utils.PhotoUtil;
import com.hyx.meet.utils.Utils;

import android.R.integer;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsoluteLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

//用户资料编辑  Ju_Noti_userEdit 
//刷新好友列表  Ju_Noti_refreshUserCache

/**
 * 公共浏览器
 * 
 * @author ww
 *
 */
public class JuHYXWebviewActivity extends Activity {

	public static Set<NamecardListener> namecardListeners = new HashSet<NamecardListener>();
	public static Set<MeetingListener> MeetingListeners = new HashSet<MeetingListener>();

	private static final int IMAGE_REQUEST = 0;
	private static final int TAKE_CAMERA_REQUEST = 1;
	private static final int GET_LOCATION = 2;
	private static final int QR = 3;

	private static final String USER_EDIT = "Ju_Noti_userEdit";
	private static final String REFRESH_USERCACHE = "Ju_Noti_refreshUserCache";
	private static final String REFRESH_MEETING_LIST = "Ju_Noti_refreshMeetingList";

	private String localCameraPath = PathUtils.getAvatarTmpPath();// 拍照缓存地址

	private static JsHanderCallBack mJsHanderCallBack;

	BridgeWebView webView;
	// LinearLayout mLinearLayout;
	private static ProgressBar mDialog;
	private ImageView mGuideOpt;
	private AbsoluteLayout mGuideOptLay;
	private ImageView mGuideOpt2;
	private RelativeLayout mGuideOptLay2;

	Intent fromIntent;
	String url;

	// private static Context context;
	// 测试网页
	// private static String loadUrl =
	// "http://dev.juyx.avosapps.com/index.html?layout=true";
	// private static String localUrl = "file:///android_asset/demo.html";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_js_sdk_demo);
		checkNetWork();
		context = JuHYXWebviewActivity.this;
		// webView = new BridgeWebView(this, this);
		webView = (BridgeWebView) findViewById(R.id.id_sdk_demo_webview);
		// ViewGroup.LayoutParams lp = new
		// ViewGroup.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);
		// webView.setLayoutParams(lp);
		// mLinearLayout.addView(webView, lp);
		webView.setDefaultHandler(new DefaultHandler());
		webView.setActivity(JuHYXWebviewActivity.this);
		// 引导操作页
		mGuideOpt = (ImageView) findViewById(R.id.id_guide_opt_img);
		// mGuideOpt.setOnClickListener(new OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// // TODO Auto-generated method stub
		// mGuideOptLay.setVisibility(View.GONE);
		// }
		// });
		mGuideOptLay = (AbsoluteLayout) findViewById(R.id.id_guide_opt_lay);
		mGuideOpt2 = (ImageView) findViewById(R.id.id_guide_opt_img_2);
		mGuideOptLay2 = (RelativeLayout) findViewById(R.id.id_guide_opt_Lay2);

		mDialog = (ProgressBar) findViewById(R.id.progressBar1);

		fromIntent = getIntent();
		// 要打开的网页地址
		url = fromIntent.getStringExtra("url");

		// 首次安装操作引导页的显示（会议详情）
		if (fromIntent.getIntExtra("showGuide", 0) == 1) {
			try {
				is_open = true;
			} catch (Exception e) {
				// TODO: handle exception
				Logger.e(e.getMessage());
			}
		}

		// webView.load(JuUrl.DEV_HEADER, null);
		// webView.load(url, null);
		// webView.load(JuUrl.DEV_FOOTER,null);
		// 注册js调用java执行方法
		registerHander();
		if (JuUrl.MODE_STYLE.equals("prod")) {
			webView.load(url, null);
		} else {
			// 自动拼接头尾
			Utils.getWWWFromSD(webView, url);
		}
		// Logger.d("[JsSdkDemo path:]"+Utils.getWWWFromSD(webView, url));
	}

	boolean is_open = false;

	/**
	 * 打开引导页
	 */
	public void isOpenGuideOpt() {
		if (is_open) {
			openGuideOpt(4);
		}
	}

	private void checkNetWork() {
		// TODO Auto-generated method stub
		// 没网络情况下
		if (!Utils.isNetworkConnected(this)) {
			Utils.toast(getString(R.string.pleaseCheckNetwork));
			this.finish();
		}

	}

	public void showDialog() {
		// Utils.toast("调用 jssdk demo");
		// if (mDialog.getVisibility() == View.GONE) {
		mDialog.setVisibility(View.VISIBLE);
		// }

	}

	public void closeDialog() {
		// Utils.toast("调用 jssdk demo");

		// if (mDialog.getVisibility() == View.VISIBLE) {
		mDialog.setVisibility(View.GONE);
		// }

	}

	/**
	 * 注册js调用java执行方法
	 */
	private void registerHander() {
		// TODO Auto-generated method stub
		/**
		 * 关闭窗口（无返回值）
		 */
		webView.registerHandler("closeWindow", new BridgeHandler() {

			@Override
			public void handler(String data, CallBackFunction function) {
				// TODO Auto-generated method stub
				closeWindow();

			}

		});

		/**
		 * 分享社会平台（无返回值）
		 */
		webView.registerHandler("shareSocialNetwork", new BridgeHandler() {

			@Override
			public void handler(String data, CallBackFunction function) {
				// TODO Auto-generated method stub
				Logger.e(data);
				try {
					org.json.JSONObject params = new org.json.JSONObject(data);
					share(params);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

		/**
		 * 地图选址（返回所选地址详情）
		 */
		webView.registerHandler("getLocation", new BridgeHandler() {

			@Override
			public void handler(String data, final CallBackFunction function) {
				// TODO Auto-generated method stub
				getLocation(new JsHanderCallBack() {

					@Override
					public void CallBack(JSONObject locationData) {
						// TODO Auto-generated method stub

						JSONObject response = new JSONObject();
						try {
							response.put("data", locationData);
							response.put("errorMessage", "success");
							response.put("errorCode", 0);
							Logger.e("[JsSdkDemo]->getlocation resopnseData:"
									+ locationData.toString());

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						function.onCallBack(response.toString());
					}
				});
			}
		});

		/**
		 * 二维码扫描
		 */
		webView.registerHandler("scanQRCode", new BridgeHandler() {

			@Override
			public void handler(String data, final CallBackFunction function) {
				// TODO Auto-generated method stub
				try {
					JSONObject params = new JSONObject(data);
					scanQR(params, new JsHanderCallBack() {

						@Override
						public void CallBack(JSONObject data) {
							// TODO Auto-generated method stub
							function.onCallBack(data.toString());
						}
					});
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

		/**
		 * 图片选择
		 */
		webView.registerHandler("chooseImage", new BridgeHandler() {

			@Override
			public void handler(String data, final CallBackFunction function) {
				// TODO Auto-generated method stub
				try {
					JSONObject params = new JSONObject(data);
					selectImage(params, new JsHanderCallBack() {

						@Override
						public void CallBack(JSONObject data) {
							// TODO Auto-generated method stub
							function.onCallBack(data.toString());
							// Logger.e(data.toString());
						}
					});
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

		/**
		 * 网页通知事件
		 */
		webView.registerHandler("postNotification", new BridgeHandler() {

			@Override
			public void handler(String data, CallBackFunction function) {
				// TODO Auto-generated method stub
				try {
					JSONObject params = new JSONObject(data);
					// 处理通知
					handNotification(params);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

		/**
		 * 打开地图导航
		 */
		webView.registerHandler("openLocation", new BridgeHandler() {

			@Override
			public void handler(String data, CallBackFunction function) {
				// TODO Auto-generated method stub
				try {
					JSONObject params = new JSONObject(data);
					// 处理通知
					openLocation(params, 0);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

		/**
		 * 打开地图导航(含会议签到)
		 */
		webView.registerHandler("mapMeetCheckIn", new BridgeHandler() {

			@Override
			public void handler(String data, CallBackFunction function) {
				// TODO Auto-generated method stub
				try {
					JSONObject params = new JSONObject(data);
					// 处理通知
					// Logger.e("[JSSDKDEMO]mapMeetCheckIn data->"+params.toString());
					openLocation(params, 1);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

		/**
		 * 打开好友详情
		 */
		webView.registerHandler("openUserNameCard", new BridgeHandler() {

			@Override
			public void handler(String data, CallBackFunction function) {
				// TODO Auto-generated method stub
				try {
					JSONObject params = new JSONObject(data);
					// 处理通知
					// Logger.e("[JSSDKDEMO]mapMeetCheckIn data->"+params.toString());
					openUserNameCard(params);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

		/**
		 * 打开系统浏览器打开url
		 */
		webView.registerHandler("openUrl", new BridgeHandler() {

			@Override
			public void handler(String data, CallBackFunction function) {
				// TODO Auto-generated method stub
				try {
					JSONObject params = new JSONObject(data);
					// 处理url
					// Logger.e("[JSSDKDEMO]mapMeetCheckIn data->"+params.toString());
					openBrowser(params.getString("url"));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

	}

	/**
	 * 打开浏览器
	 * 
	 * @param string
	 */
	protected void openBrowser(String url) {
		// TODO Auto-generated method stub
		Uri uri = Uri.parse(url);
		Intent intent = new Intent(Intent.ACTION_VIEW, uri);
		startActivity(intent);

	}

	/**
	 * 打开好友名片详情
	 */
	protected void openUserNameCard(JSONObject params) {
		// TODO Auto-generated method stub
		String nameCardId = "";
		try {
			nameCardId = params.getString("nameCardId");
			Intent intent = new Intent(JuHYXWebviewActivity.this,
					JuNameCardShowActivity.class);
			intent.putExtra("otherNameCardId", nameCardId);
			startActivity(intent);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case GET_LOCATION:
				Bundle bundle = data.getExtras();
				String locationPoiJsonString = bundle.getString("location");
				try {
					JSONObject locationData = new JSONObject(
							locationPoiJsonString);
					// LocationPoi locationPoi = new
					// LocationPoi().JSontoLocationPoi(jsonObject);
					mJsHanderCallBack.CallBack(locationData);
					// Utils.toast("json->"+locationPoiJsonString);
					// Logger.e("json->"+locationPoiJsonString+"   \njsonObject->"+jsonObject.toString()
					// +"  \nlocationPoi->"+locationPoi.toString());
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			case IMAGE_REQUEST:
				String localSelectPath = parsePathByReturnData(data);
				try {
					sendImageByPath(localSelectPath);
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				break;
			case TAKE_CAMERA_REQUEST:
				try {
					sendImageByPath(localCameraPath);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				break;
			case QR:
				String qrResult = data.getStringExtra("qrResult");
				try {
					mJsHanderCallBack.CallBack(new JSONObject(qrResult));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;

			default:
				break;
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	// ******************************************执行函数**********************************************************//

	/**
	 * 处理from js的通知
	 * 
	 * @param params
	 */
	protected void handNotification(JSONObject params) {
		// TODO Auto-generated method stub
		try {
			String name = params.getString("name");
			if (name.equals(USER_EDIT)) {
				Logger.e("更新个人资料");
			} else if (name.equals(REFRESH_USERCACHE)) {
				Logger.e("更新名片");
				for (NamecardListener namecardListener : namecardListeners) {
					if (namecardListener.onNameCardUpdate()) {
						// break;
					}
				}

			} else if (name.equals(REFRESH_MEETING_LIST)) {
				Logger.e("更新会议列表");
				for (MeetingListener meetingListener : MeetingListeners) {
					if (meetingListener.onMeetingListUpdate()) {
						// break;
					}
				}

			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * 打开地图导航
	 * 
	 * @param params
	 */
	public void openLocation(JSONObject params, int type) {
		// type 0为打开地图导航 1为会议签到，有meetingId参数
		try {
			// String name = params.getString("name");
			double latitude = params.getDouble("latitude");
			double longitude = params.getDouble("longitude");
			String address = params.getString("address");
			String meetingId = "";
			if (type == 1) {
				meetingId = params.getString("meetingId");
			}
			Intent intent = new Intent(JuHYXWebviewActivity.this,
					JuGuidanceActivity.class);
			// intent.putExtra("name", name);
			intent.putExtra("latitude", latitude);
			intent.putExtra("longitude", longitude);
			intent.putExtra("address", address);
			// 位置签到meetingID
			intent.putExtra("meetingId", meetingId);
			intent.putExtra("type", type);
			startActivity(intent);
			overridePendingTransition(R.anim.zoomin, R.anim.zoomout_static);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 分享到社交网络
	 * 
	 * @param params
	 */

	public void share(JSONObject params) {
		ShareSDK.initSDK(this);
		OnekeyShare oks = new OnekeyShare();

		Logger.e("shareParams->" + params.toString());

		String shareurl = "";
		String content = "分享内容";
		String title = "分享标题";
		String defaultContent = "默认内容";
		String imageUrl = "图片url";
		// String description = "简要概述";

		try {
			shareurl = params.getString("url");
			content = params.get("content").toString();// 分享内容
			title = params.get("title").toString();// 分享标题
			defaultContent = params.get("defaultContent").toString();
			imageUrl = params.get("image").toString();
			// description = params.get("description").toString();

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}// 网页链接

		// 关闭sso授权
		oks.disableSSOWhenAuthorize();

		// 自定义图标--短信
		final Uri uri = Uri.parse(url);
		if (!uri.getQueryParameter("meetingId").isEmpty()) {
			MeetingDomain meetingDomain = DBMeet.getMeetingById(uri
					.getQueryParameter("meetingId"));
			if (meetingDomain.getHostObjectId().equals(ChatService.getSelfId())) {// 自己发布的会议才有免费短信分享

				// 自定义短信分享事件
				Bitmap logo = BitmapFactory.decodeResource(getResources(),
						R.drawable.logo_shortmessage);
				OnClickListener listener = new OnClickListener() {
					public void onClick(View v) {
						Intent intent = new Intent(JuHYXWebviewActivity.this,
								JuShareMeetBySMSMainActivity.class);
						intent.putExtra("meetingId",
								uri.getQueryParameter("meetingId"));
						startActivity(intent);

					}
				};

				oks.setCustomerLogo(logo, logo, "免费短信", listener);
			}
		}
		// 分享时Notification的图标和文字
		oks.setNotification(R.drawable.logo_big, getString(R.string.app_name));
		// title标题，印象笔记、邮箱、信息、微信、人人网和QQ空间使用
		oks.setTitle(title);
		// titleUrl是标题的网络链接，仅在人人网和QQ空间使用
		oks.setTitleUrl(shareurl);
		// text是分享文本，所有平台都需要这个字段
		oks.setText(content);
		// imagePath是图片的本地路径，Linked-In以外的平台都支持此参数
		// File file = new File(PathUtils.getAppPath()+"1.png");
		// if (file.exists() == true) {
		// Log.i("file", "file is  exist");
		// }
		// oks.setImagePath(PathUtils.getAppPath() + "logo.jpg");//
		// 确保SDcard下面存在此张图片
		// url仅在微信（包括好友和朋友圈）中使用
		oks.setUrl(shareurl);
		oks.setImageUrl(imageUrl);

		// comment是我对这条分享的评论，仅在人人网和QQ空间使用
		oks.setComment(defaultContent);
		// site是分享此内容的网站名称，仅在QQ空间使用
		oks.setSite("会友行");
		// siteUrl是分享此内容的网站地址，仅在QQ空间使用
		oks.setSiteUrl(shareurl);

		// oks.setShareContentCustomizeCallback(new
		// ShareContentCustomizeCallback() {
		//
		// @Override
		// public void onShare(Platform platform, ShareParams paramsToShare) {
		// // TODO Auto-generated method stub
		// Utils.toast(platform.getName());
		//
		// }
		// });
		//
		// //分享按钮事件
		// oks.setOnShareButtonClickListener(new OnShareButtonClickListener() {
		//
		// @Override
		// public void onClick(View v, List<Object> checkPlatforms) {
		// // TODO Auto-generated method stub
		//
		// }
		// });

		// 启动分享GUI
		oks.show(this);
	}

	/**
	 * 关闭窗口
	 */
	private void closeWindow() {
		// TODO Auto-generated method stub
		// Logger.d("closeWindow");
		this.finish();
	}

	/**
	 * 获取地图位置
	 */
	private void getLocation(JsHanderCallBack jsHanderCallBack) {
		Intent intent = new Intent(JuHYXWebviewActivity.this,
				JuGetLocationActivity.class);
		startActivityForResult(intent, GET_LOCATION);
		mJsHanderCallBack = jsHanderCallBack;
	}

	/**
	 * 二维码扫描
	 * 
	 * @param params
	 */
	private void scanQR(JSONObject params, JsHanderCallBack jsHanderCallBack) {
		Intent intent = new Intent(JuHYXWebviewActivity.this,
				MipcaActivityCapture.class);
		int needResult = 1;// 1表示直接传回数据，0表示返回数据由会友行自己处理
		try {
			needResult = params.getInt("needResult");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (needResult == 1) {
			// 直接返回扫码结果
			intent.putExtra("needResult", 1);
			startActivityForResult(intent, QR);
		} else {
			// 会有行自己处理
			intent.putExtra("needResult", 0);
			startActivity(intent);
		}

		mJsHanderCallBack = jsHanderCallBack;

	}

	/**
	 * 选择图片控制类
	 * 
	 * @param params
	 * @param jsHanderCallBack
	 */
	protected void selectImage(JSONObject params,
			JsHanderCallBack jsHanderCallBack) {
		// TODO Auto-generated method stub
		mJsHanderCallBack = jsHanderCallBack;
		int soureType = 0;// 0表示来自相册，1表示来自拍照
		try {
			soureType = params.getInt("soureType");
			if (soureType == 0) {
				selectImageFromLocal();
			} else {
				selectImageFromCamera();
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * 从相册选取照片
	 */
	private void selectImageFromLocal() {
		// TODO Auto-generated method stub
		Intent intent;

		intent = new Intent(Intent.ACTION_PICK, null);
		intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
				"image/*");

		startActivityForResult(intent, IMAGE_REQUEST);
	}

	/**
	 * 拍照
	 */
	private void selectImageFromCamera() {
		// TODO Auto-generated method stub
		Intent openCameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		Uri imageUri = Uri.fromFile(new File(localCameraPath));
		openCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
		startActivityForResult(openCameraIntent, TAKE_CAMERA_REQUEST);
	}

	/**
	 * 本地保存并上传图片
	 * 
	 * @param localSelectPath
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private void sendImageByPath(String localSelectPath)
			throws FileNotFoundException, IOException {
		// TODO Auto-generated method stub
		final String objectId = Utils.uuid();// 随机获得文件名
		final String newPath = PathUtils.getAvatarPath(objectId);
		// PhotoUtil.simpleCompressImage(localSelectPath,newPath);
		PhotoUtil.compressImage(localSelectPath, newPath);
		upLoadCoverImage(newPath, objectId);

	}

	/**
	 * 上传图片
	 * 
	 * @param newPath
	 * @param objectId
	 */
	private void upLoadCoverImage(final String newPath, final String objectId) {
		new NetAsyncTask(App.ctx, false) {
			String uploadUrl;

			@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				super.onPreExecute();
				webView.callHandler("uploadStart", "", new CallBackFunction() {

					@Override
					public void onCallBackByJson(JSONObject jsonObject) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onCallBack(String data) {
						// TODO Auto-generated method stub

					}
				});
			}

			@Override
			protected void doInBack() throws Exception {
				uploadUrl = getUrl(newPath, objectId);

			}

			@Override
			protected void onPost(Exception e) {
				if (e != null) {
					Toast.makeText(JuHYXWebviewActivity.this, e.getMessage(),
							Toast.LENGTH_LONG).show();
					Logger.d(e.getMessage());
				} else {

					String base64Data = setImgData();
					JSONObject responseData = new JSONObject();
					JSONObject data = new JSONObject();
					try {
						data.put("base64Data", base64Data);
						data.put("imgUrl", uploadUrl);
						responseData.put("data", data);
						responseData.put("errorMessage", "success");
						responseData.put("errorCode", 0);
					} catch (JSONException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					// 回调把数据传回js
					mJsHanderCallBack.CallBack(responseData);
					// 上传完成回调
					webView.callHandler("getUploadProcess", "100",
							new CallBackFunction() {

								@Override
								public void onCallBackByJson(
										JSONObject jsonObject) {
									// TODO Auto-generated method stub

								}

								@Override
								public void onCallBack(String data) {
									// TODO Auto-generated method stub

								}
							});

				}
			}

			// 获得base64格式图像数据
			private String setImgData() {
				// TODO Auto-generated method stub
				Bitmap bitmap = BitmapFactory.decodeFile(newPath);

				ByteArrayOutputStream bStream = new ByteArrayOutputStream();

				bitmap.compress(CompressFormat.PNG, 100, bStream);

				byte[] bytes = bStream.toByteArray();

				String imgBase64 = Base64.encodeToString(bytes, Base64.NO_WRAP);
				// Logger.e("base64==>" + imgBase64);
				return imgBase64;
			}

		}.execute();

	}

	// 获得图片路径
	private String getUrl(String newPath, String objectId)
			throws FileNotFoundException, IOException, AVException {
		// TODO Auto-generated method stub
		AVFile file = AVFile.withAbsoluteLocalPath(objectId, newPath);
		file.save();
		String url = file.getUrl();
		return url;
	}

	/**
	 * 获取图片本地地址
	 * 
	 * @param data
	 * @return
	 */
	private String parsePathByReturnData(Intent data) {
		if (data == null) {
			return null;
		}
		String localSelectPath = null;
		Uri selectedImage = data.getData();
		if (selectedImage != null) {
			Cursor cursor = this.getContentResolver().query(selectedImage,
					null, null, null, null);
			cursor.moveToFirst();
			int columnIndex = cursor.getColumnIndex("_data");
			localSelectPath = cursor.getString(columnIndex);
			cursor.close();
		}
		return localSelectPath;
	}

	@Override
	protected void onDestroy() {
		if (webView != null)
			webView.onDestroy();
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		super.onPause();
		// if (webView != null) {
		// webView.pauseTimers();
		// webView.onHide();
		// }
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (webView != null) {
			webView.resumeTimers();
			webView.onShow();
		}
	}

	@Override
	protected void onNewIntent(Intent intent) {
		if (webView != null) {
			webView.onNewIntent(intent);
		}
	}

	public static void addNamecardListener(NamecardListener listener) {
		namecardListeners.add(listener);
	}

	public static void removeNamecardListener(NamecardListener listener) {
		namecardListeners.remove(listener);
	}

	public static void addMeetingListener(MeetingListener listener) {
		MeetingListeners.add(listener);
	}

	public static void removeMeetingListener(MeetingListener listener) {
		MeetingListeners.remove(listener);
	}

	/******************************************** 引导图 *************************************/
	int method = 0;
	Activity context;

	/**
	 * 显示操作引导页
	 * 
	 * @param drawable
	 */
	public void openGuideOpt(int type) {
		// 0 meet 1 message 2 namecard 3me 4meetdetail
		@SuppressWarnings("deprecation")
		SharedPreferences sp = getSharedPreferences("first_use",
				MODE_WORLD_READABLE);
		
		boolean isFirstUse = sp.getBoolean(Utils.getVersionCode() +type, true);
		if (isFirstUse) {
			Editor editor = sp.edit();
			editor.putBoolean(Utils.getVersionCode() + type, false);
			editor.commit();
			if (mGuideOptLay.getVisibility() == View.GONE) {
				mGuideOptLay.setVisibility(View.VISIBLE);
				mGuideOpt.setVisibility(View.VISIBLE);
			}
			final int WIDTH = AndroidConfig.getScreenWidth(context);
			final int HEIGHT = AndroidConfig.getScreenHeight(context);
			if (type == 4) {
				method = 0;// 步骤
				// 会议详情
				UserService.changeClear(0, PhotoUtil.sp2px(this, 50),
						PhotoUtil.sp2px(this, 90), PhotoUtil.sp2px(this, 40),
						this, mGuideOptLay);
				mGuideOpt.setImageResource(R.drawable.firstuse_meetdetail_1);
				mGuideOpt.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (method == 0) {// 公告栏
							UserService.changeClear(
									PhotoUtil.sp2px(context, 90),
									PhotoUtil.sp2px(context, 50),
									PhotoUtil.sp2px(context, 90),
									PhotoUtil.sp2px(context, 40), context,
									mGuideOptLay);
							mGuideOpt
									.setImageResource(R.drawable.firstuse_meetdetail_2);
						} else if (method == 1) {// 参会者
							UserService.changeClear(
									PhotoUtil.sp2px(context, 180),
									PhotoUtil.sp2px(context, 50),
									PhotoUtil.sp2px(context, 90),
									PhotoUtil.sp2px(context, 40), context,
									mGuideOptLay);
							mGuideOpt
									.setImageResource(R.drawable.firstuse_meetdetail_3);
						} else if (method == 2) {// 互动圈
							UserService.changeClear(
									PhotoUtil.sp2px(context, 270),
									PhotoUtil.sp2px(context, 50),
									PhotoUtil.sp2px(context, 90),
									PhotoUtil.sp2px(context, 40), context,
									mGuideOptLay);
							mGuideOpt
									.setImageResource(R.drawable.firstuse_meetdetail_4);
						} else if (method == 3) {// 管理会议
							mGuideOptLay.setVisibility(View.GONE);
							mGuideOpt.setVisibility(View.GONE);
							mGuideOptLay2.setVisibility(View.VISIBLE);
							mGuideOpt2.setVisibility(View.VISIBLE);
							final Uri uri = Uri.parse(url);
							if (!uri.getQueryParameter("meetingId").isEmpty()) {
								MeetingDomain meetingDomain = DBMeet.getMeetingById(uri
										.getQueryParameter("meetingId"));
								if (meetingDomain.getHostObjectId().equals(
										ChatService.getSelfId())) {// 主办方
									mGuideOpt2
											.setImageResource(R.drawable.firstuse_meetdetail_5_2);
								} else {
									mGuideOpt2
											.setImageResource(R.drawable.firstuse_meetdetail_5_1);
								}
							}
						} else {
							mGuideOptLay.setVisibility(View.GONE);
							mGuideOpt.setVisibility(View.GONE);
						}
						method++;
					}
				});
				mGuideOpt2.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (method == 4) {// 分享
							mGuideOptLay2.setVisibility(View.GONE);
							mGuideOpt2.setVisibility(View.GONE);
							mGuideOptLay.setVisibility(View.VISIBLE);
							mGuideOpt.setVisibility(View.VISIBLE);
							UserService.changeClear(
									WIDTH - PhotoUtil.sp2px(context, 50), 0,
									PhotoUtil.sp2px(context, 50),
									PhotoUtil.sp2px(context, 50), context,
									mGuideOptLay);
							mGuideOpt
									.setImageResource(R.drawable.firstuse_meetdetail_6);
						}
						method++;
					}
				});

			}
		}
	}

}
