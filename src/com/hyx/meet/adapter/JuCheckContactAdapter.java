package com.hyx.meet.adapter;

import java.util.ArrayList;
import java.util.List;

import com.hyx.meet.R;
import com.hyx.meet.Application.App;
import com.hyx.meet.entity.JuContact;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class JuCheckContactAdapter extends BaseAdapter {

	List<JuContact> mData = new ArrayList<JuContact>();
	Context context;

	/**
	 * ww
	 * 
	 * @param context
	 * @param mData
	 */
	public JuCheckContactAdapter(Context context, List<JuContact> mData) {
		this.mData = mData;
		this.context = context;

	}
	
	public void setDatas(List<JuContact> mData){
		this.mData = mData;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mData.size();
	}

	@Override
	public JuContact getItem(int position) {
		// TODO Auto-generated method stub
		return mData.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		checkContactViewHolder viewHolder = null;
		if (convertView == null) {
			viewHolder = new checkContactViewHolder();
			convertView = LayoutInflater.from(App.ctx).inflate(
					R.layout.contact_item, null);
			viewHolder.contactCheck = (ImageView) convertView
					.findViewById(R.id.id_contaxt_checkIcon);
			viewHolder.contactName = (TextView) convertView
					.findViewById(R.id.id_contact_name);
			viewHolder.contactNumber = (TextView) convertView
					.findViewById(R.id.id_contact_number);
			viewHolder.alpha = (TextView) convertView.findViewById(R.id.id_checkContact_item_alpha);

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (checkContactViewHolder) convertView.getTag();
		}
		viewHolder.contactName.setText(mData.get(position).getName());
		viewHolder.contactNumber.setText(mData.get(position).getPhoneNumber());
		//显示分组标签
		int section = getSectionForPosition(position);
		if (position == getPositionForSection(section)) {
			viewHolder.alpha.setVisibility(View.VISIBLE);
			viewHolder.alpha.setText(mData.get(position).getSortLetters());
		} else {
			viewHolder.alpha.setVisibility(View.GONE);
		}
		//显示选择状态
		if (mData.get(position).is_checked()) {
			viewHolder.contactCheck
					.setImageResource(R.drawable.common_icon_checkmark);
		} else {
			viewHolder.contactCheck.setImageDrawable(null);
		}
		return convertView;
	}

	public int getSectionForPosition(int position) {
		return mData.get(position).getSortLetters().charAt(0);
	}

	@SuppressLint("DefaultLocale")
	public int getPositionForSection(int section) {
		for (int i = 0; i < getCount(); i++) {
			String sortStr = mData.get(i).getSortLetters();
			char firstChar = sortStr.toUpperCase().charAt(0);
			if (firstChar == section) {
				return i;
			}
		}

		return -1;
	}

}

/**
 * ViewHolder类用以储存item中控件的引用
 */
final class checkContactViewHolder {
	TextView alpha;
	TextView contactName;
	TextView contactNumber;
	ImageView contactCheck;
}
