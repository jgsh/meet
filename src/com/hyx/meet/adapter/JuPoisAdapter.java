package com.hyx.meet.adapter;

import java.util.List;


import com.hyx.meet.R;
import com.hyx.meet.Application.App;
import com.hyx.meet.entity.LocationPoi;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class JuPoisAdapter extends BaseAdapter {
	
	
	private Context context;
	private List<LocationPoi> mList;
	
	public JuPoisAdapter(Context context,List<LocationPoi> list) {
		// TODO Auto-generated constructor stub
		this.mList = list;
		this.context = context;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mList.size();
	}

	@Override
	public LocationPoi getItem(int position) {
		// TODO Auto-generated method stub
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		poisViewHolder holder = null;
		
		if (convertView == null) {
			holder = new poisViewHolder();
			convertView = LayoutInflater.from(App.ctx).inflate(R.layout.pois_list_item, null);
			holder.poiName = (TextView) convertView.findViewById(R.id.id_poi_name);
			holder.poiAddress = (TextView) convertView.findViewById(R.id.id_poi_address);
			
			convertView.setTag(holder);
			
		}else {
			holder = (poisViewHolder) convertView.getTag();
		}
		
		holder.poiAddress.setText(getItem(position).getDistrict());
		holder.poiName.setText(getItem(position).getName());
		
		//初始化选择第一个
//		if (getItem(position).isIs_select() == true) {
//			convertView.setSelected(true);
//		}
		
		return convertView;
	}

}

/**
 * ViewHolder类用以储存item中控件的引用
 */
 final class poisViewHolder {
	 TextView poiName;
	 TextView poiAddress;
 }



















