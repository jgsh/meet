/**
 * 
 */
package com.hyx.meet.adapter;

import java.util.List;

import com.avos.avoscloud.AVObject;
import com.hyx.meet.R;
import com.hyx.meet.adapter.JuMeetAdapter.ViewHolder;
import com.hyx.meet.entity.MeetingDomain;

import android.content.ContentValues;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * 类说明
 * @author joy
 *
 */
public class JuMyreleasemeetAdapter extends BaseAdapter{
	
	private Context context;
	private List<MeetingDomain> list;
	public JuMyreleasemeetAdapter(Context context,List<MeetingDomain> list) {
		this.context=context;
		this.list=list;
		
	}
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public MeetingDomain getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	
	final class ViewHolder {
		TextView name;
		TextView address;
		TextView time;
		TextView unconfirmNumber;
		TextView attenderNumber;
		}
	
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		ViewHolder hold=null;
		if(convertView==null)
		{
			convertView=LayoutInflater.from(context).inflate(R.layout.fragment_myreleasemeet_listview_item, null);
			hold=new ViewHolder();
			hold.name=(TextView) convertView.findViewById(R.id.name);
			hold.address=(TextView) convertView.findViewById(R.id.address);
			hold.time=(TextView) convertView.findViewById(R.id.time);
			hold.unconfirmNumber=(TextView) convertView.findViewById(R.id.shenhe);
			hold.attenderNumber=(TextView) convertView.findViewById(R.id.baoming);
			
			convertView.setTag(hold);
		}else{
			hold=(ViewHolder)convertView.getTag();
		}
		
		hold.name.setText(""+list.get(position).getName());
		//System.out.println(list.toString());
		//System.out.println(""+list.get(0).getString("name"));
		hold.address.setText(""+list.get(position).getAddress());
		hold.time.setText(""+list.get(position).getStartTime());
		hold.unconfirmNumber.setText("待审核:"+list.get(position).getUnconfirmNumber()+"人");
		hold.attenderNumber.setText("已报名:"+list.get(position).getAttenderNumber()+"人");
		
		
		return convertView;
	}

}
