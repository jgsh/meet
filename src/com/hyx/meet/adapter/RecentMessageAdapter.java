package com.hyx.meet.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hyx.meet.Application.App;
import com.hyx.meet.avobject.ChatGroup;
import com.hyx.meet.custome.ViewHolder;
import com.hyx.meet.db.DBMeet;
import com.hyx.meet.entity.Conversation;
import com.hyx.meet.entity.MeetingDomain;
import com.hyx.meet.entity.Msg;
import com.hyx.meet.entity.NameCard;
import com.hyx.meet.entity.RoomType;
import com.hyx.meet.R;
import com.hyx.meet.utils.EmotionUtils;
import com.hyx.meet.utils.PhotoUtil;
import com.hyx.meet.utils.TimeUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

public class RecentMessageAdapter extends BaseListAdapter<Conversation> {

	private LayoutInflater inflater;
	private Context ctx;

	public RecentMessageAdapter(Context context) {
		super(context);
		this.ctx = context;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final Conversation item = datas.get(position);
		if (convertView == null) {
			convertView = inflater.inflate(
					R.layout.fragment_messagefragment_list_item, parent, false);
		}
		ImageView recentAvatarView = ViewHolder.findViewById(convertView,
				R.id.id_message_conv_avatar);
		TextView recentNameView = ViewHolder.findViewById(convertView,
				R.id.id_message_conv_name);
		TextView recentMsgView = ViewHolder.findViewById(convertView,
				R.id.id_message_conv_content);
		TextView recentTimeView = ViewHolder.findViewById(convertView,
				R.id.id_message_conv_time);
		TextView recentUnreadView = ViewHolder.findViewById(convertView,
				R.id.id_message_recent_unread);

		Msg msg = item.getMsg();
		if (msg.getRoomType() == RoomType.Single) {
			NameCard user = item.getToUser();
			String avatar = null;
			recentNameView.setTextColor(Color.BLACK);
			if (user != null) {
				recentNameView.setText(user.getName());
				avatar = user.getAvator();
			}

			// user.getAvatarUrl();
			if (avatar != null && !avatar.equals("")) {
				ImageLoader.getInstance().displayImage(avatar,
						recentAvatarView, PhotoUtil.avatarImageOptions);
			} else {
				recentAvatarView.setImageResource(R.drawable.default_head);
			}
			// recentNameView.setText(user.getUsername());
		} else if (msg.getRoomType() == RoomType.NOTIFY) {
			recentNameView.setTextColor(Color.parseColor("#2c98fd"));
			MeetingDomain meetingDomain = DBMeet.getMeetingById(msg
					.getFromPeerId());
			String name = "会议通知";
			String avator = null;
			if (meetingDomain != null) {
				name = meetingDomain.getName();
				// datas.get(position).getMsg().setFromUsername(name);
				avator = meetingDomain.getCoverImage();
			}
			recentNameView.setText(name);
			if (avator != null && !avator.equals("")) {
				ImageLoader.getInstance().displayImage(avator,
						recentAvatarView, PhotoUtil.avatarImageOptions);
			} else {
				recentAvatarView.setImageResource(R.drawable.group_icon);
			}
		} else {
			ChatGroup chatGroup = item.getToChatGroup();
			recentNameView.setText(chatGroup.getTitle());
			recentNameView.setTextColor(Color.parseColor("#3A5FCD"));
			recentAvatarView.setImageResource(R.drawable.group_icon);
		}

		recentTimeView
				.setText(TimeUtils.getDateByTimeStamp(msg.getTimestamp()));
		int num = item.getUnreadCount();
		if (msg.getType() == Msg.Type.Text) {
			CharSequence spannableString = EmotionUtils.replace(ctx,
					msg.getContent());
			recentMsgView.setText(spannableString);
		} else if (msg.getType() == Msg.Type.Image) {
			recentMsgView
					.setText("[" + App.ctx.getString(R.string.image) + "]");
		} else if (msg.getType() == Msg.Type.Audio) {
			recentMsgView
					.setText("[" + App.ctx.getString(R.string.audio) + "]");
		} else if (msg.getType() == Msg.Type.Board) {
			recentMsgView.setText("[通知]："
					+ EmotionUtils.replace(ctx, msg.getContent()));
		} else if (msg.getType() == Msg.Type.Invite) {
			recentMsgView.setText("[会议邀请]");
		} else if (msg.getType() == Msg.Type.Text2) {
			CharSequence spannableString = EmotionUtils.replace(ctx,
					msg.getContent());
			recentMsgView.setText(spannableString);
		} else if (msg.getType() == Msg.Type.Image2) {
			recentMsgView
					.setText("[" + App.ctx.getString(R.string.image) + "]");
		} else if (msg.getType() == Msg.Type.Audio2) {
			recentMsgView
					.setText("[" + App.ctx.getString(R.string.audio) + "]");
		}
		//
		if (num > 0) {
			recentUnreadView.setVisibility(View.VISIBLE);
			recentUnreadView.setText(num + "");
		} else {
			recentUnreadView.setVisibility(View.GONE);
		}
		return convertView;
	}
}
