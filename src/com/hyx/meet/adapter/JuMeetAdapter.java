/**
 * 
 */
package com.hyx.meet.adapter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hyx.meet.JuHYXWebviewActivity;
import com.hyx.meet.R;
import com.hyx.meet.Application.App;
import com.hyx.meet.entity.MeetingDomain;
import com.hyx.meet.meet.JuGuidanceActivity;
import com.hyx.meet.service.ChatService;
import com.hyx.meet.service.UserService;
import com.hyx.meet.utils.JuUrl;
import com.hyx.meet.utils.PhotoUtil;
import com.hyx.meet.utils.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * 类说明
 * 
 * @author joy
 * 
 */
public class JuMeetAdapter extends BaseAdapter {

	private List<MeetingDomain> mList;
	private Context context;

	public JuMeetAdapter(Context context, List<MeetingDomain> newData) {
		// TODO Auto-generated constructor stub
		this.mList = newData;
		this.context = context;

	}

	public void refresh(List<MeetingDomain> refreshData){
		this.mList.clear();
		this.mList = refreshData;
		notifyDataSetChanged();
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mList.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public MeetingDomain getItem(int position) {
		// TODO Auto-generated method stub
		return mList.get(position);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	final class ViewHolder {
		TextView inviting;//邀请函
		TextView poiSignIn;//位置签到
		TextView name;
		TextView address;
		TextView starttime;
		TextView textstate;
		TextView attenderNumber;
		ImageView coverImage;
		ImageView publica;
		RelativeLayout nameLayout;
		RelativeLayout addressLayout;
		RelativeLayout timeLayout;
		RelativeLayout coverReLayout;
		LinearLayout beginlinearlayout;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getView(int, android.view.View,
	 * android.view.ViewGroup)
	 */
	@SuppressLint({ "ResourceAsColor", "SimpleDateFormat" })
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		ViewHolder hold = null;

		if (convertView == null) {

			convertView = LayoutInflater.from(App.ctx).inflate(
					R.layout.fragment_meetfragment_listview_item, null);

			hold = new ViewHolder();

			hold.inviting = (TextView) convertView.findViewById(R.id.id_meet_inviting);
			hold.poiSignIn = (TextView) convertView.findViewById(R.id.id_meet_poi_signin);
			
			hold.nameLayout = (RelativeLayout) convertView
					.findViewById(R.id.namelayout);
			hold.addressLayout = (RelativeLayout) convertView
					.findViewById(R.id.addresslayout);
			hold.timeLayout = (RelativeLayout) convertView
					.findViewById(R.id.timelayout);

			hold.name = (TextView) convertView.findViewById(R.id.name);
			hold.address = (TextView) convertView.findViewById(R.id.address);
			hold.starttime = (TextView) convertView
					.findViewById(R.id.starttime);

			hold.textstate = (TextView) convertView
					.findViewById(R.id.id_status_txt);

			hold.coverImage = (ImageView) convertView.findViewById(R.id.cover);

			hold.publica = (ImageView) convertView.findViewById(R.id.publican);
			
			hold.attenderNumber=(TextView) convertView.findViewById(R.id.attend);

			hold.beginlinearlayout = (LinearLayout) convertView
					.findViewById(R.id.layoutbegin);
			hold.coverReLayout = (RelativeLayout) convertView
					.findViewById(R.id.coverRelative);

			// 这个方法必须加
			convertView.setTag(hold);
		} else {
			hold = (ViewHolder) convertView.getTag();
		}
		
		
		
		//int bigwidth=width/2;
//		
//		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.FILL_PARENT,300); 
//		hold.coverImage.setLayoutParams(params);
		
		// meetpending:正在进行的会议
		// meetend:已经结束的会议
		// 分别算出正在进行和已结束的会议的个数
//		int meetpending = 0;
//		int meetend = 0;
//		for (int i = 0; i < mList.size(); i++) {
//
//			if (mList.get(i).getStatus().equals("pending")) {
//				meetpending++;
//			} else if (mList.get(i).getStatus().equals("end")) {
//				meetend++;
//			}
//
//		}
		// 如果正在进行中的会议大于1条，则设置数据库中的第一条为空
//		if (meetpending > 0) {
//			if (position == 0) {
//				hold.beginlinearlayout.setVisibility(View.GONE);
//
//				hold.textstate.setTextColor(Color.rgb(44, 138, 251));
//				hold.textstate.setText(R.string.meeting_begin);
//			} else {
//
//				hold.beginlinearlayout.setVisibility(View.GONE);
//			}

//		}
//		if (meetend > 0) {

//			if (position == meetpending) {
//				hold.beginlinearlayout.setVisibility(View.VISIBLE);
//				hold.textstate.setTextColor(Color.rgb(253, 161, 89));
//				hold.textstate.setText(R.string.meeting_end);
//			} else {
//				hold.beginlinearlayout.setVisibility(View.GONE);
//			}

//		}
		final MeetingDomain meetingDomain = getItem(position);		
		//跳转到邀请函页面
		hold.inviting.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context,JuHYXWebviewActivity.class);
				String url = JuUrl.getInviting();
				url = String.format(url, getItem(position).getObjectid(),ChatService.getSelfId());
				//todo 判断方法卸载juurl类中，就不用每次调用判断了
				intent.putExtra("url", url);
				((Activity)context).startActivity(intent);
			}
		});
		//跳转到位置签到
		hold.poiSignIn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				double lat = meetingDomain.getLatitude();
				double lng = meetingDomain.getLongitude();
				String address = meetingDomain.getAddress();
				String meetingId = meetingDomain.getObjectid();
				Intent intent = new Intent(context,JuGuidanceActivity.class);
//				intent.putExtra("name", name);
				intent.putExtra("latitude", lat);
				intent.putExtra("longitude", lng);
				intent.putExtra("address", address);
//				位置签到meetingID
				intent.putExtra("meetingId", meetingId);
				intent.putExtra("type", 1);
				((Activity)context).startActivity(intent);
				((Activity)context).overridePendingTransition(R.anim.zoomin,
						R.anim.zoomout_static);		
//				Utils.toast("位置签到");
				
			}
		});
		
		
		

		hold.attenderNumber.setText(getItem(position).getAttenderNumber()+"人报名");
		hold.name.setText("" + getItem(position).getName());
		hold.address.setText("" + getItem(position).getAddress());
		
		long startTime=getItem(position).getStartTime();//开始时间
		long endTime = getItem(position).getEndTime();//结束时间
		long nowTime = System.currentTimeMillis();//当前时间
		
		long change = 8*60*60*1000;//当前时间与开始时间格式上差少了八个小时
		nowTime = nowTime+change;
		
		Date startDate=new Date(startTime);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));//减少八个小时
		String dateGMTStr = "";
		dateGMTStr = dateFormat.format(startDate);
		hold.starttime.setText("" + dateGMTStr);
		
		if (nowTime < startTime) {
//			即将开始
			hold.publica.setImageResource(R.drawable.meet_starting);
			hold.textstate.setText("即将开始");
		}else if (nowTime > endTime) {
//			已经结束
			hold.publica.setImageResource(R.drawable.meet_end);					
			hold.textstate.setText("圆满结束");
		}else {
//			正在进行
			hold.publica.setImageResource(R.drawable.meet_ing);		
			hold.textstate.setText("正在进行");
		}
		
		
//		if (getItem(position).getStatus().equals("pending")) {
//
//		} else if (getItem(position).getStatus().equals("end")) {
//
//			hold.textstate.setText(R.string.meeting_end);
//			hold.textstate.setTextColor(Color.rgb(253, 161, 89));
//		}

//		if (getItem(position).getIsPublic().equals("0")) {
//			hold.publica.setImageResource(R.drawable.meetnotpublic);
//		} else if (getItem(position).getIsPublic().equals("1")) {
//			hold.publica.setImageResource(R.drawable.meetpublic);
//		}

		// 加载一个网络中的图片
		ImageLoader imageLoader = UserService.imageLoader;
		imageLoader.displayImage((String) getItem(position).getCoverImage(),
				hold.coverImage, PhotoUtil.normalmeetingImageOptions);

		return convertView;

	}
}
