package com.hyx.meet.adapter;

import java.io.File;
import java.util.List;

import android.R.integer;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.opengl.Visibility;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.avos.avoscloud.AVUser;
import com.hyx.meet.Application.App;
import com.hyx.meet.avobject.User;
import com.hyx.meet.chat.JuChatActivity;
import com.hyx.meet.custome.PlayButton;
import com.hyx.meet.custome.ViewHolder;
import com.hyx.meet.db.DBHelper;
import com.hyx.meet.db.DBMeet;
import com.hyx.meet.db.DBNamecard;
import com.hyx.meet.entity.MeetingDomain;
import com.hyx.meet.entity.Msg;
import com.hyx.meet.entity.NameCard;
import com.hyx.meet.entity.RoomType;
import com.hyx.meet.JuHYXWebviewActivity;
import com.hyx.meet.JuImageBrowerActivity;
import com.hyx.meet.R;
import com.hyx.meet.namecard.JuMyNameCardShowActivity;
import com.hyx.meet.namecard.JuNameCardShowActivity;
import com.hyx.meet.service.AudioHelper;
import com.hyx.meet.service.CacheService;
import com.hyx.meet.service.ChatService;
import com.hyx.meet.service.UserService;
import com.hyx.meet.utils.EmotionUtils;
import com.hyx.meet.utils.JuUrl;
import com.hyx.meet.utils.PathUtils;
import com.hyx.meet.utils.PhotoUtil;
import com.hyx.meet.utils.TimeUtils;
import com.hyx.meet.utils.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;

public class ChatMsgAdapter extends BaseListAdapter<Msg> {
	int msgViewTypes = 12;
	DBHelper dbHelper = new DBHelper(App.ctx, App.DB_NAME, App.DB_VER);

	public enum MsgViewType {
		ComeText(0), ToText(1), ComeImage(2), ToImage(3), ComeAudio(4), ToAudio(
				5), ComeLocation(6), ToLocation(7), ComeBoard(8), ToBoard(9), ComeInvite(
				10), ToInvite(11);
		int value;

		MsgViewType(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}
	}

	JuChatActivity chatActivity;

	public ChatMsgAdapter(JuChatActivity chatActivity, List<Msg> datas) {
		super(chatActivity, datas);
		this.chatActivity = chatActivity;
	}

	public int getItemPosById(String objectId) {
		for (int i = 0; i < getCount(); i++) {
			Msg itemMsg = datas.get(i);
			if (itemMsg.getObjectId().equals(objectId)) {
				return i;
			}
		}
		return -1;
	}

	public Msg getItem(String objectId) {
		for (Msg msg : datas) {
			if (msg.getObjectId().equals(objectId)) {
				return msg;
			}
		}
		return null;
	}

	@Override
	public int getItemViewType(int position) {
		Msg entity = datas.get(position);
		boolean comeMsg = entity.isComeMessage();
		Msg.Type type = entity.getType();
		MsgViewType viewType;
		switch (type) {
		case Text:
			viewType = comeMsg ? MsgViewType.ComeText : MsgViewType.ToText;
			break;
		case Image:
			viewType = comeMsg ? MsgViewType.ComeImage : MsgViewType.ToImage;
			break;
		case Audio:
			viewType = comeMsg ? MsgViewType.ComeAudio : MsgViewType.ToAudio;
			break;
		case Location:
			viewType = comeMsg ? MsgViewType.ComeLocation
					: MsgViewType.ToLocation;
			break;
		case Board:
			viewType = comeMsg ? MsgViewType.ComeBoard : MsgViewType.ToBoard;
			break;
		case Invite:
			viewType = comeMsg ? MsgViewType.ComeInvite : MsgViewType.ToInvite;
			break;
		case Text2:
			viewType = comeMsg ? MsgViewType.ComeText : MsgViewType.ToText;
			break;
		case Image2:
			viewType = comeMsg ? MsgViewType.ComeImage : MsgViewType.ToImage;
			break;
		case Audio2:
			viewType = comeMsg ? MsgViewType.ComeAudio : MsgViewType.ToAudio;
			break;
		case Location2:
			viewType = comeMsg ? MsgViewType.ComeLocation
					: MsgViewType.ToLocation;
			break;

		default:
			throw new IllegalStateException();

		}
		return viewType.getValue();
	}

	public int getViewTypeCount() {
		return msgViewTypes;
	}

	public View getView(int position, View conView, ViewGroup parent) {
		Msg msg = datas.get(position);
		boolean isComMsg = msg.isComeMessage();
		if (conView == null) {
			conView = createViewByType(msg.getType(), isComMsg,msg);
		}
		TextView sendTimeView = ViewHolder.findViewById(conView,
				R.id.sendTimeView);
		TextView contentView = ViewHolder.findViewById(conView,
				R.id.textContent);
		TextView tagTextView = ViewHolder.findViewById(conView,
				R.id.id_chat_tag);
		View contentLayout = ViewHolder.findViewById(conView,
				R.id.contentLayout);
		ImageView imageView = ViewHolder.findViewById(conView, R.id.imageView);
		ImageView avatarView = ViewHolder.findViewById(conView, R.id.avatar);
		 PlayButton playBtn = ViewHolder.findViewById(conView, R.id.playBtn);
		TextView locationView = ViewHolder.findViewById(conView,
				R.id.locationView);
		TextView usernameView = ViewHolder.findViewById(conView, R.id.username);

		View statusSendFailed = ViewHolder.findViewById(conView,
				R.id.status_send_failed);
		TextView statusSendSucceed = ViewHolder.findViewById(conView,
				R.id.status_send_succeed);
		View statusSendStart = ViewHolder.findViewById(conView,
				R.id.status_send_start);

		// timestamp
		if (position == 0
				|| TimeUtils.haveTimeGap(
						datas.get(position - 1).getTimestamp(),
						msg.getTimestamp())) {
			sendTimeView.setVisibility(View.VISIBLE);
			sendTimeView.setText(TimeUtils.millisecs2DateString(msg
					.getTimestamp()));
		} else {
			sendTimeView.setVisibility(View.GONE);
		}

		final String fromPeerId = msg.getFromPeerId();
		// 如果不是通知
		if (chatActivity.roomType != RoomType.NOTIFY) {
			User user = CacheService.lookupUser(fromPeerId);
			NameCard nameCard = DBNamecard.getNameCardByUserId(dbHelper,
					fromPeerId);
			// if (user == null) {
			// throw new RuntimeException("cannot find user");
			// }
			if (fromPeerId.equals(ChatService.getSelfId())) {
				UserService
						.displayAvatar(
								AVUser.getCurrentUser().getString("avator"),
								avatarView);
			} else {
				UserService.displayAvatar(nameCard.getAvator(), avatarView);
			}
			// 设置头像点击事件
			setAvatorOnclickListener(avatarView, nameCard.getName(),
					fromPeerId, nameCard.getNamecardId());

		} else {
			// 获取头像
			// avatarView.setImageResource(R.drawable.group_icon);
			MeetingDomain meetingDomain = DBMeet.getMeetingById(fromPeerId);
			UserService
					.displayAvatar(meetingDomain.getCoverImage(), avatarView);
			avatarView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent();
					String url = "";
					if (JuUrl.MODE_STYLE.equals("prod")) {
						url = String.format(JuUrl.MEETING_DETAIL_PROD,
								fromPeerId, ChatService.getSelfId());
					} else {
						url = String.format(JuUrl.MEETING_DETAIL_DEV,
								fromPeerId, ChatService.getSelfId());
					}
					intent.putExtra("url", url);
					intent.putExtra("showGuide", 1);
					intent.setClass(App.ctx, JuHYXWebviewActivity.class);
					ctx.startActivity(intent);
					((Activity) ctx).overridePendingTransition(R.anim.zoomin,
							R.anim.zoomout_static);
				}
			});

			// avatarView.setVisibility(View.GONE);
		}
		if (isComMsg == true) {
			if (chatActivity.roomType == RoomType.Single) {
				usernameView.setVisibility(View.GONE);
			} else if (chatActivity.roomType == RoomType.NOTIFY) {
				usernameView.setVisibility(View.GONE);
			} else {
				// usernameView.setVisibility(View.VISIBLE);
				// usernameView.setText(user.getUsername());
			}
		}

		Msg.Type type = msg.getType();
		if (type == Msg.Type.Text2) {
			contentView.setText(EmotionUtils.replace(ctx, msg.getContent()));
			contentLayout.requestLayout();
		} else if (type == Msg.Type.Image2) {
			String localPath = PathUtils.getChatFileDir() + msg.getObjectId();
			String url = msg.getContent();
			displayImageByUri(imageView, localPath, url);
			setImageOnClickListener(localPath, url, imageView);
		} else if (type == Msg.Type.Audio2) {
//			contentView.setText("【暂不支持语音类型】");
//			contentLayout.requestLayout();
			 initPlayBtn(msg, playBtn);
		} else if (type == Msg.Type.Location2) {
			// setLocationView(msg, locationView);
		} else if (type == Msg.Type.Board) {
			tagTextView.setVisibility(View.VISIBLE);
			// tagTextView.setText("【会议通知】:");
			contentView.setText(EmotionUtils.replace(ctx, msg.getContent()));
			contentLayout.requestLayout();
		} else if (type == Msg.Type.Invite) {
			tagTextView.setVisibility(View.VISIBLE);
			tagTextView.setText("【会议邀请】:");
			String localPath = PathUtils.getChatFileDir() + msg.getObjectId();
			String url = msg.getContent();
			displayImageByUri(imageView, localPath, url);
			setImageOnClickListener(localPath, url, imageView);
		}
		//兼容v1本地的消息记录
		else if (type == Msg.Type.Text) {
			contentView.setText(EmotionUtils.replace(ctx, msg.getContent()));
			contentLayout.requestLayout();
		} else if (type == Msg.Type.Image) {
			String localPath = PathUtils.getChatFileDir() + msg.getObjectId();
			String url = msg.getContent();
			displayImageByUri(imageView, localPath, url);
			setImageOnClickListener(localPath, url, imageView);
		} else if (type == Msg.Type.Audio) {
//			contentView.setText("【暂不支持语音类型】");
//			contentLayout.requestLayout();
			 initPlayBtn(msg, playBtn);
		}
		if (isComMsg == false) {
			hideStatusViews(statusSendStart, statusSendFailed,
					statusSendSucceed);
			setSendFailedBtnListener(statusSendFailed, msg);
			switch (msg.getStatus()) {
			case SendFailed:
				statusSendFailed.setVisibility(View.VISIBLE);
				break;
			case SendSucceed:
				statusSendSucceed.setVisibility(View.VISIBLE);
				if (type == Msg.Type.Audio2) {
					statusSendSucceed.setText(msg.getAudioTime()+"''");
				}
				break;
			case SendStart:
				statusSendStart.setVisibility(View.VISIBLE);
				break;
			}
			if (JuChatActivity.roomType == RoomType.Group) {
				statusSendSucceed.setVisibility(View.GONE);
			}
		}else {
			if (type == Msg.Type.Audio2) {
				statusSendSucceed.setVisibility(View.VISIBLE);
				statusSendSucceed.setText(msg.getAudioTime()+"''");
			}
		}
		
		return conView;
	}

	private void setSendFailedBtnListener(View statusSendFailed, final Msg msg) {
		statusSendFailed.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				chatActivity.resendMsg(msg);
			}
		});
	}

	private void hideStatusViews(View statusSendStart, View statusSendFailed,
			View statusSendSucceed) {
		statusSendFailed.setVisibility(View.GONE);
		statusSendStart.setVisibility(View.GONE);
		statusSendSucceed.setVisibility(View.GONE);
	}

	// public void setLocationView(Msg msg, TextView locationView) {
	// try {
	// String content = msg.getContent();
	// if (content != null && !content.equals("")) {
	// String[] parts = content.split("&");
	// String address = parts[0];
	// final String latitude = parts[1];
	// final String longtitude = parts[2];
	// locationView.setText(address);
	// locationView.setOnClickListener(new View.OnClickListener() {
	//
	// @Override
	// public void onClick(View arg0) {
	// Intent intent = new Intent(ctx, LocationActivity.class);
	// intent.putExtra("type", "scan");
	// intent.putExtra("latitude", Double.parseDouble(latitude));
	// intent.putExtra("longtitude", Double.parseDouble(longtitude));
	// ctx.startActivity(intent);
	// }
	// });
	// }
	// } catch (Exception e) {
	// }
	// }

	 private void initPlayBtn(Msg msg, PlayButton playBtn) {
	 playBtn.setLeftSide(msg.isComeMessage());
	 AudioHelper audioHelper = AudioHelper.getInstance();
	 playBtn.setAudioHelper(audioHelper);
	 playBtn.setPath(msg.getAudioPath2());
	 }
	//

	/**
	 * 图片点击事件
	 * 
	 * @param path
	 * @param url
	 * @param imageView
	 */
	private void setImageOnClickListener(final String path, final String url,
			ImageView imageView) {
		imageView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(ctx, JuImageBrowerActivity.class);
				intent.putExtra("path", path);
				intent.putExtra("url", url);
				ctx.startActivity(intent);
			}
		});
	}

	/**
	 * 头像点击事件
	 * 
	 * @param avator
	 */
	private void setAvatorOnclickListener(ImageView avator, final String name,
			final String userId, final String namecardId) {
		avator.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (userId.equals(ChatService.getSelfId())) {
					Intent intent = new Intent(ctx,
							JuMyNameCardShowActivity.class);
					ctx.startActivity(intent);
					((Activity) ctx).overridePendingTransition(R.anim.zoomin,
							R.anim.zoomout_static);
				} else {
					Intent intent = new Intent(ctx,
							JuNameCardShowActivity.class);
					intent.putExtra("name", name);
					intent.putExtra("otherId", userId);
					intent.putExtra("otherNameCardId", namecardId);
					ctx.startActivity(intent);
					((Activity) ctx).overridePendingTransition(R.anim.zoomin,
							R.anim.zoomout_static);
				}

			}

		});
	}

	public static void displayImageByUri(ImageView imageView, String localPath,
			String url) {
		File file = new File(localPath);
		// ImageLoaderConfiguration configuration =
		// PhotoUtil.getImageLoaderConfig(App.ctx, file);
		// ImageLoader.getInstance().init(configuration);
		ImageLoader imageLoader = UserService.imageLoader;
		// UserService.imageLoader;
		if (file.exists()) {
			imageLoader.displayImage("file://" + localPath, imageView,
					PhotoUtil.normalImageOptions);
		} else {
			imageLoader.displayImage(url, imageView,
					PhotoUtil.normalImageOptions);
		}
	}

	public View createViewByType(Msg.Type type, boolean isComeMsg,Msg msg) {
		View baseView;
		if (isComeMsg) {
			baseView = inflater.inflate(R.layout.chat_item_base_left, null);
		} else {
			baseView = inflater.inflate(R.layout.chat_item_base_right, null);
		}
		LinearLayout contentView = (LinearLayout) baseView
				.findViewById(R.id.contentLayout);
		int contentId;
		switch (type) {
		case Text:
			contentId = R.layout.chat_item_text;
			break;
		case Audio:
			contentId = R.layout.chat_item_audio;
			break;
		case Image:
			contentId = R.layout.chat_item_image;
			break;
		case Location:
			contentId = R.layout.chat_item_location;
			break;
		case Board:
			contentId = R.layout.chat_item_text;
			break;
		case Invite:
			contentId = R.layout.chat_item_image;
			break;
		case Text2:
			contentId = R.layout.chat_item_text;
			break;
		case Audio2:
			contentId = R.layout.chat_item_audio;
			break;
		case Image2:
			contentId = R.layout.chat_item_image;
			break;
		case Location2:
			contentId = R.layout.chat_item_location;
			break;
		default:
			throw new IllegalStateException();
		}
		contentView.removeAllViews();
//		contentView.setPadding(PhotoUtil.sp2px(App.ctx, 15), PhotoUtil.sp2px(App.ctx, 5),
//				 PhotoUtil.sp2px(App.ctx, 20), PhotoUtil.sp2px(App.ctx, 11));
		View content = inflater.inflate(contentId, null, false);
		if (type == Msg.Type.Audio) {
			 PlayButton btn = (PlayButton) content;
			 btn.setLeftSide(isComeMsg);
//			TextView textView = (TextView) content;
//			textView.setTextColor(Color.BLACK);

		} 	else if (type == Msg.Type.Audio2) {
			 PlayButton btn = (PlayButton) content;
			 btn.setLeftSide(isComeMsg);
			 contentView.setMinimumWidth(PhotoUtil.sp2px(App.ctx, 
					 80+(int)(5*msg.getAudioTime())));
			 
//			TextView textView = (TextView) content;
//			textView.setTextColor(Color.BLACK);

		}else if (type == Msg.Type.Text) {
			TextView textView = (TextView) content;
			if (isComeMsg) {
				textView.setTextColor(Color.BLACK);
			} else {
				textView.setTextColor(Color.BLACK);
			}
		} else if (type == Msg.Type.Board) {
			TextView textView = (TextView) content;
			if (isComeMsg) {
				textView.setTextColor(Color.BLACK);
			} else {
				textView.setTextColor(Color.BLACK);
			}

		}else if (type == Msg.Type.Text2) {
			TextView textView = (TextView) content;
			textView.setTextIsSelectable(true);
			if (isComeMsg) {
				textView.setTextColor(Color.BLACK);
			} else {
				textView.setTextColor(Color.BLACK);
			}
		}else if(type == Msg.Type.Image2){
//			contentView.setPadding(PhotoUtil.sp2px(App.ctx, 10), PhotoUtil.sp2px(App.ctx, 5),
//					 PhotoUtil.sp2px(App.ctx, 15), PhotoUtil.sp2px(App.ctx, 10));
		}

		contentView.addView(content);
		return baseView;
	}
}
