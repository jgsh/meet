package com.hyx.meet.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hyx.meet.R;
import com.hyx.meet.Application.App;
import com.hyx.meet.chat.JuChatActivity;
import com.hyx.meet.entity.NameCard;
import com.hyx.meet.namecard.JuNameCardShowActivity;
import com.hyx.meet.service.UserService;
import com.hyx.meet.utils.PhotoUtil;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * 自定义适配器
 * 
 * @author ww
 *
 */
public class JuNameCardAdapter extends BaseAdapter {

	// private View lvItem;
	private List<NameCard> mList;
	private Context context;
	private final static int NAMECARD_SHOW = 0;

	public JuNameCardAdapter(Context context, List<NameCard> data) {
		// TODO Auto-generated constructor stub
		this.mList = data;
		this.context = context;
	}
	
	public void setNamaCards(List<NameCard> nameCards){
		this.mList = nameCards;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mList.size();
	}

	@Override
	public NameCard getItem(int position) {
		// TODO Auto-generated method stub
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView,
			final ViewGroup parent) {
		// TODO Auto-generated method stub
		namecardViewHolder hold = null;
		if (convertView == null) {
			hold = new namecardViewHolder();
			convertView = LayoutInflater.from(App.ctx).inflate(
					R.layout.fragment_namecardfragment_listview_item, null);
			hold.mLine = (LinearLayout) convertView.findViewById(R.id.id_namecard_line_change);
			hold.alpha = (TextView) convertView.findViewById(R.id.id_namecard_item_alpha);
			hold.name = (TextView) convertView
					.findViewById(R.id.id_namecard_name_tv);
			hold.state = (TextView) convertView
					.findViewById(R.id.id_namecard_state_tv);
			hold.company = (TextView) convertView
					.findViewById(R.id.id_namecard_company_tv);
			hold.firstSee = (TextView) convertView
					.findViewById(R.id.id_namecard_firstsee_tv);
			hold.opt = (LinearLayout) convertView
					.findViewById(R.id.id_namecard_opt);
			hold.phone = (RelativeLayout) convertView
					.findViewById(R.id.id_namecard_dis_phone);
			hold.sms = (RelativeLayout) convertView
					.findViewById(R.id.id_namecard_dis_sms);
			hold.message = (RelativeLayout) convertView
					.findViewById(R.id.id_namecard_dis_message);
			hold.mItemLayout = (LinearLayout) convertView.findViewById(R.id.id_namecard_item_lay);
			convertView.setTag(hold);

			hold.avator = (ImageView) convertView
					.findViewById(R.id.id_namecard_lvitem_img);

		} else {
			hold = (namecardViewHolder) convertView.getTag();
		}

		NameCard nameCard = getItem(position);
		int section = getSectionForPosition(position);
		if (position == getPositionForSection(section)) {
			hold.alpha.setVisibility(View.VISIBLE);
			hold.mLine.setVisibility(View.GONE);
			hold.alpha.setText(nameCard.getSortLetters());
		} else {
			hold.alpha.setVisibility(View.GONE);
			hold.mLine.setVisibility(View.VISIBLE);
		}

//		final View mView = convertView;
		// 向Viewholder中填入数据
		hold.name.setText( getItem(position).getName());
		hold.state.setText( getItem(position).getJobTitle());
		hold.company.setText(getItem(position).getCompanyName());
		hold.firstSee.setText( getItem(position).getFirstMeeting());

		ImageLoader imageLoader = UserService.imageLoader;
		imageLoader.displayImage( getItem(position).getAvator(),
				hold.avator, PhotoUtil.avatarImageOptions);

		hold.avator.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context,
						JuNameCardShowActivity.class);
				intent.putExtra("otherId", getItem(position).getUserId());
				intent.putExtra("name", getItem(position).getName());
				intent.putExtra("otherNameCardId", getItem(position).getNamecardId());
				((Activity)context).startActivityForResult(intent, NAMECARD_SHOW);
				((Activity) context).overridePendingTransition(
						R.anim.zoomin, R.anim.zoomout_static);
			}
		});

		hold.message.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intentMessage = new Intent(context,
						JuChatActivity.class);
				intentMessage.putExtra("toUserName",
						 getItem(position).getName());
				intentMessage.putExtra("chatUserId",
						 getItem(position).getUserId());
				((Activity) context).startActivity(intentMessage);
				((Activity) context).overridePendingTransition(
						R.anim.zoomin, R.anim.zoomout_static);

			}
		});

		hold.phone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (( getItem(position).getMobilePhone())
						.equals("")) {
					Toast.makeText(context, "电话号码为空", 0).show();

				} else {
					Intent intent = new Intent();

					intent.setAction(Intent.ACTION_DIAL);
					intent.setData(Uri
							.parse("tel:"
									+  getItem(position).getMobilePhone()));
					((Activity) context).startActivity(intent);
					((Activity) context).overridePendingTransition(
							R.anim.zoomin, R.anim.zoomout_static);

				}

			}
		});

		hold.sms.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (((String) getItem(position).getMobilePhone())
						.equals("")) {
					Toast.makeText(context, "电话号码为空", 0).show();

				} else {
					Intent intent = new Intent();
					intent.setAction(Intent.ACTION_SENDTO);
					intent.setData(Uri
							.parse("smsto:"
									+ (String) getItem(position).getMobilePhone()));
					((Activity) context).startActivity(intent);
					((Activity) context).overridePendingTransition(
							R.anim.zoomin, R.anim.zoomout_static);
				}

			}
		});

		return convertView;
	}
	
	public int getSectionForPosition(int position) {
		return mList.get(position).getSortLetters().charAt(0);
	}

	@SuppressLint("DefaultLocale")
	public int getPositionForSection(int section) {
		for (int i = 0; i < getCount(); i++) {
			String sortStr = mList.get(i).getSortLetters();
			char firstChar = sortStr.toUpperCase().charAt(0);
			if (firstChar == section) {
				return i;
			}
		}

		return -1;
	}

}

/**
 * ViewHolder类用以储存item中控件的引用
 */
 final class namecardViewHolder {
	TextView alpha;
	TextView name;
	TextView state;
	TextView company;
	TextView firstSee;
	LinearLayout opt;
	LinearLayout mItemLayout;

	LinearLayout mLine;
	ImageView avator;

	RelativeLayout phone;
	RelativeLayout sms;
	RelativeLayout message;

}
 
 
//	hold.mItemLayout.setOnClickListener(new OnClickListener() {
//	
//	@Override
//	public void onClick(View v) {
//		// TODO Auto-generated method stub
//		// 初始化所有隐藏视图
////		for (int i = 0; i < getCount(); i++) {
////			if (i != position) {
////				LinearLayout optViewAll = (LinearLayout) parent.getChildAt(i)
////						.findViewById(R.id.id_namecard_opt_lay);
//////				System.out.println(optViewAll);
////				if (optViewAll != null) {
////					optViewAll.setVisibility(View.GONE);					
////				}
////			}
////		}
//		System.out.println("count->"+getCount()+"childcount->"+parent.getChildCount()+"position->"+position);
//		// 设置被点击ITEM的显示或隐藏
//		LinearLayout optView = (LinearLayout) parent.getChildAt(position+1)
//				.findViewById(R.id.id_namecard_opt_lay);
//		if (optView.getVisibility() == View.VISIBLE) {
//			optView.setVisibility(View.GONE);
//			// optView.setLayoutAnimation(null);
//
//		} else {
//			Animation animation = AnimationUtils.loadAnimation(context,
//					R.anim.slide_top);
//			// 得到一个LayoutAnimationController对象；
//			LayoutAnimationController lac = new LayoutAnimationController(
//					animation);
//			// 设置控件显示的顺序；
//			lac.setOrder(LayoutAnimationController.ORDER_REVERSE);
//			// 设置控件显示间隔时间；
//			lac.setDelay(1);
//			// 为ListView设置LayoutAnimationController属性；
//			optView.setLayoutAnimation(lac);
//			optView.setVisibility(View.VISIBLE);
//
//		}
//		
//	}
//});
