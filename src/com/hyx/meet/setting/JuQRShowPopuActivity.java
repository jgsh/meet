package com.hyx.meet.setting;

import com.avos.avoscloud.signature.Base64Encoder;
import com.hyx.meet.R;
import com.hyx.meet.R.id;
import com.hyx.meet.R.layout;
import com.hyx.meet.R.menu;
import com.hyx.meet.service.ChatService;
import com.hyx.meet.service.UserService;
import com.hyx.meet.utils.PhotoUtil;
import com.nostra13.universalimageloader.core.ImageLoader;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class JuQRShowPopuActivity extends Activity {

	private LinearLayout mPopLinearLayout;
	private ImageView mQRImageView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ju_qrshow_popu);
		
		initView();
		initData();
	}
	private void initData() {
		// TODO Auto-generated method stub
		String qrString =  Base64Encoder.encode("qrcode://nameCard/addFriend/"+ChatService.getSelfId());
		qrString = qrString+".png";
		ImageLoader imageLoader = UserService.imageLoader;
		imageLoader.displayImage("http://api.plusman.cn/QrCode/"+qrString, mQRImageView, PhotoUtil.normalImageOptions);
   }
	private void initView() {
		// TODO Auto-generated method stub
		mPopLinearLayout = (LinearLayout) findViewById(R.id.id_qr_popuwindow_lay);
		mQRImageView = (ImageView) findViewById(R.id.id_qr_popuwindow_img);
		
		mPopLinearLayout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				JuQRShowPopuActivity.this.finish();
				
			}
		});

		
	}

}
