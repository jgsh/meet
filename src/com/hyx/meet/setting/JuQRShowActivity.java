package com.hyx.meet.setting;

import com.avos.avoscloud.signature.Base64Encoder;
import com.hyx.meet.R;
import com.hyx.meet.Application.SysApplication;
import com.hyx.meet.service.ChatService;
import com.hyx.meet.service.UserService;
import com.hyx.meet.utils.PhotoUtil;
import com.nostra13.universalimageloader.core.ImageLoader;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class JuQRShowActivity extends Activity implements OnClickListener {

	private ImageView mQRImageview;
	private Button mTopTitleBtn;
	
	private LinearLayout mBackBtnLay;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ju_qrshow);
        SysApplication.getInstance().addActivity(JuQRShowActivity.this);

		initView();
		initData();
	}
	
	private void initData() {
		// TODO Auto-generated method stub
		String qrString =  Base64Encoder.encode("qrcode://nameCard/addFriend/"+ChatService.getSelfId());
		qrString = qrString+".png";
		ImageLoader imageLoader = UserService.imageLoader;
		imageLoader.displayImage("http://api.plusman.cn/QrCode/"+qrString, mQRImageview, PhotoUtil.normalImageOptions);
	}

	private void initView() {
		// TODO Auto-generated method stub
		mQRImageview = (ImageView) findViewById(R.id.id_me_qr_imgv);
		mTopTitleBtn = (Button) findViewById(R.id.id_common_top_txtBtn);
		mTopTitleBtn.setText("我的二维码");
		
		mBackBtnLay = (LinearLayout) findViewById(R.id.id_common_top_backBtn_Lay);
		mBackBtnLay.setOnClickListener(this);
		
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.id_common_top_backBtn_Lay) {
			this.finish();
			overridePendingTransition(R.anim.zoomin_static, R.anim.zoomout);
		} else {
		}
		
	}

}
