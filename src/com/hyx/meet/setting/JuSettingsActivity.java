package com.hyx.meet.setting;

import a.a;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.amap.api.location.c;
import com.avos.avoscloud.AVUser;
import com.hyx.meet.Application.App;
import com.hyx.meet.Application.SysApplication;
import com.hyx.meet.custome.sweet_dialog.SweetAlertDialog;
import com.hyx.meet.login.JuLoginActivity;
import com.hyx.meet.meet.JuMainActivity;
import com.hyx.meet.JuHYXWebviewActivity;
import com.hyx.meet.R;
import com.hyx.meet.namecard.JuNameCardShowActivity;
import com.hyx.meet.service.ChatService;
import com.hyx.meet.service.UserService;
import com.hyx.meet.utils.JuUnZipUtil;
import com.hyx.meet.utils.JuUrl;
import com.hyx.meet.utils.PathUtils;
import com.hyx.meet.utils.Utils;

/**
 * 系统设置
 * 
 * @author ww
 *
 */
public class JuSettingsActivity extends Activity implements OnClickListener {

	private LinearLayout mLogOut;// 登出按钮
	private LinearLayout mBackBtnLay;
	// private LinearLayout mAbaout;
	// private LinearLayout mNewMessage;
	// private static Context activity;
	private ProgressDialog mProgressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ju_settings);
		SysApplication.getInstance().addActivity(JuSettingsActivity.this);
		// activity = JuSettings.this;

		initView();
	}

	/***
	 * 初始化界面
	 */
	private void initView() {
		// TODO Auto-generated method stub
		mLogOut = (LinearLayout) findViewById(R.id.id_settings_logout);
		mBackBtnLay = (LinearLayout) findViewById(R.id.id_settings_backBtn_Lay);
		// mAbaout = (LinearLayout) findViewById(R.id.id_settings_about);
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setCancelable(false);
		mProgressDialog.setMessage("正在清除缓存...");

		mLogOut.setOnClickListener(this);
		mBackBtnLay.setOnClickListener(this);
		// mAbaout.setOnClickListener(this);

	}

	/**
	 * 点击事件
	 * 
	 * @param v
	 */
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		if (id == R.id.id_settings_logout) {
			is_LogOut();
		} else if (id == R.id.id_settings_backBtn_Lay) {
			this.finish();
			overridePendingTransition(R.anim.zoomin_static, R.anim.zoomout);

		}
	}

	/**
	 * 创建注销登录提示框
	 */
	protected void is_LogOut() {
		new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
	    .setTitleText("提示")
	    .setContentText("您将登出会友行!")
	    .setCancelText("取消")
	    .setConfirmText("登出")
	    .showCancelButton(true)
	    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
	        @Override
	        public void onClick(SweetAlertDialog sDialog) {
//	            sDialog.cancel();
	            sDialog.dismiss();
	        }
	    })
	    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
			
			@Override
			public void onClick(SweetAlertDialog sweetAlertDialog) {
				// TODO Auto-generated method stub
				UserService.unsubscribe(sweetAlertDialog, JuSettingsActivity.this);
			}
		})
	    .show();

//		AlertDialog.Builder builder = new AlertDialog.Builder(JuSettings.this);
//		builder.setMessage("注销登录？");
//		builder.setTitle("提示");
//		builder.setPositiveButton("注销", new DialogInterface.OnClickListener() {
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				// dialog.dismiss();
//				UserService.unsubscribe(dialog, JuSettings.this);
//				// Intent intent = new Intent(JuSettings.this,
//				// JuLoginActivity.class);
//				// startActivity(intent);
//				// // 用户对象销毁
//				// AVUser.logOut();
//				//
//				//
//				// // 系统退出
//				// SysApplication.getInstance().exit();
//
//			}
//		});
//		builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				dialog.dismiss();
//				// 用户对象销毁
//				// AVUser.logOut();
////				SysApplication.getInstance().exit();
//			}
//		});
//		builder.create().show();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			JuSettingsActivity.this.finish();
			overridePendingTransition(R.anim.zoomin_static, R.anim.zoomout);
		}

		return super.onKeyDown(keyCode, event);
	}

	/**
	 * 跳转到新消息提醒设置页面
	 * 
	 * @param v
	 */
	public void goToJuMessageSetting(View v) {
		Intent intent = new Intent(JuSettingsActivity.this, JuNewMessageSettingsActivity.class);
		startActivity(intent);
		overridePendingTransition(R.anim.zoomin, R.anim.zoomout_static);

	}

	/**
	 * 跳转到修改密码页面
	 * 
	 * @param v
	 */
	public void goToJuModifyPassword(View v) {
		Intent intent = new Intent(JuSettingsActivity.this, JuHYXWebviewActivity.class);
		String url = "";
		if (JuUrl.MODE_STYLE.equals("prod")) {
			url = JuUrl.USER_PASSWARD_PROD;
		} else {
			url = JuUrl.USER_PASSWARD_DEV;
		}
		intent.putExtra("url", url);
		startActivity(intent);
		overridePendingTransition(R.anim.zoomin, R.anim.zoomout_static);

	}

	/**
	 * 版本介绍
	 * 
	 * @param v
	 */
	public void goToNewFeature(View v) {
		Intent intent = new Intent(JuSettingsActivity.this, JuHYXWebviewActivity.class);
		String url = "";
		if (JuUrl.MODE_STYLE.equals("prod")) {
			url = JuUrl.VERSION_PROD;
		} else {
			url = JuUrl.VERSION_DEV;
		}
		intent.putExtra("url", url);
		startActivity(intent);
		overridePendingTransition(R.anim.zoomin, R.anim.zoomout_static);

	}

	/**
	 * 跳转到隐私页面
	 * 
	 * @param v
	 */
	public void goPrivicy(View v) {
	}
	
	/**
	 * 更新网页缓存
	 * @param v
	 */
	public void updateWWW(View v){
		JuUnZipUtil.unZipToHYX(PathUtils.getZipDir(), JuSettingsActivity.this, true,true);
	}

	/**
	 * 清除缓存
	 * 
	 * @param v
	 */
	public void clearCache(View v) {
		mProgressDialog.show();
		if (PathUtils.clearFilesAndCache()) {
			mProgressDialog.dismiss();
			new SweetAlertDialog(JuSettingsActivity.this, SweetAlertDialog.SUCCESS_TYPE)
		    .setTitleText("提示")
		    .setContentText("清除缓存成功!")
		    .setConfirmText("我知道了")
		    .showCancelButton(false)
		    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
				
				@Override
				public void onClick(SweetAlertDialog sweetAlertDialog) {
					// TODO Auto-generated method stub
					sweetAlertDialog.dismiss();
				}
			})
		    .show();
//			Utils.toast("清除缓存成功!");
		} else {
			mProgressDialog.dismiss();
//			Utils.toast("清除缓存失败!");
			new SweetAlertDialog(JuSettingsActivity.this, SweetAlertDialog.ERROR_TYPE)
		    .setTitleText("提示")
		    .setContentText("清除缓存失败!")
		    .setConfirmText("我知道了")
		    .showCancelButton(false)
		    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
				
				@Override
				public void onClick(SweetAlertDialog sweetAlertDialog) {
					// TODO Auto-generated method stub
					sweetAlertDialog.dismiss();
				}
			})
		    .show();
		}
	}

}
