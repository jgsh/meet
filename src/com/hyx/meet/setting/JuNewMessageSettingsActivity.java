package com.hyx.meet.setting;

import com.hyx.meet.R;
import com.hyx.meet.R.id;
import com.hyx.meet.R.layout;
import com.hyx.meet.service.ChatService;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class JuNewMessageSettingsActivity extends Activity implements OnClickListener {

	private ImageView mNoMessageOpend;
	private ImageView mNoMessageClosed;
	private ImageView mSoundOpend;
	private ImageView mSoundClosed;
	private ImageView mShakeOpend;
	private ImageView mShakeClosed;

	private LinearLayout mBackLay;
	private Button mTopTxt;
//新消息设置存储表
	private final static String PREFS_NAME = "newMessage_setting_";
	private final static String NO_MESSAGE = "noMessage";
	private final static String SOUND = "sound";
	private final static String SHAKE = "shake";

	private static SharedPreferences sp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ju_new_message_settings);
//		存储表是与userId关联的
		sp = getSharedPreferences(PREFS_NAME+ChatService.getSelfId(), MODE_PRIVATE);

		initView();
		initSwitch();
	}

	private void initSwitch() {
		// TODO Auto-generated method stub
		// 初始化开关
		boolean is_nomessage = sp.getBoolean(NO_MESSAGE, false);
		boolean is_sound = sp.getBoolean(SOUND, true);
		boolean is_shake = sp.getBoolean(SHAKE, true);
		if (!is_shake) {
			setVisible(mShakeClosed);
			setGone(mShakeOpend);
		}
		if (!is_sound) {
			setVisible(mSoundClosed);
			setGone(mSoundOpend);

		}
		if (is_nomessage) {
			setVisible(mNoMessageOpend);
			setGone(mNoMessageClosed);

		}
	}

	private void initView() {
		// TODO Auto-generated method stub
		mNoMessageClosed = (ImageView) findViewById(R.id.id_setting_noMessage_closed);
		mNoMessageOpend = (ImageView) findViewById(R.id.id_setting_noMessage_opened);
		mShakeClosed = (ImageView) findViewById(R.id.id_setting_shake_closed);
		mShakeOpend = (ImageView) findViewById(R.id.id_setting_Shake_opened);
		mSoundClosed = (ImageView) findViewById(R.id.id_setting_Sound_closed);
		mSoundOpend = (ImageView) findViewById(R.id.id_setting_Sound_opened);

		mBackLay = (LinearLayout) findViewById(R.id.id_common_top_backBtn_Lay);
		mTopTxt = (Button) findViewById(R.id.id_common_top_txtBtn);
		mTopTxt.setText("新消息提醒");
		mBackLay.setOnClickListener(this);

		mNoMessageClosed.setOnClickListener(this);
		mNoMessageOpend.setOnClickListener(this);
		mShakeClosed.setOnClickListener(this);
		mShakeOpend.setOnClickListener(this);
		mSoundClosed.setOnClickListener(this);
		mSoundOpend.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.id_setting_noMessage_closed:
			setVisible(mNoMessageOpend);
			setGone(mNoMessageClosed);
			// mNoMessageOpend.setVisibility(View.VISIBLE);
			// mNoMessageClosed.setVisibility(View.GONE);
			setting(NO_MESSAGE, true);
			break;
		case R.id.id_setting_noMessage_opened:
			setVisible(mNoMessageClosed);
			setGone(mNoMessageOpend);
			// mNoMessageOpend.setVisibility(View.GONE);
			// mNoMessageClosed.setVisibility(View.VISIBLE);
			setting(NO_MESSAGE, false);
			break;
		case R.id.id_setting_shake_closed:
			setVisible(mShakeOpend);
			setGone(mShakeClosed);
			// mShakeOpend.setVisibility(View.VISIBLE);
			// mShakeClosed.setVisibility(View.GONE);
			setting(SHAKE, true);
			break;
		case R.id.id_setting_Shake_opened:
			setVisible(mShakeClosed);
			setGone(mShakeOpend);
			// mShakeOpend.setVisibility(View.GONE);
			// mShakeClosed.setVisibility(View.VISIBLE);
			setting(SHAKE, false);
			break;
		case R.id.id_setting_Sound_closed:
			setVisible(mSoundOpend);
			setGone(mSoundClosed);
			// mSoundOpend.setVisibility(View.VISIBLE);
			// mSoundClosed.setVisibility(View.GONE);
			setting(SOUND, true);
			break;
		case R.id.id_setting_Sound_opened:
			setVisible(mSoundClosed);
			setGone(mSoundOpend);
			// mSoundOpend.setVisibility(View.GONE);
			// mSoundClosed.setVisibility(View.VISIBLE);
			setting(SOUND, false);
			break;
		case R.id.id_common_top_backBtn_Lay:
			this.finish();
			break;

		default:
			break;
		}

	}

	void setVisible(View v) {
		v.setVisibility(View.VISIBLE);
	}

	void setGone(View v) {
		v.setVisibility(View.GONE);
	}

	public static void setting(String name, boolean status) {
		Editor editor = sp.edit();
		editor.putBoolean(name, status);
		editor.commit();
	}

	public static SharedPreferences getSharePreference() {
		return sp;
	}

	/***************************** 开关事件 ****************************/

	// void openNoMessage(View v) {
	//
	// }
	//
	// void closeNoMessage(View v) {
	//
	// }
	//
	// void openNoise(View v) {
	//
	// }
	//
	// void closeNoise(View v) {
	//
	// }
	//
	// void openShake(View v) {
	//
	// }
	//
	// void closeShase(View v) {
	//
	// }

}
